<?php

declare(strict_types=1);

namespace Drupal\votewatch\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a vote count party block.
 *
 * @Block(
 *   id = "votewatch_vote_count_party",
 *   admin_label = @Translation("Vote Count Party"),
 *   category = @Translation("Custom"),
 * )
 */
final class VoteCountPartyBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'example' => $this->t('Hello world!'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['example'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Example'),
      '#default_value' => $this->configuration['example'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['example'] = $form_state->getValue('example');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    [$term, $node, $is_party, $is_mep, $is_social] = _votewatch_context_get();
    // dpm([$term, $node, $is_party, $is_mep, $is_social]);

    // $query_all_party = _votewatch_query_request_get('term_vote_count_tid_depth') ?? [];
    $query_all_party = _votewatch_query_request_get('party_tid') ?? [];


    $query_all_country = _votewatch_query_request_get('country') ?? [];

    // $query_all_title = _votewatch_query_request_get('title');
    $query_all_title = _votewatch_query_request_get('search_api_fulltext');

    // Initial load, no filter params
    // @see Drupal\votewatch\Plugin\views\exposed_form\ViewsExposedForm
    if (is_null($query_all_title) && !$query_all_country) {
      $query_all_country = ['FRA'];
      \Drupal::request()->request->set('country', $query_all_country);
      \Drupal::request()->query->set('country', $query_all_country);

    }

    $view_renderable = [];
    if ($node) {
      $view_renderable = views_embed_view('votes', 'block_3', $node->id());
    }
    // Plusieurs parti ou droite ou gauche
    else if (count($query_all_party) != 1 || $term && isset($term->field_machine_name->value) && in_array($term->field_machine_name->value, ['droite', 'gauche'])) {
      $view_renderable = views_embed_view('votes', 'page_1');
    }
    //else if (count($query_all_country) == 1) {
    else if ($is_mep) {
      $view_renderable = views_embed_view('votes', 'block_1');
    }
    else {
      $view_renderable = views_embed_view('votes', 'block_2');
    }
    // dpm([$query_all_party, $query_all_country, $query_all_title]);
    // dpm(\Drupal::time()->getRequestMicroTime());

    $build['content'] = [
      '#cache' => ['max-age' => 0],
      $view_renderable,

    ];
    return $build;
  }

}
