<?php

namespace Drupal\votewatch\Plugin\views\exposed_form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

class ViewsExposedForm {

  public static function exposedFormInput(&$form, FormStateInterface $form_state) {
    $view = $form_state->get('view');



    // @todo Find a way to extend this class
    $user_input = $form_state->getUserInput();

    $route_match = \Drupal::routeMatch();

    if ($view->id() == 'votes') {

      if (!isset($user_input['search_api_fulltext']) && !isset($user_input['country'])) {
        $user_input['country'] = ['FRA'];
      }

      $form_state->setUserInput($user_input);
    }
  }
}
