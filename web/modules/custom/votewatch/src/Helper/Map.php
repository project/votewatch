<?php
namespace Drupal\votewatch\Helper;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Symfony\Component\DomCrawler\Crawler;
use Drupal\Component\Utility\NestedArray;

class Map {

  public static function mepsFromHtml($country) {
    $file_system = \Drupal::service('file_system');
    $mep_map = $mep_map_id = [];
    $uri = '../data/map/meps_' . $country . '.html';
    if (file_exists($uri)) {
      $content = file_get_contents($uri);
      $dom = Html::load($content);
      $crawler = new Crawler($dom);
      $selector = '.erpl_member-list .erpl_member-list-item .erpl_title-h4';
      /**@var $mep_list \Symfony\Component\DomCrawler\Crawler: */
      $mep_list = $crawler->filter($selector);
      foreach ($mep_list as $mep_node) {
        $mep_full_name = trim($mep_node->nodeValue);
        // Empty array
        $mep_cnode = new Crawler($mep_node->parentNode);
        $additional_info_list = $mep_cnode->filter('.sln-additional-info');
        $eu_party = trim($additional_info_list->first()->getNode(0)->nodeValue);
        $country_party = trim($additional_info_list->last()->getNode(0)->nodeValue);

        $mep_cnode1 = new Crawler($mep_node);
        $mep_a = $mep_cnode1->closest('a.erpl_member-list-item-content[itemprop="url"]');
        $mep_id = $file_system->basename($mep_a->getNode(0)->getAttribute('href'));

        if (isset($mep_map[$eu_party][$country_party][$mep_id])) {
          dpm($mep_full_name, 'Duplicate id ' . $mep_id, 'error');
        }
        $mep_map[$eu_party][$country_party][$mep_id] = $mep_full_name;
      }

      $mep_map_id = [];
      foreach ($mep_map as $eu_party => $n_a) {
        // dpm($eu_party);
        foreach (['NI' => 'Non-inscrit', 'ID' => 'Identité et Démocratie', 'ECR' => 'Conservateurs et Réformistes', 'PPE' => 'Démocrates-Chrétiens', 'Renew' => 'Renew', 'S&D' => 'Socialistes et Démocrates', 'Verts/ALE' => 'Verts/', 'The Left' => 'GUE/NGL'] as $id => $pattern) {
          if (mb_stripos($eu_party, $pattern) !== FALSE) {
            $mep_map_id[$id][$country] = $mep_map[$eu_party];
          }
        }

      }
      if (count($mep_map) != count($mep_map_id)) {
        dpm('$mep_map_id count error', '', 'error');
      }
    }

    return $mep_map_id;
  }

  /**
   * drush ev 'Drupal\votewatch\Helper\Map::meps(TRUE, "2023-09-12");'
   */
  public static function meps($force_write = FALSE, $plenary = NULL){
    $map = [];
    $filename ='../data/map/meps' . ($plenary ? '.' . $plenary : '') . '.json';
    if (!$force_write && file_exists($filename)) {
      $map = Json::decode(file_get_contents($filename));

      return $map;
    }

    $curent_user = \Drupal::currentUser();
    //     csv
    $row = 1;
    $count_all = [];
    if (($handle = fopen('../data/map/count_all.csv', 'r')) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
        $num = count($data);

        if ($row == 1) {
          $cols = $data;
        }
        else {
          $count_all[] = array_combine($cols, $data);
        }


        $row++;
      }
      fclose($handle);
    }
    $error = FALSE;

    $map = [];
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    foreach ($count_all as $row) {
      if ($row['count_all']) {

        // Here is map crawled at 22/05/2024
        $mep_map_id = static::mepsFromHtml($row['country']);


        foreach (['NI', 'ID', 'ECR', 'PPE', 'Renew', 'S&D', 'Verts/ALE', 'The Left'] as $id) {
          $map[$id][$row['country']]['count'] = $row[$id];

          if (!empty($mep_map_id[$id][$row['country']])) {


            $meps_count = 0;
            foreach ($mep_map_id[$id][$row['country']] as $country_party => $meps) {
              $meps_count += count($meps);

              foreach ($meps as $mep_id => $mep) {
                $query = 'SELECT * FROM `taxonomy_term_field_data` WHERE `name` LIKE BINARY \'' . addslashes($mep) . '\' AND `vid` = \'member\'';
                $term_sdcs = \Drupal::database()->query($query)
                //                 ->execute()
                ->fetchAll();

                // Case sensitive
                if (!$term_sdcs || count($term_sdcs) != 1) {
                  if (count($term_sdcs)) {
                    $msg = 'Mep %name "case sensitive" found %count: %party, %country';
                    $ctxt = ['%name' => $mep, '%count' => count($term_sdcs), '%party' => $id, '%country' => $row['country']];
                    if (function_exists('drush_main')) {
                      \Drupal::logger('votewatch')->error($msg, $ctxt);
                    }
                    else if (!$curent_user->isAnonymous()) {
                      \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                    }
                  }
                  else {
                    $msg = 'Mep %name "case sensitive" not found : %party, %country';
                    $ctxt = ['%name' => $mep, '%party' => $id, '%country' => $row['country']];
                    if (function_exists('drush_main')) {
                      \Drupal::logger('votewatch')->error($msg, $ctxt);
                    }
                    else if (!$curent_user->isAnonymous()) {
                      \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                    }
                  }
                }

                // Case insensitive
                $terms = $term_storage->loadByProperties(['name' => $mep, 'vid' => 'member']);
                if (!$terms || count($terms) != 1) {
                  if (count($terms)) {

                    $term_ids = [];
                    foreach ($term_sdcs as $term_sdc_i => $term_sdc) {
                      if (isset($terms[$term_sdc->tid])) {
                        unset($term_sdcs[$term_sdc_i]);
                      }
                      else {
                        $term_ids[] = $term_sdc->tid;
                      }
                    }

                    $msg = 'Mep %name "case INsensitive" found %count: %party, %country';
                    $ctxt = ['%name' => $mep, '%count' => count($terms), '%party' => $id, '%country' => $row['country']];
                    if (function_exists('drush_main')) {
                      \Drupal::logger('votewatch')->error($msg, $ctxt);
                    }
                    else if (!$curent_user->isAnonymous()) {
                      \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                    }

                    $select = \Drupal::database()->select('taxonomy_term_field_data', 'tfd')
                    ->fields('tfd')
                    ->condition('tfd.tid', array_merge($term_ids, array_keys($terms)), 'IN');
                    $select->join('migrate_map_mep_watch_member', 'mim', 'tfd.tid = mim.destid1');
                    $mim = $select->fields('mim', ['sourceid1'])
                    //->addTag('debug')
                    ->execute()
                    ->fetchAll();
                    $msg = '@count found in migrate map';
                    $ctxt = ['@count' => count($mim)];
                    if (function_exists('drush_main')) {
                      \Drupal::logger('votewatch')->error($msg, $ctxt);
                    }
                    else if (!$curent_user->isAnonymous()) {
                      \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                    }
                  }
                  else {
                    $msg = 'Mep %name "case INsensitive" not found : %party, %country';
                    $ctxt = ['%name' => $mep, '%party' => $id, '%country' => $row['country']];
                    if (function_exists('drush_main')) {
                      \Drupal::logger('votewatch')->error($msg, $ctxt);
                    }
                    else if (!$curent_user->isAnonymous()) {
                      \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                    }


                    $query = $term_storage->getQuery()
                    ->accessCheck(FALSE);
                    $or = $query->orConditionGroup();
                    foreach (preg_split('/\s+/', $mep) as $mep_part) {
                      $or->condition('name', $mep_part);
                    }

                    $ids = $query->condition($or, NULL)
                    ->condition('vid', 'member')
                    //                     ->addTag('debug')
                    ->execute();

                    $msg = '@count found when search by splitted name';
                    $ctxt = ['@count' => count($ids)];
                    if (function_exists('drush_main')) {
                      \Drupal::logger('votewatch')->error($msg, $ctxt);
                    }
                    else if (!$curent_user->isAnonymous()) {
                      \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                    }
                  }
                }

                $select = \Drupal::database()->select('migrate_map_mep_watch_member', 'mim');
                $mim = $select->fields('mim', ['sourceid1', 'destid1'])
                ->condition('sourceid1', $mep_id)
                //                 ->addTag('debug')
                ->execute()->fetchAll();
                if (!$mim || count($mim) > 1) {
                  $msg = '%count imported for %name (%id) : %party, %country';
                  $ctxt = ['%count' => count($mim), '%id' => $mep_id, '%name' => $mep, '%party' => $id, '%country' => $row['country']];
                  if (function_exists('drush_main')) {
                    \Drupal::logger('votewatch')->error($msg, $ctxt);
                  }
                  else if (!$curent_user->isAnonymous()) {
                    \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                  }
                }

              }

                asort($mep_map_id[$id][$row['country']][$country_party]);
              }

              if ($row[$id] == $meps_count) {
                $map[$id][$row['country']] += $mep_map_id[$id][$row['country']]; //+ $map[$id][$row['country']];
              }
              else {
                $msg = 'Error counts @id  @country';
                $ctxt = ['@id' => $id, '@country' => $row['country']];
                if (function_exists('drush_main')) {
                  \Drupal::logger('votewatch')->error($msg, $ctxt);
                }
                else if (!$curent_user->isAnonymous()) {
                  \Drupal::messenger()->addError(t($msg, $ctxt), TRUE);
                }
                $error = TRUE;
              }
            }
          }
        }
      }

      $changes = [
        '2024-05-06' => [
          // Move 'Elena YONCHEVA' from NI/BGR/Bulgarian Socialist Party to
          // https://www.europarl.europa.eu/meps/en/197842/ELENA_YONCHEVA/home
          // 02-07-2019 / 06-05-2024 : Group of the Progressive Alliance of Socialists and Democrats in the European Parliament - Member
          // 07-05-2024 ... : Non-attached Members
          '197842:Elena YONCHEVA' => [
            // Out
            ['NI', 'BGR', 'Bulgarian Socialist Party'],
            // In
            ['S&D', 'BGR', 'Bulgarian Socialist Party']
          ],
          // https://www.europarl.europa.eu/meps/en/130256/DACE_MELBARDE/history/9#detailedcardmep
          // 02-07-2019 / 06-09-2022 : European Conservatives and Reformists Group - Member
          // 07-09-2022 / 04-10-2022 : Non-attached Members
          // 05-10-2022 / 06-05-2024 : Group of the European People's Party (Christian Democrats) - Member
          // --1 IN ===>
          '130256:Dace MELBĀRDE' => [
            [],
            ['PPE', 'LVA', 'Partija "VIENOTĪBA"']
          ]
        ],
        '2024-04-28' => [
          // https://www.europarl.europa.eu/meps/fr/243912/MARIA+VERONICA_ROSSI/home
          // 06-04-2023 / 28-04-2024 : Groupe «Identité et démocratie» - Membre
          // 29-04-2024 ... : Non-inscrits
          '243912:Maria Veronica ROSSI' => [
            // Out
            ['NI', 'ITA', 'Independent'],
            // IN
            ['ID', 'ITA', 'Lega'],
          ]
        ],

        // https://www.europarl.europa.eu/meps/fr/125067/THEODOROS_ZAGORAKIS/history/9#detailedcardmep
        // 02-07-2019 / 05-04-2024 : Groupe du Parti populaire européen (Démocrates-Chrétiens) - Membre
        // 06-04-2024 / 17-04-2024 : Non-inscrits
        // 18-04-2024 ... : Groupe de l'Alliance Progressiste des Socialistes et Démocrates au Parlement européen - Membre
        // ---
        // 02-07-2019 / 05-04-2024 : Nea Demokratia (Grèce)
        // 06-04-2024 ... : Independent (Grèce)
        '2024-04-17' => [
          '125067:Theodoros ZAGORAKIS' => [
            ['S&D', 'GRC', 'Independent'],
            ['NI', 'GRC', 'Independent']
          ]
        ],
        '2024-04-05' => [
          '125067:Theodoros ZAGORAKIS' => [
            ['NI', 'GRC', 'Independent'],
            ['PPE', 'GRC', 'Nea Demokratia']
          ],
        ],
        // ++1 OUT ===>
        // https://www.europarl.europa.eu/meps/fr/254876/RICARDO_MORGADO/history/9#detailedcardmep
        // 05-04-2024 ... : Groupe du Parti populaire européen (Démocrates-Chrétiens) - Membre
        // 05-04-2024 ... : Partido Social Democrata (Portugal)
        '2024-04-04' => [
          '254876:Ricardo MORGADO' => [
            ['PPE', 'PRT', 'Partido Social Democrata'],
            []
          ],
          // --1 IN ===>
          // https://www.europarl.europa.eu/meps/en/124734/CLAUDIA_MONTEIRO+DE+AGUIAR/history/9
          // 02-07-2019 / 04-04-2024 : Group of the European People's Party (Christian Democrats) - Member
          // 02-07-2019 / 04-04-2024 : Partido Social Democrata (Portugal)
          '124734:Cláudia MONTEIRO DE AGUIAR' => [
            [],
            ['PPE', 'PRT', 'Partido Social Democrata'],
          ]
        ],
        '2024-04-01' => [
          // ++1 OUT ===>
          // https://www.europarl.europa.eu/meps/en/254720/ANA+MIGUEL_DOS+SANTOS/history/9#detailedcardmep
          // 02-04-2024 ... : Group of the European People's Party (Christian Democrats) - Member
          // 02-04-2024 ... : Partido Social Democrata (Portugal)
          '254720:Ana Miguel DOS SANTOS' => [
            ['PPE', 'PRT', 'Partido Social Democrata'],
            []
          ],
          // ++1 OUT ===>
          // https://www.europarl.europa.eu/meps/en/254722/TEOFILO_SANTOS/history/9#detailedcardmep
          // 02-04-2024 ... : Group of the European People's Party (Christian Democrats) - Member
          // 02-04-2024 ... : Partido Social Democrata (Portugal)
          '254722:Teófilo SANTOS' => [
            ['PPE', 'PRT', 'Partido Social Democrata'],
            []
          ],
          // ++1 OUT ===>
          // https://www.europarl.europa.eu/meps/en/254721/VANIA_NETO/history/9#detailedcardmep
          // 02-04-2024 ... : Group of the European People's Party (Christian Democrats) - Member
          // 02-04-2024 ... : Partido Social Democrata (Portugal)
          '254721:Vânia NETO' => [
            ['PPE', 'PRT', 'Partido Social Democrata'],
            []
          ],
          // --1 IN ===>
          // https://www.europarl.europa.eu/meps/en/96899/JOSE+MANUEL_FERNANDES/history/9
          // 02-07-2019 / 01-04-2024 : Group of the European People's Party (Christian Democrats) - Member
          // 02-07-2019 / 01-04-2024 : Partido Social Democrata (Portugal)
          '96899:José Manuel FERNANDES' => [
            [],
            ['PPE', 'PRT', 'Partido Social Democrata'],
          ],
          // --1 IN ===>
          // https://www.europarl.europa.eu/meps/en/96867/MARIA+DA+GRACA_CARVALHO/history/9#detailedcardmep
          // 02-07-2019 / 01-04-2024 : Group of the European People's Party (Christian Democrats) - Member
          // 02-07-2019 / 01-04-2024 : Partido Social Democrata (Portugal)
          '96867:Maria da Graça CARVALHO' => [
            [],
            ['PPE', 'PRT', 'Partido Social Democrata'],
          ],
          // --1 IN ===>
          // https://www.europarl.europa.eu/meps/en/96903/PAULO_RANGEL/history/9
          // 02-07-2019 / 01-04-2024 : Group of the European People's Party (Christian Democrats) - Vice-Chair
          // 02-07-2019 / 01-04-2024 : Partido Social Democrata (Portugal)
          '96903:Paulo RANGEL' => [
            [],
            ['PPE', 'PRT', 'Partido Social Democrata'],
          ],
        ],
        // ++1 OUT ===>
        '2024-03-25' => [
          // https://www.europarl.europa.eu/meps/en/254718/ANABELA_RODRIGUES/history/9#detailedcardmep
          // 26-03-2024 ... : The Left group in the European Parliament - GUE/NGL - Member
          '254718:Anabela RODRIGUES' => [
            ['The Left', 'PRT', 'Bloco de Esquerda'],
            []
          ],
          // ++1 OUT ===>
          // https://www.europarl.europa.eu/meps/fr/254719/VASCO_BECKER-WEINBERG/history/9#detailedcardmep
          // 26-03-2024 ... : Groupe du Parti populaire européen (Démocrates-Chrétiens) - Membre
          // 26-03-2024 ... : Partido do Centro Democrático Social-Partido Popular (Portugal)
          '254719:VASCO BECKER-WEINBERG' => [
            ['PPE', 'PRT', 'Partido do Centro Democrático Social-Partido Popular'],
            []
          ],
          // --1 IN ===>
          // https://www.europarl.europa.eu/meps/en/96978/NUNO_MELO/history/9
          // 02-07-2019 / 25-03-2024 : Group of the European People's Party (Christian Democrats) - Member
          // 02-07-2019 / 25-03-2024 : Partido do Centro Democrático Social-Partido Popular (Portugal)
          '96978:Nuno MELO' => [
            [],
            ['PPE', 'PRT', 'Partido do Centro Democrático Social-Partido Popular'],
          ],

          // --1 IN ===>
          // https://www.europarl.europa.eu/meps/en/96820/MARISA_MATIAS/history/9
          // 02-07-2019 / 17-12-2020 : Group of the European United Left - Nordic Green Left - Vice-Chair
          // 18-12-2020 / 25-03-2024 : The Left group in the European Parliament - GUE/NGL - Vice-Chair
          // 02-07-2019 / 25-03-2024 : Bloco de Esquerda (Portugal)
          '96820:Marisa MATIAS' => [
            [],
            ['The Left', 'PRT', 'Bloco de Esquerda'],
          ],
        ],
        '2024-03-13' => [
          // ===== /!\ Change rrom rcv => revert change 2024-04-05
          '125067:Theodoros ZAGORAKIS' => [
            ['PPE', 'GRC', 'Nea Demokratia'],
            ['NI', 'GRC', 'Independent'],
          ]
        ],
        '2024-03-11' => [
          // ++1 OUT ===>
          // https://www.europarl.europa.eu/meps/fr/232843/JAN_OVELGONNE/history/9#detailedcardmep
          // 12-03-2024 ... : Groupe des Verts/Alliance libre européenne - Membre
          '232843:Jan Henrik OVELGÖNNE' => [
            ['Verts/ALE', 'DEU', 'Bündnis 90/Die Grünen'],
            [],
          ],
        ],
        '2024-03-10' => [
          // --1 IN ===>
          // https://www.europarl.europa.eu/meps/fr/229352/MALTE_GALLEE/history/9
          // 22-12-2021 / 05-03-2024 : Groupe des Verts/Alliance libre européenne - Membre
          // 06-03-2024 / 10-03-2024 : Non-inscrits
          // 22-12-2021 / 10-03-2024 : Bündnis 90/Die Grünen (Allemagne)
          '229352:Malte GALLÉE' => [
            [],
            ['NI', 'DEU', 'Bündnis 90/Die Grünen'],
          ],
        ],
        '2024-03-05' => [
          '229352:Malte GALLÉE' => [
            ['NI', 'DEU', 'Bündnis 90/Die Grünen'],
            ['Verts/ALE', 'DEU', 'Bündnis 90/Die Grünen'],
          ],
        ]


        // @todo

        // https://www.europarl.europa.eu/meps/fr/36392/ALDO_PATRICIELLO/history/9#detailedcardmep
        // 29-01-2024 ... : Groupe «Identité et démocratie» - Membre

        //       '18-04-2023' => [
        //         // https://www.europarl.europa.eu/meps/en/197819/ALEXIS_GEORGOULIS/history/9#detailedcardmep
        //         // 02-07-2019 / 17-12-2020 : Group of the European United Left - Nordic Green Left - Member
        //         // 18-12-2020 / 18-04-2023 : The Left group in the European Parliament - GUE/NGL - Member
        //         // 19-04-2023 ... : Non-attached Members
        //         // --
        //         // 02-07-2019 / 17-04-2023 : Coalition of the Radical Left (Greece)
        //         // 18-04-2023 ... : Independent (Greece)
        //         'Alexis GEORGOULIS' => [
        //           ['NI', 'GRC', 'Independent'],
        //           ['The Left', 'GRC', 'Coalition of the Radical Left']
        //         ]
        //       ],


      ];

      // $plenary < 22/05/2025 : Apply all changes between $plenary and 22/05/2025
      if ($plenary) {
        foreach ($changes as $date => $change) {
          if ($plenary < '2024-05-22') {
            if ($date >= $plenary) {
              foreach ($change as $mep => $out_in) {
                // Move
                if ($out_in[0] && $out_in[1]) {
                  static::mepsMv($map, $mep, $out_in[0], $out_in[1]);
                }
                else if ($out_in[1]) {
                  static::mepsPush($map, $mep, $out_in[1]);
                }
                else if ($out_in[0]) {
                  $mep = static::mepsPop($map, $mep, $out_in[0]);
                }
              }
            }
          }
        }
      }

      if (!$error) {
        file_put_contents($filename, json_encode($map, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT));
      }

      return $map;
  }

  public static function mepsMv(&$map, $mep_name, $from_parents, $to_parents) {
    $mep = static::mepsPop($map, $mep_name, $from_parents);
    if ($mep) {
      static::mepsPush($map, $mep, $to_parents);
    }
  }

  public static function mepsPop(&$map, $mep_name, $from_parents) {
    $mep = NULL;
    $key_exists = FALSE;

    [$mep_id, $mep_name] = explode(':', $mep_name, 2);

    $country_parents = $from_parents;
    $party = array_pop($from_parents);
    //dpm([$map, $from_parents]);
    $country_arr = & NestedArray::getValue($map, $from_parents, $key_exists);
    $mep_key = FALSE;
    if (isset($country_arr[$party])) {
      $mep_key = array_search($mep_name, $country_arr[$party]); //$map['NI']['BGR']['Bulgarian Socialist Party']);
    }
    //dpm([$mep_name, $country_arr[$party]]);
    if ($key_exists && $mep_key !== FALSE && $mep_key == $mep_id) {
      $mep = $mep_id . ':' . $country_arr[$party][$mep_key];
      unset($country_arr[$party][$mep_key]);
      $count = $country_arr['count'];
      $count--;
      $country_arr['count'] = $count;
      if (!$country_arr[$party]) {
        unset($country_arr[$party]);
      }
    }
    else {
      dpm(var_export([$key_exists, $mep_key, $mep_id, $mep_name], TRUE), 'error on "from" party mep search', 'error');
    }

    return $mep;
  }

  public static function mepsPush(&$map, $mep_name, $to_parents) {
    $key_exists = FALSE;

    [$mep_id, $mep_name] = explode(':', $mep_name, 2);

    $country_parents = $to_parents;
    $party = array_pop($to_parents);
    //dpm([$map, $from_parents]);
    $country_arr = & NestedArray::getValue($map, $to_parents, $key_exists);
    if ($key_exists) {

      if (!isset($country_arr[$party])) {
        $country_arr[$party] = [];
      }

      if (!isset($country_arr[$party][$mep_id])) {
        $country_arr[$party][$mep_id] = $mep_name;
      }
      else {
        dpm([$mep_id , $mep_name], 'error on "to" party mep push', 'error');
      }

      $count = $country_arr['count'];
      $count++;
      $country_arr['count'] = $count;

    }

    return $key_exists;
  }
}
