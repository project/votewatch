<?php
namespace Drupal\votewatch\Helper;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Serialization\Json;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Serialization\Yaml;

/**
 * @deprecated
 */
class RcvResult {
  public static function members($rcv_result_node, & $groups_for_mem, $rcv_type = 'Result.For') {
    $dom = $rcv_result_node->ownerDocument;
    $dom_xpath = new \DOMXPath($dom);
    $rcv_for = $dom_xpath->query('./' . $rcv_type, $rcv_result_node);
    $rcv_for_nb = 0;
    if ($rcv_for_node = $rcv_for->item(0)) {
      $rcv_for_nb = $rcv_for_node->getAttribute('Number');
    }
    else {
      dpm($dom->saveXML($rcv_result_node), $rcv_type, 'error');
    }



    $rcv_for_group = $dom_xpath->query('./Result.PoliticalGroup.List', $rcv_for_node);
    $rcv_for_groups = $rcv_for_groups_mem = [];
//     $rcv_for_group_aliases = [];
    foreach ($rcv_for_group as $rcv_for_group_node) {
      $rcv_for_group = $rcv_for_group_node->getAttribute('Identifier');
      //

      $rcv_for_group_alias = static::partiMap($rcv_for_group)['alias'] ?? '';//: $rcv_for_group;

      $rcv_for_member = $dom_xpath->query('./PoliticalGroup.Member.Name', $rcv_for_group_node);
      $rcv_for_group_label = $rcv_for_member->length  . ' : ' . $rcv_for_group . ($rcv_for_group_alias ? ' (' . $rcv_for_group_alias . ')'  : '');
      $rcv_for_groups[$rcv_for_group_label]['count'] = $rcv_for_member->length;
      $rcv_for_groups[$rcv_for_group_label]['id'] = $rcv_for_group_node->getAttribute('Identifier');
      foreach ($rcv_for_member as $rcv_for_member_node) {
        $rcv_for_groups[$rcv_for_group_label][] = [
          'member' => $rcv_for_member_node->nodeValue,
          'MepId' => $rcv_for_member_node->getAttribute('MepId'),
          'PersId' => $pers_id = $rcv_for_member_node->getAttribute('PersId'),
        ];
        if (!is_null($groups_for_mem)) {
          $groups_for_mem[$pers_id][$rcv_for_group] = $rcv_for_group;
        }
      }
    }
    uasort($rcv_for_groups, function($a, $b) {
      return $a['count'] - $b['count'];
    });

    // $var['A9-0176/2024']['for_group']['ECR (Conservateurs et Réformistes) : 46'][0]['member']

      return [$rcv_for_nb, $rcv_for_groups];
  }

  /**
   * https://fr.wikipedia.org/wiki/Parlement_europ%C3%A9en
   * https://fr.wikipedia.org/wiki/Liste_des_d%C3%A9put%C3%A9s_europ%C3%A9ens_de_la_9e_l%C3%A9gislature#D%C3%A9put%C3%A9s_sortants_et_entrants_du_Parlement_europ%C3%A9en
   *
   *
   *
   *
   *
   * @param unknown $parti_id
   * @return NULL
   */
  public static function partiMap($parti_id){
    static $terms_map = [];
    $first = empty($terms_map);

    $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $map = [];



    $id = 'NI';
    $map['NI']['title'] = 'non-inscrits';
    $map['NI']['alias'] = 'non-inscrits';
    $map[$id]['term'] = $id . ' (' . $map[$id]['alias'] . ')';
    $map['NI']['nb'] = '41';
    $map['NI']['pc'] = '5,81';
    if(empty($terms_map[$map[$id]['term']]) && ($terms = $storage->loadByProperties(['name' => $map[$id]['term']]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$map[$id]['term']] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$map[$id]['term']];
    }

    $id = 'ID';
    $map['ID']['title'] = 'groupe Identité et Démocratie';
    $map['ID']['alias'] = 'Identité et Démocratie';
    $map[$id]['term'] = $id . ' (' . $map[$id]['alias'] . ')';
    $map['ID']['nb'] = '65';
    $map['ID']['pc'] = '9,21';
    if(empty($terms_map[$map[$id]['term']]) && ($terms = $storage->loadByProperties(['name' => $map[$id]['term']]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$map[$id]['term']] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$map[$id]['term']];
    }

    $id = 'ECR';
    $map['ECR']['title'] = 'groupe des Conservateurs et Réformistes européens';
    $map['ECR']['alias'] = 'Conservateurs et Réformistes';
    $map[$id]['term'] = $id . ' (' . $map[$id]['alias'] . ')';
    $map['ECR']['nb'] = '64';
    $map['ECR']['pc'] = '9,08';
    if(empty($terms_map[$map[$id]['term']]) && ($terms = $storage->loadByProperties(['name' => $map[$id]['term']]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$map[$id]['term']] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$map[$id]['term']];
    }

    $id = 'PPE';
    $map['PPE']['title'] = 'groupe du Parti Populaire Européen (Démocrates-Chrétiens)';
    $map['PPE']['alias'] = 'Démocrates-Chrétiens';
    $map[$id]['term'] = $id . ' (' . $map[$id]['alias'] . ')';
    $map['PPE']['nb'] = '176';
    $map['PPE']['pc'] = '24,96';
    if(empty($terms_map[$map[$id]['term']]) && ($terms = $storage->loadByProperties(['name' => $map[$id]['term']]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$map[$id]['term']] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$map[$id]['term']];
    }

    $id = 'Renew';
    $map['Renew']['title'] = 'groupe Renew Europe';
    $map['Renew']['nb'] = '103';
    $map['Renew']['pc'] = '14,60';
    if(empty($terms_map[$id]) && ($terms = $storage->loadByProperties(['name' => $id]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$id] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$id];
    }

    $id = 'S&D';
    $map[$id]['title'] = 'groupe de l’Alliance Progressiste des Socialistes et Démocrates au Parlement européen';
    $map[$id]['alias'] = 'Socialistes et Démocrates';
    $map[$id]['term'] = $id . ' (' . $map[$id]['alias'] . ')';
    $map[$id]['nb'] = '145';
    $map[$id]['pc'] = '20,56';
    if(empty($terms_map[$map[$id]['term']]) && ($terms = $storage->loadByProperties(['name' => $map[$id]['term']]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$map[$id]['term']] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$map[$id]['term']];
    }

    $id = 'Verts/ALE';
    $map['Verts/ALE']['title'] = 'groupe des Verts/Alliance libre européenne';
    $map['Verts/ALE']['nb'] = '72';
    $map['Verts/ALE']['pc'] = '10,21';
    if ($first) {
//       dpm($id);
//       dpm($storage->loadByProperties(['name' => $id]));
    }
    if(empty($terms_map[$id]) && ($terms = $storage->loadByProperties(['name' => $id]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$id] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$id];
    }

    $id = 'The Left';
    $map['The Left']['title'] = 'groupe de la gauche au Parlement européen - GUE/NGL';
    $map['The Left']['nb'] = '39';
    $map['The Left']['pc'] = '5,53';
    if(empty($terms_map[$id]) && ($terms = $storage->loadByProperties(['name' => $id]))) {
      $map[$id]['term_id'] = key($terms);
      $terms_map[$id] = $map[$id]['term_id'];
    }
    else {
      $map[$id]['term_id'] = $terms_map[$id];
    }


//     foreach ($map as $id => $map_item) {

//     }

    if ($first) {
      dpm($map, 'term_map()');
    }

    return $map[$parti_id] ?? NULL;
  }

  public static function tocMapModeId($doc_rcv_id) {
    $mode_id = in_array($doc_rcv_id, ['PV-9-2024-03-14-RCV', 'PV-9-2024-03-13-RCV', 'PV-9-2024-03-12-RCV', 'PV-9-2024-02-29-RCV', 'PV-9-2024-02-28-RCV', 'PV-9-2024-02-27-RCV', 'PV-9-2024-02-08-RCV',
      'PV-9-2024-02-07-RCV', 'PV-9-2024-02-06-RCV', 'PV-9-2024-01-18-RCV', 'PV-9-2024-01-17-RCV', 'PV-9-2024-01-16-RCV', 'PV-9-2023-12-14-RCV',
    ]);

    $mode_id = $mode_id || !empty(static::tocMapReplaceWithId($doc_rcv_id));

    return $mode_id;
  }

  public static function tocMapReplaceWithId($doc_rcv_id) {
    $str_replace_with_id = [
      'PV-9-2023-12-14-RCV' => [0 => 7, 9 => 2, 39 => 1, 45 => 1, 49 => 1],
      'PV-9-2023-12-13-RCV' => [0 => 1, 13 => 1, 14 => 1, 16 => 1, 17 => 1, 19 => 1, 20 => 1, 21 => 1, 40 => 1, 46 => 1, 48 => 1, 58 => 1],
      'PV-9-2023-12-12-RCV' => [0 => 1, 22 => 1, 23 => 1, 24 => 1, 25 => 1, 26 => 1, 31 => 1, 32 => 1, 41 => 1, 42 => 1, 43 => 1, 58 => 1, 59 => 1, 66 => 1, 67 => 1],
      'PV-9-2023-11-22-RCV' => array_fill_keys([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 60, 128, 129, 276], 1),
      'PV-9-2023-11-21-RCV' => array_fill_keys([0, 1, 2, 4, 17, 19, 50, 51, 68, 86, 124, 126, 138, 139], 1),
      'PV-9-2023-11-21-RCV_pv' => array_fill_keys([0, 1, 2, 4, 17, 19, 51, 52, 69, 87, 125, 127, 139, 140], 1),
      'PV-9-2023-11-09-RCV' => array_fill_keys([0, 1, 2, 3, 4, 5, 15, 16, 17, 18, 109, 113, 114], 1),
      'PV-9-2023-10-19-RCV_pv' => array_fill_keys([0, 95, 103, 111], 1),
      'PV-9-2023-10-18-RCV_pv' => [0 => 33] + array_fill_keys([33, 80, 81, 82, 83, 84, 85], 1),
      'PV-9-2023-10-17-RCV_pv' => array_fill_keys([0, 1, 2, 3, 10, 32, 35], 1),
      'PV-9-2023-10-05-RCV' => array_fill_keys([0, 1, 2, 4, 5, 6, 7, 8, 21, 27], 1),
      'PV-9-2023-10-04-RCV' => array_fill_keys([0, 10, 11, 12, 13], 1),
      'PV-9-2023-10-03-RCV' => array_fill_keys([0, 1, 2, 3, 23, 60, 61, 62], 1),
      'PV-9-2023-09-14-RCV' => array_fill_keys([0, 1, 2, 3, 11, 12, 13, 14], 1),
      'PV-9-2023-09-13-RCV' => array_fill_keys([0, 1, 2, 3, 4, 51, 59, 87, 88, 105], 1),
      'PV-9-2023-09-12-RCV' => array_fill_keys([0, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12], 1),
    ];

    return $str_replace_with_id[$doc_rcv_id] ?? [];
  }

  public static function tocMapStartsWithDash($doc_rcv_id) {
    $starts_with_dash_cf = [
      'PV-9-2023-12-13-RCV' => [
//       '41.	Relations UE-Chine' => 2,
//       '47.	Mise en œuvre des dispositions du traité relatives aux procédures législatives spéciales' => 2,
//       '49.	30 ans des critères de Copenhague' => 1,
        'Relations UE-Chine' => 2,
        'Mise en œuvre des dispositions du traité relatives aux procédures législatives spéciales' => 2,
        '30 ans des critères de Copenhague' => 1,
      ],
      'PV-9-2023-12-12-RCV' => [
        'Poursuite de la réforme des règles d’imposition des sociétés' => 2,
      ],
      'PV-9-2023-11-22-RCV' => [
        'Mobilisation du Fonds européen d’ajustement à la mondialisation en faveur des travailleurs licenciés' => 2
      ],
      'PV-9-2023-11-09-RCV_pv' => array_fill_keys(['Règlement sur les données', 'Modification de certains règlements en ce qui concerne l’établissement et le fonctionnement du point d’accès unique européen',
        'Point d’accès unique européen: accès aux informations concernant les services financiers, les marchés des capitaux et la durabilité',
        'Modification de certaines directives eu égard à l’établissement et le fonctionnement du point d’accès unique européen',
        'Discipline en matière de règlement, la prestation transfrontalière de services, la coopération en matière de surveillance, la fourniture de services accessoires de type bancaire et les exigences relatives aux dépositaires centraux de titres de pays tiers',
        'Comptes économiques européens de l\'environnement: nouveaux modules', 'Mesures de conservation et d’exécution applicables dans la zone de réglementation de l’Organisation des pêches de l’Atlantique du Nord-Ouest (OPANO)',
        'Déchets d’équipements électriques et électroniques (DEEE)', 'Accord de partenariat dans le secteur de la pêche durable UE/Madagascar et protocole de mise en oeuvre (2023-2027)',
        'Réception par type des véhicules à moteur et des moteurs en ce qui concerne leurs émissions et leur durabilité (Euro 7)', 'Systèmes des ressources propres de l\'Union',
        'Renforcer le droit à la participation: légitimité et résilience des processus électoraux dans les systèmes politiques illibéraux et les régimes autoritaires',
        'Efficacité des sanctions de l\'UE à l’encontre de la Russie',
        ], 2) + [
      ],
      'PV-9-2023-10-17-RCV_pv' => ['Décharge 2021: budget général de l’UE' => 1, 'Contrôle des pêches' => 2],
      'PV-9-2023-09-13-RCV' => ['Coopération administrative dans le domaine fiscal' => 1],
    ];



    return $starts_with_dash_cf[$doc_rcv_id] ?? [];
  }

  public static function tocMapInsertMissingId($doc_rcv_id) {
    $insert_missing_id = [
      'PV-9-2023-10-18-RCV' => [
        // 'Projet de budget général de l\'Union européenne pour l\'exercice 2024' => 'some_id',
        'Projet de budget général de l\'Union européenne pour l\'exercice 2024' => 'PV-9-2023-10-18-RCV__1',
      ],
      'PV-9-2023-10-03-RCV' => [
        'Rapport intérimaire sur la proposition de révision à mi-parcours du cadre financier pluriannuel 2021-2027' => 'A9-0273/2023'
      ],
      'PV-9-2023-09-12-RCV' => [
        'Objection formulée conformément à l’article 112, paragraphes 2 et 3, du règlement intérieur: maïs génétiquement modifié MON 87419' => 'B9-0362/2023'
      ]
    ];
    return $insert_missing_id[$doc_rcv_id] ?? [];
  }

  /**
   * "..._toc_map" means that is comming from "rcv .pdf summary (TableOfContent)"
   * @param unknown $doc_rcv_id
   * @param boolean $force_write
   * @return array|mixed
   */
  public static function tocMap($doc_rcv_id, $force_write = FALSE) {/**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');
    $host = 'data.europarl.europa.eu';

    $mode_id = static::tocMapModeId($doc_rcv_id); //, ['PV-9-2024-03-14-RCV']);

    $rcv_tocs_doc_grp = [];

    $filename_yaml = 'public://' . $host . '/map/' . $doc_rcv_id . '.pdf.yml';
    if ($force_write || !file_exists($filename_yaml)) {

      $rcv_toc_doc = static::tocMapSrc($doc_rcv_id);
      if ($rcv_toc_doc) {

        //     $rcv_toc_doc = $rcv_toc[$doc_rcv_id];
        $rcv_toc_doc = preg_replace('/\s*\.{2,}\s*\d+/', '\',', $rcv_toc_doc);


        $rcv_toc_doc = preg_replace('/([^,])\n/', '$1 ', $rcv_toc_doc);
        $rcv_tocs_doc = explode("\n", $rcv_toc_doc);
        dpm($rcv_tocs_doc);
        $desc_id_parent = $desc_id_prev = '';
        $rcv_tocs_doc_line_i = 0;
        $start = NULL;

        $str_replace = [
          'Stratégie de l\'UE pour l\'Asie centrale' => 'A9-0407/2023',
          'Conscience historique européenne' => 'A9-0402/2023',
        ];
//         $str_replace_with_id = [
//           'PV-9-2023-12-14-RCV' => [0 => 7, 9 => 2, 39 => 1, 45 => 1, 49 => 1]
//         ];
        $str_replace_with_id = static::tocMapReplaceWithId($doc_rcv_id);
//         $starts_with_dash_cf = ['PV-9-2023-12-13-RCV' => [
//           '41.	Relations UE-Chine' => 2,
//           '47.	Mise en œuvre des dispositions du traité relatives aux procédures législatives spéciales' => 2,
//           '49.	30 ans des critères de Copenhague' => 1,
//         ]];
        $starts_with_dash_cf = static::tocMapStartsWithDash($doc_rcv_id);
        foreach ($rcv_tocs_doc as $rcv_tocs_doc_line) {
          // Drop trailing comma
          $rcv_tocs_doc_line = preg_replace('/\',$/', '', $rcv_tocs_doc_line);
          // Split 2 cols only, on the first "whitespaced dash"
          [$desc_id, $desc_text] = array_pad(preg_split('/\s+-\s+/', $rcv_tocs_doc_line, 2), 2, NULL);

          $desc_id_no_digit = preg_replace('/^\d+\.\s*/', '', $desc_id);



          $matches = [];
          $is_sub = preg_match('/(\d+(\.\d+)*)\s*/', $desc_id, $matches);
          $is_sub = $is_sub && isset($matches[2]);

          //dpm([$desc_id_no_digit, $desc_id_prev]);
          $is_sub = !$mode_id && $is_sub || $mode_id && $desc_id_no_digit == $desc_id_prev;
          if ($is_sub) {
            if (!$mode_id) {
              dpm($matches);
              // drop starting chapter digits
              $sub_chapter_pattern = str_replace('.', '\.', $matches[0]);
              $desc_id = preg_replace('/' . $sub_chapter_pattern . '/', '', $desc_id);
            }
            else {
              $desc_id = $desc_id_no_digit;
            }
            //         [$desc_id, $desc_text] = preg_split('/\s+-\s+/', $rcv_tocs_doc_line, 2);

          }
          else {
            if ($desc_text) {
              $desc_text = $desc_id . ' - ' . $desc_text;
            }
            else {
              $desc_text = $desc_id;
            }
          }

          //preg_replace('/^' . str_replace('.', '\.', $desc_id) . '/', $vote_number . '. ', trim($oj_node->parentNode->nodeValue));

          if ($is_sub && ($desc_id_parent && $desc_id_parent != $desc_id && !$mode_id || $mode_id)) {
            //         $rcv_tocs_doc_grp[$desc_id_parent][$desc_id][$matches[2]] = $desc_text;
            if ($mode_id) {
              $rcv_tocs_doc_grp[$desc_id_no_digit][$desc_id_parent][] = $desc_text;
            }
            else {
              $rcv_tocs_doc_grp[$desc_id][$desc_id_parent][$matches[2]] = $desc_text;
            }
          }
          if (!$is_sub && ($desc_id_parent && !$mode_id || $mode_id)) { // && $desc_id_parent != $desc_id) {
            //         $rcv_tocs_doc_grp[$desc_id_parent]['Desc'] = $desc_text_parent;

            if ($mode_id) {
              [$desc_text_tmp_id, $desc_text_tmp_desc] = array_pad(preg_split('/\s+-\s+/', $desc_text, 2), 2, NULL);


              if ($starts_with_dash_cf) {
                $starts_with_dash_nb = reset($starts_with_dash_cf);
                $starts_with_dash = key($starts_with_dash_cf);
                $starts_with_dash_i = 0;

                //$starts_with_dash_id = $desc_text_tmp_id;
                $starts_with_dash_text = $desc_text_tmp_desc;
                $desc_text_tmp_id_no_digit = preg_replace('/^\d+\.\s*/', '', $desc_text_tmp_id);
                if ($desc_text_tmp_id_no_digit == $starts_with_dash) {
                  unset($starts_with_dash_cf[$starts_with_dash]);

                  while ($starts_with_dash_i < $starts_with_dash_nb) {
                    [$starts_with_dash_tmp_id, $starts_with_dash_tmp_text] = array_pad(explode(' - ', $starts_with_dash_text, 2), 2, NULL);
                    $desc_text_tmp_id .= ' - ' . $starts_with_dash_tmp_id;
                    $starts_with_dash_text = $starts_with_dash_tmp_text;

                    $starts_with_dash_i++;
                  }
                  $desc_text_tmp_desc = $starts_with_dash_text;

                }
                else  {
                  dpm([$desc_text_tmp_id, $starts_with_dash]);
                }
              }
              //

              [$desc_text_tmp2_id, $desc_text_tmp2_desc] = array_pad(explode(' - ', $desc_text_tmp_desc, 2), 2, NULL);
              if ($str_replace_with_id) {// && is_null($start)) {
                if (is_null($start) || $start >= $length) {
                  $length = reset($str_replace_with_id);
                  $start = key($str_replace_with_id);
                  //

                  if($start !== NULL && $rcv_tocs_doc_line_i == $start) {
                    unset($str_replace_with_id[$start]);
                    $length += $start;




                  }
                  else {
                    $start = NULL;
                  }
                }
                // dpm([$start, $length, $str_replace_with_id, $rcv_tocs_doc_line_i]);
                if (!is_null($start) && $start < $length) {

                  if ($rcv_tocs_doc_line_i == $start) {
                    $desc_id_no_digit = $desc_text_tmp2_id;
                    $desc_text_tmp_desc = $desc_text_tmp2_desc;
                    // dpm([$desc_id_no_digit, $desc_text_tmp_desc]);
                  }

                  $start++;
                }
              }

              if (isset($str_replace[$desc_id_no_digit]) && $str_replace[$desc_id_no_digit] == $desc_text_tmp2_id) {
                // $tmp = $desc_id_no_digit;
                $desc_id_no_digit = $desc_text_tmp2_id;
                $desc_text_tmp_desc = $desc_text_tmp2_desc;
              }


              $rcv_tocs_doc_grp[$desc_id_no_digit]['Desc'] = $desc_id_no_digit;
              $rcv_tocs_doc_grp[$desc_id_no_digit][$desc_text_tmp_id][] = $desc_text_tmp_desc;
            }
            else {
              $rcv_tocs_doc_grp[$desc_id_sub]['Desc'] = $desc_text_parent;
            }
          }

//           dpm($desc_id);

          // $desc_id_parent is changing "only" on each not sub
          if (!$is_sub) {
            $desc_id_parent = $desc_id;
            $desc_text_parent = $desc_text;
          }
          // $desc_id_sub is changing "every" on each sub
          else {
            $desc_id_sub = $desc_id;
            $desc_text_sub = $desc_text;
          }

          $desc_id_prev = $desc_id_no_digit;
          $rcv_tocs_doc_line_i++;
        }
        // Last is sub
        if ($is_sub && ($desc_id_parent && !$mode_id || $mode_id)) { //&& $desc_id_parent != $desc_id) {
          if ($mode_id) {
            $rcv_tocs_doc_grp[$desc_id]['Desc'] = $desc_id;
          }
          else {
            $rcv_tocs_doc_grp[$desc_id]['Desc'] = $desc_text_parent;
          }
        }
        // Last is not sub, already ok
        if (!$is_sub && ($desc_id_parent && !$mode_id || $mode_id)) { // && $desc_id_parent != $desc_id) {
          if (!$mode_id) {
            dpm('Last is not sub, already must be ok for : ' . $desc_id_parent, '', 'error');
          }
        }

//         dpm($rcv_tocs_doc_grp);

      //     dpm($rcv_toc_doc);



        $yaml = Yaml::encode($rcv_tocs_doc_grp);

        $dirname_yaml = $file_system->dirname($filename_yaml);
        if (!is_dir($dirname_yaml)) {
          $is_dir = $file_system->prepareDirectory($dirname_yaml, FileSystemInterface::CREATE_DIRECTORY);
          dpm($is_dir);
        }
        file_put_contents($filename_yaml, $yaml);
        dpm($rcv_tocs_doc_grp, 'Written TOC to ' . $doc_rcv_id . ' .pdf');
      }
    }
    else if (file_exists($filename_yaml)) {
      $rcv_tocs_doc_grp = Yaml::decode(file_get_contents($filename_yaml));
    }

    return $rcv_tocs_doc_grp;
  }

  /**
   * https://oeil.secure.europarl.europa.eu/oeil//popups/ficheprocedure.do?reference=2023/0226(COD)&l=en
   *  COD - Ordinary legislative procedure (ex-codecision procedure)
   *  Regulation
   *  Amending Regulation 2017/625 2013/0140(COD)
   *
   * @param unknown $doc_rcv_id
   * @param unknown $groups_for_mem
   * @return array[]|unknown[][]|unknown[]|string[]|mixed[]
   */
  public static function rcvResults($doc_rcv_id, & $groups_for_mem) {
    $host = 'data.europarl.europa.eu';
    $api_path = '/api/v2/documents';
    $api_path_files = '/api/v2/document';

    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');

    $filename = 'public://' . $host . $api_path_files . '/' . $doc_rcv_id;
    $content = file_get_contents($filename);
    $results_docs = Json::decode($content);
    foreach ($results_docs['data'][0]['is_realized_by'][0]['is_embodied_by'] as $is_embodied_by) {
      $path = '/' . $is_embodied_by['is_exemplified_by'];
      $filename_xml = 'public://' . $host . $path;

      if (preg_match('/\.xml$/', $filename_xml)) {
//         dpm($filename_xml);
        $filename_xml_rcv = $filename_xml;
      }
    }

    $rcv_results = $rcv_results_mep = $vote_for_mem = [];
    if (file_exists($filename_xml_rcv)) {
      //     $filename_xml_rcv = 'public://data.europarl.europa.eu/distribution/reds_iPlPv_Rcv/PV-9-2024-04-25-RCV/PV-9-2024-04-25-RCV-FNL_fr.xml';
      $content = file_get_contents($filename_xml_rcv);
      //     $dom = Html::load($content);
      $dom = new \DOMDocument();
      // Ignore warnings during HTML soup loading.
      @$dom->loadXML($content);

      //     $dom = Html::load($content);
      $dom_xpath = new \DOMXPath($dom);
      $xpath_rcv_result = '/PV.RollCallVoteResults/RollCallVote.Result'; '/Result.For/Result.PoliticalGroup.List/PoliticalGroup.Member.Name';
      $range = '[position() >= 0 and position() < 10]';
      $range = '';
      /**@var $rcv_result \DOMNodeList */
      $rcv_result = $dom_xpath->query($xpath_rcv_result.$range);
      if ($rcv_result->length < 1) {
        \Drupal::messenger()->addError(t('Onle one <article> is required, but have @count', ['@count' => $rcv_result->length]));
      }
      // Onle one is required, but have 22711
      else {
        //       dpm([$xpath_article, $article->length]);
        //       $xpath_img = $css_selector->toXpath('img.lazy, img.image_logo');
        //       dpm($xpath_img);
        //       $img = $dom_xpath->query($xpath_img, $article[0]);
        //       $dirname_url = file_create_url($file_system->dirname($uri));
        $i = 0;
        $str_replace = [
          'Stratégie de l\'UE pour l\'Asie centrale' => 'A9-0407/2023',
          'Conscience historique européenne' => 'A9-0402/2023',
        ];
//         $str_replace_with_id = [
//           'PV-9-2023-12-14-RCV' => [0 => 7, 9 => 2, 39 => 1, 45 => 1, 49 => 1]
//         ];
        $str_replace_with_id = static::tocMapReplaceWithId($doc_rcv_id . '_pv');
        if (!$str_replace_with_id) {
          $str_replace_with_id = static::tocMapReplaceWithId($doc_rcv_id);
        }
        $starts_with_dash_cf = static::tocMapStartsWithDash($doc_rcv_id . '_pv');
        if (!$starts_with_dash_cf) {
          $starts_with_dash_cf = static::tocMapStartsWithDash($doc_rcv_id);
        }
        $insert_missing_id = static::tocMapInsertMissingId($doc_rcv_id);
        $rcv_tocs_doc_line_i = 0;
        $start = NULL;
        foreach ($rcv_result as $rcv_result_node) {
          //foreach ([$rcv_result->item(0)] as $rcv_result_node) {
          //         if ($i) {
          //           continue;
            //         }
            //         $src = $dirname_url . '/' . $img_node->getAttribute('src');
            //         $img_node->setAttribute('src', $src);
            /**@var $rcv_result_node \DOMElement */

            $rcv_desc = $dom_xpath->query('./RollCallVote.Description.Text', $rcv_result_node);
            $desc = $rcv_desc->item(0)->nodeValue;

            if ($insert_missing_id) {
              foreach ($insert_missing_id as $insert_missing_id_txt => $insert_missing_id_id) {
//               $insert_missing_id_id = reset($insert_missing_id[$doc_rcv_id]);
//               $insert_missing_id_txt = key($insert_missing_id[$doc_rcv_id]);
              if (stripos(trim($desc), $insert_missing_id_txt) === 0) {
                //unset($insert_missing_id[$doc_rcv_id][$insert_missing_id_txt]);

                $desc = str_ireplace($insert_missing_id_txt, $insert_missing_id_txt . ' - ' . $insert_missing_id_id, $desc);
              }
              }
            }

            [$desc_id, $desc_text] = array_pad(preg_split('/\s+-\s+/', $desc, 2), 2, NULL);

            if ($starts_with_dash_cf) {
              $starts_with_dash_nb = reset($starts_with_dash_cf);
              $starts_with_dash = key($starts_with_dash_cf);
              $starts_with_dash_i = 0;

              //$starts_with_dash_id = $desc_text_tmp_id;
              $starts_with_dash_text = $desc_text;

              $desc_id_no_digit = preg_replace('/^\d+\.\s*/', '', $desc_id);
              if ($desc_id_no_digit == $starts_with_dash) {
                unset($starts_with_dash_cf[$starts_with_dash]);

                while ($starts_with_dash_i < $starts_with_dash_nb) {
                  [$starts_with_dash_tmp_id, $starts_with_dash_tmp_text] = array_pad(explode(' - ', $starts_with_dash_text, 2), 2, NULL);
                  $desc_id .= ' - ' . $starts_with_dash_tmp_id;
                  $starts_with_dash_text = $starts_with_dash_tmp_text;

                  $starts_with_dash_i++;
                }
                $desc_text = $starts_with_dash_text;

              }
              else  {
                // dpm([$desc_id, $starts_with_dash]);
              }
            }
            //

            [$desc_tmp_id, $desc_tmp_text] = array_pad(explode(' - ', $desc_text, 2), 2, NULL);

            if ($str_replace_with_id) {// && is_null($start)) {
              if (is_null($start) || $start >= $length) {
                $length = reset($str_replace_with_id);
                $start = key($str_replace_with_id);
                //

                if($start !== NULL && $rcv_tocs_doc_line_i == $start) {
                  unset($str_replace_with_id[$start]);
                  $length += $start;

                }
                else {
                  $start = NULL;
                }
              }
              //dpm([$start, $length, $str_replace_with_id, $rcv_tocs_doc_line_i]);
              if (!is_null($start) && $start < $length) {

                if ($rcv_tocs_doc_line_i == $start) {
                  $desc_id = $desc_tmp_id;
                  $desc_text = $desc_tmp_text;
                  // dpm([$start, $desc_id, $desc_text, $desc]);
                }

                $start++;
              }
            }

            if (isset($str_replace[$desc_id]) && $str_replace[$desc_id] == $desc_tmp_id) {
              // $tmp = $desc_id_no_digit;
              $desc_id = $desc_tmp_id;
              $desc_text = $desc_tmp_text;
            }


            [$rcv_for_nb, $rcv_for_groups] = static::members($rcv_result_node, $groups_for_mem, $rcv_type = 'Result.For');
            [$rcv_agst_nb, $rcv_agst_groups] = static::members($rcv_result_node, $groups_for_mem, $rcv_type = 'Result.Against');
            [$rcv_abst_nb, $rcv_abst_groups] = static::members($rcv_result_node, $groups_for_mem, $rcv_type = 'Result.Abstention');

            $rcv_results_row = [
              'DlvId' => $rcv_result_node->getAttribute('DlvId'),
              'Date' => $rcv_result_node->getAttribute('Date'),
              'Desc' => $desc_text,

              'for_nb' => $rcv_for_nb,
              'against_nb' => $rcv_agst_nb,
              'abst_nb' => $rcv_abst_nb,

              //           'for_group' => $rcv_for_groups,
              //           'against_group' => $rcv_agst_groups,
              //           'abst_group' => $rcv_abst_groups,
            ];

            $vote_for_mem_row = [];
            $vote_for_mem_row['for'] = $rcv_for_groups;
            $vote_for_mem_row['against'] = $rcv_agst_groups;
            $vote_for_mem_row['abstain'] = $rcv_abst_groups;

            $vote_for_mem[trim($desc_id)][] = $vote_for_mem_row;

            $nb_abs = 705 - ($rcv_for_nb + $rcv_agst_nb + $rcv_abst_nb);
            // // $var['A9-0176/2024']['for_group']['ECR (Conservateurs et Réformistes) : 46'][0]['member']
            $rcv_groups_keys = [
              $rcv_for_nb => [' : for', $rcv_for_groups],
              $rcv_agst_nb => [' : against', $rcv_agst_groups],
              $rcv_abst_nb => [' : abstain', $rcv_abst_groups],
            ];
            ksort($rcv_groups_keys, SORT_NUMERIC);
            $rcv_groups_keys = array_reverse($rcv_groups_keys, TRUE);
            $rcv_groups_keys[$nb_abs] = [' : absents', round(($nb_abs / 705)*100, 0) . '%'];
            $rcv_groups = [];
            foreach ($rcv_groups_keys as $rcv_groups_key => $rcv_groups_key_item) {
              $rcv_groups[$rcv_groups_key . $rcv_groups_key_item[0]] = $rcv_groups_key_item[1];
            }

//             $rcv_groups = [
//               $rcv_for_nb . ' : for' => $rcv_for_groups,
//               $rcv_agst_nb . ' : against' => $rcv_agst_groups,
//               $rcv_abst_nb . ' : abst' => $rcv_abst_groups,
//             ];
            $rcv_results_mep[trim($desc_id)][] = $rcv_groups;


            if (!empty($rcv_results_row['DlvId'])) {
              $xpath_rcv_title = '/PV.RollCallVoteResults/VoteTitles/VoteTitle[@DlvId=' . $rcv_results_row['DlvId'] . ']';
              /**@var $rcv_result \DOMNodeList */
              $rcv_title = $dom_xpath->query($xpath_rcv_title);
              $rcv_results_row['title'] = trim($rcv_title->item(0)->nodeValue);
            }
            if (empty($rcv_results_row['title'])) {
              $rcv_results_row['title'] = $rcv_results_row['Desc'];
            }

            $rcv_results[trim($desc_id)][] = $rcv_results_row;
            $rcv_results[trim($desc_id)]['vote'][$rcv_results_row['title']] = $rcv_results_row['title'];
            //         dpm($rcv_result_node->getAttribute('DlvId'));
            //         dpm($article_node->nodeValue);

            //         $i++;
          $rcv_tocs_doc_line_i++;
        }
        dpm($rcv_results);

      }

    }

//     $groups_mem = array_merge_recursive($groups_for_mem, $groups_agst_mem, $groups_abst_mem);

    return [$rcv_results, $results_docs, $rcv_results_mep, $vote_for_mem];
  }

  public static function pvResults($doc_rcv_id, & $rcv_results, $results_docs, $rcv_tocs_doc_grp, $rcv_results_mep, $force_write = FALSE) {
    $host = 'data.europarl.europa.eu';
    $api_path = '/api/v2/documents';
    $api_path_files = '/api/v2/document';

    $rcv = $doc_rcv_id;

    $mode_id = static::tocMapModeId($rcv);

    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');

    $is_annex_of = $file_system->basename($results_docs['data'][0]['is_annex_of'][0]);
    if ($is_annex_of && $is_annex_of == preg_replace('/-RCV$/', '', $rcv)) {
      $doc = $is_annex_of;


      $filename = 'public://' . $host . $api_path_files . '/' . $doc;
      $content = file_get_contents($filename);
      $results_docs = Json::decode($content);

      foreach ($results_docs['data'][0]['is_realized_by'][0]['is_embodied_by'] as $is_embodied_by) {
        $path = '/' . $is_embodied_by['is_exemplified_by'];
        $filename_xml = 'public://' . $host . $path;

        if (preg_match('/\.xml$/', $filename_xml)) {
//           dpm($filename_xml);
          $filename_xml_pv = $filename_xml;
        }
      }

    }


    //     $filename_xml_pv = 'public://data.europarl.europa.eu/distribution/reds_iPlPv_Sit/PV-9-2024-04-25/PV-9-2024-04-25-FNL_fr.xml';
    $content = file_get_contents($filename_xml_pv);
    //     $dom = Html::load($content);
    $dom = new \DOMDocument();
    // Ignore warnings during HTML soup loading.
    @$dom->loadXML($content);

    //     $dom = Html::load($content);
    $dom_xpath = new \DOMXPath($dom);
    $candidates = [
      '/PV/PV.Vote.Text/text:h/OJ.ReportShortDescription.Text', // @Report.Document.Reference => explode ' - ', trim([])
      //   <text:chapter-number>7.1</text:chapter-number>
      //   <OJ.ReportShortDescription.Text Report.Document.Reference="[COM(2024)0043 - C9-0010/2024 - 2024/0021(COD)]">'];

      // '/PV/PV.Vote.Text/text:h/OJ.ReportShortDescription.Text' => nextAll
      './following-sibling::text:p/Document.Reference.List.Text/Document.Reference.Text', // @Document.Identifier => get
      //       <Document.Reference.Text Document.Identifier="RC-B9-0227/2024">',


    ];

    $filename_yaml = 'public://' . $host . '/map/' . $rcv . '.yml';
    $map = [];
    if ($force_write || !file_exists($filename_yaml)) {
      // Map must be coppied from var_export > firefox > network > raw
      // comment the line to set empty map to have the "var_export"
      $map = static::resultMap($rcv);
    }
    else if (file_exists($filename_yaml)) {
      $map = Yaml::decode(file_get_contents($filename_yaml));
//
    }
    // dpm($map);

//     return;

//     if (!$force_write && file_exists($filename_yaml)) {

//     }
//     else {
//       $map = rcv_result_map($rcv);
//     }

    $not_found = [];
    // <text:p text:style="italic" text:weight="normal">(Majorité des suffrages exprimés requise)</text:p>
    // <text:p text:style="normal" text:weight="normal">PROPOSITION DE LA COMMISSION</text:p>
    // <text:p text:style="normal" text:weight="normal">
    //   Approuvé tel qu'amendé le 12 septembre 2023
    //
    // parent > text:p > Approuvé, Approuvé par vote unique
    //
    // Approuvé tel qu'amendé, par vote unique
    // <text:p text:style="italic">(Majorité des suffrages exprimés requise)</text:p>
    // <text:p text:style="normal">ACCORD PROVISOIRE</text:p>
    // <text:p text:style="normal">
    //   Adopté
    // <text:p text:style="italic" text:weight="normal">(Majorité des suffrages exprimés requise)</text:p>
    // <text:p text:style="normal" text:weight="normal">PROPOSITION DE LA COMMISSION et AMENDEMENTS</text:p>
    // <text:p text:style="italic">(Majorité des suffrages exprimés requise)</text:p>
    // <text:p text:style="normal">PROPOSITION DE RÉSOLUTION</text:p>

    $map_from_toc = [];
    $filename_toc_yaml = 'public://' . $host . '/map/' . $doc_rcv_id . '.pdf.yml';
    if (file_exists($filename_toc_yaml)) {
      $map_from_toc = file_get_contents($filename_toc_yaml);
    }

    $oj = $dom_xpath->query('/PV/PV.Vote.Text/text:h//OJ.ReportShortDescription.Text');
    dpm($oj->length);
    $rcv_results_last_found = '';
    $rcv_results_keys = array_keys($rcv_results);

    $i_nbs = [];
    $i_nbs_in = [];
    $i_nb = 0;

    $str_replace_with_id = static::tocMapReplaceWithId($doc_rcv_id . '_pv');
    if (!$str_replace_with_id) {
      $str_replace_with_id = static::tocMapReplaceWithId($doc_rcv_id);
    }

    if (in_array($rcv, ['PV-9-2023-09-12-RCV'])) {
      $i_nbs_in = [
        0 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-09-13-RCV'])) {
      $i_nbs_in = [
        1 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-09-14-RCV'])) {
      $i_nbs_in = [
        1 => 1,
        5 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-10-03-RCV'])) {
      $i_nbs_in = [
        0 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-10-04-RCV'])) {
      $i_nbs_in = [
        3 => 2,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-10-05-RCV'])) {
      $i_nbs_in = [
        0 => 2,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-10-17-RCV'])) {
      $i_nbs_in = [
        3 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-10-18-RCV'])) {
      $i_nbs = [
        0 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-11-09-RCV'])) {
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        0 => 9,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-11-22-RCV'])) {
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        7 => 2,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-12-12-RCV'])) {
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        0 => 1,
        6 => 2,
      ];
    }
    else if (in_array($rcv, ['PV-9-2023-12-14-RCV'])) {
      $i_nbs = [
        7 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-01-16-RCV'])) {
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        0 => 2,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-01-17-RCV'])) {
      // Found, shift out an item
      $i_nbs = [
        0 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-01-18-RCV'])) {
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        0 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-02-06-RCV'])) {
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        0 => 3,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-02-08-RCV'])) {
      // Found, shift out an item
      $i_nbs = [
        0 => 3,
        4 => 2
      ];
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        4 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-02-29-RCV'])) {
      // Found, shift out an item
      $i_nbs = [
        9 => 2,
        8 => 1,
      ];
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        8 => 1,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-03-12-RCV'])) {
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        0 => 2,
      ];
    }
    else if (in_array($rcv, ['PV-9-2024-03-14-RCV'])) {
      // Found
      $i_nbs = [
        0 => 4,
        11 => 3,
      ];
      // Not in RCV, need to "push in" an item
      $i_nbs_in = [
        7 => 1,
      ];
//       $i_nb = 4;
    }
    else if (in_array($rcv, ['PV-9-2024-04-11-RCV'])) {
      array_unshift($rcv_results_keys, 'Skiped 1');
      array_unshift($rcv_results_keys, 'Skiped 2');
//       array_unshift($rcv_results_keys, 'Skiped 3');
    }
    else if (in_array($rcv, ['PV-9-2024-04-23-RCV'])) {
      array_unshift($rcv_results_keys, 'Skiped 1');
      array_unshift($rcv_results_keys, 'Skiped 2');
    }
    else if (in_array($rcv, ['PV-9-2024-04-25-RCV'])) {
      array_shift($rcv_results_keys);
      array_shift($rcv_results_keys);
    }
    else if ($rcv == 'PV-9-2024-04-10-RCV') {
      array_shift($rcv_results_keys);
      array_shift($rcv_results_keys);
    }
    foreach ($i_nbs as $i_start => $i_nb) {
      for ($i = $i_start; $i<($i_start+$i_nb); $i++) {
        unset($rcv_results_keys[$i]);
      }
    }
    dpm($rcv_results_keys);
    foreach ($i_nbs_in as $i_start => $i_nb) {
//       for ($i = $i_start; $i<($i_start+$i_nb); $i++) {
      $rcv_results_keys_a = array_slice($rcv_results_keys, 0, $i_start);
      $rcv_results_keys_b = array_slice($rcv_results_keys, $i_start);
      $rcv_results_keys = array_merge($rcv_results_keys_a, array_fill($i_start, $i_nb, 'pushed'), $rcv_results_keys_b);
//       }
    }
    dpm($rcv_results_keys);
    $empties = [];
    foreach ($oj as $oj_node) {
      // Candidate @Report.Document.Reference => explode ' - ', trim([])

      // @todo closest nodeName == 'text:h'
      $oj_node_parent = $oj_node->parentNode;
      if ($oj_node_parent->nodeName != 'text:h') {
        $oj_node_parent = $oj_node_parent->parentNode;
        dpm($oj_node_parent->nodeName);
      }


      $chapter_number = $dom_xpath->query('./text:chapter-number', $oj_node_parent);
      $chapter_number_value = $chapter_number->item(0)->nodeValue;
      $tmp = explode('.', $chapter_number_value);
      $vote_number = end($tmp);
      $chapter_number_vote = preg_replace('/^' . str_replace('.', '\.', $chapter_number_value) . '/', $vote_number . '. ', trim($oj_node_parent->nodeValue));

      $oj_node_parent->setAttribute('processed', 'processed');

      $found = FALSE;
      if ($oj_node->hasAttribute('Report.Document.Reference')) {
        $doc = '';
        $doc = trim($oj_node->getAttribute('Report.Document.Reference'), '[]');
        $docs = [];
        foreach (preg_split('/\s+-\s+/', $doc) as $id) {
          $id = trim($id);
          if (isset($rcv_results[$id])) {
            $found = $id;
//             $oj_node_found = $oj_node;
          }
        }

      }
      // Look in //Document.Reference.Text
      if (!$found) {
        $doc_ref = $dom_xpath->query('./following-sibling::text:p[1]//Document.Reference.List.Text/Document.Reference.Text', $oj_node_parent);
        $docs = [];
        foreach ($doc_ref as $doc_ref_node) {
          $id = trim($doc_ref_node->getAttribute('Document.Identifier'));
          if (isset($rcv_results[$id])) {
            $found = $id;
//             $oj_node_found = $oj_node;
          }
        }
        //         'Document.Identifier';
        //         dpm($doc_ref->length);

      }
      // Look into map
      if (!$found) {
        if (isset($map[$chapter_number_vote])) {
          $id = key($map[$chapter_number_vote]);
          $chapter_number_vote_compare = $id ? (
            !is_array($map[$chapter_number_vote][$id]) ? [$map[$chapter_number_vote][$id] => $map[$chapter_number_vote][$id]]
            : $map[$chapter_number_vote][$id])
          : NULL;
          // if ([$map[$chapter_number_vote][$id] => $map[$chapter_number_vote][$id]] === $rcv_results[$id]['vote']) {
          // Dont compare if empty in map
          if (!$chapter_number_vote_compare || $chapter_number_vote_compare && (
              $chapter_number_vote_compare === $rcv_results[$id]['vote']
              || $mode_id && $map[$chapter_number_vote][$id] == []
            )) {
            $found = $id;
          }
          else {
            dpm([$chapter_number_vote, $map[$chapter_number_vote], $map[$chapter_number_vote][$id], $rcv_results[$id]['vote']], '', 'error');
          }
        }
        // Missing from -RCV file
        else {
          dpm($chapter_number_vote, '', 'error');
//           dpm($map);exit;
//                     var_export($chapter_number_vote);
          //           var_export($map);
//                     exit;
        }

      }
      if ($found) {
        // No more warning => moved to ammends results
        if (0 && count($rcv_results[$found]['vote']) > 1) {
          // Log more than one different title by vote group $rcv_results[$id]
          dpm('more than one different title by vote group $rcv_results[$id] : ' . $found, '', 'error');
          dpm($rcv_results[$found]['vote'], '', 'error');
        }

        // Backup current 'vote' into 'vote_rcv' (first process of vote title from rcv xml)
        // $rcv_results[$found]['vote_rcv'] = key($rcv_results[$found]['vote']);
        $rcv_results[$found]['vote_rcv'] = $rcv_results[$found]['vote'];
        unset($rcv_results[$found]['vote']);
        $rcv_results[$found]['vote'] = $chapter_number_vote;

        $rcv_results_last_found = $found;

        //         array_shift($rcv_results_keys);
      }
      else {
        // dpm($chapter_number_vote);
      }

      // Look or TA
      //       <text:p text:style="italic">(Majorité des suffrages exprimés requise)</text:p>
      //       <text:p text:style="normal">PROPOSITION DE RÉSOLUTION</text:p>
      //       <text:p text:style="normal">
      //       Adopté
      //       <text:span text:style="italic">(</text:span>
      //         <text:span text:style="italic">
      //         <TA.Reference.List.Text>
      //         <TA.Reference.Text TA.Identifier="P9_TA(2024)0368">
      //         <a href="#reds:iPlTa_Itm/TA-9-2024-0368"
      $ta_node_name = '';
      $ta_parent_list = $dom_xpath->query('./following-sibling::text:p[descendant-or-self::TA.Reference.List.Text][1]', $oj_node_parent);
      $ta_parent_list_node = $ta_parent_list->item(0);
      $doc_ref = NULL;
      if ($ta_parent_list_node) {
        // Check that there is not skiped chapter : the first preceding-sibling must have 'processed'
        $doc_ref = $dom_xpath->query('./preceding-sibling::text:h[1]', $ta_parent_list_node);
      }
      $skiped = FALSE;
      $error_ta_doc_dpm = [];
      if (empty($doc_ref)) {
        $skiped = TRUE;
        $ta_parent_list_node = NULL;
      }
      else if (!$doc_ref->length || $doc_ref->item(0)->getAttribute('processed') != 'processed') {
        // @todo Just notify here we dont have adopted text (TA.Reference.List.Text), but we shoud have Pv.Reference.List.Text
        $error_ta_doc_dpm[] = 'Skipped (for TA.Reference.List.Text) text:h/OJ.ReportShortDescription.Text : (' . ($found ?: 'Not found' ) . ') ' . $chapter_number_vote . '';
        // dpm('Skipped (for TA.Reference.List.Text) text:h/OJ.ReportShortDescription.Text : (' . ($found ?: 'Not found' ) . ') ' . $chapter_number_vote . '', '', 'error');
        $chapter_number_vote_check = static::pvChapterNumberVote($ta_parent_list_node);
        $error_ta_doc_dpm[] = [$chapter_number_vote_check, $chapter_number_vote];
        // dpm([$chapter_number_vote_check, $chapter_number_vote], '', 'error');
        $skiped = TRUE;
      }
      else {
        $ta_node_name = 'TA.Reference.List.Text';
      }
      // For not adopted there is no TA.Reference.List.Text. Look for Pv.Reference.List.Text
      // @todo Not skiped, but has also together with TA.Reference.List.Text : Document.Reference.List.Text, Pv.Reference.List.Text
      if ($skiped) {
        $ta_parent_list = $dom_xpath->query('./following-sibling::text:p[descendant-or-self::Pv.Reference.List.Text][1]', $oj_node_parent);
        $ta_parent_list_node = $ta_parent_list->item(0);
        $doc_ref = NULL;
        if ($ta_parent_list_node) {
          // Check that there is not skiped chapter : the first preceding-sibling must have 'processed'
          $doc_ref = $dom_xpath->query('./preceding-sibling::text:h[1]', $ta_parent_list_node);
        }
        //$skiped = FALSE;
        if (empty($doc_ref)) {
          $ta_parent_list_node = NULL;
        }
        else if (!$doc_ref->length || $doc_ref->item(0)->getAttribute('processed') != 'processed') {
          dpm('Skipped (for Pv.Reference.List.Text) text:h/OJ.ReportShortDescription.Text : (' . ($found ?: 'Not found' ) . ') ' . $chapter_number_vote . '', '', 'error');
          $chapter_number_vote_check = static::pvChapterNumberVote($ta_parent_list_node);
          dpm([$chapter_number_vote_check, $chapter_number_vote], '', 'error');
          //$skiped = TRUE;
        }
        // <text:p text:style="italic" text:weight="normal"><Pv.Reference.List.Text><Pv.Reference.Text Pv.AnnexVote="1">("Résultats des votes", point 1)</Pv.Reference.Text></Pv.Reference.List.Text> </text:p>
        else {
          $ta_node_name = 'Pv.Reference.List.Text';
//           $tmp = new \DOMElement;
          // dpm($ta_parent_list_node->ownerDocument->saveXML($ta_parent_list_node));
          if ($error_ta_doc_dpm) {
            $error_ta_doc_dpm = [];
          }
        }
      }
      foreach ($error_ta_doc_dpm as $error_ta_doc_dpm_item) {
        dpm($error_ta_doc_dpm_item, '', 'error');
      }

      $majorite_list = $dom_xpath->query('./ancestor-or-self::text:p/preceding-sibling::text:p[contains(descendant-or-self::text(),\'Majorité\')][1]', $ta_parent_list_node);
      $majorite_list_node = $majorite_list->item(0);
      // Check that there is not skiped chapter : the first preceding-sibling must have 'processed'
      $doc_ref = $dom_xpath->query('./preceding-sibling::text:h[1]//OJ.ReportShortDescription.Text', $majorite_list_node);
      $chapter_number_vote_check = '';
      if ($doc_ref->length) {
        $chapter_number_vote_check = static::pvChapterNumberVote($doc_ref->item(0));
      }
      $skiped = FALSE;
      if (!$chapter_number_vote_check || $chapter_number_vote_check != $chapter_number_vote) {
        dpm('Skipped (for Majorité : (' . ($found ?: 'Not found' ) . ') ' . $chapter_number_vote . '', '', 'error');
        dpm([$chapter_number_vote_check, $chapter_number_vote], '', 'error');
        $skiped = TRUE;
        $majorite_list_node = NULL;
      }
      else {
        $majorite_list_node->setAttribute('processed', 'processed');
      }


      $pv_text = [];



      //         dpm($doc_ref->length);


      //       $ta_val = $dom_xpath->query('./text()', $ta_list->item(0));
      //       foreach ($ta_val as $ta_val_node) {
//       dpm($ta_parent_list_node->nodeValue);
      //       }
      $prev_sibling = $ta_parent_list_node->previousSibling;
      while ($prev_sibling && $prev_sibling->getAttribute('processed') != 'processed') {
        $pv_text[] = $prev_sibling->nodeValue;

        $prev_sibling = $prev_sibling->previousSibling;
      }
      if ($majorite_list_node) {
        $pv_text[] = $majorite_list_node->nodeValue;
      }

      $pv_text = array_reverse($pv_text);


      if ($ta_parent_list_node) {
        $pv_text[] = $ta_parent_list_node->nodeValue;
      }

      // TODO
      // <text:p text:style="italic">
      // (Majorité des députés qui composent le Parlement requise)
      // </text:p>
      // <text:p text:style="normal">PROPOSITION DE RÉSOLUTION</text:p>
      // <text:p text:style="normal">Rejeté</text:p>
      // <text:p text:style="italic">
      //   <Pv.Reference.List.Text>
      //     <Pv.Reference.Text Pv.AnnexVote="3">("Résultats des votes", point 3)</Pv.Reference.Text>
      //   </Pv.Reference.List.Text>
      // </text:p>


      $ta_list_a = $dom_xpath->query('.//TA.Reference.List.Text/TA.Reference.Text/a', $ta_parent_list_node);
      if ($ta_list_a->length && $ta_node_name) {
        if ($ta_list_a->length > 1) {
          dpm('More than one adopt texts : ' . ($found ?: $chapter_number_vote), '', 'error');
        }
        $ta_list_a_doc = $file_system->basename($ta_list_a->item(0)->getAttribute('href'));

        if ($found) {
          $rcv_results[$found][$ta_node_name] = $ta_list_a_doc;
        }
        else {
          $empties[$chapter_number_vote][$ta_node_name] = $ta_list_a_doc;
        }
      }



      if ($ta_node_name) {
        if ($found) {
          $rcv_results[$found]['text_pv'] = $pv_text;
        }
        else {
          $empties[$chapter_number_vote]['text_pv'] = $pv_text;
        }
      }
      else {
        dpm($found ?: $chapter_number_vote, 'No TA reference list text, nor PV : not added to .rsl.yml', 'error');
      }
      //
      //   //*[text() = 'qwerty']
      //   The above selects every element in the document that has at least one text-node child with value 'qwerty'.
      //     //*[text() = 'qwerty' and not(text()[2])]
      //   The above selects every element in the document that has only one text-node child and its value is: 'qwerty'.

      if (!$found && !$map) {
        //         if ($rcv_results_last_found) {
        $rcv_results_last_found = array_shift($rcv_results_keys);
        //         }


        $not_found[$chapter_number_vote][$rcv_results_last_found] = 0 && is_array($rcv_results[$rcv_results_last_found]['vote']) ? key($rcv_results[$rcv_results_last_found]['vote']) : $rcv_results[$rcv_results_last_found]['vote'];

        if ($str_replace_with_id) {
          if ($rcv_results_last_found == 'pushed') {
            $not_found[$chapter_number_vote] = [];
          }
          else {
            $not_found[$chapter_number_vote][$rcv_results_last_found] = [];
          }
        }
      }
    }

    if ($not_found) {
      if (!$map && $force_write) {
        var_export($not_found);
        exit;
      }
    }
    if ($map) {
      dpm($not_found);

      $yaml = Yaml::encode($map);

      if ($force_write || !file_exists($filename_yaml)) {
        $dirname_yaml = $file_system->dirname($filename_yaml);
        if (!is_dir($dirname_yaml)) {
          $is_dir = $file_system->prepareDirectory($dirname_yaml, FileSystemInterface::CREATE_DIRECTORY);
          dpm($is_dir);
        }
        file_put_contents($filename_yaml, $yaml);
        dpm($map, 'Written map to ' . $doc_rcv_id . ' PV');
      }
    }
    else {
      dpm($not_found);
    }

    $insert_missing_id_flip = array_flip(static::tocMapInsertMissingId($doc_rcv_id));
    dpm($insert_missing_id_flip);

    if (1 || $range) {
      // Log results with non english, to be checked manually
      $rcv_results_empty_en = $rcv_results_ammends =
      $rcv_results_proposal = $rcv_results_proposal2 =
      $rcv_results_accord = $rcv_results_accord2 =
      $rcv_results_projet = $rcv_results_projet2 =
      $rcv_results1 = [];
      $rcv_results_readable = [];
      foreach ($rcv_results as $id => & $rcv_result) {
        // This if is deprecated ?
        //   Used only when $map is not finished ?
        if (empty($rcv_result['vote_rcv'])) {
          //                 dpm([$rcv_result['vote'], $id, $map[$rcv_result['vote']][$id]]);
          if (is_array($rcv_result['vote'])) {
            dpm(['line: ' . __LINE__, $rcv_result['vote']]);
            $rcv_result['vote'] = key($rcv_result['vote']);
          }
          else {
            dpm(['line: ' . __LINE__, $rcv_result['vote']]);
          }
          if (!empty($map[$rcv_result['vote']][$id])) {
            $rcv_result['vote_rcv'] = $map[$rcv_result['vote']][$id];
            dpm(['line: ' . __LINE__, $rcv_result['vote']]);
          }else {
            // '2. Objections formulées en vertu de l’article 111, paragraphe 3, du règlement: Nouveaux aliments - définition d’un «nanomatériau manufacturé» (vote)'
            //                   var_export($rcv_result);
            //                   exit;
          }

          if (empty($rcv_result['vote_rcv'])) {
            if (isset($insert_missing_id_flip[$id])) {
              $rcv_result['vote'] = $insert_missing_id_flip[$id];
            }
            $rcv_results_empty_en[$id] = $rcv_result;
          }
        }
        else {

        }

        // Backup current 'vote' into 'vote_pv' (second process of vote title from pv xml)
        $rcv_result['vote_pv'] = $rcv_result['vote'];
        if (!empty($map[$id])) {
          $rcv_result['vote'] = $map[$id];
        }

//         if ($mode_id && empty($rcv_result['vote_rcv'])) {
//           $rcv_result['vote_rcv'] = [$rcv_result['vote_pv'] => $rcv_result['vote_pv']];
// //           $rcv_result['vote_pv'] = $chapter_number_vote;
//         }


        if (isset($rcv_tocs_doc_grp[$id]['Desc'])) {
          $rcv_result['vote'] = $rcv_tocs_doc_grp[$id]['Desc'];
          unset($rcv_tocs_doc_grp[$id]);
        }
        // Only if having a TOC map
        else if ($rcv_tocs_doc_grp){
          dpm('Missing vote title from the rcv TOC .pdf : ' . $rcv_result['vote_pv'], '', 'error');
        }


        $rcv_result_tmp = $rcv_result;
        $rcv_result_tmp = array_diff_key($rcv_result_tmp, array_flip(['vote', 'vote_rcv', 'vote_pv', 'TA.Reference.List.Text', 'text_pv']));

        if (count($rcv_result_tmp) > 1) {
          $rcv_results_ammends[$id] = $rcv_result;
          $rcv_result_end = end($rcv_result_tmp);
          $rcv_result_tmp = [$rcv_result_end] + array_intersect_key($rcv_result, array_flip(['vote', 'vote_rcv', 'vote_pv', 'TA.Reference.List.Text', 'text_pv']));
        }
        else {
          $rcv_result_tmp = $rcv_result;
        }

        if ($mode_id) {
          $vote_pv__for_cmp = preg_replace('/^\d+\.\s*/', '', $rcv_result_tmp['vote_pv']);
          $vote_pv__for_cmp = preg_replace('/\s*\(vote\)\s*$/', '', $vote_pv__for_cmp);
          $vote_pv__for_cmp = preg_replace('/\s*\*+I*$/', '', $vote_pv__for_cmp);

          $vote_pv__for_cmp = preg_replace('/\s+/', ' ', $vote_pv__for_cmp);
          $title__for_cmp = preg_replace('/\s+/', ' ', $rcv_result_tmp[0]['title']);
          $vote_pv__for_cmp = str_replace('\'', '’', $vote_pv__for_cmp);
          $title__for_cmp = str_replace('\'', '’', $title__for_cmp);

          $str_ireplace = [
            'Propositions de résolution - ' => '',
            // 13 March 2024
            'Procédure de demande unique en vue de la délivrance d’un permis unique autorisant les ressortissants de pays tiers à résider et à travailler sur le territoire d’un État membre et socle commun'
              => 'Procédure de demande unique en vue de la délivrance d’un permis unique autorisant les ressortissants de pays tiers à résider et à travailler sur le territoire d’un État membre et établissant un socle commun',
            //  8 February 2024
            'Échange automatisé de données aux fins de la coopération policière («Prüm II»)'
              => 'Échange automatisé de données dans le cadre de la coopération policière («Prüm II»)',
            //  7 February 2024
            'Modification de la Directive sur les gestionnaires de fonds d’investissement alternatifs (GFIA) et de la directive'
              => 'Amendements à la directive sur les gestionnaires de fonds d’investissement alternatifs (GFIA) et à la directive',
            // 18/01/2024
            'Extension de la liste des infractions de l’UE aux discours de haine et aux crimes de haine - Extending the list of EU crimes to hate speech and hate crime - Erweiterung der Liste der EU-Straftatbestände um Hetze und Hasskriminalität'
              => 'Extension de la liste des infractions de l’UE aux discours de haine et aux crimes de haine'
          ];
          $title__for_cmp = str_ireplace(array_keys($str_ireplace), array_values($str_ireplace), $title__for_cmp);

          if (isset($map[$rcv_result_tmp['vote_pv']][$id])
            && $map[$rcv_result_tmp['vote_pv']][$id] == []
            ) {

          }
          else if ($title__for_cmp != $vote_pv__for_cmp) {
            dpm($title__for_cmp . ' not equals ' . $vote_pv__for_cmp, '', 'error');
          }
        }

        $rcv_results_mep_end = [];
        if (!empty($rcv_results_mep[$id])) {
          $rcv_results_mep_end = end($rcv_results_mep[$id]);
//           dpm($rcv_results_mep_end);exit;
        }

        $rcv_results_group_end = [];
        foreach ($rcv_results_mep_end as $rcv_results_mep_end_key => $rcv_results_mep_end_group) {
          if (!is_array($rcv_results_mep_end_group)) {
//             dpm($rcv_results_mep_end_key);
            $rcv_results_group_end[$rcv_results_mep_end_key] = $rcv_results_mep_end_group;
            continue;
          }
          foreach ($rcv_results_mep_end_group as $rcv_results_mep_end_group_key => $rcv_results_mep_end_group_item) {

            $group_map = static::partiMap($rcv_results_mep_end_group_item['id']);
            $rcv_results_group_end[$rcv_results_mep_end_key][$rcv_results_mep_end_group_key] = round(($rcv_results_mep_end_group_item['count'] / $group_map['nb']) * 100, 0) . '%';
//             [
//               round(($rcv_results_mep_end_group_item['count'] / $group_map['nb']) * 100, 0) . '%',
//               $rcv_results_mep_end_group_item['count'] / $group_map['nb'],
//               $rcv_results_mep_end_group_item['count'],
//             ];
          }
          // $rcv_results_group_end[$rcv_results_mep_end_key] = array_fill_keys(array_keys($rcv_results_mep_end_group), '');
        }
        // $rcv_result_tmp += $rcv_results_mep_end;
        $rcv_results_readable[$mode_id ? $rcv_result['vote_pv'] : $rcv_result['vote']] = $rcv_results_group_end
          + array_intersect_key($rcv_result, array_flip(['TA.Reference.List.Text', 'text_pv']))
          + ['id' => $id]
        ;

        $found = FALSE;
        $has_desc_pers = static::resultItem($rcv_result_tmp, 'proposition');
        if (!is_null($has_desc_pers)) {
          $rcv_results_proposal[$id] = $rcv_result_tmp;

          if (!$has_desc_pers) {
            $rcv_results_proposal2[$id] = $rcv_result_tmp;
          }
          $found = TRUE;
        }
        // Try accord only if not found previous
        if (!$found) {
          $has_desc_pers = static::resultItem($rcv_result_tmp, 'accord');
          if (!is_null($has_desc_pers)) {
            $rcv_results_accord[$id] = $rcv_result_tmp;

            if (!$has_desc_pers) {
              $rcv_results_accord2[$id] = $rcv_result_tmp;
            }
            $found = TRUE;
          }
        }
        // Try accord only if not found previous
        if (!$found) {
          $has_desc_pers = static::resultItem($rcv_result_tmp, 'projet');
          if (!is_null($has_desc_pers)) {
            $rcv_results_projet[$id] = $rcv_result_tmp;

            if (!$has_desc_pers) {
              $rcv_results_projet2[$id] = $rcv_result_tmp;
            }
            $found = TRUE;
          }
          else {
            dpm($rcv_result_tmp);
          }
        }
      }
      if ($rcv_tocs_doc_grp) {
        dpm('Missing vote title in results, the rcv TOC .pdf value is : ' . $rcv_tocs_doc_grp[$id]['Desc'], '', 'error');
      }

      $pv_results_empty_fr = [];
      foreach ($map as $id => $pv_result) {
        if (!$pv_result) {
          $pv_result = [];

          if (!empty($empties[$id])) {
            $pv_result = $empties[$id];
            $rcv_results[$id] = $empties[$id];
          }

          $pv_results_empty_fr[$id] = $pv_result;
        }
      }



//       dpm($rcv_results);
      if ($rcv_results_empty_en) {
        dpm($rcv_results_empty_en, 'RCV Voted, but not in PV', 'error');
      }
      dpm($pv_results_empty_fr);
//       dpm($rcv_results_ammends);
//       dpm($rcv_results_proposal);
//       dpm($rcv_results_accord);
//       dpm($rcv_results_projet);
//       dpm($rcv_results_proposal2);
//       dpm($rcv_results_accord2);
//       dpm($rcv_results_projet2);


    }else {
      dpm(count($rcv_results));
    }

    return $rcv_results_readable;
  }

  public static function taResultsDocNCorr($doc_rcv_id, & $rcv_results, $force_write = FALSE) {
    $host = 'data.europarl.europa.eu';
    $api_path = '/api/v2';
    $api_path_docs = $api_path . '/documents';
    $distri_path_doc = '/distribution/doc';
    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');

    $params = [
      'format' => 'application/ld+json',
      'language' => 'fr',
    ];

    $correction_pv = [
      // pv : A9-0150/2023 ==> TA-9-2024-0365
      'A9-0150/2023' => 'TA-9-2024-0364',
      // pv : TA-9-2024-0366
      'A9-0151/2023' => 'TA-9-2024-0365',
      // pv : TA-9-2024-0367
      'A9-0128/2023' => 'TA-9-2024-0366',
      // pv : TA-9-2024-0368
      'B9-0223/2024' => 'TA-9-2024-0367',
    ];

    $rc_eli_prefix_adopts = [
      'B9-0227/2024', 'B9-0228/2024', 'B9-0253/2024', 'B9-0262/2024', 'B9-0244/2024', 'B9-0235/2024', 'B9-0175/2024', 'B9-0179/2024', 'B9-0187/2024', 'B9-0147/2024', 'B9-0144/2024', 'B9-0143/2024',
      'B9-0124/2024', 'B9-0110/2024', 'B9-0106/2024', 'B9-0105/2024', 'B9-0102/2024', 'B9-0097/2024', 'B9-0068/2024', 'B9-0509/2023', 'B9-0511/2023', 'B9-0510/2023', 'B9-0436/2023', 'B9-0395/2023',
      'B9-0415/2023', 'B9-0396/2023', 'B9-0369/2023', 'B9-0378/2023'
    ];

    $limit = 2*3;
    $dl = 0;

    $rcv_result = reset($rcv_results);
    $id = key($rcv_results);
    // doc_eli_TA => [current_doc_id_eli_or_id => [others_doc_id_eli]]
    // doc_eli_TA => [currents_doc_id_eli]
    $doc_ids_others = $ordre_dujours = [];
    while ($rcv_result && $limit > 0) {
      $identifier = $rcv_result['TA.Reference.List.Text'] ?? '';

      if (stripos($identifier, 'TA-') === 0) {
        $doc_dir = 'document-ta';

        $path = $api_path_docs . '/' . $identifier;
        $filename = 'public://' . $host . $api_path . '/' . $doc_dir . '/' . $identifier;

        // process of 10 items
        if (!file_exists($filename)) {
          dpm($id);
          dpm($rcv_result);

          $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);
          $content = static::uriGetContents($filename, $uri);

          $limit--;
        }
        else {
          $content = file_get_contents($filename);
        }

        $results_ta_docs = Json::decode($content);
        // Double check, doc_id to doc_eli
        $doc_id = $doc_eli = '';
        $doc_ids = [];
        if (!empty($results_ta_docs['data'][0]['adopts'][0])) {
          foreach ($results_ta_docs['data'][0]['adopts'] as $i => $value) {
            $doc_eli = $file_system->basename($value);
            // @todo log if count($results_ta_docs['data'][0]['adopts']) > 1
            $matches = [];
            if (preg_match('/-(19\d{2,2}|20\d{2,2})-/', $doc_eli, $matches)) {
              [$prefix, $suffix] = preg_split('/-(19\d{2,2}|20\d{2,2})-/', $doc_eli);
              $prefix = str_replace('-', '', $prefix);
              $doc_id = $prefix . '-' . $suffix . '/' . trim($matches[1], '-');
              if (in_array($doc_id, $rc_eli_prefix_adopts)) {
                $doc_id = 'RC-' . $doc_id;
              }
              $doc_ids[$doc_id] = $doc_id;
            }
          }
          $doc_id = reset($doc_ids);
          if (count($results_ta_docs['data'][0]['adopts']) > 1) {
            $doc_ids_curr = array_intersect_key($doc_ids, $rcv_results);
            $doc_id_for_others = $id;
            if ($doc_ids_curr) {
              $doc_id = $doc_id_for_others = array_shift($doc_ids_curr);
            }

            $doc_ids_others[$results_ta_docs['data'][0]['id']][$doc_id_for_others] = array_diff_key($doc_ids, $rcv_results);
            $rcv_results[$id]['TA.Reference.List.Text__others'] = $doc_ids_others[$results_ta_docs['data'][0]['id']][$doc_id_for_others];

            // Log error only when writing
            $filename_yaml = 'public://' . $host . '/map/' . $doc_rcv_id . '.rsl.yml';
            if ($force_write || !file_exists($filename_yaml)) {
              dpm($results_ta_docs['data'][0]['id'] . ' adopts more than one for $id (' . $id . ') : ' . implode(',', $doc_ids_curr) . '; ' . implode(',', $doc_ids_others[$results_ta_docs['data'][0]['id']][$doc_id_for_others]), '', 'error');
            }

            $doc_ids_others[$results_ta_docs['data'][0]['id']] += $doc_ids_curr;
            $rcv_results[$id]['TA.Reference.List.Text__currents'] = $doc_ids_curr;

          }
          $results_ta_docs['adopts'] = $doc_id;
        }

        if (/*$doc_eli &&*/ $doc_id != $id) {

          // Look if corrected
          if (isset($correction_pv[$id])) {
            $rcv_results[$id]['TA.Reference.List.Text__corr'] = $correction_pv[$id];

            $path = $api_path_docs . '/' . $correction_pv[$id];
            $filename = 'public://' . $host . $api_path. '/' . $doc_dir . '/' . $correction_pv[$id];

            // process of 10 items
            if (!file_exists($filename)) {
              $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);
              $content = static::uriGetContents($filename, $uri);
            }
            else {
              $content = file_get_contents($filename);
            }
            $results_ta_docs_corr = Json::decode($content);
            dpm($results_ta_docs_corr, '', 'error');

            $results_ta_docs = $results_ta_docs_corr;
          }
          else {
            if ($doc_id) {
              dpm($results_ta_docs['data'][0]['id'] . ' adopts (' . $doc_id . ') and not the $id (' . $id . ')', '', 'error');
              $rcv_results[$id]['TA.Reference.List.Text__err'] = $rcv_results[$id]['TA.Reference.List.Text'] . ' adopts (' . $doc_id . ') and not the $id (' . $id . ')';
            }
            else {
              dpm($results_ta_docs['data'][0]['id'] . ' miss adopts for the $id (' . $id . ')', '', 'error');
              // dpm($results_ta_docs, '', 'error');
              $rcv_results[$id]['TA.Reference.List.Text__err'] = 'Missing adopts for ' . $rcv_results[$id]['TA.Reference.List.Text'];
            }
          }
        }
        else {
          //               dpm($results_ta_docs);
        }
        // $var['data'][0]['adopts'][0]

        // data.europarl.europa.eu/distribution/doc/    TA-9-2024-0361    _fr.docx
        $filename_docx = 'public://' . $host . $distri_path_doc . '/' . $file_system->basename($results_ta_docs['data'][0]['id']) . '_' . $params['language'] . '.docx';
        if (!file_exists($filename_docx)) {
          static::docGetContents($results_ta_docs);
          dpm($results_ta_docs['data'][0]['id']);
          $limit--;
        }

        $dl++;
      }

      $vote_for_cmp = preg_replace('/^\d+\.\s*/', '', $rcv_result['vote']);
      $vote_for_cmp = preg_replace('/\s+/', ' ', $vote_for_cmp);
      $vote_pv__for_cmp = preg_replace('/^\d+\.\s*/', '', $rcv_result['vote_pv']);
      $vote_pv__for_cmp = preg_replace('/\s*\(vote\)\s*$/', '', $vote_pv__for_cmp);
      $vote_pv__for_cmp = preg_replace('/\s+/', ' ', $vote_pv__for_cmp);

      // $vote_for_cmp != $vote_pv__for_cmp && not ordre du jour
      if ($is_ordre_dujour = mb_stripos($vote_for_cmp, 'ordre du jour') !== FALSE) {
        $ordre_dujours[$rcv_result['vote']] = $id;
      }
      if ($vote_for_cmp != $vote_pv__for_cmp && ($is_ordre_dujour && mb_stripos($vote_for_cmp, $id) === FALSE)) {
        dpm('Not equals vote and pv : ' . $vote_pv__for_cmp . ' for id ' . $id, '', 'error');
      }

      // $var['A9-0158/2024']['vote']
      // $var['A9-0158/2024']['vote_pv']

      $rcv_result = next($rcv_results);
      $id = key($rcv_results);

    }
    foreach ($ordre_dujours as $ordre_dujours_vote => $ordre_dujours_id) {
      unset($rcv_results[$ordre_dujours_id]);
    }

    $filename_yaml = 'public://' . $host . '/map/' . $doc_rcv_id . '.rsl.yml';
    $yaml = Yaml::encode($rcv_results);

    $msg = 'Read from ';
    if ($force_write || !file_exists($filename_yaml)) {
      $dirname_yaml = $file_system->dirname($filename_yaml);
      if (!is_dir($dirname_yaml)) {
        $is_dir = $file_system->prepareDirectory($dirname_yaml, FileSystemInterface::CREATE_DIRECTORY);
        dpm($is_dir);
      }
      file_put_contents($filename_yaml, $yaml);

      $msg = 'Written to ';
    }

    dpm($dl);


    dpm($rcv_results,  $msg . '$rcv_results');
    // 'https://data.europarl.europa.eu/api/v2/meps?parliamentary-term=9&format=application%2Fld%2Bjson&offset=0&limit=1000';



    return [$doc_ids_others, $ordre_dujours];
  }

  public static function allMeps($parliament_nb = '9') {
    $host = 'data.europarl.europa.eu';
    $api_path_mep9_files = '/api/v2/meps';
    $filename = 'public://' . $host . $api_path_mep9_files . '/9.json';
    $results = Json::decode(file_get_contents($filename));
//     dpm($results);

    $params = [
      'format' => 'application/ld+json',
      'language' => 'fr',
    ];

    $limit = 20;
    $dl = 0;

    $result = array_pop($results['data']);
    while ($result && $limit > 0) {

      $api_path_a_mep9_files = '/api/v2/mep';
      $path = $api_path_mep9_files . '/' . $result['identifier'];
      $filename = 'public://' . $host . $api_path_a_mep9_files . '/' . $result['identifier'];
      // https://data.europarl.europa.eu/api/v2/meps/249443?format=application%2Fld%2Bjson
      $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);

      // process of 10 items
      if (!file_exists($filename)) {
        dpm($result);

        $limit--;



        try {
          $content = static::uriGetContents($filename, $uri);
        }
        catch (\Exception $e) {
          dpm($e->getMessage(), '', 'error');
        }

        $results_mep = Json::decode($content);
        dpm($results_mep);
      }
      //         else {
      $dl++;
      //         }
      $result = array_pop($results['data']);
    }
    dpm($dl);
  }

  public static function allMembers($groups_mem) {
    $host = 'data.europarl.europa.eu';
    $filename_mem = 'public://' . $host . '/map' . '/members.json';

    $filename_mem_groups = 'public://' . $host . '/map' . '/members_groups.json';
    if (!is_null($groups_mem)) {
      // Write newly added members from rcv_results (-rcv.xml)
      file_put_contents($filename_mem, Json::encode($groups_mem));

      dpm($groups_mem, __FUNCTION__ . ' at ' . __LINE__ . ' $groups_mem');
      $members = [];
      foreach ($groups_mem as $pers_id => $group) {
        if (count($group) > 1) {
          dpm($pers_id . ' has more than 1 parti', __FUNCTION__ . ' at ' . __LINE__, 'error');
        }

        // @see allMeps(), legacy called from ta_results_docnerr
        $api_path_a_mep9_files = '/api/v2/mep';
        $filename = 'public://' . $host . $api_path_a_mep9_files . '/' . $pers_id;
        if (file_exists($filename)) {
          $member = Json::decode(file_get_contents($filename));
        }
        else {
          $params = [
            'format' => 'application/ld+json',
            'language' => 'fr',
          ];
          $api_path_mep9_files = '/api/v2/meps';
          $path = $api_path_mep9_files . '/' . $pers_id;
          $filename = 'public://' . $host . $api_path_a_mep9_files . '/' . $pers_id;
          // https://data.europarl.europa.eu/api/v2/meps/249443?format=application%2Fld%2Bjson
          $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);

          // process of 10 items
          if (!file_exists($filename)) {

            try {
              $content = static::uriGetContents($filename, $uri);
            }
            catch (\Exception $e) {
              _dpm_menu_breadcrumb($e->getMessage(), [], 'error');
            }

            $member = Json::decode($content);
            dpm($member);
          }
          // continue becuse check it before to be added to $members[], then written to $filename_mem_groups
          continue;
        }


        //             $member['data'][0]['familyName'];
        //             $member['data'][0]['label'];
        //             $member['data'][0]['img'];

        $members_pers = [];
        $members_pers['id'] = $pers_id;
        $members_pers['label'] = $member['data'][0]['label'];
        $members_pers['img'] = $member['data'][0]['img'];
        // @todo correction BEL => to FRA for Caroline Roose
        $members_pers['citizenship'] = str_replace('http://publications.europa.eu/resource/authority/country/', '', $member['data'][0]['citizenship']);
        $group_id = key($group);
        $group_term = static::partiMap($group_id)['term_id'] ?? $group_id;
        if ($group_term == $group_id) {
          _dpm_menu_breadcrumb('Coudnt get term_id @term_id from group_id @group_id', ['@term_id' => $group_term, '@group_id' => $group_id], 'error');
        }
        $members_pers['group'] = $group_term;
        $members[] = $members_pers;

        //           dpm($members);
        //             dpm($member);return;
      }
      //           dpm($vote_for_mem);return;
      //

      file_put_contents($filename_mem_groups, Json::encode(['data' => $members]));
      //         dpm(['Line ' . __LINE__, $rcv_results]);
      //         dpm($rcv_results);

//       return;

    }
    else {
      $groups_mem = Json::decode(file_get_contents($filename_mem));
      $members = Json::decode(file_get_contents($filename_mem_groups))['data']; // ['data' => $members]));
    }

    return [$groups_mem, $members];
  }

  public static function resultItem(&$rcv_result, $starts_with = 'proposition') {
    $has_desc_pers = NULL;
    if (empty($rcv_result[0]['Desc'])) {
      dpm($rcv_result);
    }else if (($stripos = stripos($rcv_result[0]['Desc'], $starts_with)) !== FALSE) {
      $has_desc_pers = $stripos;
      if ($has_desc_pers) {
        [$desc_pers, $desc_text] = array_pad(preg_split('/\s+-\s+/', $rcv_result[0]['Desc'], 2), 2, NULL);
        if ($desc_text) {
          $rcv_result[0]['Desc'] = $desc_text;
          $rcv_result[0]['DescPers'] = $desc_pers;
        }
      }
    }

    return $has_desc_pers;
  }

  public static function pvChapterNumberVote($oj_node_or_parent) {
    $dom = $oj_node_or_parent->ownerDocument;
    $dom_xpath = new \DOMXPath($dom);

    $oj_node_parent = $oj_node_or_parent->nodeName == 'OJ.ReportShortDescription.Text' ? $oj_node_or_parent->parentNode : $oj_node_or_parent;
    if ($oj_node_parent->nodeName != 'text:h') {
      $oj_node_parent = $oj_node_parent->parentNode;
//       dpm($oj_node_parent->nodeName);
    }

    $chapter_number = $dom_xpath->query('./text:chapter-number', $oj_node_parent);
    if ($chapter_number->length) {
      $chapter_number_value = $chapter_number->item(0)->nodeValue;
      $tmp = explode('.', $chapter_number_value);
      $vote_number = end($tmp);
      $chapter_number_vote = preg_replace('/^' . str_replace('.', '\.', $chapter_number_value) . '/', $vote_number . '. ', trim($oj_node_parent->nodeValue));
    }
    else {
      $chapter_number_vote = 'Missing child <text:chapter-number> to node with value : ' . trim($oj_node_parent->nodeValue);
    }

    return $chapter_number_vote;
  }

  /**
   * @example
   *
    $stage = 'toc';
    $stage = 'pv';
    $stage = 'mep';
    // stage: drush mim mep_watch_member
    $stage = 'vote';

    Drupal\votewatch\Helper\RcvResult::result($stage)
   * @param string $stage
   */
  public static function result($stage = '') {
    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');

    $host = 'data.europarl.europa.eu';
    $api_path = '/api/v2/documents';
    $api_path_files = '/api/v2/document';

    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');

    $filename_mem = 'public://' . $host . '/map' . '/members.json';
    if (file_exists($filename_mem)) {
      $groups_mem = Json::decode(file_get_contents($filename_mem));
    }
    else {
      $groups_mem = [];
    }
    $force_write = TRUE;
    $force_write = FALSE;
    if (!$force_write) {
      $groups_mem = NULL;
    }
    else {
      // 'Gilbert Collard'
      $groups_mem['197683']['NI'] = 'NI';
      // MLT Roberta METSOLA
      $groups_mem['118859']['PPE'] = 'PPE';
      // LVA Ansis PŪPOLS
      // 14-05-2024 ... : European Conservatives and Reformists Group - Member
      $groups_mem['256134']['ECR'] = 'ECR';
      // GRC Ioannis LAGOS
      $groups_mem['197737']['NI'] = 'NI';
      dpm($groups_mem);
    }

    $processed = [
      'PV-9-2024-04-25-RCV',
      'PV-9-2024-04-24-RCV',
      'PV-9-2024-04-23-RCV',
      'PV-9-2024-04-11-RCV',
      'PV-9-2024-04-10-RCV',
      'PV-9-2024-03-14-RCV',
      'PV-9-2024-03-13-RCV',
      'PV-9-2024-03-12-RCV',
      'PV-9-2024-02-29-RCV',
//       'PV-9-2024-02-28-RCV',
//       'PV-9-2024-02-27-RCV',
//       'PV-9-2024-02-08-RCV',
//       'PV-9-2024-02-07-RCV',
//       'PV-9-2024-02-06-RCV',
//       'PV-9-2024-01-18-RCV',
//       'PV-9-2024-01-17-RCV',
//       'PV-9-2024-01-16-RCV',
//       'PV-9-2023-12-14-RCV',
//       'PV-9-2023-12-13-RCV',
//       'PV-9-2023-12-12-RCV',
//       'PV-9-2023-11-22-RCV',
//       'PV-9-2023-11-21-RCV',
//       'PV-9-2023-11-09-RCV',
//       'PV-9-2023-10-19-RCV',
//       'PV-9-2023-10-18-RCV',
//       'PV-9-2023-10-17-RCV',
//       'PV-9-2023-10-05-RCV',
//       'PV-9-2023-10-04-RCV',
//       'PV-9-2023-10-03-RCV',
//       'PV-9-2023-09-14-RCV',
//       'PV-9-2023-09-13-RCV',
//       'PV-9-2023-09-12-RCV',
    ];
    // @todo A traiter ultérieurement
    $duplicates = [
      'PV-9-2024-04-11-RCV' => ['C9-0120/2024' => '2. Simplification de certaines règles de la PAC ***I'
        . ' - Demande d\'application de la procédure d\'urgence'],
      // PV-9-2024-04-23-RCV
      'PV-9-2024-03-13-RCV' => ['A9-0077/2024' => '2. Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits ukrainiens au titre de l\'accord d’association UE/Euratom/Ukraine ***I (vote)'
        . ' - PROPOSITION DE REJET DE LA PROPOSITION DE LA COMMISSION'],
      'PV-9-2024-02-07-RCV' => [
        // PV-9-2024-04-24-RCV
        'A9-0014/2024' => '12. Végétaux obtenus au moyen de certaines nouvelles techniques génomiques et denrées alimentaires et aliments pour animaux qui en sont dérivés ***I (vote)'
        . ' - PROPOSITION DE REJET DE LA PROPOSITION DE LA COMMISSION',
        // PV-9-2024-04-10-RCV
        'A9-0021/2024' => '1. Dérogation temporaire: lutte contre les abus sexuels commis contre des enfants en ligne ***I (vote)'
        . ' - DÉCISION D\'ENGAGER DES NÉGOCIATIONS INTERINSTITUTIONNELLES',
      ],
      'PV-9-2024-01-17-RCV' => [
        // 	2024-04-23
        'A9-0439/2023' => 'Coordination efficace des politiques économiques et surveillance budgétaire multilatérale'
          . ' - Décision d\'engager des négociations interinstitutionnelles: ',
        //  2024-04-10
        'A9-0002/2024' => '5. Mercure: amalgames dentaires et autres produits contenant du mercure ajouté faisant l’objet de restrictions de fabrication, d’importation et d’exportation ***I (vote)'
          . ' - 17/01/2024',
      ],
      // 2024-04-24
      'PV-9-2023-12-13-RCV' => [
        'A9-0395/2023' => '1. Espace européen des données de santé ***I (vote)'
          . ' -  13/12/2023',
      ],
      //
      'PV-9-2023-12-12-RCV' => [
        // 	2024-04-10
        'A9-0385/2023' => '2. Aliments destinés à la consommation humaine: modification de certaines des directives dites "petit-déjeuner" ***I (vote)'
          . ' - 12/12/2023',
        // 2024-03-13
        'A9-0370/2023' => '10. Exigences minimales relatives aux durées minimales des pauses et des temps de repos journaliers et hebdomadaires dans le secteur du transport occasionnel de voyageurs ***I (vote)'
          . ' - 12/12/2023',
      ],
      'PV-9-2023-11-22-RCV' => [
        // 2024-04-24
        'A9-0319/2023' => '14. Emballages et déchets d\'emballages ***I (vote)'
          . ' - PROPOSITION DE REJET DE LA PROPOSITION DE LA COMMISSION'
      ],
      'PV-9-2023-11-21-RCV' => [
        // 2024-04-25
        'A9-0343/2023' => '4. Cadre de mesures en vue de renforcer l’écosystème européen de la fabrication de produits de technologie «zéro net» (règlement pour une industrie «zéro net») ***I (vote)'
          . ' - 21/11/2023',
        // 2024-04-23
        'A9-0316/2023' => '3. Règles communes visant à promouvoir la réparation des biens ***I (vote)'
          . ' - 21/11/2023',
        // 2024-04-10
        'A9-0313/2023' => '6. Renforcement des normes de performance en matière d’émissions de CO2 pour les véhicules utilitaires lourds neufs ***I (vote)'
          . ' - PROPOSITION DE REJET DE LA PROPOSITION DE LA COMMISSION',
        // 	2024-04-10
        'A9-0329/2023' => '5. Cadre de certification de l’Union relatif aux absorptions de carbone ***I (vote)'
          . ' - 21/11/2023',
      ],
      'PV-9-2023-11-09-RCV' => [
        // 2024-04-10
        'A9-0296/2023' => '15. Comptes économiques européens de l\'environnement: nouveaux modules ***I (vote)'
          . ' - 09/11/2023',
        // 2024-03-13
        'A9-0298/2023' => '19. Réception par type des véhicules à moteur et des moteurs en ce qui concerne leurs émissions et leur durabilité (Euro 7) ***I (vote)'
          . ' - PROPOSITION DE REJET DE LA PROPOSITION DE LA COMMISSION',
        // 	2024-02-06
        'A9-0311/2023' => '17. Déchets d’équipements électriques et électroniques (DEEE) ***I (vote)'
          . ' - 09/11/2023',
      ],
      'PV-9-2023-10-17-RCV' => [
        // 2024-02-27
        'A9-0286/2023' => '5. Création de la facilité pour l\'Ukraine ***I (vote)'
          . ' - 17/10/2023',
        // 2024-02-27
        'A9-0290/2023' => '6. Établissement de la plateforme Technologies stratégiques pour l\'Europe («STEP») ***I (vote)'
          . ' - 17/10/2023',
      ],
      'PV-9-2023-10-05-RCV' => [
        // 	2024-04-24
        'A9-0280/2023' => '3. Modification du règlement (UE) 2016/399 concernant un code de l’Union relatif au régime de franchissement des frontières par les personnes (vote)'
          . ' - 05/10/2023',
        // 	2024-04-10
        'A9-0276/2023' => '10. Traitement des eaux urbaines résiduaires ***I (vote)'
          . ' - 05/10/2023',
      ],
      'PV-9-2023-10-04-RCV' => [
        // 2024-04-23
        'A9-0271/2023' => '1. Classification, étiquetage et emballage des substances et des mélanges ***I (vote)'
          . ' - 04/10/2023',
      ],
      'PV-9-2023-10-03-RCV' => [
        // 2024-03-13
        'A9-0264/2023' => '6. Législation européenne sur la liberté des médias ***I (vote)'
          . ' - 03/10/2023',
      ],
      'PV-9-2023-09-14-RCV' => [
        // 2024-04-11
        'A9-0255/2023' => '1. Modification des règlements (UE) 2019/943 et (UE) 2019/942 ainsi que des directives (UE) 2018/2001 et (UE) 2019/944 pour améliorer l\'organisation du marché de l\'électricité de l\'Union ***I (vote)'
          . ' - 14/09/2023',
        // 2023-12-12
        'A9-0260/2023' => '5. Cadre permettant d’assurer un approvisionnement durable et sûr en matières premières critiques ***I (vote)'
          . ' - 14/09/2023',
      ],
      'PV-9-2023-09-13-RCV' => [
        'A9-0233/2023' => '8. La qualité de l’air ambiant et un air pur pour l’Europe ***I (vote)'
          . ' - PROPOSITION DE REJET DE LA PROPOSITION DE LA COMMISSION',
        'A9-0246/2023' => '7. Instrument du marché unique pour les situations d’urgence ***I (vote)'
          . ' - 13/09/2023',
      ],
      'PV-9-2023-09-12-RCV' => [
        // 2024-04-24
        'A9-0250/2023' => '4. Normes de qualité et de sécurité des substances d’origine humaine destinées à une application humaine ***I (vote)'
          . ' - 12/09/2023',
        // 2024-04-24
        'A9-0238/2023' => '7. Polluants des eaux de surface et des eaux souterraines ***I (vote)'
        . ' - 12/09/2023',
      ]
    ];

    $route_match = \Drupal::routeMatch();
    // if ($route_match->getRouteName() == 'system.admin_structure') {
      foreach ([end($processed)] as $rcv) {
        $doc = $rcv;

        $force_write = TRUE;
        $force_write = $stage == 'toc';
        $rcv_tocs_doc_grp = static::tocMap($doc, $force_write);

//         dpm(['Line ' . __LINE__, $rcv_tocs_doc_grp]);
        if ($force_write) {
          //dpm($rcv_tocs_doc_grp);
          $mode_id = static::tocMapModeId($doc);
          foreach ($rcv_tocs_doc_grp as $rcv_tocs_doc_grp_id => $rcv_tocs_doc_grp_item) {

            reset($rcv_tocs_doc_grp_item);
            $rcv_tocs_doc_grp_title = $mode_id ? $rcv_tocs_doc_grp_id : key($rcv_tocs_doc_grp_item);
            if ($rcv_tocs_doc_grp_title
              && (mb_stripos($rcv_tocs_doc_grp_item['Desc'], $rcv_tocs_doc_grp_title) === 0 || mb_stripos($rcv_tocs_doc_grp_title, 'Ordre du jour') !== FALSE)
              && (!$mode_id && preg_match('/^\d+\./', $rcv_tocs_doc_grp_title) || $mode_id)
              ) {

            }
            else {
              dpm($rcv_tocs_doc_grp_title . ' not equals ' . $rcv_tocs_doc_grp_item['Desc'], '', 'error');
              return;
            }
          //$var['A9-0329/2023']['27.Cadre de certification de l’Union relatif aux absorptions de carbone ***I'];
          }

          // Exit only when having a TOC map
          if ($rcv_tocs_doc_grp) {
            return;
          }
        }
        else {
          dpm($rcv_tocs_doc_grp, 'Read TOC from ' . $rcv . ' .pdf');
        }


        if (!is_null($groups_mem)) {
          //? $groups_mem : $groups_mem_curr
          [$rcv_results, $results_docs, $rcv_results_mep, $vote_for_mem] = static::rcvResults($rcv, $groups_mem);

          [$groups_mem, $members] = static::allMembers($groups_mem);
          dpm($members, __FUNCTION__ . ' at ' . __LINE__ . ' $members');
        }
        else {
          $groups_mem_curr = [];
          [$rcv_results, $results_docs, $rcv_results_mep, $vote_for_mem] = static::rcvResults($rcv, $groups_mem_curr);

          [$groups_mem_read, $members] = static::allMembers(NULL);
          dpm(array_diff_key($groups_mem_curr, $groups_mem_read), 'New in current $rcv_doc_id ' . $rcv);
          dpm(array_diff_key($groups_mem_read, $groups_mem_curr), 'Already in, but not in current $rcv_doc_id ' . $rcv);

          // Lasts
          // (Array, 644 elements) + 197683 'Gilbert Collard'
          // (Array, 658 elements) New in current $rcv_doc_id PV-9-2024-04-23-RCV t=> (Array, 13 elements)
          // (Array, 691 elements) New in current $rcv_doc_id PV-9-2024-04-11-RCV t=> (Array, 33 elements) (33 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 694 elements) PV-9-2024-04-10-RCV t=> (Array, 3 elements) (3 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 701 elements) New in current $rcv_doc_id PV-9-2024-03-14-RCV t=> (Array, 7 elements) (7 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 702 elements) New in current $rcv_doc_id PV-9-2024-03-13-RCV t=> (Array, 1 element) (1 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 704 elements) New in current $rcv_doc_id PV-9-2024-03-12-RCV t=> (Array, 2 elements) (2 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 709 elements) New in current $rcv_doc_id PV-9-2024-02-29-RCV t=> (Array, 5 elements) (5 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 709 elements) New in current $rcv_doc_id PV-9-2024-02-28-RCV t=> (Array, 0 elements)
          // (Array, 709 elements) New in current $rcv_doc_id PV-9-2024-02-27-RCV t=> (Array, 0 elements)
          // (Array, 710 elements) New in current $rcv_doc_id PV-9-2024-02-08-RCV t=> (Array, 1 element) (1 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 710 elements) New in current $rcv_doc_id PV-9-2024-02-07-RCV t=> (Array, 0 elements)
          // (Array, 710 elements) New in current $rcv_doc_id PV-9-2024-02-06-RCV t=> (Array, 0 elements)
          // (Array, 711 elements) New in current $rcv_doc_id PV-9-2024-01-18-RCV t=> (Array, 1 element) (1 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 711 elements) New in current $rcv_doc_id PV-9-2024-01-17-RCV t=> (Array, 0 elements)
          // (Array, 711 elements) New in current $rcv_doc_id PV-9-2024-01-16-RCV t=> (Array, 0 elements)
          // (Array, 713 elements) New in current $rcv_doc_id PV-9-2023-12-14-RCV t=> (Array, 2 elements) (2 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 713 elements) New in current $rcv_doc_id PV-9-2023-12-13-RCV t=> (Array, 0 elements)
          // (Array, 713 elements) New in current $rcv_doc_id PV-9-2023-12-12-RCV t=> (Array, 0 elements)
          // (Array, 715 elements) New in current $rcv_doc_id PV-9-2023-11-22-RCV t=> (Array, 2 elements) (2 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 715 elements) New in current $rcv_doc_id PV-9-2023-11-21-RCV t=> (Array, 0 elements)
          // (Array, 717 elements) New in current $rcv_doc_id PV-9-2023-11-09-RCV t=> (Array, 2 elements) (2 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 717 elements) New in current $rcv_doc_id PV-9-2023-10-19-RCV t=> (Array, 0 elements)
          // (Array, 720 elements) New in current $rcv_doc_id PV-9-2023-10-18-RCV t=> (Array, 3 elements) (3 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 721 elements) New in current $rcv_doc_id PV-9-2023-10-17-RCV t=> (Array, 1 element) (1 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 723 elements) New in current $rcv_doc_id PV-9-2023-10-05-RCV t=> (Array, 2 elements) (2 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 724 elements) New in current $rcv_doc_id PV-9-2023-10-04-RCV t=> (Array, 1 element) (1 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 724 elements) New in current $rcv_doc_id PV-9-2023-10-03-RCV t=> (Array, 0 elements)
          // (Array, 726 elements) New in current $rcv_doc_id PV-9-2023-09-14-RCV t=> (Array, 2 elements) (2 created, 0 updated, 0 failed, 0 ignored) - done with 'mep_watch_member'
          // (Array, 726 elements) New in current $rcv_doc_id PV-9-2023-09-13-RCV t=> (Array, 0 elements)
          // (Array, 726 elements) New in current $rcv_doc_id PV-9-2023-09-12-RCV t=> (Array, 0 elements)

          $force_write = TRUE;
          $force_write = $stage == 'mep';
          if ($force_write) {
            [$groups_mem, $members] = static::allMembers($groups_mem_read + $groups_mem_curr);
          }
          else {
            dpm($members);
          }
        }

        $force_write = TRUE;
        $force_write = $stage == 'pv';
        // Write map (PV to RCV)
        $rcv_results_readable = static::pvResults($rcv, $rcv_results, $results_docs, $rcv_tocs_doc_grp, $rcv_results_mep, $force_write);
        $filename_map_pv_yaml = 'public://' . $host . '/map/' . $rcv . '.yml';
        if (file_exists($filename_map_pv_yaml) && !$force_write) {
          dpm(Yaml::decode(file_get_contents($filename_map_pv_yaml)), 'Read map from ' . $rcv . ' PV');
        }

        // === For refresh : $force_write TRUE for "taResultsDocNCorr" and "members_votes.json"
        $force_write = TRUE;
        $force_write = $stage == 'vote';
        [$doc_ids_others, $ordre_dujours] = static::taResultsDocNCorr($rcv, $rcv_results, $force_write);
        $filename_ta_results = 'public://' . $host . '/map/' . $rcv . '.rsl.yml';
        if (file_exists($filename_ta_results)) {
          $ta_results = Yaml::decode(file_get_contents($filename_ta_results));
        }
        dpm($doc_ids_others);

        $force_write = TRUE;
        $force_write = $stage == 'vote';
        $matches = [];
        $filename_mem_votes = '';
        if (preg_match('/^PV-9-(\d{4,4}-\d{2,2}-\d{2,2})-RCV$/', $rcv, $matches)) {
          $filename_mem_votes = 'public://' . $host . '/map' . '/members_votes_' . $matches[1] . '.json';
        }

        $vote_mems = [];

        if ($filename_mem_votes && ($force_write || !file_exists($filename_mem_votes))) {
//         $term_map = static::term_map();

        dpm($ordre_dujours);

        foreach ($rcv_results_readable as $vote_label => & $vote_readable) {
          // Get the last (proposition de la commission)
          if (isset($ordre_dujours[$vote_label])) {
            continue;
          }

          $vote_id = $vote_readable['id'];
          $vote = end($vote_for_mem[$vote_id]);

          $vote_id_import = $vote_id;
          $vote_label_import = $vote_label;
          if (isset($duplicates[$rcv][$vote_id])) {
            $vote_id_import = $vote_id . '__' . $rcv;
            $vote_label_import = $duplicates[$rcv][$vote_id];
          }

          $vote_mem = [
            'id' => $vote_id_import,
            'label' => $vote_label_import,
//             'for' => $vote['for'],
//             'against' => $vote['against'],
//             'abstain' => $vote['abstain'],
          ];

          // $map['B9-0225/2024']['field_terms']
          if (isset($term_map[$vote_id]['field_terms'])) {
            $vote_mem['field_terms'] = $term_map[$vote_id]['field_terms'];
          }

          foreach (['for', 'against', 'abstain'] as $for) {
          foreach ($vote[$for] as $group_count => $vote_members) {
              foreach ($vote_members as $member) {
              // @todo when $member is not a
              if (is_array($member)) {
                $vote_mem[$for][] = $member['PersId'];
              }
            }
          }
          }


          if (isset($ta_results[$vote_id])) {
            foreach (['TA.Reference.List.Text__corr', 'TA.Reference.List.Text__err', 'TA.Reference.List.Text__others', 'TA.Reference.List.Text__currents'] as $ta_key) {
              if (isset($ta_results[$vote_id][$ta_key])) {
                $vote_mem[$ta_key] = $ta_results[$vote_id][$ta_key];
                $vote_readable[$ta_key] = $ta_results[$vote_id][$ta_key];
              }
            }

//             $vote_mem
          }

          $vote_mems[] = $vote_mem + $vote_readable;
        }


        file_put_contents($filename_mem_votes, Json::encode(['data' => $vote_mems]));
        dpm($vote_mems, 'Write $vote_mems to ' . $file_system->basename($filename_mem_votes));
        }
        else {
          if ($filename_mem_votes) {
            $vote_mems = Json::decode(file_get_contents($filename_mem_votes))['data'];
            dpm($vote_mems, 'Read $vote_mems from ' . $filename_mem_votes);
          }
        }
        dpm($rcv_results_readable, __LINE__ . ' $rcv_results_readable');


//         foreach ($rcv_results as $id => $rcv_result) {
//         }


      }

      // Check not chevauchement
      $ta_results_prevs = [];
      $rcv_prev = '';
      foreach ($processed as $rcv) {

        $filename_ta_results = 'public://' . $host . '/map/' . $rcv . '.rsl.yml';
        $ta_results = Yaml::decode(file_get_contents($filename_ta_results));
        if ($dublicate = array_intersect_key($ta_results_prevs, $ta_results)) {
          // Only not yet processed
          if (isset($duplicates[$rcv])) {
            $dublicate = array_diff_key($dublicate, $duplicates[$rcv]);
            // $ta_results = array_diff_key($ta_results, $duplicates[$rcv]);
          }
          if ($dublicate) {
            dpm($dublicate, $rcv . '.rsl.yml', 'error');
          }
        }
        else {
          // dpm('No duplicate', $rcv . '.rsl.yml');
        }

        $ta_results_prevs += $ta_results;
        $rcv_prev = $rcv;
      }

        //     $crawler = new Crawler($dom);
        //     $uri_message = $crawler->filter('PV.RollCallVoteResults RollCallVote.Result Result.For Result.PoliticalGroup.List PoliticalGroup.Member.Name')->html('empty');
        //     dpm($uri_message);
        // PV.RollCallVoteResults

        //     dpm($variables);

        // curl -X 'GET' \
        // 'https://data.europarl.europa.eu/api/v2/documents?work-type=VOTE_ROLLCALL_PLENARY&format=application%2Fld%2Bjson&offset=400&limit=100' \
        // -H 'accept: */*'
        //  https://data.europarl.europa.eu/api/v2/documents?work-type=VOTE_ROLLCALL_PLENARY&format=application%2Fld%2Bjson&offset=400&limit=100
        $host = 'data.europarl.europa.eu';
        $path = '/api/v2/documents';


        if(0) {

          $params = [
            'work-type' => 'AMENDMENT_LIST',
//             'work-type' => 'AMENDMENT_PLENARY',
            'format' => 'application/ld+json',
            // A-8-2014-0001-AM-001-003
            // A-8-2016-0013-AM-017-017
            'offset'=> '0',
            'limit' => '500',
            // A-8-2016-0017-AM-001-003
            // A-8-2017-0031-AM-001-046
//             'offset'=> '500',
//             'limit' => '500',
            // A-8-2017-0031-AM-047-047
            // A-8-2017-0347-AM-001-245
//             'offset'=> '1000',
//             'limit' => '500',
            // A-8-2014-0001-AM-001-003
            // A-8-2017-0347-AM-001-245
//             'offset'=> '0',
//             'limit' => '1500',
            // A-8-2017-0347-AM-246-246
            // A-9-2022-0001-AM-008-015
//             'offset'=> '1500',
//             'limit' => '3000',
            // A-9-2022-0001-AM-016-021
            // A-9-2024-0439-AM-001-001
            // AFCO-AM-703031
            // ...
            // BUDG-AM-752790
//             'offset'=> '4500',
//             'limit' => '3000',
            'offset'=> '7500',
            'limit' => '3000',

          ];
          $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);

          $path_files = $path;
          if ($params['work-type'] == 'AMENDMENT_PLENARY') {
            $path_files = '/api/v2/documents-am';
          }

          $filename = 'public://' . $host . $path_files . '_' . $params['offset'] . '-' . $params['limit'] . '';

          $content = static::uriGetContents($filename, $uri);
          $results_docs = Json::decode($content);
          usort($results_docs['data'], function($a, $b) {
            return strcasecmp($a['identifier'], $b['identifier']);
          });
          dpm($results_docs);

          return;
        }
        if (0) {
          $params = [
            'format' => 'application/ld+json',
            'language' => 'fr',
          ];
          $identifier = 'A-9-2024-0439-AM-001-001';
          $identifier = 'A-9-2023-0247-AM-023-024';
          $identifier = 'RC-9-2024-0124-AM-017-026';
          $doc_dir = 'document-am';
          // $var['data'][0]['foresees_change_of'] :  eli/dl/doc/A-9-2024-0439
//           $identifier = $file_system->basename('eli/dl/doc/A-9-2024-0439');
          $identifier = 'TA-8-2014-0040';
//           $identifier = 'B-8-2014-0166';
          // "Règlement - 2021_2115 - EN - EUR-Lex.html" ==> https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32021R2115#d1e1871-1-1
//           $identifier = 'TA-9-2024-0344';
//           $identifier = 'P9_TC1-COD(2024)0073';
          $doc_dir = 'document-tmp';

          if (stripos($identifier, 'TA-') === 0) {
            $doc_dir = 'document-ta';
          }

          $path = '/api/v2/documents/' . $identifier;
          $filename = 'public://' . $host . '/api/v2/' . $doc_dir . '/' . $identifier;
          // https://data.europarl.europa.eu/api/v2/documents/PV-9-2024-04-25-RCV?format=application%2Fld%2Bjson&language=fr
          //       $var['identifier']
          $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);
          $content = static::uriGetContents($filename, $uri);
          $results_docs = Json::decode($content);
          dpm($results_docs);

          // work-type=TEXT_ADOPTED
//           {
//             "id": "eli/dl/doc/TA-8-2014-0040",
//             "type": "Work",
//             "work_type": "def/ep-document-types/TEXT_ADOPTED",
//             "identifier": "TA-8-2014-0040"
//           }

          if (in_array($identifier, ['TA-8-2014-0040', 'A-9-2023-0247-AM-023-024', 'RC-9-2024-0124-AM-017-026', 'TA-9-2024-0344'])) {
            static::docGetContents($results_docs);
          }

//           $params = [
//             'format' => 'application/ld+json',
// //             'language' => 'fr',
//           ];
//           $path = '/api/v2/controlled-vocabularies/ep-document-types';
//           $filename = 'public://' . $host . $path;
//           // https://data.europarl.europa.eu/api/v2/controlled-vocabularies/ep-document-types?format=application%2Fld%2Bjson
//           $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);
//           $content = static::uriGetContents($filename, $uri);
//           $results_docs = Json::decode($content);
//           dpm($results_docs);

          return;
        }

        if (0) {
          $path = '/api/v2/documents';
          $params = [
            'work-type' => 'VOTE_ROLLCALL_PLENARY',
            'format' => 'application/ld+json',
            'offset'=> '0',
            'limit' => '500',
          ];
          $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);

          /**@var $file_system \Drupal\Core\File\FileSystem */
          $file_system = \Drupal::service('file_system');


          $filename = 'public://' . $host . $path;

          $content = static::uriGetContents($filename, $uri);

          $results_docs = Json::decode($content);

          usort($results_docs['data'], function($a, $b) {
            return strcasecmp($a['identifier'], $b['identifier']);
          });
          //     dpm(Json::decode($content)); (Array, 2 elements)
          //     data (Array, 473 elements)
          //     @context (Array, 2 elements)
          //     0 (Array, 252 elements)
          //     1 (Array, 1 element)
          //     data (String, 6 characters ) @graph
          dpm($results_docs);

          $result = array_pop($results_docs['data']);
          $server_error = [
            'PV-8-2018-10-24-RCV',
            'PV-8-2018-10-23-RCV',
          ];
        }
        else {
          $result = [];
        }

          $limit = 0;
          $dl = 0;
          while ($result && $limit > 0) {

            if (in_array($result['identifier'], $server_error)) {
              $result = array_pop($results_docs['data']);
              continue;
            }

            $params = [
              'format' => 'application/ld+json',
              // @todo Get also english and other languages verions
              'language' => 'fr',
            ];
            // PV-9-2024-04-25-RCV
            // Procès-verbal- Votes par appel nominal - Jeudi 25 avril 2024
            $path = '/api/v2/documents/' . $result['identifier'];
            $filename = 'public://' . $host . '/api/v2/document/' . $result['identifier'];
            // https://data.europarl.europa.eu/api/v2/documents/PV-9-2024-04-25-RCV?format=application%2Fld%2Bjson&language=fr
            //       $var['identifier']
            $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);

            // process of 10 items
            if (!file_exists($filename)) {
              dpm($result);

              $limit--;

              try {
                $content = static::uriGetContents($filename, $uri);
              }
              catch (\Exception $e) {
                dpm($e->getMessage(), '', 'error');
              }

              $results = Json::decode($content);
              dpm($results);

              static::docGetContents($results);

              // not found : distribution/PV-9-2023-06-15/PV-9-2023-06-15-FNL_fr.xml
              //
              // @todo $var['data'][0]['is_annex_of'][0] :  eli/dl/doc/PV-9-2024-04-25
              $is_annex_of = $file_system->basename($results['data'][0]['is_annex_of'][0]);
              if ($is_annex_of && $is_annex_of == preg_replace('/-RCV$/', '', $result['identifier'])) {
                // $var['data'][0]['title_dcterms']['fr'] : Procès-verbal - Séance plénière - Jeudi 25 avril 2024
                $path = '/api/v2/documents/' . $is_annex_of;
                $filename = 'public://' . $host . '/api/v2/document/' . $is_annex_of;
                // https://data.europarl.europa.eu/api/v2/documents/PV-9-2024-04-25?format=application%2Fld%2Bjson&language=fr
                //       $var['identifier']
                $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);
                try {
                  $content = static::uriGetContents($filename, $uri);
                }
                catch (\Exception $e) {
                  dpm($e->getMessage(), '', 'error');
                }

                $results = Json::decode($content);
                dpm($results);

                static::docGetContents($results);

              }

            }
            //         else {
            $dl++;
            //         }
            $result = array_pop($results_docs['data']);
          }
          dpm($dl);

//           return;
          //       $result = ['identifier' => 'PV-9-2023-06-15-RCV'];
          $result = ['identifier' => 'PV-9-2024-04-24-RCV'];
          //       $result = ['identifier' => 'PV-8-2018-10-23-RCV'];
        $result = [];
        if ($result) {
          $params = [
            'format' => 'application/ld+json',
            'language' => 'fr',
          ];
          // PV-9-2024-04-25-RCV
          // Procès-verbal- Votes par appel nominal - Jeudi 25 avril 2024
          $path = '/api/v2/documents/' . $result['identifier'];
          $filename = 'public://' . $host . '/api/v2/document/' . $result['identifier'];
          // https://data.europarl.europa.eu/api/v2/documents/PV-9-2024-04-25-RCV?format=application%2Fld%2Bjson&language=fr
          //       $var['identifier']
          $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);

          try {
            $content = static::uriGetContents($filename, $uri);
          }
          catch (\Exception $e) {
            dpm($e->getMessage(), '', 'error');
          }

          $results = Json::decode($content);
          dpm($results);

          static::docGetContents($results);


          //       $results = ['data' => [
          //         ['is_annex_of' => ['url/path/PV-9-2023-06-15']]
          //       ]];
          $results = ['data' => [
            ['is_annex_of' => ['url/path/PV-9-2024-04-24']]
          ]];
          //       $results = ['data' => [
          //         ['is_annex_of' => ['url/path/PV-8-2018-10-23']]
          //       ]];
          $is_annex_of = $file_system->basename($results['data'][0]['is_annex_of'][0]);
          $path = '/api/v2/documents/' . $is_annex_of;
          $filename = 'public://' . $host . '/api/v2/document/' . $is_annex_of;
          // https://data.europarl.europa.eu/api/v2/documents/PV-9-2024-04-25?format=application%2Fld%2Bjson&language=fr
          //       $var['identifier']
          $uri = 'https://' . $host . $path . '?' . UrlHelper::buildQuery($params);

          try {
            $content = static::uriGetContents($filename, $uri);
          }
          catch (\Exception $e) {
            dpm($e->getMessage(), '', 'error');
          }

          $results = Json::decode($content);
          dpm($results);

          static::docGetContents($results);
        }

//     }
  }

  /**
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-15/PV-9-2023-06-15-FNL_fr.xml` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-14/PV-9-2023-06-14-FNL_fr.xml` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-13/PV-9-2023-06-13-FNL_fr.pdf` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-12/PV-9-2023-06-12-FNL_fr.xml` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-01/PV-9-2023-06-01-FNL_fr.xml` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-05-31/PV-9-2023-05-31-FNL_fr.docx` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-05-11/PV-9-2023-05-11-FNL_fr.pdf` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-05-10/PV-9-2023-05-10-FNL_fr.pdf` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-05-09/PV-9-2023-05-09-FNL_fr.xml` resulted in a `404 Not Found` response
   *
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-05-08/PV-9-2023-05-08-FNL_fr.pdf` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-05-08/PV-9-2023-05-08-FNL_fr.docx` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-05-08/PV-9-2023-05-08-FNL_fr.xml` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-15/PV-9-2023-06-15-FNL_fr.xml` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-15/PV-9-2023-06-15-FNL_fr.pdf` resulted in a `404 Not Found` response
   * Client error: `GET https://data.europarl.europa.eu/distribution/PV-9-2023-06-15/PV-9-2023-06-15-FNL_fr.docx` resulted in a `404 Not Found` response
   *
   *
   * Server error: `GET https://data.europarl.europa.eu/api/v2/documents/PV-8-2018-10-24-RCV?format=application/ld%2Bjson&language=fr` resulted in a `500 Internal Server Error` response
   * Server error: `GET https://data.europarl.europa.eu/api/v2/documents/PV-8-2018-10-23-RCV?format=application/ld%2Bjson&language=fr` resulted in a `500 Internal Server Error` response
   * Server error: `GET https://data.europarl.europa.eu/api/v2/documents/PV-9-2024-04-24?format=application/ld%2Bjson&language=fr` resulted in a `500 Internal Server Error` response


   * @param unknown $results
   */
  public static function docGetContents($results) {
    $host = 'data.europarl.europa.eu';

    //   $results['data'][0]['is_realized_by'][0]['title']['fr'];
    //   $results['data'][0]['is_realized_by'][0]['is_embodied_by'];
    // $var['data'][0]['is_realized_by'][0]['is_embodied_by'][0]['is_exemplified_by']
    foreach ($results['data'][0]['is_realized_by'][0]['is_embodied_by'] as $is_embodied_by) {
      //         dpm($is_embodied_by['is_exemplified_by']);
      if (!is_array($is_embodied_by['is_exemplified_by'])) {
        $is_embodied_by['is_exemplified_by'] = [$is_embodied_by['is_exemplified_by']];
      }
      foreach ($is_embodied_by['is_exemplified_by'] as $is_exemplified_by) {
        $path = '/' . $is_exemplified_by; //$is_embodied_by['is_exemplified_by'];
        $uri = 'https://' . $host . $path;
        $filename = 'public://' . $host . $path;
        //         dpm([$path, $uri, $filename]);
        try {
          $content = static::uriGetContents($filename, $uri);
        }
        catch (\Exception $e) {
          _dpm_menu_breadcrumb($e->getMessage(), [], 'error');
        }
      }

//       if (preg_match('/\.xml$/', $filename)) {
//         dpm($filename);
//       }
    }
  }

  public static function uriGetContents($filename, $uri, $force_write = FALSE) {
    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');
    if ($force_write || !file_exists($filename)) {
      $dirname = $file_system->dirname($filename);
      if (!is_dir($dirname)) {
        if (!$file_system->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY)) {
          _dpm_menu_breadcrumb('Directory is misconfigured : @dirname', ['@dirname' => $dirname], 'error');
        }
      }

      _dpm_menu_breadcrumb('Writing @uri to @filename', ['@uri' => $uri, '@filename' => $filename], 'status');
      if ($response = \Drupal::httpClient()->get($uri)) {
        $content = $response->getBody()->getContents();
        file_put_contents($filename, $content);
      }
    }
    else {
      $content = file_get_contents($filename);
    }

    return $content;
  }



  function tocMapSrc($doc_id) {
    $rcv_toc = [];

    $rcv_toc['PV-9-2023-09-12-RCV'] = '1.	Étiquetage des aliments biologiques pour animaux familiers - A9-0159/2023 - Martin Häusling - Accord provisoire - Am 17	..4
2.	Normes de qualité et de sécurité des substances d’origine humaine destinées à une application humaine - A9-0250/2023 - Nathalie Colin-Oesterlé - Amendements de la commission compétente - vote en bloc - Am 59	..6
3.	A9-0250/2023 - Nathalie Colin-Oesterlé - Amendements de la commission compétente - vote en bloc - Am 85	..8
4.	A9-0250/2023 - Nathalie Colin-Oesterlé - Proposition de la Commission	..10
5.	Adhésion à l’acte de Genève de l’arrangement de Lisbonne sur les appellations d’origine et les indications géographiques - A9-0237/2023 - Marion Walsmann - Projet de décision du Conseil	..12
6.	Mise en place de l’instrument visant à renforcer l’industrie européenne de la défense au moyen d’acquisitions conjointes - A9-0161/2023 - Michael Gahler, Zdzisław Krasnodębski - Accord provisoire - Am 2	..14
7.	Polluants des eaux de surface et des eaux souterraines - A9-0238/2023 - Milan Brglez - Proposition de la Commission	..16
8.	Directive sur les énergies renouvelables - A9-0208/2022 - Markus Pieper - Accord provisoire - Am 81	..18
9.	Crédits aux consommateurs - A9-0212/2022 - Kateřina Konečná - Accord provisoire - Am 232	..20
10.	Protection des indications géographiques pour les produits artisanaux et industriels - A9-0049/2023 - Marion Walsmann - Accord provisoire - Am 210	..22
11.	Système des écoles européennes – état des lieux, enjeux et perspectives - A9-0205/2023 - Ilana Cicurel - Proposition de résolution (ensemble du texte)	..24
12.	Objection formulée conformément à l’article 112, paragraphes 2 et 3, du règlement intérieur: maïs génétiquement modifié MON 87419 - B9-0362/2023 - Proposition de résolution (ensemble du texte)	..26
13.	Objection formulée conformément à l’article 112, paragraphes 2 et 3, du règlement intérieur: maïs génétiquement modifié GA21 x T25 - B9-0363/2023 - Proposition de résolution (ensemble du texte)	..28';

    $rcv_toc['PV-9-2023-09-13-RCV'] = '1.
Composition du Parlement européen - A9-0265/2023 - Loránt Vincze, Sandro Gozi - Projet de décision du
Conseil européen ..............................................................................................................................................................6
2.
Reconduction de l’accord de coopération scientifique et technologique UE/USA - A9-0242/2023 - Cristian-Silviu
Buşoi - Projet de décision du Conseil ...............................................................................................................................8
3.
Lignes directrices pour les politiques de l’emploi des États membres - A9-0241/2023 - Dragoş Pîslaru - Projet de
décision du Conseil .........................................................................................................................................................10
4.
Coopération administrative dans le domaine fiscal - Taxation: administrative cooperation - A9-0236/2023 -
Rasmus Andresen - Proposition de la Commission........................................................................................................12
5.
Modifications du règlement intérieur du Parlement en vue de renforcer l\'intégrité, l\'indépendance et la
responsabilité - A9-0262/2023 - Gabriele Bischoff - commission - votes par division et votes séparés - Am 5 .............14
6. A9-0262/2023 - Gabriele Bischoff - commission - votes par division et votes séparés - Am 14 ............................16
7. A9-0262/2023 - Gabriele Bischoff - Article 176, § 4, après le sous-§ 1 - Am 29....................................................18
8. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 2, § 1, point b - Am 8PC1....................................................20
9. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 2, § 1, point c - Am 22 .........................................................22
10. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 2, § 1, point c - Am 8PC2................................................24
11. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 3 - Am 23 ........................................................................26
12. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 3 - Am 9/1 .......................................................................28
13. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 3 - Am 9/2 .......................................................................30
14. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 3 - Am 9/3 .......................................................................32
15. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 3 - Am 9/4 .......................................................................34
16. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 3 - Am 9/5 .......................................................................36
17. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 4, § 2, après le point b - Am 19.......................................38
18. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 4, § 2, point c - Am 30.....................................................40
19. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 4, § 2, point c - Am 10PC1/1...........................................42
20. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 4, § 2, point c - Am 10PC1/2...........................................44
21. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 4, § 5 - Am 10PC2/1 .......................................................46
22. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 4, § 5 - Am 10PC2/2 .......................................................48
23. A9-0262/2023 - Gabriele Bischoff - Annexe I, après l\'article 4 - Am 11.............................................................50
24. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 5 - Am 24 ........................................................................52
25. A9-0262/2023 - Gabriele Bischoff - Annexe I, après l\'article 5 - Am 13/1..........................................................54
26. A9-0262/2023 - Gabriele Bischoff - Annexe I, après l\'article 5 - Am 13/2..........................................................56
27. A9-0262/2023 - Gabriele Bischoff - Annexe I, après l\'article 5 - Am 13/3..........................................................58
28. A9-0262/2023 - Gabriele Bischoff - Annexe I, après l\'article 5 - Am 20.............................................................60
29. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 6 - Am 27 ........................................................................62
30. A9-0262/2023 - Gabriele Bischoff - Titre et § 1 - Am 25PC1 .............................................................................64
31. A9-0262/2023 - Gabriele Bischoff - § 2, sous-§ 1 - Am 35 ................................................................................66
32. A9-0262/2023 - Gabriele Bischoff - § 2, sous-§ 1 - Am 25PC2=16PC1=/1 .......................................................68
33. A9-0262/2023 - Gabriele Bischoff - § 2, sous-§ 1 - Am 25PC2=16PC1=/2 .......................................................70
34. A9-0262/2023 - Gabriele Bischoff - § 2, sub§ 1 - Am 25PC2=16PC1=/3 ..........................................................72
35. A9-0262/2023 - Gabriele Bischoff - § 3 - Am 25PC4=16PC3= ..........................................................................74
36. A9-0262/2023 - Gabriele Bischoff - Après le § 3 - Am 25PC5=16PC4=............................................................76
37. A9-0262/2023 - Gabriele Bischoff - § 4, sous-§§ 1 et 2 - Am 25PC6 ................................................................78
38. A9-0262/2023 - Gabriele Bischoff - § 4, sous-§§ 1 et 2 - Am 16PC5 ................................................................80
39. A9-0262/2023 - Gabriele Bischoff - § 4, sous-§ 3 (contrôle du respect du code) - Am 31PC1 .........................82
40. A9-0262/2023 - Gabriele Bischoff - § 4, sous-§ 3 (contrôle du respect du code) - Am 25PC7 .........................84
41. A9-0262/2023 - Gabriele Bischoff - § 4, sous-§ 3 (contrôle du respect du code) - Am 16PC6/1 ......................86
42. A9-0262/2023 - Gabriele Bischoff - § 4, sous-§ 3 (contrôle du respect du code) - Am 16PC6/2 ......................88
43. A9-0262/2023 - Gabriele Bischoff - §4, sous-§ 4 (violations présumées) - Am 31PC2 .....................................90
44. A9-0262/2023 - Gabriele Bischoff - §4, sous-§ 4 (violations présumées) - Am 25PC8 .....................................92
45. A9-0262/2023 - Gabriele Bischoff - §4, sous-§ 4 (violations présumées) - Am 16PC7 .....................................94
46. A9-0262/2023 - Gabriele Bischoff - § 5 et § 6 - Am 21PC7=25PC9=16PC8=...................................................96
47. A9-0262/2023 - Gabriele Bischoff - Ressources - Am 25PC10 .........................................................................98
48. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 8 - Am 26 ......................................................................100
49. A9-0262/2023 - Gabriele Bischoff - Annexe I, article 8 - Am 17 ......................................................................102
50. A9-0262/2023 - Gabriele Bischoff - Après le § 3 - Am 32 ................................................................................104
51. A9-0262/2023 - Gabriele Bischoff - Proposition de décision ...........................................................................106
52.
Instrument du marché unique pour les situations d’urgence - A9-0246/2023 - Andreas Schwab -
Amendements de la commission compétente - votes séparés - Am 128 .....................................................................108
53. A9-0246/2023 - Andreas Schwab - Amendements de la commission compétente - votes séparés - Am 136 ..110
54. A9-0246/2023 - Andreas Schwab - Amendements de la commission compétente - votes séparés - Am 151 ..112
55. A9-0246/2023 - Andreas Schwab - Article 27 - Am 295S ................................................................................114
56. A9-0246/2023 - Andreas Schwab - Article 27 - Am 233-237 ...........................................................................116
57. A9-0246/2023 - Andreas Schwab - Article 28 - Am 238-242 ...........................................................................118
58. A9-0246/2023 - Andreas Schwab - Article 33 - Am 252-255 ...........................................................................120
59. A9-0246/2023 - Andreas Schwab - Proposition de la Commission .................................................................122
60. La qualité de l’air ambiant et un air pur pour l’Europe - A9-0233/2023 - Javi López - Proposition de rejet - Am 310 ..124
61. A9-0233/2023 - Javi López - Amendements de la commission compétente - votes séparés - Am 53 ............126
62. A9-0233/2023 - Javi López - Amendements de la commission compétente - votes séparés - Am 76 ............128
63. A9-0233/2023 - Javi López - Amendements de la commission compétente - votes séparés - Am 105 ..........130
64. A9-0233/2023 - Javi López - Amendements de la commission compétente - votes séparés - Am 276 ..........132
65. A9-0233/2023 - Javi López - Amendements de la commission compétente - votes séparés - Am 285/1 .......134
66. A9-0233/2023 - Javi López - Amendements de la commission compétente - votes séparés - Am 285/2 .......136
67. A9-0233/2023 - Javi López - Article 1, § 2 - Am 312 .......................................................................................138
68. A9-0233/2023 - Javi López - Article 1, § 2 - Am 340 .......................................................................................140
69. A9-0233/2023 - Javi López - Article 3, § 1 - Am 313=341= .............................................................................142
70. A9-0233/2023 - Javi López - Article 3, § 2, sous § 2 - Am 342........................................................................144
71. A9-0233/2023 - Javi López - Article 18, § 1, introduction - Am 314.................................................................146
72. A9-0233/2023 - Javi López - Article 19, après le § 5 - Am 345 .......................................................................148
73. A9-0233/2023 - Javi López - Article 20, § 2 - Am 346 .....................................................................................150
74. A9-0233/2023 - Javi López - Article 28 - Am 326S ..........................................................................................152
75. A9-0233/2023 - Javi López - Article 28 - Am 173-175 .....................................................................................154
76. A9-0233/2023 - Javi López - Article 29, § 1 - Am 327 .....................................................................................156
77. A9-0233/2023 - Javi López - Article 29, § 2 - Am 328 .....................................................................................158
78. A9-0233/2023 - Javi López - Annexe I, section 1, tableau 1, titre - Am 300=330=..........................................160
79. A9-0233/2023 - Javi López - Annexe I, section 1, tableau 1 - Am 185/1 .........................................................162
80. A9-0233/2023 - Javi López - Annexe I, section 1, tableau 1 - Am 185/2 .........................................................164
81. A9-0233/2023 - Javi López - Annexe I, section 1, tableau 1 - Am 185/3 .........................................................166
82. A9-0233/2023 - Javi López - Annexe I, section 1, tableau 1 - Am 185/4 .........................................................168
83. A9-0233/2023 - Javi López - Annexe I, section 1, tableau 2 - Am 331 ............................................................170
84. A9-0233/2023 - Javi López - Annexe IV, partie C, § 1, point c - Am 351 ........................................................172
85. A9-0233/2023 - Javi López - Considérant 4 - Am 293 .....................................................................................174
86. A9-0233/2023 - Javi López - Après le considérant 8 - Am 311 .......................................................................176
87. A9-0233/2023 - Javi López - Proposition de la Commission ...........................................................................178
88.
Carburants durables pour l’aviation (Initiative «ReFuel EU Aviation») - A9-0199/2022 - José Ramón Bauzá
Díaz - Accord provisoire - Am 137 ................................................................................................................................180
89. Rapport 2022 concernant la Turquie - A9-0247/2023 - Nacho Sánchez Amor - § 4 - Am 11..........................182
90. A9-0247/2023 - Nacho Sánchez Amor - § 7 - Am 12S ....................................................................................184
91. A9-0247/2023 - Nacho Sánchez Amor - § 7 - Am 7.........................................................................................186
92. A9-0247/2023 - Nacho Sánchez Amor - § 7 - Am 13.......................................................................................188
93. A9-0247/2023 - Nacho Sánchez Amor - Après le § 17 - Am 8 ........................................................................190
94. A9-0247/2023 - Nacho Sánchez Amor - § 18 - Am 14=26= ............................................................................192
95. A9-0247/2023 - Nacho Sánchez Amor - § 26 - Am 15=27= ............................................................................194
96. A9-0247/2023 - Nacho Sánchez Amor - Après le § 26 - Am 16 ......................................................................196
97. A9-0247/2023 - Nacho Sánchez Amor - § 28 - Am 3.......................................................................................198
98. A9-0247/2023 - Nacho Sánchez Amor - § 29 - Am 17S ..................................................................................200
99. A9-0247/2023 - Nacho Sánchez Amor - Après le § 32 - Am 4 ........................................................................202
100. A9-0247/2023 - Nacho Sánchez Amor - Après le § 33 - Am 20=24=25= ........................................................204
101. A9-0247/2023 - Nacho Sánchez Amor - § 36 - Am 6.......................................................................................206
102. A9-0247/2023 - Nacho Sánchez Amor - Après le considérant N - Am 1 .........................................................208
103. A9-0247/2023 - Nacho Sánchez Amor - Après le considérant O - Am 2 .........................................................210
104. A9-0247/2023 - Nacho Sánchez Amor - Considérant P - Am 10.....................................................................212
105. A9-0247/2023 - Nacho Sánchez Amor - Proposition de résolution (ensemble du texte).................................214
106.
Relations avec la Biélorussie - A9-0258/2023 - Petras Auštrevičius - Proposition de résolution (ensemble du texte) ..216';

    $rcv_toc['PV-9-2023-09-14-RCV'] = '1. Modification des règlements (UE) 2019/943 et (UE) 2019/942 ainsi que des directives (UE) 2018/2001 et (UE)
2019/944 pour améliorer l\'organisation du marché de l\'électricité de l\'Union - A9-0255/2023 - Nicolás González
Casares - Décision d\'engager des négociations interinstitutionnelles......................................................................4
2. Le cas de Gubad Ibadoghlu, emprisonné en Azerbaïdjan - RC-B9-0369/2023 - Proposition de résolution ............6
3. Situation des droits de l’homme au Bangladesh, en particulier le cas d’Odhikar - RC-B9-0378/2023 - § 4 - Am 3..8
4. Cadre permettant d’assurer un approvisionnement durable et sûr en matières premières critiques - A9-
0260/2023 - Nicola Beer - Article 5, § 1, point c - Am 18 .......................................................................................10
5. A9-0260/2023 - Nicola Beer - Article 7, § 2 - Am 9S=13S= ...................................................................................12
6. A9-0260/2023 - Nicola Beer - Article 18, § 2, partie introductive - Am 10=14=......................................................14
7. A9-0260/2023 - Nicola Beer - Annexe I, section 1, alinéa 1, avant le point a - Am 5.............................................16
8. A9-0260/2023 - Nicola Beer - Annexe III, point 4, alinéa 1, après le sous-point i - Am 11 ....................................18
9. A9-0260/2023 - Nicola Beer - Après le considérant 9 - Am 15 ..............................................................................20
10. A9-0260/2023 - Nicola Beer - Considérant 19 - Am 8=12=....................................................................................22
11. A9-0260/2023 - Nicola Beer - Proposition de la Commission ................................................................................24
12. Modification du mécanisme proposé visant à lever les obstacles juridiques et administratifs dans un contexte
transfrontalier - A9-0252/2023 - Sandro Gozi - Proposition de résolution..............................................................26
13. Réglementation de la prostitution dans l’Union européenne: implications transfrontières et incidence sur l’égalité
entre les hommes et les femmes et les droits des femmes - A9-0240/2023 - Maria Noichl - Proposition de
résolution (ensemble du texte) ...............................................................................................................................28
14. L’avenir du secteur européen du livre - A9-0257/2023 - Tomasz Frankowski - Proposition de résolution ............30
15. Parlementarisme, citoyenneté européenne et démocratie - A9-0249/2023 - Alin Mituța, Niklas Nienass - § 5 .....32
16. A9-0249/2023 - Alin Mituța, Niklas Nienass - § 10/2..............................................................................................34
17. A9-0249/2023 - Alin Mituța, Niklas Nienass - § 16.................................................................................................36
18. A9-0249/2023 - Alin Mituța, Niklas Nienass - § 17/2..............................................................................................38
19. A9-0249/2023 - Alin Mituța, Niklas Nienass - § 31.................................................................................................40
20. A9-0249/2023 - Alin Mituța, Niklas Nienass - Considérant F/2 ..............................................................................42
21. A9-0249/2023 - Alin Mituța, Niklas Nienass - Considérant U.................................................................................44
22. A9-0249/2023 - Alin Mituța, Niklas Nienass - Proposition de résolution (ensemble du texte) ...............................46';

    $rcv_toc['PV-9-2023-10-03-RCV'] = '1. Protection des travailleurs contre l\'amiante - A9-0160/2023 - Véronique Trillet Lenoir - Accord provisoire - Am 69 ..5
2. Coercition économique exercée par des pays tiers - A9-0246/2022 - Bernd Lange - Accord provisoire - Am 78 ... 7
3. Systèmes de transport routier intelligents - A9-0265/2022 - Rovana Plumb - Accord provisoire - Am 63 .............. 9
4. Rapport intérimaire sur la proposition de révision à mi-parcours du cadre financier pluriannuel 2021-2027 - A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 1 - Am 10 ........................................................................ 11
5. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 2 - Am 2 ............................................................ 13
6. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 4 - Am 17 ....................................................................... 15
7. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 4 - Am 18 .......................................................... 17
8. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 10 - Am 11 ........................................................ 19
9. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 15 .................................................................................. 21
10. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 17/2 ........................................................................... 23
11. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 20 - Am 19.................................................... 25
12. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 22 - Am 15.................................................... 27
13. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 25 - Am 4 ................................................................... 29
14. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 28 - Am 20.................................................... 31
15. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 32 - Am 21 ................................................................. 33
16. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 32 - Am 16/1 ................................................. 35
17. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 37/2 ........................................................................... 37
18. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 38 - Am 5 ................................................................... 39
19. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 40 - Am 6 ..................................................... 41
20. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Après le § 40 - Am 7 ..................................................... 43
21. A9-0273/2023 - Jan Olbrycht, Margarida Marques - § 48, point vii - Am 8 ..................................................... 45
22. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Modification 2 - Am 9 .................................................... 47
23. A9-0273/2023 - Jan Olbrycht, Margarida Marques - Proposition de résolution (ensemble du texte) ............... 49
24. Législation européenne sur la liberté des médias - A9-0264/2023 - Sabine Verheyen - Amendements de la
commission compétente - votes séparés - Am 98 ...................................................................................................... 51
25. A9-0264/2023 - Sabine Verheyen - Amendements de la commission compétente - votes séparés - Am 99 ... 53
26. A9-0264/2023 - Sabine Verheyen - Amendements de la commission compétente - votes séparés - Am 109 .. 55
27. A9-0264/2023 - Sabine Verheyen - Amendements de la commission compétente - votes séparés - Am 111 .. 57
28. A9-0264/2023 - Sabine Verheyen - Amendements de la commission compétente - votes séparés - Am 112.. 59
29. A9-0264/2023 - Sabine Verheyen - Amendements de la commission compétente - votes séparés - Am 113.. 61
30. A9-0264/2023 - Sabine Verheyen - Article 2, § 1, point 17 - Am 296S ........................................................... 63
31. A9-0264/2023 - Sabine Verheyen - Article 3, § 1 - Am 306 ............................................................................ 65
32. A9-0264/2023 - Sabine Verheyen - Article 3, § 1 - Am 297/1 ......................................................................... 67
33. A9-0264/2023 - Sabine Verheyen - Article 3, § 1 - Am 297/2 ......................................................................... 69
34. A9-0264/2023 - Sabine Verheyen - Article 3, § 1 - Am 297/3 ......................................................................... 71
35. A9-0264/2023 - Sabine Verheyen - Article 4, § 2, partie introductive - Am 298 ............................................... 73
36. A9-0264/2023 - Sabine Verheyen - Article 4, § 2, point b - Am 108/1 ............................................................. 75
37. A9-0264/2023 - Sabine Verheyen - Article 4, § 2, point b - Am 108/2 ............................................................. 77
38. A9-0264/2023 - Sabine Verheyen - Article 4, § 2, point c - Am 110/1 ............................................................. 79
39. A9-0264/2023 - Sabine Verheyen - Article 4, § 2, point c - Am 110/2 ............................................................. 81
40. A9-0264/2023 - Sabine Verheyen - Article 4, après le § 2 - Am 114/1 ............................................................ 83
41. A9-0264/2023 - Sabine Verheyen - Article 4, après le § 2 - Am 114/2 ............................................................ 85
42. A9-0264/2023 - Sabine Verheyen - Article 4, après le § 2 - Am 115 ............................................................... 87
43. A9-0264/2023 - Sabine Verheyen - Article 4, après le § 2 - Am 116 ............................................................... 89
44. A9-0264/2023 - Sabine Verheyen - Article 4, après le § 2 - Am 301 ............................................................... 91
45. A9-0264/2023 - Sabine Verheyen - Article 6, § 1, point b - Am 308 ................................................................ 93
46. A9-0264/2023 - Sabine Verheyen - Article 6, § 2, après le point b - Am 309 .................................................. 95
47. A9-0264/2023 - Sabine Verheyen - Chapitre III - section 2 - Am 311S ........................................................... 97
48. A9-0264/2023 - Sabine Verheyen - Chapitre III - section 2 - Am 150 ............................................................. 99
49. A9-0264/2023 - Sabine Verheyen - Article 17 - Am 319S rev ....................................................................... 101
50. A9-0264/2023 - Sabine Verheyen - Article 19, titre - Am 320=328= ............................................................. 103
51. A9-0264/2023 - Sabine Verheyen - Article 19, § 1 - Am 321=329= .............................................................. 105
52. A9-0264/2023 - Sabine Verheyen - Après l\'article 22 - Am 330 rev & 331 .................................................... 107
53. A9-0264/2023 - Sabine Verheyen - Après le considérant 1 - Am 303 ........................................................... 109
54. A9-0264/2023 - Sabine Verheyen - Considérant 11 - Am 302 ....................................................................... 111
55. A9-0264/2023 - Sabine Verheyen - Après le considérant 16 - Am 313=322= ................................................ 113
56. A9-0264/2023 - Sabine Verheyen - Après le considérant 16 - Am 314=323= ................................................ 115
57. A9-0264/2023 - Sabine Verheyen - Considérant 18 - Am 304 ...................................................................... 117
58. A9-0264/2023 - Sabine Verheyen - Considérant 31 - Am 315S .................................................................... 119
59. A9-0264/2023 - Sabine Verheyen - Après le considérant 37 - Am 316 ......................................................... 121
60. A9-0264/2023 - Sabine Verheyen - Proposition de la Commission .............................................................. 123
61. Objection conformément à l\'article 112, paragraphes 2 et 3, du règlement: maïs génétiquement modifié MON
89034 × 1507 × MIR162 × NK603 × DAS-40278-9 et neuf sous-combinaisons - B9-0387/2023 - Proposition de
résolution ................................................................................................................................................................ 125
62. Objection conformément à l\'article 112, paragraphes 2 et 3, du règlement: maïs génétiquement modifié
MIR162 - B9-0388/2023 - Proposition de résolution ................................................................................................ 127
63. Des transports européens qui fonctionnent pour les femmes - A9-0239/2023 - Elżbieta Katarzyna
Łukacijewska - Proposition de résolution ................................................................................................................. 129';

    $rcv_toc['PV-9-2023-10-04-RCV'] = '1.	Classification, étiquetage et emballage des substances et des mélanges - A9-0271/2023 - Maria Spyraki - Article premier, alinéa 1, point 4, Règlement (CE) nº 1272/2008, Article 5, § 3 - Am 111S	..4
2.	A9-0271/2023 - Maria Spyraki - Article premier, alinéa 1, point 4, Règlement (CE) nº 1272/2008 Article 5, § 3 - Am 115	..6
3.	A9-0271/2023 - Maria Spyraki - Article 1, alinéa 1, point 6, Règlement (CE) nº 272/2008 Article 9, § 3 - Am 105	..8
4.	A9-0271/2023 - Maria Spyraki - Article 1, alinéa 1, point 12, Règlement (CE) nº 1272/2008 Article 30, § 1 - Am 112	..10
5.	A9-0271/2023 - Maria Spyraki - Article 1, alinéa 1, point 23, Règlement (CE) n° 1272/2008 Article 48, après le § 2 - Am 68	..12
6.	A9-0271/2023 - Maria Spyraki - Article 1, alinéa 1, après le point 29, Règlement (CE) n° 1272/2008, après l\'article 54 - Am 113	..14
7.	A9-0271/2023 - Maria Spyraki - Annexe I, alinéa 1, point 2, Règlement (UE) nº 1272/2008 Annexe I, Partie 1, Section 1.2.1.4., Tableau 1.3 - Am 114	..16
8.	A9-0271/2023 - Maria Spyraki - Considérant 4 - Am 104	..18
9.	A9-0271/2023 - Maria Spyraki - Considérant 10 - Am 110	..20
10.	A9-0271/2023 - Maria Spyraki - Proposition de la Commission	..22
11.	Mobilisation du Fonds de solidarité de l’Union européenne pour venir en aide à la Roumanie, à l\'Italie et à la Turquie - A9-0269/2023 - Katalin Cseh - Proposition de résolution (ensemble du texte)	..24
12.	Ségrégation et discrimination des enfants roms dans l’éducation - B9-0394/2023 - Après le § 20 - Am 13	..26
13.	Relations UE-Suisse - A9-0248/2023 - Lukas Mandl - Proposition de résolution (ensemble du texte)	..28
14.	Ouzbékistan - A9-0227/2023 - Ilhan Kyuchyuk - Proposition de résolution	..30';

    $rcv_toc['PV-9-2023-10-05-RCV'] = '1.	Modification du règlement (UE) 2016/399 concernant un code de l’Union relatif au régime de franchissement des frontières par les personnes - A9-0280/2023 - Sylvie Guillaume - Décision d\'engager des négociations interinstitutionnelles	..4
2.	Situation des droits de l\'homme en Afghanistan, en particulier la persécution d\'anciens responsables du gouvernement - RC-B9-0395/2023 - Proposition de résolution (ensemble du texte)	..6
3.	Le cas de Zarema Moussaïeva en Tchétchénie - RC-B9-0415/2023 - Après le § 4 - Am 1	..8
4.	RC-B9-0415/2023 - Proposition de résolution (ensemble du texte)	..10
5.	Égypte, en particulier la condamnation d\'Hicham Kassem - RC-B9-0396/2023 - Proposition de résolution (ensemble du texte)	..12
6.	Obligations vertes européennes - A9-0156/2022 - Paul Tang - Accord provisoire - Am 2	..14
7.	Schéma de préférences tarifaires généralisées - A9-0267/2023 - Heidi Hautala - Proposition de la Commission	..16
8.	Contrats de services financiers conclus à distance - A9-0097/2023 - Arba Kokalari - Accord provisoire - Am 54	..18
9.	Traitement des eaux urbaines résiduaires - A9-0276/2023 - Nils Torvalds - Amendements de la commission compétente - votes séparés - Am 19/2	..20
10.	A9-0276/2023 - Nils Torvalds - Amendements de la commission compétente - votes séparés - Am 36/2	..22
11.	A9-0276/2023 - Nils Torvalds - Amendements de la commission compétente - votes séparés - Am 92/2	..24
12.	A9-0276/2023 - Nils Torvalds - Amendements de la commission compétente - votes séparés - Am 109	..26
13.	A9-0276/2023 - Nils Torvalds - Amendements de la commission compétente - votes séparés - Am 237/2	..28
14.	A9-0276/2023 - Nils Torvalds - Article 7, après le § 4 - Am 253/1	..30
15.	A9-0276/2023 - Nils Torvalds - Article 8, § 1, sous § 1 - Am 95	..32
16.	A9-0276/2023 - Nils Torvalds - Article 8, § 1, sous § 2 - Am 96	..34
17.	A9-0276/2023 - Nils Torvalds - Article 9, § 1, après l\'alinéa 1 - Am 108	..36
18.	A9-0276/2023 - Nils Torvalds - Article 11, après le § 2 - Am 267	..38
19.	A9-0276/2023 - Nils Torvalds - Considérant 13 - Am 15/2	..40
20.	A9-0276/2023 - Nils Torvalds - Considérant 13 - Am 15/3	..42
21.	A9-0276/2023 - Nils Torvalds - Proposition de la Commission	..44
22.	La situation au Haut-Karabakh après l\'attaque menée par l\'Azerbaïdjan et les menaces continues contre l\'Arménie - RC-B9-0393/2023 - § 10 - Am 3	..46
23.	RC-B9-0393/2023 - § 11 - Am 4	..48
24.	RC-B9-0393/2023 - § 12 - Am 2/1	..50
25.	RC-B9-0393/2023 - § 12 - Am 2/2	..52
26.	RC-B9-0393/2023 - Après le § 19 - Am 1	..54
27.	RC-B9-0393/2023 - Proposition de résolution (ensemble du texte)	..56
28.	Le point sur la progression de la Moldavie sur la voie de l’adhésion à l’Union européenne - RC-B9-0408/2023 - Proposition de résolution	..58';

    $rcv_toc['PV-9-2023-11-09-RCV'] = '1. Règlement sur les données - A9-0031/2023 - Pilar del Castillo Vera - Accord provisoire - Am 27....................... 6
2. Modification de certains règlements en ce qui concerne l’établissement et le fonctionnement du point d’accès
unique européen - A9-0024/2023 - Pedro Silva Pereira - Accord provisoire - Am 2 ................................................. 8
3. Point d’accès unique européen: accès aux informations concernant les services financiers, les marchés des
capitaux et la durabilité - A9-0026/2023 - Pedro Silva Pereira - Accord provisoire - Am 2 ....................................... 10
4. Modification de certaines directives eu égard à l’établissement et le fonctionnement du point d’accès unique
européen - A9-0023/2023 - Pedro Silva Pereira - Accord provisoire - Am 2.......................................................... 12
5. Discipline en matière de règlement, la prestation transfrontalière de services, la coopération en matière de
surveillance, la fourniture de services accessoires de type bancaire et les exigences relatives aux dépositaires
centraux de titres de pays tiers - A9-0047/2023 - Johan Van Overtveldt - Accord provisoire - Am 2 ......................... 14
6. Comptes économiques européens de l\'environnement: nouveaux modules - A9-0296/2023 - Pascal Canfin -
Amendements de la commission compétente - votes séparés - Am 10/1............................................................. 16
7. A9-0296/2023 - Pascal Canfin - Amendements de la commission compétente - votes séparés - Am 10/2......... 18
8. A9-0296/2023 - Pascal Canfin - Amendements de la commission compétente - votes séparés - Am 12/1......... 20
9. A9-0296/2023 - Pascal Canfin - Amendements de la commission compétente - votes séparés - Am 12/2......... 22
10. A9-0296/2023 - Pascal Canfin - Amendements de la commission compétente - votes séparés - Am 12/3 ..... 24
11. A9-0296/2023 - Pascal Canfin - Amendements de la commission compétente - votes séparés - Am 18/1 ..... 26
12. A9-0296/2023 - Pascal Canfin - Amendements de la commission compétente - votes séparés - Am 18/2 ..... 28
13. A9-0296/2023 - Pascal Canfin - Amendements de la commission compétente - votes séparés - Am 20........ 30
14. A9-0296/2023 - Pascal Canfin - Article 1, alinéa 1, point 2, après le sous-point a; Règlement (UE) n°
691/2011; Article 3, après le paragraphe 1 - Am 40 .......................................................................................... 32
15. A9-0296/2023 - Pascal Canfin - Proposition de la Commission .............................................................. 34
16. Mesures de conservation et d’exécution applicables dans la zone de réglementation de l’Organisation des
pêches de l’Atlantique du Nord-Ouest (OPANO) - A9-0279/2023 - Grace O\'Sullivan - Proposition de la Commission et
amendements............................................................................................................................................ 36
17.
Déchets d’équipements électriques et électroniques (DEEE) - A9-0311/2023 - Anna Zalewska - Proposition de
la Commission et amendements................................................................................................................... 38
18.
Accord de partenariat dans le secteur de la pêche durable UE/Madagascar et protocole de mise en oeuvre
(2023-2027) - A9-0299/2023 - Clara Aguilera - Projet de décision du Conseil ...................................................... 40
19.
Réception par type des véhicules à moteur et des moteurs en ce qui concerne leurs émissions et leur
durabilité (Euro 7) - A9-0298/2023 - Alexandr Vondra - Rejet - Am 220 ............................................................... 42
20.
A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 1-9, 11, 13-21, 24-44, 46-47, 49-58, 60-65, 67-69, 71-81, 84-90, 92, 95, 97-98, 104, 108-110, 112-118, 121-136, 138, 141-144, 152, 155-158, 161-165 ................................................................................................................................ 44
21. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 10/1 .. 46
22. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 10/2 .. 48
23. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 12 .... 50
24. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 48 .... 52
25. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 59 .... 54
26. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 70 .... 56
27. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 82 .... 58
28. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 93 .... 60
29. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 96 .... 62
30. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 99 .... 64
31. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 111... 66
32. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 119/1 ..68
33. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 119/2 ..70
34. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 120/1 ..72
35. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 120/2 ..74
36. A9-0298/2023 - Alexandr Vondra - Amendements de la commission compétente - votes séparés - Am 139 .. 76
37. A9-0298/2023 - Alexandr Vondra - Titre - Am 238................................................................................. 78
38. A9-0298/2023 - Alexandr Vondra - Article 3, paragraphe 2, point 38 - Am 45 ............................................ 80
39. A9-0298/2023 - Alexandr Vondra - Article 3, paragraphe 2, point 42 - Am 225 .......................................... 82
40. A9-0298/2023 - Alexandr Vondra - Article 3, paragraphe 2, point 48 - Am 244S ........................................ 84
41. A9-0298/2023 - Alexandr Vondra - Article 3, paragraphe 2, après le point 57 - Am 172=187=209= .............. 86
42. A9-0298/2023 - Alexandr Vondra - Article 3, paragraphe 2, après le point 57 - Am 173=188=210= .............. 88
43. A9-0298/2023 - Alexandr Vondra - Article 3, paragraphe 2, point 67 - Am 174=189=211=........................... 90
44. A9-0298/2023 - Alexandr Vondra - Article 4, paragraphe 2 - Am 240 ....................................................... 92
45. A9-0298/2023 - Alexandr Vondra - Article 4, paragraphe 6, point g - Am 66 .............................................. 94
46. A9-0298/2023 - Alexandr Vondra - Après l\'article 5 - Am 176 =190=212= ................................................. 96
47. A9-0298/2023 - Alexandr Vondra - Article 6, paragraphe 6, point b - Am 83 .............................................. 98
48. A9-0298/2023 - Alexandr Vondra - Article 7, paragraphe 4 - Am 228 ..................................................... 100
49. A9-0298/2023 - Alexandr Vondra - Après l\'article 7 - Am 91/1............................................................... 102
50. A9-0298/2023 - Alexandr Vondra - Après l\'article 7 - Am 91/2............................................................... 104
51. A9-0298/2023 - Alexandr Vondra - Après l\'article 7 - Am 91/3............................................................... 106
52. A9-0298/2023 - Alexandr Vondra - Après l\'article 7 - Am 91/4............................................................... 108
53. A9-0298/2023 - Alexandr Vondra - Article 8, paragraphe 2 - Am 94 ........................................................110
54. A9-0298/2023 - Alexandr Vondra - Article 10, après le paragraphe 3 - Am 194 ........................................112
55. A9-0298/2023 - Alexandr Vondra - Article 10, après le paragraphe 3 - Am 100 ........................................114
56. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 4 - Am 195 ....................................................116
57. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 4 - Am 101 ....................................................118
58. A9-0298/2023 - Alexandr Vondra - Article 10, après le paragraphe 4 - Am 196 ....................................... 120
59. A9-0298/2023 - Alexandr Vondra - Article 10, après le paragraphe 4 - Am 102 ....................................... 122
60. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 5 - Am 197 ................................................... 124
61. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 5 - Am 103 ................................................... 126
62. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 6 - Am 245S ................................................. 128
63. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 6 - Am 178=214=229=.................................... 130
64. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 7 - Am 179=215=230=.................................... 132
65. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 11 - Am 198 .................................................. 134
66. A9-0298/2023 - Alexandr Vondra - Article 10, paragraphe 11 - Am 105 .................................................. 136
67. A9-0298/2023 - Alexandr Vondra - Article 11, paragraphe 2 - Am 199.................................................... 138
68. A9-0298/2023 - Alexandr Vondra - Article 11, paragraphe 2 - Am 106.................................................... 140
69. A9-0298/2023 - Alexandr Vondra - Article 10, après le paragraphe 3 - Am 107 ....................................... 142
70. A9-0298/2023 - Alexandr Vondra - Article 14, paragraphe 4, alinéa 1, point n - Am 246S.......................... 144
71. A9-0298/2023 - Alexandr Vondra - Article 15, paragraphe 2, après le point c - Am 231............................. 146
72. A9-0298/2023 - Alexandr Vondra - Article 15, paragraphe 2, après le point c - Am 137............................. 148
73. A9-0298/2023 - Alexandr Vondra - Article 15, paragraphe 2, après le point e - Am 180=191=213= ............ 150
74. A9-0298/2023 - Alexandr Vondra - Article 15, paragraphe 2, après l\'alinéa 1 - Am 140/1 .......................... 152
75. A9-0298/2023 - Alexandr Vondra - Article 15, paragraphe 2, après l\'alinéa 1 - Am 140/2 .......................... 154
76. A9-0298/2023 - Alexandr Vondra - Article 15, paragraphe 2, après l\'alinéa 1 - Am 140/3 .......................... 156
77. A9-0298/2023 - Alexandr Vondra - Article 19, paragraphe 1 - Am 202 ................................................... 158
78. A9-0298/2023 - Alexandr Vondra - Article 19, paragraphe 1 - Am 182=216=232=.................................... 160
79. A9-0298/2023 - Alexandr Vondra - Article 19, paragraphe 1 - Am 146 ................................................... 162
80. A9-0298/2023 - Alexandr Vondra - Article 19, paragraphe 2 - Am 203 ................................................... 164
81. A9-0298/2023 - Alexandr Vondra - Article 19, paragraphe 2 - Am 183=217=233=.................................... 166
82. A9-0298/2023 - Alexandr Vondra - Article 19, paragraphe 2 - Am 147 ................................................... 168
83. A9-0298/2023 - Alexandr Vondra - Article 20, paragraphe 2 - Am 204 ................................................... 170
84. A9-0298/2023 - Alexandr Vondra - Article 20, paragraphe 2 - Am 148 ................................................... 172
85. A9-0298/2023 - Alexandr Vondra - Article 20, paragraphe 3 - Am 184=219=234PC2= ............................. 174
86. A9-0298/2023 - Alexandr Vondra - Article 20, paragraphe 3 - Am 149 ................................................... 176
87. A9-0298/2023 - Alexandr Vondra - Annexe I, tableau 1, ligne 3 - Am 150 ............................................... 178
88. A9-0298/2023 - Alexandr Vondra - Annexe I, tableau 2 - Am 236 .......................................................... 180
89. A9-0298/2023 - Alexandr Vondra - Annexe I, tableau 2 - Am 205 .......................................................... 182
90. A9-0298/2023 - Alexandr Vondra - Annexe I, tableau 2 - Am 151 .......................................................... 184
91. A9-0298/2023 - Alexandr Vondra - Annexe I, après le tableau 4 - Am 237 .............................................. 186
92. A9-0298/2023 - Alexandr Vondra - Annexe I, après le tableau 4 - Am 153 .............................................. 188
93. A9-0298/2023 - Alexandr Vondra - Annexe I, après le tableau 4 - Am 154 .............................................. 190
94. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 1 - Am 159/1...................................................... 192
95. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 1 - Am 159/2...................................................... 194
96. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 1 - Am 159/3...................................................... 196
97. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 1 - Am 159/4...................................................... 198
98. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 1 - Am 159/5...................................................... 200
99. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 1 - Am 159/6...................................................... 202
100. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 1 - Am 159/7...................................................... 204
101. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 2 - Am 206 ........................................................ 206
102. A9-0298/2023 - Alexandr Vondra - Annexe III, tableau 2 - Am 160 ........................................................ 208
103. A9-0298/2023 - Alexandr Vondra - Considérant 1 - Am 221 ................................................................. 210
104. A9-0298/2023 - Alexandr Vondra - Après le considérant 7 - Am 166...................................................... 212
105. A9-0298/2023 - Alexandr Vondra - Considérant 18 - Am 22S ............................................................... 214
106. A9-0298/2023 - Alexandr Vondra - Après le considérant 18 - Am 168 .................................................... 216
107. A9-0298/2023 - Alexandr Vondra - Après le considérant 18 - Am 186=208= ........................................... 218
108. A9-0298/2023 - Alexandr Vondra - Considérant 19 - Am 23 ................................................................. 220
109. A9-0298/2023 - Alexandr Vondra - Proposition de la Commission......................................................... 222
110.
Systèmes des ressources propres de l\'Union - A9-0295/2023 - José Manuel Fernandes, Valérie Hayer - Article
1, alinéa 1, point 1, sous-point b; Décision (UE, Euratom) 2020/2053; Article 2, § 1, après le point f - Am 9 ........... 224
111. A9-0295/2023 - José Manuel Fernandes, Valérie Hayer - Considérant 6 - Am 7...................................... 226
112. A9-0295/2023 - José Manuel Fernandes, Valérie Hayer - Considérant 9 - Am 8...................................... 228
113. A9-0295/2023 - José Manuel Fernandes, Valérie Hayer - Proposition de la Commission.......................... 230
114.
Renforcer le droit à la participation: légitimité et résilience des processus électoraux dans les systèmes
politiques illibéraux et les régimes autoritaires - A9-0323/2023 - Nacho Sánchez Amor - Proposition de résolution .. 232
115. Efficacité des sanctions de l\'UE à l’encontre de la Russie - RC-B9-0453/2023 - Après le § 8 - Am 2 .......... 234
116. RC-B9-0453/2023 - Après le § 8 - Am 3 ............................................................................................ 236
117. RC-B9-0453/2023 - Après le § 18 - Am 4 .......................................................................................... 238
118. RC-B9-0453/2023 - Après le considérant L - Am 1 ............................................................................. 240';

    $rcv_toc['PV-9-2023-11-21-RCV'] = '1. Mesures destinées à assurer un niveau élevé commun de cybersécurité dans les institutions, organes et
organismes de l’Union - A9-0064/2023 - Henna Virkkunen - Accord provisoire - Am 2 ..................................... 7
2. Programme de documentation des captures de thon rouge - A9-0172/2021 - Gabriel Mato - Accord provisoire -
Am 11 ................................................................................................................................................. 9
3. Règles communes visant à promouvoir la réparation des biens - A9-0316/2023 - René Repasi - Amendements
de la commission compétente - vote séparé - Am 1.................................................................................. 11
4. A9-0316/2023 - René Repasi - Proposition de la Commission ................................................................... 13
5. Cadre de mesures en vue de renforcer l’écosystème européen de la fabrication de produits de technologie «zéro
net» (règlement pour une industrie «zéro net») - A9-0343/2023 - Christian Ehler - Article 6, § 9 bis - Am 1PC ... 15
6. A9-0343/2023 - Christian Ehler - Article 13 bis, § 4, point b - Am 1PC ......................................................... 17
7. A9-0343/2023 - Christian Ehler - Article 1, § 2, après le point a - Am 10 ...................................................... 19
8. A9-0343/2023 - Christian Ehler - Après l\'article 3 - Am 1PC/2 .................................................................... 21
9. A9-0343/2023 - Christian Ehler - Article 10, § 1, partie introductive - Am 15 ................................................. 23
10. A9-0343/2023 - Christian Ehler - Article 10, § 1, point b après le point iv - Am 33.......................................... 25
11. A9-0343/2023 - Christian Ehler - Article 10, § 2, après le point b - Am 17..................................................... 27
12. A9-0343/2023 - Christian Ehler - Article 10, après le § 4 - Am 18................................................................ 29
13. A9-0343/2023 - Christian Ehler - Article 16, § 1 - Am 21............................................................................ 31
14. A9-0343/2023 - Christian Ehler - Article 19, § 2, point d - Am 26................................................................. 33
15. A9-0343/2023 - Christian Ehler - Après le considérant 44 - Am 30 .............................................................. 35
16. A9-0343/2023 - Christian Ehler - Après le considérant 44 - Am 31 .............................................................. 37
17. A9-0343/2023 - Christian Ehler - Proposition de la Commission ................................................................. 39
18. Cadre de certification de l’Union relatif aux absorptions de carbone -
A9-0329/2023 - Lídia Pereira - Après l\'annexe I - Am 155 ......................................................................... 41
19. A9-0329/2023 - Lídia Pereira - Proposition de la Commission.................................................................... 43
20. Renforcement des normes de performance en matière d’émissions de CO2 pour les véhicules utilitaires lourds
neufs - A9-0313/2023 - Bas Eickhout - Rejet - Am 97 ............................................................................... 45
21. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 41................ 47
22. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 44................ 49
23. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 49................ 51
24. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 50................ 53
25. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 51................ 55
26. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 52................ 57
27. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 53................ 59
28. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 54................ 61
29. A9-0313/2023 - Bas Eickhout - Amendements de la commission compétente - vote séparé - Am 57................ 63
30. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 3, sous-point g Règlement (UE) 2019/1242 Article 3, alinéa 1, point 11, sous-point a - Am 37 .................................................................................................. 65
31. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 3, sous-point i Règlement (UE) 2019/1242 Article 3, après le point 23 - Am 87=135= ............................................................................................................ 67
32. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 3, sous-point i Règlement (UE) 2019/1242 Article 3, après le point 23 - Am 136=88=102= ..................................................................................................... 69
33. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 3, sous-point i Règlement (UE) 2019/1242 Article 3, après le point 23 - Am 127 ................................................................................................................... 71
34. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 4 Règlement (UE) 2019/1242 Article 3 bis, § 1, point c -
Am 42 ............................................................................................................................................... 73
35. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 4 Règlement (UE) 2019/1242 Article 3 ter, § 1 -
Am 106............................................................................................................................................. 75
36. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 4 Règlement (UE) 2019/1242 Article 3 ter, § 1 - Am 89. 77
37. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 5 Règlement (UE) 2019/1242 Article 4, alinéa 1, point a -
Am 128 ............................................................................................................................................. 79
38. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 5 Règlement (UE) 2019/1242 Article 4, alinéa 1, après le
point b - Am 90=107=129=137= ............................................................................................................ 81
39. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 5 Règlement (UE) 2019/1242 Article 4, alinéa 1, après le
point b - Am 91 ................................................................................................................................... 83
40. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, après le point 5 Règlement (UE) 2019/1242 Après l\'article 4 -
Am 92=108=138= ............................................................................................................................... 85
41. A9-0313/2023 - Bas Eickhout - Article 1, alinéa 1, point 18 Règlement (UE) 2019/1242 Article 15, § 1 - Am 63 .. 87
42. A9-0313/2023 - Bas Eickhout - Annexe 1, sous-point 2.1 - Am 95=118=130=139=........................................ 89
43. A9-0313/2023 - Bas Eickhout - Annexe 1, sous-point 2.4 - Am 132............................................................. 91
44. A9-0313/2023 - Bas Eickhout - Annexe 1, point 4, sous-point 4.3, sous-point 4.3.1 - Am 80 ........................... 93
45. A9-0313/2023 - Bas Eickhout - Annexe 1, point 4, sous-point 4.3, sous-point 4.3.1 - Am 93 ........................... 95
46. A9-0313/2023 - Bas Eickhout - Annexe 1, point 4, sous-point 4.3, sous-point 4.3.2 - Am 117 .......................... 97
47. A9-0313/2023 - Bas Eickhout - Annexe 1, point 4, sous-point 4.3, sous-point 4.3.2 - Am 94 ........................... 99
48. A9-0313/2023 - Bas Eickhout - Annexe 1, après le § 6 - Am 96=119=131=140= ......................................... 101
49. A9-0313/2023 - Bas Eickhout - Après le considérant 15 - Am 85=124=133= .............................................. 103
50. A9-0313/2023 - Bas Eickhout - Après le considérant 25 - Am 86 .............................................................. 105
51. A9-0313/2023 - Bas Eickhout - Proposition de la Commission ................................................................. 107
52. Possibilités d’amélioration de la fiabilité des audits et des contrôles réalisés par les autorités nationales dans le cadre de la gestion partagée - A9-0297/2023 - Monika Hohlmeier - Proposition de résolution ....................... 109
53. Cadre de l’Union pour la situation sociale et professionnelle des artistes et des travailleurs des secteurs de la
culture et de la création - A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 34/1 ....................... 111
54. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 34/2 ..........................................................113
55. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 34/3 ..........................................................115
56. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 49/1 ..........................................................117
57. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 49/2 ..........................................................119
58. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 51 ............................................................ 121
59. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 52 - Am 1 .................................................. 123
60. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 55 - Am 2 .................................................. 125
61. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 63/1 ......................................................... 127
62. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 63/2 ......................................................... 129
63. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 67/1 ......................................................... 131
64. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - § 67/2 ......................................................... 133
65. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - Annexe, Recommendation 3, § 2, tiret 1 - Am 3 .. 135
66. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - Annexe, Recommendation 3, § 2, tiret 2 ........... 137
67. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - Annexe, Recommendation 3, § 2, tiret 10 - Am 4 ..139
68. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - Annex, le reste ............................................. 141
69. A9-0304/2023 - Antonius Manders, Domènec Ruiz Devesa - Proposition de résolution (ensemble du texte) ... 143
70. Mise en œuvre du principe de la primauté du droit de l’Union européenne -
A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le § 1 - Am 2 ............................................................. 145
71. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le § 1 - Am 3 ............................................................. 147
72. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le § 1 - Am 12 ........................................................... 149
73. A9-0341/2023 - Yana Toom, Cyrus Engerer - § 4 ................................................................................... 151
74. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le § 5 - Am 13 ........................................................... 153
75. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le § 5 - Am 14 ........................................................... 155
76. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le § 8 - Am 16 ........................................................... 157
77. A9-0341/2023 - Yana Toom, Cyrus Engerer - § 18 ................................................................................. 159
78. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le § 18 - Am 7 ........................................................... 161
79. A9-0341/2023 - Yana Toom, Cyrus Engerer - § 24 - Am 19...................................................................... 163
80. A9-0341/2023 - Yana Toom, Cyrus Engerer - § 24 ................................................................................. 165
81. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le considérant A - Am 9............................................... 167
82. A9-0341/2023 - Yana Toom, Cyrus Engerer - Après le considérant D - Am 1 .............................................. 169
83. A9-0341/2023 - Yana Toom, Cyrus Engerer - Considérant E/1 ................................................................. 171
84. A9-0341/2023 - Yana Toom, Cyrus Engerer - Considérant E/2 ................................................................. 173
85. A9-0341/2023 - Yana Toom, Cyrus Engerer - Considérant J/1 ................................................................. 175
86. A9-0341/2023 - Yana Toom, Cyrus Engerer - Considérant J/2 ................................................................. 177
87. A9-0341/2023 - Yana Toom, Cyrus Engerer - Proposition de résolution (ensemble du texte) ........................ 179
88. Conférence des Nations unies sur le changement climatique 2023, Dubaï, Émirats arabes unis (COP28) - B9-0458/2023 - Après le § 2 - Am 15 ................................................................................................... 181
89. B9-0458/2023 - § 7/1......................................................................................................................... 183
90. B9-0458/2023 - § 7/2......................................................................................................................... 185
91. B9-0458/2023 - § 7/3......................................................................................................................... 187
92. B9-0458/2023 - Après le § 7 - Am 7 ..................................................................................................... 189
93. B9-0458/2023 - After §17 - Am 17/1..................................................................................................... 191
94. B9-0458/2023 - After §17 - Am 17/2..................................................................................................... 193
95. B9-0458/2023 - § 24/2 ....................................................................................................................... 195
96. B9-0458/2023 - Après le § 24 - Am 19.................................................................................................. 197
97. B9-0458/2023 - Après le § 31 - Am 4 ................................................................................................... 199
98. B9-0458/2023 - Après le § 31 - Am 18.................................................................................................. 201
99. B9-0458/2023 - § 36 - Am 1 ............................................................................................................... 203
100. B9-0458/2023 - § 36/1 ....................................................................................................................... 205
101. B9-0458/2023 - § 36/2 ....................................................................................................................... 207
102. B9-0458/2023 - § 36/3 ....................................................................................................................... 209
103. B9-0458/2023 - § 37 - Am 10...............................................................................................................211
104. B9-0458/2023 - § 47/2 ....................................................................................................................... 213
105. B9-0458/2023 - § 47/3 ....................................................................................................................... 215
106. B9-0458/2023 - § 48/3 ....................................................................................................................... 217
107. B9-0458/2023 - § 49 - Am 2 ............................................................................................................... 219
108. B9-0458/2023 - § 49/1 ....................................................................................................................... 221
109. B9-0458/2023 - § 49/2 ....................................................................................................................... 223
110. B9-0458/2023 - § 49/3 ....................................................................................................................... 225
111. B9-0458/2023 - § 49/4 ....................................................................................................................... 227
112. B9-0458/2023 - § 49/5 ....................................................................................................................... 229
113. B9-0458/2023 - § 49/6 ....................................................................................................................... 231
114. B9-0458/2023 - § 49/7 ....................................................................................................................... 233
115. B9-0458/2023 - § 49/8 ....................................................................................................................... 235
116. B9-0458/2023 - § 75 - Am 11S ............................................................................................................ 237
117. B9-0458/2023 - § 75 - Am 3 ............................................................................................................... 239
118. B9-0458/2023 - § 78 - Am 12S............................................................................................................ 241
119. B9-0458/2023 - § 79 - Am 13S............................................................................................................ 243
120. B9-0458/2023 - Après le § 92 - Am 8 ................................................................................................... 245
121. B9-0458/2023 - Après le § 94 - Am 14.................................................................................................. 247
122. B9-0458/2023 - Après le § 102 - Am 9.................................................................................................. 249
123. B9-0458/2023 - Après le considérant B - Am 5 ...................................................................................... 251
124. B9-0458/2023 - Après le considérant B - Am 6 ...................................................................................... 253
125. B9-0458/2023 - Proposition de résolution (ensemble du texte)................................................................. 255
126. Réduire les inégalités et promouvoir l’inclusion sociale en temps de crise pour les enfants et leurs familles -
A9-0360/2023 - Sandra Pereira - Après le § 7 - Am 1 ............................................................................. 257
127. A9-0360/2023 - Sandra Pereira - Proposition de résolution (ensemble du texte) ......................................... 259
128. Les enfants d’abord – Renforcer la garantie pour l’enfance deux ans après son adoption -
B9-0462/2023 - § 1/2......................................................................................................................... 261
129. B9-0462/2023 - § 18/1 ....................................................................................................................... 263
130. B9-0462/2023 - § 22/1 ....................................................................................................................... 265
131. B9-0462/2023 - § 22/2 ....................................................................................................................... 267
132. B9-0462/2023 - § 22/3 ....................................................................................................................... 269
133. B9-0462/2023 - § 22/4 ....................................................................................................................... 271
134. B9-0462/2023 - § 22/5 ....................................................................................................................... 273
135. B9-0462/2023 - § 22/6 ....................................................................................................................... 275
136. B9-0462/2023 - § 22/7 ....................................................................................................................... 277
137. B9-0462/2023 - Après le § 23 - Am 1 ................................................................................................... 279
138. B9-0462/2023 - § 32/2 ....................................................................................................................... 281
139. B9-0462/2023 - Considérant C/2 ......................................................................................................... 283
140. Mise en œuvre du programme "Corps européen de solidarité" 2021-2027 - A9-0308/2023 - Michaela Šojdrová -
Proposition de résolution ................................................................................................................... 285
141. Mise en oeuvre du règlement instituant des mesures de reconstitution du stock d’anguilles européennes -
A9-0353/2023 - Bert-Jan Ruissen - § 13 - Am 3..................................................................................... 287
142. A9-0353/2023 - Bert-Jan Ruissen - Proposition de résolution (ensemble du texte) ...................................... 289';

    $rcv_toc['PV-9-2023-11-22-RCV'] = '1. Projet de budget rectificatif nº 4/2023 – Réduction des crédits de paiement, autres ajustements et actualisations
techniques - A9- 0363/2023 - Fabienne Keller - Proposition de résolution ...................................................... 12
2. Projet commun de budget général de l’Union européenne pour l’exercice 2024 - A9-0362/2023 - Siegfried
Mureşan, Nils Ušakovs - Projet commun .................................................................................................... 14
3. Mobilisation du Fonds européen d’ajustement à la mondialisation en faveur des travailleurs licenciés - demande
EGF/2023/002 BE/Makro - Belgique - A9-0351/2023 - Petri Sarvamaa - Proposition de décision ...................... 16
4. Publication électronique du Journal officiel de l’Union européenne - A9-0352/2023 - Adrián Vázquez Lázara -
Projet de règlement du Conseil ................................................................................................................. 18
5. Conclusion d\'un accord entre l’Union européenne et le Monténégro sur les activités opérationnelles menées par
l’Agence européenne de garde-frontières et de garde-côtes sur le territoire du Monténégro - A9-0369/2023 - Lena
Düpont - Projet de décision du Conseil ...................................................................................................... 20
6. Accord de libre-échange entre l\'Union européenne et la Nouvelle-Zélande - A9-0305/2023 - Daniel Caspary -
Projet de décision du Conseil .................................................................................................................... 22
7. Accord de libre-échange entre l’Union européenne et la Nouvelle-Zélande (résolution) - A9-0314/2023 - Daniel
Caspary - Proposition de résolution ........................................................................................................... 24
8. TVA: les règles à l’ère du numérique - A9-0327/2023 - Olivier Chastel - Proposition de la Commission ............. 26
9. TVA: accords de coopération administrative à l’ère du numérique - A9-0324/2023 - Olivier Chastel - Proposition
de la Commission .................................................................................................................................... 28
10. TVA: assujettis, régimes particuliers concernant les ventes à distance de biens importés et la déclaration et le
paiement de la TVA à l\'importation - A9-0320/2023 - Olivier Chastel - Proposition de la Commission ................ 30
11. Utilisation des produits phytopharmaceutiques compatible avec le développement durable - A9-0339/2023 - Sarah
Wiener - Rejet ......................................................................................................................................... 32
12. A9-0339/2023 - Sarah Wiener - Objectifs de réduction - vote en bloc - Am 91, 94, 96-99, 113, 129, 134-135, 164, 188, 363-365, 369, ............................................................................................................................................. 34
13. A9-0339/2023 - Sarah Wiener - Objectifs de réduction - vote en bloc - Am 685, 464-465, 467-469, 475-477, 479, 490, 492, 494, 496- ............................................................................................................................................ 36
14. A9-0339/2023 - Sarah Wiener - Période de réference - vote en bloc - Am 475, 495, 615-616 .............................. 38
15. A9-0339/2023 - Sarah Wiener - Lignes directrices - vote en bloc - Am 463, 470-472, 474, 525, 533-535, 540-541, 544, 548-549, 552- ............................................................................................................................................. 40
16. A9-0339/2023 - Sarah Wiener - Registre - vote en bloc - Am 441, 443-44, 446, 448 ............................................ 42
17. A9-0339/2023 - Sarah Wiener - Am 93 ............................................................................................................... 44
18. A9-0339/2023 - Sarah Wiener - Am 108/1 ........................................................................................................... 46
19. A9-0339/2023 - Sarah Wiener - Am 108/2 ........................................................................................................... 48
20. A9-0339/2023 - Sarah Wiener - Am 109/2 ........................................................................................................... 50
21. A9-0339/2023 - Sarah Wiener - Am 113/1 ........................................................................................................... 52
22. A9-0339/2023 - Sarah Wiener - Am 113/2 ........................................................................................................... 54
23. A9-0339/2023 - Sarah Wiener - Am 117 .............................................................................................................. 56
24. A9-0339/2023 - Sarah Wiener - Am 247 .............................................................................................................. 58
25. A9-0339/2023 - Sarah Wiener - Am 376/2 ........................................................................................................... 60
26. A9-0339/2023 - Sarah Wiener - Am 638 .............................................................................................................. 62
27. A9-0339/2023 - Sarah Wiener - Am 622S ........................................................................................................... 64
28. A9-0339/2023 - Sarah Wiener - Am 459 .............................................................................................................. 66
29. A9-0339/2023 - Sarah Wiener - Am 482 .............................................................................................................. 68
30. A9-0339/2023 - Sarah Wiener - Am 491 .............................................................................................................. 70
31. A9-0339/2023 - Sarah Wiener - Am 462 .............................................................................................................. 72
32. A9-0339/2023 - Sarah Wiener - Am 641 .............................................................................................................. 74
33. A9-0339/2023 - Sarah Wiener - Am 501=686PC= ............................................................................................... 76
34. A9-0339/2023 - Sarah Wiener - Am 503S=686PC= ............................................................................................. 78
35. A9-0339/2023 - Sarah Wiener - Am 505S=686PC S= ......................................................................................... 80
36. A9-0339/2023 - Sarah Wiener - Am 510S=686PC S= ......................................................................................... 82
37. A9-0339/2023 - Sarah Wiener - Am 512S=686PC S= ......................................................................................... 84
38. A9-0339/2023 - Sarah Wiener - Article 5, § 10 - Am 686PC................................................................................. 86
39. A9-0339/2023 - Sarah Wiener - Am 642 .............................................................................................................. 88
40. A9-0339/2023 - Sarah Wiener - Am 435S ........................................................................................................... 90
41. A9-0339/2023 - Sarah Wiener - Am 545 .............................................................................................................. 92
42. A9-0339/2023 - Sarah Wiener - Am 557S ........................................................................................................... 94
43. A9-0339/2023 - Sarah Wiener - Am 455 .............................................................................................................. 96
44. A9-0339/2023 - Sarah Wiener - Am 437=559= .................................................................................................... 98
45. A9-0339/2023 - Sarah Wiener - Am 438 ............................................................................................................ 100
46. A9-0339/2023 - Sarah Wiener - Am 461 ............................................................................................................ 102
47. A9-0339/2023 - Sarah Wiener - Am 630 ............................................................................................................ 104
48. A9-0339/2023 - Sarah Wiener - Am 584S ......................................................................................................... 106
49. A9-0339/2023 - Sarah Wiener - Am 591 ............................................................................................................ 108
50. A9-0339/2023 - Sarah Wiener - Am 442 ............................................................................................................ 110
51. A9-0339/2023 - Sarah Wiener - Am 445 ............................................................................................................ 112
52. A9-0339/2023 - Sarah Wiener - Am 600 ............................................................................................................ 114
53. A9-0339/2023 - Sarah Wiener - Am 609S ......................................................................................................... 116
54. A9-0339/2023 - Sarah Wiener - Am 648 ............................................................................................................ 118
55. A9-0339/2023 - Sarah Wiener - Am 660 ............................................................................................................ 120
56. A9-0339/2023 - Sarah Wiener - Am 456/1 ......................................................................................................... 122
57. A9-0339/2023 - Sarah Wiener - Am 456/2 ......................................................................................................... 124
58. A9-0339/2023 - Sarah Wiener - Am 454 ............................................................................................................ 126
59. A9-0339/2023 - Sarah Wiener - Am 661 ............................................................................................................ 128
60. A9-0339/2023 - Sarah Wiener - Proposition de la Commission .......................................................................... 130
61. Emballages et déchets d\'emballages - A9-0319/2023 - Frédérique Ries - Rejet - Am 401 .................................. 132
62. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 94 .... 134
63. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 133 .. 136
64. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 134 .. 138
65. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 136 .. 140
66. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 137 .. 142
67. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 305/1 ..144
68. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 305/2 ..146
69. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 306 .. 148
70. A9-0319/2023 - Frédérique Ries - Amendements de la commission compétente - votes par division - Am 307 .. 150
71. A9-0319/2023 - Frédérique Ries - Article 3, § 1, après le point 32 - Am 414/1 ................................................... 152
72. A9-0319/2023 - Frédérique Ries - Article 3, § 1, après le point 32 - Am 414/2 ................................................... 154
73. A9-0319/2023 - Frédérique Ries - Article 6, § 2, alinéa 1, point d - Am 415........................................................ 156
74. A9-0319/2023 - Frédérique Ries - Article 6, § 10, après le point a - Am 454 ...................................................... 158
75. A9-0319/2023 - Frédérique Ries - Article 7, après le § 4 - Am 502 ..................................................................... 160
76. A9-0319/2023 - Frédérique Ries - Article 8, après le § 5 - Am 487 ..................................................................... 162
77. A9-0319/2023 - Frédérique Ries - Article 9, § 2 - Am 503 .................................................................................. 164
78. A9-0319/2023 - Frédérique Ries - Article 9, § 4, alinéa 1, après le point a - Am 405 .......................................... 166
79. A9-0319/2023 - Frédérique Ries - Article 21, § 1 - Am 439 ................................................................................ 168
80. A9-0319/2023 - Frédérique Ries - Article 22 - Am 406S .................................................................................... 170
81. A9-0319/2023 - Frédérique Ries - Article 22, titre - Am 361 ............................................................................... 172
82. A9-0319/2023 - Frédérique Ries - Article 22, § 1 - Am 437=499=/1 ................................................................... 174
83. A9-0319/2023 - Frédérique Ries - Article 22, § 1 - Am 437=499=/2 ................................................................... 176
84. A9-0319/2023 - Frédérique Ries - Article 22, § 1 - Am 437=499=/3 ................................................................... 178
85. A9-0319/2023 - Frédérique Ries - Article 22, § 1 - Am 437=499=/4 ................................................................... 180
86. A9-0319/2023 - Frédérique Ries - Article 22, § 2 - Am 445/1 ............................................................................. 182
87. A9-0319/2023 - Frédérique Ries - Article 22, § 2 - Am 445/2 ............................................................................. 184
88. A9-0319/2023 - Frédérique Ries - Article 22, après le § 4 - Am 465 ................................................................... 186
89. A9-0319/2023 - Frédérique Ries - Article 26 - Am 407S .................................................................................... 188
90. A9-0319/2023 - Frédérique Ries - Article 26, après le § 3 - Am 448 ................................................................... 190
91. A9-0319/2023 - Frédérique Ries - Article 26, après le § 3 - Am 201/1 ................................................................ 192
92. A9-0319/2023 - Frédérique Ries - Article 26, après le § 3 - Am 201/2 ................................................................ 194
93. A9-0319/2023 - Frédérique Ries - Article 26, après le § 3 - Am 201/3 ................................................................ 196
94. A9-0319/2023 - Frédérique Ries - Article 26, après le § 3 - Am 201/4 ................................................................ 198
95. A9-0319/2023 - Frédérique Ries - Article 26, après le § 3 - Am 201/5 ................................................................ 200
96. A9-0319/2023 - Frédérique Ries - Article 26, après le § 3 - Am 467 ................................................................... 202
97. A9-0319/2023 - Frédérique Ries - Article 26, après le § 6 - Am 396/1 ................................................................ 204
98. A9-0319/2023 - Frédérique Ries - Article 26, après le § 6 - Am 396/2 ................................................................ 206
99. A9-0319/2023 - Frédérique Ries - Article 26, § 7, partie introductive - Am 205 ................................................... 208
100. A9-0319/2023 - Frédérique Ries - Article 26, § 7, partie introductive - Am 468 ................................................... 210
101. A9-0319/2023 - Frédérique Ries - Article 26, § 12, alinéa 2 - Am 469 ................................................................ 212
102. A9-0319/2023 - Frédérique Ries - Article 26, § 13, alinéa 2 - Am 470 ................................................................ 214
103. A9-0319/2023 - Frédérique Ries - Article 26, après le § 13 - Am 504 ................................................................. 216
104. A9-0319/2023 - Frédérique Ries - Article 26, après le § 13 - Am 505 ................................................................. 218
105. A9-0319/2023 - Frédérique Ries - Article 26, § 14, après le point b - Am 494 ..................................................... 220
106. A9-0319/2023 - Frédérique Ries - Article 26, après le § 15 - Am 386/1 .............................................................. 222
107. A9-0319/2023 - Frédérique Ries - Article 26, après le § 15 - Am 386/2 .............................................................. 224
108. A9-0319/2023 - Frédérique Ries - Article 26, après le § 15 - Am 506 ................................................................. 226
109. A9-0319/2023 - Frédérique Ries - Article 26, après le § 15 - Am 507 ................................................................. 228
110. A9-0319/2023 - Frédérique Ries - Article 38, § 1 - Am 428S .............................................................................. 230
111. A9-0319/2023 - Frédérique Ries - Article 44, § 1, partie introductive - Am 409=429=426= ................................. 232
112. A9-0319/2023 - Frédérique Ries - Article 44, § 1, partie introductive - Am 455 ................................................... 234
113. A9-0319/2023 - Frédérique Ries - Article 44, § 2, point a - Am 508 .................................................................... 236
114. A9-0319/2023 - Frédérique Ries - Article 44, après le § 4 - Am 433 ................................................................... 238
115. A9-0319/2023 - Frédérique Ries - Article 63, § 1 - Am 509 ................................................................................ 240
116. A9-0319/2023 - Frédérique Ries - Annexe I, § 6 - Am 510 ................................................................................. 242
117. A9-0319/2023 - Frédérique Ries - Annexe I, § 15 - Am 511 ............................................................................... 244
118. A9-0319/2023 - Frédérique Ries - Annexe I, après la sous-rubrique 3 - Am 410 ................................................ 246
119. A9-0319/2023 - Frédérique Ries - Annexe I, après la sous-rubrique 3 - Am 411 ................................................ 248
120. A9-0319/2023 - Frédérique Ries - Annexe II, après le tableau 2 - Am 474 ......................................................... 250
121. A9-0319/2023 - Frédérique Ries - Annexe V - Am 413S .................................................................................... 252
122. A9-0319/2023 - Frédérique Ries - Annexe V, ligne 2 - Am 391PC1S=512S= ..................................................... 254
123. A9-0319/2023 - Frédérique Ries - Annexe V, ligne 3 - Am 391PC2S=513S= ..................................................... 256
124. A9-0319/2023 - Frédérique Ries - Après le considérant 1 - Am 402 ................................................................... 258
125. A9-0319/2023 - Frédérique Ries - Après le considérant 12 - Am 343 ................................................................. 260
126. A9-0319/2023 - Frédérique Ries - Après le considérant 12 - Am 344 ................................................................. 262
127. A9-0319/2023 - Frédérique Ries - Après le considérant 110 - Am 356 ............................................................... 264
128. A9-0319/2023 - Frédérique Ries - Proposition de la Commission ...................................................................... 266
129. Transition numérique et droit administratif - A9-0309/2023 - Karen Melchior - Proposition de résolution (ensemble
du texte) ........................................................................................................................................................... 268
130. Propositions du Parlement européen pour la révision des traités - A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements de la commission compétente - votes séparés -
Am 2 ................................................................................................................................................................ 270
131. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 7 ...................................................................................... 272
132. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 10..................................................................................... 274
133. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 12..................................................................................... 276
134. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 20..................................................................................... 278
135. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 23..................................................................................... 280
136. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 26..................................................................................... 282
137. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 32..................................................................................... 284
138. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 33..................................................................................... 286
139. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 34..................................................................................... 288
140. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 35..................................................................................... 290
141. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 36..................................................................................... 292
142. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 47..................................................................................... 294
143. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 49/1 .................................................................................. 296
144. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 49/2 .................................................................................. 298
145. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 53..................................................................................... 300
146. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 55..................................................................................... 302
147. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 57..................................................................................... 304
148. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 60..................................................................................... 306
149. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 62..................................................................................... 308
150. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 67..................................................................................... 310
151. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 68..................................................................................... 312
152. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 70..................................................................................... 314
153. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 72..................................................................................... 316
154. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 74..................................................................................... 318
155. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 76..................................................................................... 320
156. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 77..................................................................................... 322
157. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 82..................................................................................... 324
158. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 83..................................................................................... 326
159. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 85..................................................................................... 328
160. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 86..................................................................................... 330
161. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 87..................................................................................... 332
162. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 88..................................................................................... 334
163. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 89..................................................................................... 336
164. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 90..................................................................................... 338
165. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 101 ................................................................................... 340
166. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 102 ................................................................................... 342
167. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 105/1 ................................................................................ 344
168. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 105/2 ................................................................................ 346
169. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 109 ................................................................................... 348
170. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 112 ................................................................................... 350
171. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 113 ................................................................................... 352
172. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 115 ................................................................................... 354
173. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 116/1 ................................................................................ 356
174. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 116/2 ................................................................................ 358
175. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 117 ................................................................................... 360
176. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 121 ................................................................................... 362
177. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 122 ................................................................................... 364
178. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 127 ................................................................................... 366
179. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 128 ................................................................................... 368
180. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 130 ................................................................................... 370
181. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 178 ................................................................................... 372
182. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 185 ................................................................................... 374
183. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 186 ................................................................................... 376
184. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 193 ................................................................................... 378
185. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 194 ................................................................................... 380
186. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 204 ................................................................................... 382
187. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 210 ................................................................................... 384
188. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 213 ................................................................................... 386
189. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 226 ................................................................................... 388
190. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 253 ................................................................................... 390
191. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Amendements
de la commission compétente - votes séparés - Am 262 ................................................................................... 392
192. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 3, § 3, après l\'alinéa 1 TUE - Am 16 ............................................................................................................................ 394
193. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 3, § 3, après l\'alinéa 1 TUE - Am 17 ............................................................................................................................ 396
194. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 21, § 2, point a TUE - Am 72 ......................................................................................................................................... 398
195. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 21, § 2, point a TUE - Am 52 ......................................................................................................................................... 400
196. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 42, § 1
TUE - Am 73 .................................................................................................................................................... 402
197. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 42, § 1
TUE - Am 59 .................................................................................................................................................... 404
198. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 42, après
le § 1 TUE - Am 23 ........................................................................................................................................... 406
199. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 42, § 3
TUE - Am 74 .................................................................................................................................................... 408
200. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 42, § 3
TUE - Am 61 .................................................................................................................................................... 410
201. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 42, § 4
bis, après l\'alinéa 2 TUE - Am 75 ...................................................................................................................... 412
202. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 42, § 4
bis, après l\'alinéa 2 TUE - Am 64 ...................................................................................................................... 414
203. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après l\'article 49
TUE - Am 30 .................................................................................................................................................... 416
204. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 4, § 2, point e TFUE - Am 18=59= ............................................................................................................................... 418
205. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 8 TFUE -
Am 71=19= ...................................................................................................................................................... 420
206. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 10 TFUE
- Am 68=20= .................................................................................................................................................... 422
207. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 13 TFUE
- Am 25 ............................................................................................................................................................ 424
208. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 19, § 1
TFUE - Am 69=21= .......................................................................................................................................... 426
209. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 39, § 1
TFUE - Am 26 .................................................................................................................................................. 428
210. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 88, après
le § 1 TFUE - Am 126 ....................................................................................................................................... 430
211. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 88, après
le § 1 TFUE - Am 76 ......................................................................................................................................... 432
212. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 126, § 2, alinéa 2 TFUE - Am 62 ..................................................................................................................................... 434
213. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 126, § 2, alinéa 2 TFUE - Am 63 ..................................................................................................................................... 436
214. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 168, § 4, après le point c TFUE - Am 24=60= .................................................................................................................. 438
215. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 3 CDFUE
- Am 27 ............................................................................................................................................................ 440
216. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Article 21, § 1
CDFUE - Am 70=28= ....................................................................................................................................... 442
217. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Annexe
(ensembe du texte)........................................................................................................................................... 444
218. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 1 - Am 29 . ..446
219. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 1 -
Am 35 .............................................................................................................................................................. 448
220. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 1 -
Am 36 .............................................................................................................................................................. 450
221. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 3 - Am 49 . ..452
222. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 3/1 ........... 454
223. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 3/2 ........... 456
224. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 4 - Am 50 . ..458
225. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 4 .............. 460
226. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 4 -
Am 37 .............................................................................................................................................................. 462
227. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 4 -
Am 38 .............................................................................................................................................................. 464
228. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 5 .............. 466
229. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 5 -
Am 39 .............................................................................................................................................................. 468
230. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 6/1 ........... 470
231. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 6/2 ........... 472
232. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 11 - Am 40 ..474
233. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 11/1 ......... 476
234. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 11/2 ......... 478
235. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 11 -
Am 51 ..480
236. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 13 ............ 482
237. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 14 - Am 53 ..484
238. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 14 ............ 486
239. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 17 - Am 54 ..488
240. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 17 ............ 490
241. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 17 -
Am 41 .............................................................................................................................................................. 492
242. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 20 - Am 55 ..494
243. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 20/1 ......... 496
244. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 20/2 ......... 498
245. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 21 - Am 13 ..500
246. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 21 - Am 77 ..502
247. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 21 -
Am 42 .............................................................................................................................................................. 504
248. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 23/1 ......... 506
249. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 23/2 ......... 508
250. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 23 -
Am 43 .............................................................................................................................................................. 510
251. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 25 -
Am 4 ................................................................................................................................................................ 512
252. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 25 -
Am 5 ................................................................................................................................................................ 514
253. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 26 - Am 7 . ..516
254. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 27 ............ 518
255. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 27 -
Am 44 .............................................................................................................................................................. 520
256. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 28 -
Am 45 .............................................................................................................................................................. 522
257. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 29 ............ 524
258. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 29 -
Am 8 ................................................................................................................................................................ 526
259. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 30 -
Am 67=9= ........................................................................................................................................................ 528
260. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 30/2 ......... 530
261. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 33 -
Am 56 .............................................................................................................................................................. 532
262. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 33 -
Am 46 .............................................................................................................................................................. 534
263. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 33/1 ......... 536
264. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 33/2 ......... 538
265. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 34 - Am 78 ..540
266. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 35/1 ......... 542
267. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 35/2 ......... 544
268. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 36 -
Am 11 .............................................................................................................................................................. 546
269. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - § 36 -
Am 57 .............................................................................................................................................................. 548
270. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 41 -
Am 58 .............................................................................................................................................................. 550
271. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le § 43 -
Am 12 .............................................................................................................................................................. 552
272. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Visa 2 ......... 554
273. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le
considérant A - Am 33 ...................................................................................................................................... 556
274. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Considérant B -
Am 47 .............................................................................................................................................................. 558
275. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Après le
considérant B - Am 34 ...................................................................................................................................... 560
276. A9-0337/2023 - Guy Verhofstadt, Sven Simon, Gabriele Bischoff, Daniel Freund, Helmut Scholz - Proposition de
résolution (ensemble du texte).......................................................................................................................... 562
277. Négociations concernant un accord sur le statut des activités opérationnelles menées par Frontex en Mauritanie -
A9-0358/2023 ................................................................................................................................................... 564
277. A9-0358/2023 - a. Tineke Strik - § 1/3 ............................................................................................................................................... 564
278. A9-0358/2023 - Tineke Strik - Après le § 1 - Am 1 ............................................................................................. 566
279. A9-0358/2023 - Tineke Strik - Après le § 1 - Am 2 ............................................................................................. 568
280. A9-0358/2023 - Tineke Strik - § 6.1, point c....................................................................................................... 570
281. A9-0358/2023 - Tineke Strik - § 6.2, point d/1.................................................................................................... 572
282. A9-0358/2023 - Tineke Strik - § 6.2, point d/2.................................................................................................... 574
283. A9-0358/2023 - Tineke Strik - Considérant H .................................................................................................... 576
284. A9-0358/2023 - Tineke Strik - Proposition de recommandation .......................................................................... 578';

    $rcv_toc['PV-9-2023-12-12-RCV'] = '1.	Aliments destinés à la consommation humaine: modification de certaines des directives "petit-déjeuner" -  A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 12/1	..5
2.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 12/2	..7
3.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 16/1	..9
4.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 16/2	..11
5.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 33/1	..13
6.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 33/2	..15
7.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 33/3	..17
8.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 39/1	..19
9.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 39/2	..21
10.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 39/3	..23
11.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 39/4	..25
12.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 39/5	..27
13.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 39/6	..29
14.	A9-0385/2023 - Alexander Bernhuber - Amendements de la commission compétente - votes séparés - Am 39/7	..31
15.	A9-0385/2023 - Alexander Bernhuber - Article 1, alinéa 1, après le point 1 Directive 2001/110/CE Article 2, alinéa 2, point 2, après le sous-point b - Am 21/3	..33
16.	A9-0385/2023 - Alexander Bernhuber - Article 1, après l\'alinéa 1 Directive 2001/110/CE Article 4, alinéa 1 - Am 55	..35
17.	A9-0385/2023 - Alexander Bernhuber - Article 2,§ 1, point 1, point c Directive 2001/112/EC Article 3, § 4 - Am 53=57=64=	..37
18.	A9-0385/2023 - Alexander Bernhuber - Article 2,§ 1, point 1, point c Directive 2001/112/EC Article 3, § 4 - Am 34	..39
19.	A9-0385/2023 - Alexander Bernhuber - Annexe I, alinéa 1, point 1, sous-point i, tiret 2 Directive 2001/112/CE Annex I, part 2, point 2, tiret 5 - Am 54=65=66=	..41
20.	A9-0385/2023 - Alexander Bernhuber - Annexe I, alinéa 1, point 1, sous-point b ii, tiret 2 Directive 2001/112/CE Annexe I, partie II, point 3, tiret 13 bis - Am 51	..43
21.	A9-0385/2023 - Alexander Bernhuber - Après le considérant 8 - Am 52	..45
22.	A9-0385/2023 - Alexander Bernhuber - Proposition de la Commission	..47
23.	Redevances et droits dus à l’Agence européenne des médicaments - A9-0224/2023 - Cristian-Silviu Buşoi - Accord provisoire - Am 64	..49
24.	Reconnaissance des qualifications professionnelles des infirmiers responsables de soins généraux formés en Roumanie - A9-0381/2023 - Adam Bielan - Proposition de la Commission	..51
25.	Autorisation octroyée à la France de négocier un accord bilatéral avec l’Algérie sur des questions liées à la coopération judiciaire en matière civile et commerciale - A9-0356/2023 - Ilana Cicurel - Proposition de la Commission	..53
26.	Autorisation octroyée à la France de négocier un accord bilatéral avec l’Algérie sur des questions liées à la coopération judiciaire en matière de droit de la famille - A9-0355/2023 - Ilana Cicurel - Proposition de la Commission au Conseil	..55
27.	Accord de partenariat dans le secteur de la pêche CE/Kiribati (2023-2028). Protocole de mise en œuvre - A9-0380/2023 - João Pimenta Lopes - Projet de décision du Conseil	..57
28.	A9-0370/2023 - Henna Virkkunen - Article 1, alinéa 1, point 3, sous-point b Règlement (CE) No 561/2006 Article 8, § 6 bis, partie introductive et point a - Am 29S	..59
29.	A9-0370/2023 - Henna Virkkunen - Considérant 7 - Am 25S	..61
30.	A9-0370/2023 - Henna Virkkunen - Considérant 10 - Am 27S	..63
31.	A9-0370/2023 - Henna Virkkunen - Proposition de la Commission	..65
32.	Cadre permettant d’assurer un approvisionnement durable et sûr en matières premières critiques - A9-0260/2023 - Nicola Beer - Accord provisoire - Am 26	..67
33.	Les élections européennes 2024 - A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - § 28/1	..69
34.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - § 28/2	..71
35.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - § 28/3	..73
36.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - § 28/4	..75
37.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - Considérant B/1	..77
38.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - Considérant B/2	..79
39.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - Considérant C/1	..81
40.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - Considérant C/2	..83
41.	A9-0332/2023 - Domènec Ruiz Devesa, Sven Simon - Proposition de résolution (ensemble du texte)	..85
42.	Petits réacteurs modulaires - A9-0408/2023 - Franc Bogovič - Proposition de résolution	..87
43.	Santé mentale - A9-0367/2023 - Sara Cerdas - Proposition de résolution	..89
44.	Mise en œuvre de l\'instrument de voisinage, de coopération au développement et de coopération internationale - A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 3 - Am 6	..91
45.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 6 - Am 8	..93
46.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 6 - Am 19	..95
47.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 7 - Am 9	..97
48.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 12 - Am 10	..99
49.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 14 - Am 16	..101
50.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 14 - Am 20	..103
51.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - Après le § 14 - Am 1	..105
52.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - Après le § 14 - Am 11	..107
53.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - Après le § 14 - Am 21	..109
54.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - Après le § 33 - Am 2	..111
55.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 37 - Am 22	..113
56.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - § 38 - Am 3	..115
57.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - Considérant J - Am 18	..117
58.	A9-0374/2023 - Tomas Tobé, Pedro Marques, Charles Goerens, Michael Gahler - Proposition de résolution (ensemble du texte)	..119
59.	Conception addictive des services en ligne et protection des consommateurs sur le marché unique de l’UE - A9-0340/2023 - Kim Van Sparrentak - Proposition de résolution	..121
60.	Le rôle de la politique fiscale en temps de crise - A9-0336/2023 - Kira Marie Peter-Hansen - § 19 - Am 5	..123
61.	A9-0336/2023 - Kira Marie Peter-Hansen - § 19 - Am 1	..125
62.	A9-0336/2023 - Kira Marie Peter-Hansen - Après le § 19 - Am 2	..127
63.	A9-0336/2023 - Kira Marie Peter-Hansen - § 22 - Am 6	..129
64.	A9-0336/2023 - Kira Marie Peter-Hansen - § 22 - Am 3	..131
65.	A9-0336/2023 - Kira Marie Peter-Hansen - Après le § 25 - Am 4	..133
66.	A9-0336/2023 - Kira Marie Peter-Hansen - Proposition de résolution (ensemble du texte)	..135
67.	Poursuite de la réforme des règles d’imposition des sociétés - Further reform of corporate taxation rules - Eine weitere Reform der Vorschriften zur Unternehmensbesteuerung - A9-0359/2023 - Isabel Benjumea Benjumea - Proposition de résolution	..137
68.	Redéfinition du futur cadre des fonds structurels de l’Union visant à soutenir les régions particulièrement touchées par les défis liés aux transitions écologique, numérique et du secteur automobile - A9-0326/2023 - Susana Solís Pérez - Proposition de résolution	..139';

    $rcv_toc['PV-9-2023-12-13-RCV'] = '1.	Espace européen des données de santé - A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Amendements de la commission compétente - votes séparés - Am 16/2	..5
2.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Amendements de la commission compétente - votes séparés - Am 34	..7
3.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Amendements de la commission compétente - votes séparés - Am 122/2	..9
4.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Article 3, après le § 9 - Am 552	..11
5.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Après l\'article 3 - Am 557	..13
6.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Article 7, après le § 1 - Am 555	..15
7.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Article 33, § 5 - Am 553	..17
8.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Article 34, § 1, point e - Am 554	..19
9.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Après l\'article 60 - Am 551	..21
10.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Article 70, § 2 - Am 545	..23
11.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Article 70, après le § 3 - Am 546	..25
12.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Après le considérant 41 - Am 556	..27
13.	A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Proposition de la Commission	..29
14.	Objection à un acte délégué conformément à l’article 111, paragraphe 3: Ajustement des critères de taille pour les micro-, petites, moyennes et grandes entreprises ou pour les groupes - B9-0493/2023 - Proposition de résolution	..31
15.	Relations UE-Japon - A9-0373/2023 - Reinhard Bütikofer - § 4 - Am 1	..33
16.	A9-0373/2023 - Reinhard Bütikofer - Proposition de résolution (ensemble du texte)	..35
17.	La situation des enfants privés de liberté dans le monde - A9-0371/2023 - María Soraya Rodríguez Ramos - Proposition de résolution (ensemble du texte)	..37
18.	Rôle de la politique de développement de l\'UE dans la transformation des industries extractives pour le développement durable dans les pays en développement - A9-0322/2023 - Barry Andrews - § 32	..39
19.	A9-0322/2023 - Barry Andrews - Proposition de résolution (ensemble du texte)	..41
20.	La coopération au développement de l’Union européenne pour améliorer l’accès à l’éducation et à la formation dans les pays en développement - A9-0338/2023 - György Hölvényi - Proposition de résolution	..43
21.	Maladies non transmissibles - A9-0366/2023 - Erik Poulsen - Proposition de résolution	..45
22.	Relations UE-États-Unis - A9-0372/2023 - Tonino Picula - § 1, après le point a - Am 28	..47
23.	A9-0372/2023 - Tonino Picula - § 1, après le point i - Am 1/1	..49
24.	A9-0372/2023 - Tonino Picula - § 1, après le point i - Am 1/2	..51
25.	A9-0372/2023 - Tonino Picula - § 1, après le point i - Am 1/3	..53
26.	A9-0372/2023 - Tonino Picula - § 1, après le point i - Am 37	..55
27.	A9-0372/2023 - Tonino Picula - § 1, point m/2	..57
28.	A9-0372/2023 - Tonino Picula - § 1, point t - Am 16	..59
29.	A9-0372/2023 - Tonino Picula - § 1, point ca/2	..61
30.	A9-0372/2023 - Tonino Picula - Après le § 1 - Am 31	..63
31.	A9-0372/2023 - Tonino Picula - Après le § 1 - Am 34	..65
32.	A9-0372/2023 - Tonino Picula - Après le § 1 - Am 35	..67
33.	A9-0372/2023 - Tonino Picula - Après le § 1 - Am 36	..69
34.	A9-0372/2023 - Tonino Picula - Considérant D - Am 2	..71
35.	A9-0372/2023 - Tonino Picula - Après le considérant D - Am 3	..73
36.	A9-0372/2023 - Tonino Picula - Après le considérant D - Am 4	..75
37.	A9-0372/2023 - Tonino Picula - Considérant F - Am 7	..77
38.	A9-0372/2023 - Tonino Picula - Après le considérant S - Am 9	..79
39.	A9-0372/2023 - Tonino Picula - Considérant U - Am 11	..81
40.	A9-0372/2023 - Tonino Picula - Projet de recommendation	..83
41.	Relations UE-Chine - EU-China relations - Beziehungen zwischen der EU und China - A9-0375/2023 - Hilde Vautmans - § 1, après le point a - Am 3	..85
42.	A9-0375/2023 - Hilde Vautmans - § 1, sous-titre 3 - Am 4	..87
43.	A9-0375/2023 - Hilde Vautmans - § 1, après le point af - Am 7	..89
44.	A9-0375/2023 - Hilde Vautmans - § 1, après le point ak - Am 8	..91
45.	A9-0375/2023 - Hilde Vautmans - Après le § 1 - Am 11	..93
46.	A9-0375/2023 - Hilde Vautmans - Projet de recommendation	..95
47.	Mise en œuvre des dispositions du traité relatives aux procédures législatives spéciales - Implementation of the Treaty provisions on special legislative procedures - Anwendung der Vertragsbestimmungen zu besonderen Gesetzgebungsverfahren - A9-0384/2023 - Victor Negrescu - Après le § 21 - Am 2	..97
48.	A9-0384/2023 - Victor Negrescu - Proposition de résolution (ensemble du texte)	..99
49.	30 ans des critères de Copenhague - donner un nouvel élan à la politique d\'élargissement de l\'Union européenne - RC-B9-0500/2023 - Après le § 1 - Am 4	..101
50.	RC-B9-0500/2023 - § 8/2	..103
51.	RC-B9-0500/2023 - § 17 - Am 7	..105
52.	RC-B9-0500/2023 - § 17/2	..107
53.	RC-B9-0500/2023 - § 17/3	..109
54.	RC-B9-0500/2023 - § 17/4	..111
55.	RC-B9-0500/2023 - § 17/5	..113
56.	RC-B9-0500/2023 - Considérant A	..115
57.	RC-B9-0500/2023 - Considérant H - Am 6	..117
58.	RC-B9-0500/2023 - Proposition de résolution (ensemble du texte)	..119
59.	Mise en œuvre du règlement de 2018 relatif au blocage géographique dans le marché unique numérique - A9-0335/2023 - Beata Mazurek - § 21/1	..121
60.	A9-0335/2023 - Beata Mazurek - § 21/2	..123
61.	A9-0335/2023 - Beata Mazurek - § 23 - Am 4	..125
62.	A9-0335/2023 - Beata Mazurek - § 23 - Am 7	..127
63.	A9-0335/2023 - Beata Mazurek - § 24/1	..129
64.	A9-0335/2023 - Beata Mazurek - § 24/2	..131
65.	A9-0335/2023 - Beata Mazurek - § 24/3	..133
66.	A9-0335/2023 - Beata Mazurek - § 24/4	..135
67.	A9-0335/2023 - Beata Mazurek - § 25/1	..137
68.	A9-0335/2023 - Beata Mazurek - § 25/2	..139
69.	A9-0335/2023 - Beata Mazurek - § 25/3	..141
70.	A9-0335/2023 - Beata Mazurek - § 25/4	..143
71.	A9-0335/2023 - Beata Mazurek - Après le considérant H - Am 3	..145
72.	A9-0335/2023 - Beata Mazurek - Après le considérant H - Am 6	..147
73.	A9-0335/2023 - Beata Mazurek - Proposition de résolution (ensemble du texte)	..149';

    $rcv_toc['PV-9-2023-12-14-RCV'] = '1.	Objection conformément à l’article 112, paragraphes 2 et 3 ainsi que 4, point c): limites maximales applicables aux résidus de fipronil - B9-0488/2023 - Proposition de résolution (ensemble du texte)	..5
2.	Objection conformément à l’article 112, paragraphes 2 et 3 ainsi que 4, point c): limites maximales applicables aux résidus de tricyclazole - B9-0494/2023 - Proposition de résolution (ensemble du texte)	..7
3.	Objection conformément à l’article 112, paragraphes 2 et 3: maïs génétiquement modifié Bt11 × MIR162 × MIR604 × MON 89034 × 5307 × GA21 et trente sous-combinaisons - B9-0492/2023 - Proposition de résolution	..9
4.	Objection conformément à l’article 112, paragraphes 2 et 3: colza génétiquement modifié Ms8, Rf3 et Ms8 × Rf3 - B9-0490/2023 - Proposition de résolution	..11
5.	Absence d\'informations sur la situation de Mikalaï Statkevitch et récentes attaques contre des membres de la famille de personnalités politiques et de militants biélorusses - RC-B9-0509/2023 - Proposition de résolution	..13
6.	Les communautés massaï en Tanzanie - RC-B9-0511/2023 - Proposition de résolution (ensemble du texte)	..15
7.	Enlèvement d\'enfants tibétains et pratiques d\'assimilation forcée dans des internats chinois au Tibet - RC-B9-0510/2023 - § 1	..17
8.	RC-B9-0510/2023 - § 2/1	..19
9.	RC-B9-0510/2023 - Proposition de résolution (ensemble du texte)	..21
10.	Accroître l’innovation et la compétitivité industrielle et technologique grâce à un environnement favorable aux jeunes pousses et aux entreprises en expansion - A9-0383/2023 - Tsvetelina Penkova - Proposition de résolution	..23
11.	Compétence, loi applicable, reconnaissance des décisions et acceptation d\'actes authentiques en matière de parentalité et concernant la création d\'un certificat européen de parentalité - A9-0368/2023 - Maria-Manuel Leitão-Marques - Rejet - Am 84	..25
12.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 2/2	..27
13.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 8	..29
14.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 19	..31
15.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 21	..33
16.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 39	..35
17.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 44	..37
18.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 50	..39
19.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Amendements de la commission compétente - votes séparés - Am 54	..41
20.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 1, après le § 1 - Am 91	..43
21.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 3, § 2, après le point e - Am 92	..45
22.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 3, après le § 3 - Am 111	..47
23.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 3, après le § 3 - Am 112	..49
24.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 3, après le § 3 - Am 113	..51
25.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 4, alinéa 1, après le point 1 - Am 115	..53
26.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 4, alinéa 1, après le point 1 - Am 93	..55
27.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 17, § 1 - Am 118	..57
28.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Article 39, alinéa 1, après le point d - Am 142	..59
29.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Après l\'article 41 - Am 97	..61
30.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Chapitre VI - Am 99S=125-132&134-137=	..63
31.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Refus de reconnaissance du certificat - Am 100	..65
32.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Après le considérant 1 - Am 104	..67
33.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Après le considérant 8 - Am 85	..69
34.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Considérant 20 - Am 87	..71
35.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Considérant 24 - Am 106	..73
36.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Après le considérant 24 - Am 88	..75
37.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Après le considérant 27 - Am 107	..77
38.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Considérant 75 - Am 89	..79
39.	A9-0368/2023 - Maria-Manuel Leitão-Marques - Proposition de la Commission	..81
40.	Frontex: construire sur la base de l’enquête du groupe de travail LIBE sur le contrôle de Frontex - B9-0499/2023 - § 13/2	..83
41.	B9-0499/2023 - § 13/6	..85
42.	B9-0499/2023 - Après le § 17 - Am 12	..87
43.	B9-0499/2023 - Après le considérant H - Am 2	..89
44.	B9-0499/2023 - Considérant Q - Am 11	..91
45.	B9-0499/2023 - Proposition de résolution (ensemble du texte)	..93
46.	Banque européenne de l\'hydrogène - A9-0379/2023 - Robert Hajšel - § 3/2	..95
47.	A9-0379/2023 - Robert Hajšel - Considérant K/2	..97
48.	A9-0379/2023 - Robert Hajšel - Considérant K/3	..99
49.	A9-0379/2023 - Robert Hajšel - Proposition de résolution (ensemble du texte)	..101
50.	Tentative de coup d\'État au Guatemala - RC-B9-0526/2023 - § 5	..103
51.	RC-B9-0526/2023 - Proposition de résolution (ensemble du texte)	..105';

    $rcv_toc['PV-9-2024-01-16-RCV'] = '1. A9-0050/2023 - Jessica Polfjärd - Accord provisoire - Am 74...................................................................................4
2. A9-0048/2023 - Bas Eickhout - Accord provisoire - Am 189 .....................................................................................6
3. A9-0039/2023 - Danuta Maria Hübner - Accord provisoire - Am 2............................................................................8
4. A9-0040/2023 - Danuta Maria Hübner - Accord provisoire - Am 2..........................................................................10
5. A9-0001/2024 - Magdalena Adamowicz - Proposition de la Commission...............................................................12
6. A9-0387/2023 - Luděk Niedermayer - Chapitre II - titre - Am 29.............................................................................14
7. A9-0387/2023 - Luděk Niedermayer - Article 4 - Am 30S .......................................................................................16
8. A9-0387/2023 - Luděk Niedermayer - Proposition de la Commission ....................................................................18
9. A9-0413/2023 - Milan Zver - Après le § 52 - Am 1..................................................................................................20
10. A9-0413/2023 - Milan Zver - § 117..........................................................................................................................22
11. A9-0413/2023 - Milan Zver - Après le § 117 - Am 2................................................................................................24
12. A9-0413/2023 - Milan Zver - Proposition de résolution (ensemble du texte) ..........................................................26
13. A9-0425/2023 - Massimiliano Smeriglio - Proposition de résolution .......................................................................28
14. A9-0392/2023 - Łukasz Kohut - Proposition de résolution ......................................................................................30
15. A9-0420/2023 - Marcos Ros Sempere - Proposition de résolution .........................................................................32
16. A9-0427/2023 - Stéphanie Yon-Courtin - § 6 - Am 2 ..............................................................................................34
17. A9-0427/2023 - Stéphanie Yon-Courtin - Après le § 7 - Am 3 ................................................................................36
18. A9-0427/2023 - Stéphanie Yon-Courtin - Après le § 14 - Am 4 ..............................................................................38
19. A9-0427/2023 - Stéphanie Yon-Courtin - § 25 - Am 5 ............................................................................................40
20. A9-0427/2023 - Stéphanie Yon-Courtin - § 50/1 .....................................................................................................42
21. A9-0427/2023 - Stéphanie Yon-Courtin - § 50/2 .....................................................................................................44
22. A9-0427/2023 - Stéphanie Yon-Courtin - § 50/3 .....................................................................................................46
23. A9-0427/2023 - Stéphanie Yon-Courtin - Proposition de résolution (ensemble du texte).......................................48
24. A9-0431/2023 - Ivars Ijabs - § 9 - Am 2 ..................................................................................................................50
25. A9-0431/2023 - Ivars Ijabs - Après le § 12 - Am 3 ..................................................................................................52
26. A9-0431/2023 - Ivars Ijabs - § 25 - Am 4 ................................................................................................................54
27. A9-0431/2023 - Ivars Ijabs - Considérant H - Am 1 ................................................................................................56
28. A9-0431/2023 - Ivars Ijabs - Proposition de résolution (ensemble du texte)...........................................................58
29. A9-0405/2023 - Jordi Solé - Proposition de résolution............................................................................................60
30. A9-0438/2023 - Erik Bergkvist - § 11/2 ...................................................................................................................62
31. A9-0438/2023 - Erik Bergkvist - Proposition de résolution (ensemble du texte) .....................................................64
32. A9-0415/2023 - Daniel Buda - Proposition de résolution ........................................................................................66';

    $rcv_toc['PV-9-2024-01-17-RCV'] = '1. A9-0439/2023 - Esther de Lange, Margarida Marques .............................................................................................5
2. B9-0057/2024 - Proposition de résolution .................................................................................................................7
3. A9-0428/2023 - Andrzej Halicki - Projet de décision du Conseil ...............................................................................9
4. A9-0099/2023 - Biljana Borzan - Accord provisoire - Am 91...................................................................................11
5. A9-0002/2024 - Marlene Mortler - Après le considérant 4 - Am 16.........................................................................13
6. A9-0002/2024 - Marlene Mortler - Proposition de la Commission...........................................................................15
7. A9-0388/2023 - Ibán García Del Blanco - Proposition de résolution.......................................................................17
8. B9-0062/2024 - Proposition de résolution ...............................................................................................................19
9. A9-0393/2023 - Christian Ehler - Après le § 14 - Am 1 ...........................................................................................21
10. A9-0393/2023 - Christian Ehler - Annexe I, après le point 23 - Am 2 .....................................................................23
11. A9-0393/2023 - Christian Ehler - Proposition de résolution (ensemble du texte) ...................................................25
12. A9-0429/2023 - Paulo Rangel - § 10.......................................................................................................................27
13. A9-0429/2023 - Paulo Rangel - § 17 - Am 2 ...........................................................................................................29
14. A9-0429/2023 - Paulo Rangel - Après le § 17 - Am 3 .............................................................................................31
15. A9-0429/2023 - Paulo Rangel - Considérant T .......................................................................................................33
16. A9-0429/2023 - Paulo Rangel - Considérant U - Am 1 ...........................................................................................35
17. A9-0429/2023 - Paulo Rangel - Considérant U.......................................................................................................37
18. A9-0429/2023 - Paulo Rangel - Proposition de résolution (ensemble du texte) .....................................................39
19. A9-0436/2023 - Maite Pagazaurtundúa - § 1 - Am 6...............................................................................................41
20. A9-0436/2023 - Maite Pagazaurtundúa - Après le § 1 - Am 1 ................................................................................43
21. A9-0436/2023 - Maite Pagazaurtundúa - § 2 - Am 7...............................................................................................45
22. A9-0436/2023 - Maite Pagazaurtundúa - § 4 - Am 8...............................................................................................47
23. A9-0436/2023 - Maite Pagazaurtundúa - § 5 - Am 9...............................................................................................49
24. A9-0436/2023 - Maite Pagazaurtundúa - § 10 - Am 10...........................................................................................51
25. A9-0436/2023 - Maite Pagazaurtundúa - § 15 ........................................................................................................53
26. A9-0436/2023 - Maite Pagazaurtundúa - § 19 - Am 11...........................................................................................55
27. A9-0436/2023 - Maite Pagazaurtundúa - § 24 ........................................................................................................57
28. A9-0436/2023 - Maite Pagazaurtundúa - § 25 - Am 12...........................................................................................59
29. A9-0436/2023 - Maite Pagazaurtundúa - § 25 ........................................................................................................61
30. A9-0436/2023 - Maite Pagazaurtundúa - § 29 - Am 13...........................................................................................63
31. A9-0436/2023 - Maite Pagazaurtundúa - § 29 ........................................................................................................65
32. A9-0436/2023 - Maite Pagazaurtundúa - Proposition de résolution (ensemble du texte).......................................67
33. A9-0443/2023 - Tom Berendsen - Proposition de résolution (ensemble du texte) .................................................69
34. A9-0400/2023 - Javier Moreno Sánchez - Proposition de résolution ......................................................................71
35. Stratégie de l\'UE pour l\'Asie centrale - A9-0407/2023 - Karsten Lucke - Proposition de résolution (ensemble du
texte) .......................................................................................................................................................................73
36. A9-0401/2023 - Klemen Grošelj - Proposition de résolution (ensemble du texte) ..................................................75
37. A9-0442/2023 - Axel Voss, Ibán García Del Blanco - Proposition de résolution.....................................................77
38. Conscience historique européenne - A9-0402/2023 - Sabine Verheyen - Proposition de résolution .....................79
39. A9-0441/2023 - Caroline Roose - Proposition de résolution (ensemble du texte) ..................................................81
40. A9-0397/2023 - Pablo Arias Echeverría - Proposition de résolution .......................................................................83
41. A9-0435/2023 - Alviina Alametsä - Projet de recommandation...............................................................................85
42. A9-0404/2023 - Željana Zovko - § 1, point h ...........................................................................................................87
43. A9-0404/2023 - Željana Zovko - § 1, après le point ad - Am 1................................................................................89
44. A9-0404/2023 - Željana Zovko - § 1, après le point aq - Am 2................................................................................91
45. A9-0404/2023 - Željana Zovko - § 1, après le point aq - Am 3................................................................................93
46. A9-0404/2023 - Željana Zovko - § 1, point bq .........................................................................................................95
47. A9-0404/2023 - Željana Zovko - Considérant X ......................................................................................................97
48. A9-0404/2023 - Željana Zovko - Après le considérant AL - Am 7 ...........................................................................99
49. A9-0404/2023 - Željana Zovko - Projet de recommandation.................................................................................101
50. A9-0414/2023 - Peter Jahr - Après le § 2 - Am 5/1 ...............................................................................................103
51. A9-0414/2023 - Peter Jahr - Après le § 14 - Am 6 ................................................................................................105
52. A9-0414/2023 - Peter Jahr - Considérant O - Am 1 ..............................................................................................107
53. A9-0414/2023 - Peter Jahr - Après le considérant P - Am 2 .................................................................................109
54. A9-0414/2023 - Peter Jahr - Après le considérant P - Am 3 .................................................................................111
55. A9-0414/2023 - Peter Jahr - Après le considérant T - Am 4 .................................................................................113
56. A9-0414/2023 - Peter Jahr - Proposition de résolution (ensemble du texte) ........................................................115
57. A9-0446/2023 - Markus Pieper - Proposition de résolution de remplacement - Am 1 ..........................................117
58. A9-0446/2023 - Markus Pieper - Après le § 24 - Am 2 .........................................................................................119
59. A9-0446/2023 - Markus Pieper - Après le § 26 - Am 3 .........................................................................................121
60. A9-0446/2023 - Markus Pieper - Après le § 47 - Am 4 .........................................................................................123
61. A9-0446/2023 - Markus Pieper - § 48 - Am 5........................................................................................................125
62. A9-0446/2023 - Markus Pieper - Proposition de résolution (ensemble du texte)..................................................127';

    $rcv_toc['PV-9-2024-01-18-RCV'] = '1. B9-0064/2024 - § 5/2.................................................................................................................................................7
2. B9-0064/2024 - Proposition de résolution (ensemble du texte) ................................................................................9
3. A9-0416/2023 - Tilly Metz - § 6 - Am 1....................................................................................................................11
4. A9-0416/2023 - Tilly Metz - § 8/2 ............................................................................................................................13
5. A9-0416/2023 - Tilly Metz - Après le § 9 - Am 2 .....................................................................................................15
6. A9-0416/2023 - Tilly Metz - Considérant M/2..........................................................................................................17
7. A9-0416/2023 - Tilly Metz - Proposition de résolution (ensemble du texte)............................................................19
8. A9-0434/2023 - Maria Grapini - Après le § 27 - Am 6 .............................................................................................21
9. A9-0434/2023 - Maria Grapini - Après le § 46 - Am 8/1 ..........................................................................................23
10. A9-0434/2023 - Maria Grapini - Après le § 46 - Am 8/2 ..........................................................................................25
11. A9-0434/2023 - Maria Grapini - Après le § 55 - Am 7 .............................................................................................27
12. A9-0434/2023 - Maria Grapini - Proposition de résolution (ensemble du texte) .....................................................29
13. A9-0421/2023 - Heidi Hautala - Proposition de résolution ......................................................................................31
14. A9-0433/2023 - Nuno Melo - Proposition de résolution ..........................................................................................33
15. A9-0377/2023 - Maite Pagazaurtundúa - Après le § 1 - Am 4 ................................................................................35
16. A9-0377/2023 - Maite Pagazaurtundúa - Après le § 1 - Am 7 ................................................................................37
17. A9-0377/2023 - Maite Pagazaurtundúa - Après le § 1 - Am 8 ................................................................................39
18. A9-0377/2023 - Maite Pagazaurtundúa - § 7 - Am 10.............................................................................................41
19. A9-0377/2023 - Maite Pagazaurtundúa - Après le § 9 - Am 9 ................................................................................43
20. A9-0377/2023 - Maite Pagazaurtundúa - § 10 - Am 11...........................................................................................45
21. A9-0377/2023 - Maite Pagazaurtundúa - Après le considérant M - Am 6...............................................................47
22. A9-0377/2023 - Maite Pagazaurtundúa - Proposition de résolution (ensemble du texte).......................................49
23. A9-0357/2023 - Gabriel Mato - § 15........................................................................................................................51
24. A9-0357/2023 - Gabriel Mato - § 31 - Am 5 ............................................................................................................53
25. A9-0357/2023 - Gabriel Mato - § 53........................................................................................................................55
26. A9-0357/2023 - Gabriel Mato - § 82 - Am 2 ............................................................................................................57
27. A9-0357/2023 - Gabriel Mato - § 83........................................................................................................................59
28. A9-0357/2023 - Gabriel Mato - § 88........................................................................................................................61
29. A9-0357/2023 - Gabriel Mato - Après le § 89 - Am 1 ..............................................................................................63
30. A9-0357/2023 - Gabriel Mato - § 109/2...................................................................................................................65
31. A9-0357/2023 - Gabriel Mato - Proposition de résolution (ensemble du texte) ......................................................67
32. A9-0437/2023 - Niclas Herbst - Après le § 1 - Am 1 ...............................................................................................69
33. A9-0437/2023 - Niclas Herbst - § 7 .........................................................................................................................71
34. A9-0437/2023 - Niclas Herbst - Après le § 10 - Am 2 .............................................................................................73
35. A9-0437/2023 - Niclas Herbst - Après le § 10 - Am 3 .............................................................................................75
36. A9-0437/2023 - Niclas Herbst - Après le § 11 - Am 4 .............................................................................................77
37. A9-0437/2023 - Niclas Herbst - § 13/2 ....................................................................................................................79
38. A9-0437/2023 - Niclas Herbst - Après le § 13 - Am 5 .............................................................................................81
39. A9-0437/2023 - Niclas Herbst - Après le § 13 - Am 6 .............................................................................................83
40. A9-0437/2023 - Niclas Herbst - Après le § 14 - Am 7 .............................................................................................85
41. A9-0437/2023 - Niclas Herbst - Après le § 17 - Am 8 .............................................................................................87
42. A9-0437/2023 - Niclas Herbst - Après le § 18 - Am 9 .............................................................................................89
43. A9-0437/2023 - Niclas Herbst - Après le § 18 - Am 10 ...........................................................................................91
44. A9-0437/2023 - Niclas Herbst - § 19/2 ....................................................................................................................93
45. A9-0437/2023 - Niclas Herbst - § 23/2 ....................................................................................................................95
46. A9-0437/2023 - Niclas Herbst - Après le § 23 - Am 11 ...........................................................................................97
47. A9-0437/2023 - Niclas Herbst - Après le § 23 - Am 12 ...........................................................................................99
48. A9-0437/2023 - Niclas Herbst - Après le § 23 - Am 13 .........................................................................................101
49. A9-0437/2023 - Niclas Herbst - § 24/2 ..................................................................................................................103
50. A9-0437/2023 - Niclas Herbst - § 25 .....................................................................................................................105
51. A9-0437/2023 - Niclas Herbst - Après le § 39 - Am 14 .........................................................................................107
52. A9-0437/2023 - Niclas Herbst - Proposition de résolution (ensemble du texte)....................................................109
53. A9-0406/2023 - Izaskun Bilbao Barandica - § 20/2...............................................................................................111
54. A9-0406/2023 - Izaskun Bilbao Barandica - § 25..................................................................................................113
55. A9-0406/2023 - Izaskun Bilbao Barandica - § 38/2...............................................................................................115
56. A9-0406/2023 - Izaskun Bilbao Barandica - Considérant A/2 ...............................................................................117
57. A9-0406/2023 - Izaskun Bilbao Barandica - Proposition de résolution (ensemble du texte) ................................119
58. A9-0430/2023 - Alice Kuhnke - § 3 .......................................................................................................................121
59. A9-0430/2023 - Alice Kuhnke - § 4/2 ....................................................................................................................123
60. A9-0430/2023 - Alice Kuhnke - § 4/3 ....................................................................................................................125
61. A9-0430/2023 - Alice Kuhnke - § 14 .....................................................................................................................127
62. A9-0430/2023 - Alice Kuhnke - § 16/2 ..................................................................................................................129
63. A9-0430/2023 - Alice Kuhnke - § 16/3 ..................................................................................................................131
64. A9-0430/2023 - Alice Kuhnke - § 18/2 ..................................................................................................................133
65. A9-0430/2023 - Alice Kuhnke - § 20/2 ..................................................................................................................135
66. A9-0430/2023 - Alice Kuhnke - § 23/2 ..................................................................................................................137
67. A9-0430/2023 - Alice Kuhnke - § 23/3 ..................................................................................................................139
68. A9-0430/2023 - Alice Kuhnke - § 23/4 ..................................................................................................................141
69. A9-0430/2023 - Alice Kuhnke - § 24/2 ..................................................................................................................143
70. A9-0430/2023 - Alice Kuhnke - § 35/2 ..................................................................................................................145
71. A9-0430/2023 - Alice Kuhnke - § 35/3 ..................................................................................................................147
72. A9-0430/2023 - Alice Kuhnke - Considérant I .......................................................................................................149
73. A9-0430/2023 - Alice Kuhnke - Considérant AF ...................................................................................................151
74. A9-0430/2023 - Alice Kuhnke - Proposition de résolution (ensemble du texte) ....................................................153
75. A9-0432/2023 - Zdzisław Krasnodębski - Proposition de résolution .....................................................................155
76. A9-0376/2023 - Katarina Barley - Après le § 7 - Am 22 ........................................................................................157
77. A9-0376/2023 - Katarina Barley - § 10 - Am 16S..................................................................................................159
78. A9-0376/2023 - Katarina Barley - § 10/2...............................................................................................................161
79. A9-0376/2023 - Katarina Barley - § 25 - Am 26 ....................................................................................................163
80. A9-0376/2023 - Katarina Barley - Après le § 31 - Am 27 ......................................................................................165
81. A9-0376/2023 - Katarina Barley - § 34 - Am 17S..................................................................................................167
82. A9-0376/2023 - Katarina Barley - § 35 - Am 18S..................................................................................................169
83. A9-0376/2023 - Katarina Barley - § 37 - Am 19 ....................................................................................................171
84. A9-0376/2023 - Katarina Barley - Après le § 37 - Am 20 ......................................................................................173
85. A9-0376/2023 - Katarina Barley - § 43 - Am 21S..................................................................................................175
86. A9-0376/2023 - Katarina Barley - Après le visa 3 - Am 1......................................................................................177
87. A9-0376/2023 - Katarina Barley - Visa 4 - Am 2S.................................................................................................179
88. A9-0376/2023 - Katarina Barley - Visa 15 - Am 3S...............................................................................................181
89. A9-0376/2023 - Katarina Barley - Visa 17 - Am 4S...............................................................................................183
90. A9-0376/2023 - Katarina Barley - Visa 22 - Am 5S...............................................................................................185
91. A9-0376/2023 - Katarina Barley - Visa 34 - Am 6S...............................................................................................187
92. A9-0376/2023 - Katarina Barley - Visa 47 - Am 7S...............................................................................................189
93. A9-0376/2023 - Katarina Barley - Visa 48 - Am 8S...............................................................................................191
94. A9-0376/2023 - Katarina Barley - Visa 50 - Am 9S...............................................................................................193
95. A9-0376/2023 - Katarina Barley - Visa 53 - Am 10S.............................................................................................195
96. A9-0376/2023 - Katarina Barley - Après le considérant A - Am 11 .......................................................................197
97. A9-0376/2023 - Katarina Barley - Considérant G/2...............................................................................................199
98. A9-0376/2023 - Katarina Barley - Considérant P - Am 12 ....................................................................................201
99. A9-0376/2023 - Katarina Barley - Considérant R..................................................................................................203
100. A9-0376/2023 - Katarina Barley - Considérant U - Am 13S..................................................................................205
101. A9-0376/2023 - Katarina Barley - Après le considérant AA - Am 14.....................................................................207
102. A9-0376/2023 - Katarina Barley - Après le considérant AA - Am 15.....................................................................209
103. A9-0376/2023 - Katarina Barley - Proposition de résolution (ensemble du texte) ................................................211
104. RC-B9-0068/2024 - § 1 - Am 12............................................................................................................................213
105. RC-B9-0068/2024 - § 1 - Am 26............................................................................................................................215
106. RC-B9-0068/2024 - § 1 - Am 37............................................................................................................................217
107. RC-B9-0068/2024 - § 1 - Am 35............................................................................................................................219
108. RC-B9-0068/2024 - § 1 - Am 36............................................................................................................................221
109. RC-B9-0068/2024 - § 1 - Am 24............................................................................................................................223
110. RC-B9-0068/2024 - Après le § 1 - Am 38 .............................................................................................................225
111. RC-B9-0068/2024 - Après le § 1 - Am 39 .............................................................................................................227
112. RC-B9-0068/2024 - Après le § 1 - Am 40 .............................................................................................................229
113. RC-B9-0068/2024 - § 2 - Am 13............................................................................................................................231
114. RC-B9-0068/2024 - § 2/2 ......................................................................................................................................233
115. RC-B9-0068/2024 - Après le § 3 - Am 28 .............................................................................................................235
116. RC-B9-0068/2024 - § 4 - Am 14............................................................................................................................237
117. RC-B9-0068/2024 - § 4 - Am 41............................................................................................................................239
118. RC-B9-0068/2024 - § 5 - Am 43............................................................................................................................241
119. RC-B9-0068/2024 - Après le § 5 - Am 30 .............................................................................................................243
120. RC-B9-0068/2024 - Après le § 5 - Am 44 .............................................................................................................245
121. RC-B9-0068/2024 - Après le § 5 - Am 45 .............................................................................................................247
122. RC-B9-0068/2024 - Après le § 5 - Am 46 .............................................................................................................249
123. RC-B9-0068/2024 - Après le § 5 - Am 47 .............................................................................................................251
124. RC-B9-0068/2024 - Après le § 5 - Am 59 .............................................................................................................253
125. RC-B9-0068/2024 - § 6 - Am 48............................................................................................................................255
126. RC-B9-0068/2024 - Après le § 6 - Am 49 .............................................................................................................257
127. RC-B9-0068/2024 - § 7 - Am 15/1.........................................................................................................................259
128. RC-B9-0068/2024 - § 7 - Am 15/2.........................................................................................................................261
129. RC-B9-0068/2024 - § 7 - Am 50............................................................................................................................263
130. RC-B9-0068/2024 - Après le § 7 - Am 16 .............................................................................................................265
131. RC-B9-0068/2024 - § 10 - Am 17..........................................................................................................................267
132. RC-B9-0068/2024 - § 10 - Am 51..........................................................................................................................269
133. RC-B9-0068/2024 - Après le § 10 - Am 33 ...........................................................................................................271
134. RC-B9-0068/2024 - § 11 - Am 18..........................................................................................................................273
135. RC-B9-0068/2024 - § 12 - Am 52..........................................................................................................................275
136. RC-B9-0068/2024 - § 13 - Am 53..........................................................................................................................277
137. RC-B9-0068/2024 - § 13 - Am 23..........................................................................................................................279
138. RC-B9-0068/2024 - § 13 - Am 29..........................................................................................................................281
139. RC-B9-0068/2024 - § 13 - Am 19..........................................................................................................................283
140. RC-B9-0068/2024 - § 14 - Am 20..........................................................................................................................285
141. RC-B9-0068/2024 - § 14 - Am 54..........................................................................................................................287
142. RC-B9-0068/2024 - § 14 - Am 27..........................................................................................................................289
143. RC-B9-0068/2024 - Après le § 14 - Am 55 ...........................................................................................................291
144. RC-B9-0068/2024 - § 19 - Am 56..........................................................................................................................293
145. RC-B9-0068/2024 - Après le § 19 - Am 57 ...........................................................................................................295
146. RC-B9-0068/2024 - § 22 - Am 22..........................................................................................................................297
147. RC-B9-0068/2024 - § 22 - Am 58..........................................................................................................................299
148. RC-B9-0068/2024 - Après le visa 5 - Am 1 ...........................................................................................................301
149. RC-B9-0068/2024 - Considérant A - Am 2............................................................................................................303
150. RC-B9-0068/2024 - Après le considérant C - Am 3 ..............................................................................................305
151. RC-B9-0068/2024 - Après le considérant C - Am 4 ..............................................................................................307
152. RC-B9-0068/2024 - Considérant D - Am 5............................................................................................................309
153. RC-B9-0068/2024 - Considérant E - Am 6............................................................................................................311
154. RC-B9-0068/2024 - Considérant F - Am 7 ............................................................................................................313
155. RC-B9-0068/2024 - Considérant H - Am 8............................................................................................................315
156. RC-B9-0068/2024 - Après le considérant H - Am 9 ..............................................................................................317
157. RC-B9-0068/2024 - Considérant K - Am 10..........................................................................................................319
158. RC-B9-0068/2024 - Considérant L - Am 11 ..........................................................................................................321
159. B9-0059/2024 - § 1 - Am 1 ....................................................................................................................................323
160. B9-0059/2024 - § 2 - Am 2 ....................................................................................................................................325
161. B9-0059/2024 - § 4 - Am 3 ....................................................................................................................................327
162. B9-0059/2024 - § 5 - Am 4 ....................................................................................................................................329
163. B9-0086/2024 - § 2/2.............................................................................................................................................331
164. B9-0086/2024 - Après le § 6 - Am 1......................................................................................................................333
165. B9-0086/2024 - § 8 - Am 2 ....................................................................................................................................335
166. B9-0086/2024 - § 11..............................................................................................................................................337
167. B9-0086/2024 - Proposition de résolution (ensemble du texte) ............................................................................339';

    $rcv_toc['PV-9-2024-02-06-RCV'] = '1. A9-0410/2023 - Petar Vitanov - Article 6, § 2, point a - Am 54S ...............................................................................4
2. A9-0410/2023 - Petar Vitanov - Article 11, § 1, point d - Am 55 ...............................................................................6
3. A9-0410/2023 - Petar Vitanov - Article 14, § 2 - Am 56 ............................................................................................8
4. A9-0410/2023 - Petar Vitanov - Article 15, § 2 - Am 57 ..........................................................................................10
5. A9-0410/2023 - Petar Vitanov - Après le considérant 26 - Am 51 ..........................................................................12
6. A9-0410/2023 - Petar Vitanov - Proposition de la Commission ..............................................................................14
7. A9-0361/2023 - Cornelia Ernst - Accord provisoire - Am 14 ...................................................................................16
8. A9-0311/2023 - Anna Zalewska - Accord provisoire - Am 9 ...................................................................................18
9. A9-0254/2023 - Ivars Ijabs - Accord provisoire - Am 2............................................................................................20
10. A9-0301/2023 - Clara Aguilera - Accord provisoire - Am 89 ...................................................................................22';

    $rcv_toc['PV-9-2024-02-07-RCV'] = '1. A9-0021/2024 - Birgit Sippel - Décision d\'engager des négociations interinstitutionnelles.......................................5
2. A9-0018/2024 - Petar Vitanov - Proposition de la Commission et amendements ....................................................7
3. A9-0005/2024 - Joachim Schuster - Projet de décision du Conseil ..........................................................................9
4. A9-0020/2023 - Isabel Benjumea Benjumea - Accord provisoire - Am 2 ................................................................11
5. A9-0230/2023 - Michiel Hoogeveen - Accord provisoire - Am 2 .............................................................................13
6. A9-0263/2023 - Nikolaj Villumsen - Accord provisoire - Am 48...............................................................................15
7. A9-0014/2024 - Jessica Polfjärd - Rejet - Am 109= 137= .......................................................................................17
8. A9-0014/2024 - Jessica Polfjärd - Amendements de la commission compétente - vote séparé - Am 68...............19
9. A9-0014/2024 - Jessica Polfjärd - Amendements de la commission compétente - vote séparé - Am 84...............21
10. A9-0014/2024 - Jessica Polfjärd - Article 1, § 1 - Am 244.......................................................................................23
11. A9-0014/2024 - Jessica Polfjärd - Article 2, § 1, point 1 - Am 245..........................................................................25
12. A9-0014/2024 - Jessica Polfjärd - Article 2, après le § 1 - Am 94= 246=................................................................27
13. A9-0014/2024 - Jessica Polfjärd - Article 3, § 1, point 6 - Am 247..........................................................................29
14. A9-0014/2024 - Jessica Polfjärd - Article 3, alinéa 1, point 7, après le sous-point b - Am 299...............................31
15. A9-0014/2024 - Jessica Polfjärd - Article 4 - Am 32................................................................................................33
16. A9-0014/2024 - Jessica Polfjärd - Chapitre II - Am 122S= 138S= ..........................................................................35
17. A9-0014/2024 - Jessica Polfjärd - Chapitre II - Am 47 ............................................................................................37
18. A9-0014/2024 - Jessica Polfjärd - Article 5, § 1 - Am 301.......................................................................................39
19. A9-0014/2024 - Jessica Polfjärd - Article 5, § 2 - Am 303.......................................................................................41
20. A9-0014/2024 - Jessica Polfjärd - Article 5, § 2 - Am 34.........................................................................................43
21. A9-0014/2024 - Jessica Polfjärd - Article 5, après le § 2 - Am 304.........................................................................45
22. A9-0014/2024 - Jessica Polfjärd - Article 6, § 3, après le point c - Am 253 ............................................................47
23. A9-0014/2024 - Jessica Polfjärd - Article 6, § 3, après le point e - Am 255 ............................................................49
24. A9-0014/2024 - Jessica Polfjärd - Article 6, § 3, après le point e - Am 256 ............................................................51
25. A9-0014/2024 - Jessica Polfjärd - Article 7, § 2, après le point d - Am 260 ............................................................53
26. A9-0014/2024 - Jessica Polfjärd - Après l\'article 7 - Am 313 ..................................................................................55
27. A9-0014/2024 - Jessica Polfjärd - Article 10, § 1 - Am 264.....................................................................................57
28. A9-0014/2024 - Jessica Polfjärd - Article 10, après le § 1 - Am 265.......................................................................59
29. A9-0014/2024 - Jessica Polfjärd - Article 10, après le § 1 - Am 314.......................................................................61
30. A9-0014/2024 - Jessica Polfjärd - Après l\'article 11 - Am 266 ................................................................................63
31. A9-0014/2024 - Jessica Polfjärd - Article 12, § 1 - Am 123.....................................................................................65
32. A9-0014/2024 - Jessica Polfjärd - Article 17, après le § 2 - Am 268.......................................................................67
33. A9-0014/2024 - Jessica Polfjärd - Article 20, § 4 - Am 228.....................................................................................69
34. A9-0014/2024 - Jessica Polfjärd - Article 21, après le § 1 - Am 270.......................................................................71
35. A9-0014/2024 - Jessica Polfjärd - Article 23, § 1 - Am 125.....................................................................................73
36. A9-0014/2024 - Jessica Polfjärd - Article 24 - Am 272............................................................................................75
37. A9-0014/2024 - Jessica Polfjärd - Article 24 - Am 58/1...........................................................................................77
38. A9-0014/2024 - Jessica Polfjärd - Article 24 - Am 58/2...........................................................................................79
39. A9-0014/2024 - Jessica Polfjärd - Après l\'article 24 - Am 140 ................................................................................81
40. A9-0014/2024 - Jessica Polfjärd - Après l\'article 24 - Am 273 ................................................................................83
41. A9-0014/2024 - Jessica Polfjärd - Article 25, § 1 - Am 274.....................................................................................85
42. A9-0014/2024 - Jessica Polfjärd - Article 32, § 1 - Am 129.....................................................................................87
43. A9-0014/2024 - Jessica Polfjärd - Après l\'article 32 - Am 229 ................................................................................89
44. A9-0014/2024 - Jessica Polfjärd - Après l\'article 33; Directive 98/44/CE; Article 4 - Am 69= 291PC1= .................91
45. A9-0014/2024 - Jessica Polfjärd - Après l\'article 33; Directive 98/44/CE; Article 4 - Am 315 .................................93
46. A9-0014/2024 - Jessica Polfjärd - Après l\'article 33; Directive 98/44/CE; Article 6, § 1 - Am 131 ..........................95
47. A9-0014/2024 - Jessica Polfjärd - Après l\'article 33; Directive 98/44/CE; Article 15 - Am 134 ...............................97
48. A9-0014/2024 - Jessica Polfjärd - Annexe I - Am 136S= 139S= ............................................................................99
49. A9-0014/2024 - Jessica Polfjärd - Annexe I, point 1 - Am 278..............................................................................101
50. A9-0014/2024 - Jessica Polfjärd - Annexe I, après le point 1 - Am 279................................................................103
51. A9-0014/2024 - Jessica Polfjärd - Annexe II, partie 1, alinéa 3, après le point c - Am 284 ..................................105
52. A9-0014/2024 - Jessica Polfjärd - Annexe III, partie 2, point 1 - Am 231..............................................................107
53. A9-0014/2024 - Jessica Polfjärd - Après le considérant 1 - Am 163.....................................................................109
54. A9-0014/2024 - Jessica Polfjärd - Après le considérant 1 - Am 164/1..................................................................111
55. A9-0014/2024 - Jessica Polfjärd - Après le considérant 1 - Am 164/2..................................................................113
56. A9-0014/2024 - Jessica Polfjärd - Après le considérant 1 - Am 165.....................................................................115
57. A9-0014/2024 - Jessica Polfjärd - Après le considérant 1 - Am 166.....................................................................117
58. A9-0014/2024 - Jessica Polfjärd - Après le considérant 1 - Am 167.....................................................................119
59. A9-0014/2024 - Jessica Polfjärd - Après le considérant 1 - Am 168.....................................................................121
60. A9-0014/2024 - Jessica Polfjärd - Après le considérant 3 - Am 171.....................................................................123
61. A9-0014/2024 - Jessica Polfjärd - Après le considérant 6 - Am 112.....................................................................125
62. A9-0014/2024 - Jessica Polfjärd - Considérant 8 - Am 4 ......................................................................................127
63. A9-0014/2024 - Jessica Polfjärd - Considérant 9 - Am 5/1 ...................................................................................129
64. A9-0014/2024 - Jessica Polfjärd - Considérant 9 - Am 5/2 ...................................................................................131
65. A9-0014/2024 - Jessica Polfjärd - Après le considérant 24 - Am 238...................................................................133
66. A9-0014/2024 - Jessica Polfjärd - Après le considérant 29 - Am 294...................................................................135
67. A9-0014/2024 - Jessica Polfjärd - Considérant 37 - Am 239S..............................................................................137
68. A9-0014/2024 - Jessica Polfjärd - Après le considérant 37 - Am 204...................................................................139
69. A9-0014/2024 - Jessica Polfjärd - Considérant 38 - Am 240 ................................................................................141
70. A9-0014/2024 - Jessica Polfjärd - Considérant 39 - Am 20/1 ...............................................................................143
71. A9-0014/2024 - Jessica Polfjärd - Considérant 39 - Am 20/2 ...............................................................................145
72. A9-0014/2024 - Jessica Polfjärd - Après le considérant 42 - Am 210...................................................................147
73. A9-0014/2024 - Jessica Polfjärd - Après le considérant 43 - Am 212...................................................................149
74. A9-0014/2024 - Jessica Polfjärd - Après le considérant 46 - Am 296...................................................................151
75. A9-0014/2024 - Jessica Polfjärd - Après le considérant 47 - Am 241...................................................................153
76. A9-0014/2024 - Jessica Polfjärd - Après le considérant 47 - Am 243...................................................................155
77. A9-0014/2024 - Jessica Polfjärd - Proposition de la Commission.........................................................................157
78. B9-0095/2024 - § 1 - Am 1 ....................................................................................................................................159
79. B9-0095/2024 - Proposition de résolution (ensemble du texte) ............................................................................161
80. B9-0098/2024 - Après le considérant Q - Am 1 ....................................................................................................163
81. B9-0098/2024 - Proposition de résolution (ensemble du texte) ............................................................................165';

    $rcv_toc['PV-9-2024-02-08-RCV'] = '1. RC-B9-0110/2024 - Proposition de résolution (ensemble du texte)..........................................................................5
2. RC-B9-0102/2024 - § 1 - Am 2..................................................................................................................................7
3. RC-B9-0105/2024 - Après le § 1 - Am 1 ...................................................................................................................9
4. RC-B9-0105/2024 - Après le § 1 - Am 2 .................................................................................................................11
5. RC-B9-0105/2024 - Après le considérant F - Am 8.................................................................................................13
6. A9-0200/2023 - Paulo Rangel - Accord provisoire - Am 287 ..................................................................................15
7. B9-0091/2024 - § 1, point p/2..................................................................................................................................17
8. B9-0091/2024 - § 1, point p/3..................................................................................................................................19
9. B9-0091/2024 - § 1, point w/2 .................................................................................................................................21
10. B9-0091/2024 - § 1, point w/3 .................................................................................................................................23
11. B9-0091/2024 - § 1, point w/4 .................................................................................................................................25
12. B9-0091/2024 - § 1, point x/2 ..................................................................................................................................27
13. B9-0091/2024 - § 1, point x/3 ..................................................................................................................................29
14. B9-0091/2024 - § 1, point y/1 ..................................................................................................................................31
15. B9-0091/2024 - § 1, point y/2 ..................................................................................................................................33
16. B9-0091/2024 - Considérant M/1 ............................................................................................................................35
17. B9-0091/2024 - Considérant M/2 ............................................................................................................................37
18. B9-0091/2024 - Considérant M/3 ............................................................................................................................39
19. B9-0091/2024 - Considérant M/4 ............................................................................................................................41
20. B9-0091/2024 - Considérant M/5 ............................................................................................................................43
21. RC-B9-0106/2024 - Proposition de résolution (ensemble du texte)........................................................................45
22. A9-0030/2024 - José Gusmão - § 1 - Am 1.............................................................................................................47
23. A9-0030/2024 - José Gusmão - Après le § 4 - Am 6 ..............................................................................................49
24. A9-0030/2024 - José Gusmão - § 7 - Am 2/1..........................................................................................................51
25. A9-0030/2024 - José Gusmão - § 7 - Am 2/2..........................................................................................................53
26. A9-0030/2024 - José Gusmão - Après le § 13 - Am 7 ............................................................................................55
27. A9-0030/2024 - José Gusmão - Après le § 19 - Am 8 ............................................................................................57
28. A9-0030/2024 - José Gusmão - Après le § 44 - Am 5 ............................................................................................59
29. A9-0030/2024 - José Gusmão - Après le visa 20 - Am 3 ........................................................................................61
30. A9-0030/2024 - José Gusmão - Après le considérant S - Am 4 .............................................................................63
31. A9-0030/2024 - José Gusmão - Proposition de résolution (ensemble du texte).....................................................65
32. B9-0096/2024 - § 11/1.............................................................................................................................................67
33. RC-B9-0124/2024 - Avant le § 1 - Am 29................................................................................................................69
34. RC-B9-0124/2024 - § 1 ...........................................................................................................................................71
35. RC-B9-0124/2024 - § 2 - Am 6................................................................................................................................73
36. RC-B9-0124/2024 - Après le § 2 - Am 28 ...............................................................................................................75
37. RC-B9-0124/2024 - § 3 ...........................................................................................................................................77
38. RC-B9-0124/2024 - § 4 ...........................................................................................................................................79
39. RC-B9-0124/2024 - § 5 ...........................................................................................................................................81
40. RC-B9-0124/2024 - § 6 ...........................................................................................................................................83
41. RC-B9-0124/2024 - § 9 - Am 12..............................................................................................................................85
42. RC-B9-0124/2024 - Après le § 10 - Am 2 ...............................................................................................................87
43. RC-B9-0124/2024 - § 11 - Am 13............................................................................................................................89
44. RC-B9-0124/2024 - § 11 - Am 16............................................................................................................................91
45. RC-B9-0124/2024 - § 15 .........................................................................................................................................93
46. RC-B9-0124/2024 - § 18 - Am 17............................................................................................................................95
47. RC-B9-0124/2024 - § 18 - Am 18............................................................................................................................97
48. RC-B9-0124/2024 - Après le § 18 - Am 22 .............................................................................................................99
49. RC-B9-0124/2024 - Après le § 18 - Am 23 ...........................................................................................................101
50. RC-B9-0124/2024 - Après le § 18 - Am 24 ...........................................................................................................103
51. RC-B9-0124/2024 - § 20 - Am 21..........................................................................................................................105
52. RC-B9-0124/2024 - § 22 - Am 19..........................................................................................................................107
53. RC-B9-0124/2024 - § 22 - Am 20..........................................................................................................................109
54. RC-B9-0124/2024 - § 23 .......................................................................................................................................111
55. RC-B9-0124/2024 - Considérant G - Am 14 .........................................................................................................113
56. RC-B9-0124/2024 - Après le considérant G - Am 1 ..............................................................................................115
57. RC-B9-0124/2024 - Considérant H/1 ....................................................................................................................117
58. RC-B9-0124/2024 - Considérant H/2 ....................................................................................................................119
59. RC-B9-0124/2024 - Après le considérant H - Am 4 ..............................................................................................121
60. RC-B9-0124/2024 - Considérant M - Am 11 .........................................................................................................123
61. RC-B9-0124/2024 - Considérant M - Am 15 .........................................................................................................125
62. RC-B9-0124/2024 - Après le considérant U - Am 5 ..............................................................................................127
63. RC-B9-0124/2024 - Proposition de résolution (ensemble du texte)......................................................................129
64. RC-B9-0097/2024 - Proposition de résolution.......................................................................................................131';


    $rcv_toc['PV-9-2024-02-27-RCV'] = '1. A9-0051/2024 - Jan Olbrycht, Margarida Marques - Projet de règlement du Conseil...............................................5
2. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Avant le § 1 - Am 1 .................................................................7
3. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Avant le § 1 - Am 2 .................................................................9
4. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Avant le § 1 - Am 3 ...............................................................11
5. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Avant le § 1 - Am 4 ...............................................................13
6. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 2 - Am 5 .............................................................................15
7. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 2 - Am 17= 22= ..................................................................17
8. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Après le § 2 - Am 27 .............................................................19
9. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 3 - Am 7 .............................................................................21
10. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Après le § 3 - Am 6 ...............................................................23
11. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Après le § 3 - Am 14 .............................................................25
12. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 14 - Am 15 .........................................................................27
13. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 15 .......................................................................................29
14. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 19 - Am 18= 23= ................................................................31
15. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 20 - Am 8 ...........................................................................33
16. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 20 .......................................................................................35
17. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Après le § 32 - Am 9 .............................................................37
18. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Après le § 32 - Am 10 ...........................................................39
19. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Après le § 32 - Am 11 ...........................................................41
20. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 34 - Am 16 .........................................................................43
21. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Titre après le § 35 - Am 19= 24= ..........................................45
22. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 37 - Am 12 .........................................................................47
23. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 38 - Am 13 .........................................................................49
24. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 39 - Am 20= 25= ................................................................51
25. A9-0053/2024 - Jan Olbrycht, Margarida Marques - § 40 - Am 21= 26= ................................................................53
26. A9-0053/2024 - Jan Olbrycht, Margarida Marques - Proposition de résolution (ensemble du texte)......................55
27. A9-0286/2023 - Michael Gahler, Eider Gardiazabal Rubial - Accord provisoire - Am 8..........................................57
28. A9-0290/2023 - José Manuel Fernandes, Christian Ehler - Accord provisoire - Am 145........................................59
29. A9-0223/2023 - Tiemo Wölken - Accord provisoire - Am 126 .................................................................................61
30. A9-0278/2023 - Ilana Cicurel - Accord provisoire - Am 2 ........................................................................................63
31. A9-0290/2022 - Pernille Weiss - Accord provisoire - Am 159 .................................................................................65
32. A9-0344/2023 - Jonás Fernández - Accord provisoire - Am 2 ................................................................................67
33. A9-0220/2023 - César Luena - Rejet - Am 139= 140=............................................................................................69
34. A9-0220/2023 - César Luena - Demande de mettre au voix les amendements au projet d\'acte législatif.............71
35. A9-0220/2023 - César Luena - Accord provisoire - Am 137 ...................................................................................73
36. A9-0220/2023 - César Luena - Déclaration de la Commission - Am 138 ...............................................................75
37. A9-0009/2023 - Sandro Gozi - Accord provisoire - Am 287 ....................................................................................77
38. A9-0039/2024 - Manuela Ripa - Proposition de la Commission et amendements..................................................79
39. A9-0378/2023 - Paolo Borchia - Accord provisoire - Am 2......................................................................................81
40. A9-0087/2023 - Antonius Manders - Accord provisoire - Am 161 ...........................................................................83
41. A9-0412/2023 - Johan Van Overtveldt - Avant le § 1 - Am 14 ................................................................................85
42. A9-0412/2023 - Johan Van Overtveldt - Avant le § 1 - Am 15 ................................................................................87
43. A9-0412/2023 - Johan Van Overtveldt - Avant le § 1 - Am 16 ................................................................................89
44. A9-0412/2023 - Johan Van Overtveldt - Avant le § 1 - Am 17 ................................................................................91
45. A9-0412/2023 - Johan Van Overtveldt - Après le § 5 - Am 6 ..................................................................................93
46. A9-0412/2023 - Johan Van Overtveldt - § 8............................................................................................................95
47. A9-0412/2023 - Johan Van Overtveldt - Après le § 22 - Am 7 ................................................................................97
48. A9-0412/2023 - Johan Van Overtveldt - Après le § 24 - Am 1 ................................................................................99
49. A9-0412/2023 - Johan Van Overtveldt - Après le considérant B - Am 11 .............................................................101
50. A9-0412/2023 - Johan Van Overtveldt - Après le considérant C - Am 13.............................................................103
51. A9-0412/2023 - Johan Van Overtveldt - Proposition de résolution (ensemble du texte) ......................................105';

    return $rcv_toc[$doc_id] ?? tocMapSrcArchive($doc_id);
    // return $rcv_toc + tocMapSrcArchive($doc_id);
  }

  function tocMapSrcArchive($doc_id) {

    $rcv_toc['PV-9-2024-02-28-RCV'] = '1. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 4 ........................................................................................................................................................................10
2. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 59/2 ...................................................................................................................................................................12
3. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 61 ......................................................................................................................................................................14
4. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 62 ......................................................................................................................................................................16
5. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 63 ......................................................................................................................................................................18
6. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 64 ......................................................................................................................................................................20
7. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 65 ......................................................................................................................................................................22
8. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 66 ......................................................................................................................................................................24
9. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 67 ......................................................................................................................................................................26
10. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 68 ......................................................................................................................................................................28
11. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 71/1 ...................................................................................................................................................................30
12. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 71/2 ...................................................................................................................................................................32
13. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 72 ......................................................................................................................................................................34
14. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 95 ......................................................................................................................................................................36
15. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 96 ......................................................................................................................................................................38
16. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 137 ....................................................................................................................................................................40
17. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 168 ....................................................................................................................................................................42
18. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 209/2 .................................................................................................................................................................44
19. A9-0445/2023 - Karima Delli - Amendements de la commission compétente - vote séparé et vote par division -
Am 219/2 .................................................................................................................................................................46
20. A9-0445/2023 - Karima Delli - Article 1, § 2 - Am 312 ............................................................................................48
21. A9-0445/2023 - Karima Delli - Article 2, § 1, après le point 11 - Am 264................................................................50
22. A9-0445/2023 - Karima Delli - Article 2, § 1, après le point 11 - Am 41..................................................................52
23. A9-0445/2023 - Karima Delli - Article 2, § 1, après le point 11 - Am 42/1...............................................................54
24. A9-0445/2023 - Karima Delli - Article 2, § 1, après le point 11 - Am 42/2...............................................................56
25. A9-0445/2023 - Karima Delli - Article 6, § 1, point a, alinéa 2, tiret 1 - Am 238= 248=...........................................58
26. A9-0445/2023 - Karima Delli - Article 7, § 1, point a - Am 239= 249= ....................................................................60
27. A9-0445/2023 - Karima Delli - Article 7, § 1, après le point a - Am 240= 250=.......................................................62
28. A9-0445/2023 - Karima Delli - Article 7, § 1, point b - Am 296................................................................................64
29. A9-0445/2023 - Karima Delli - Article 7, § 2, partie introductive - Am 297 ..............................................................66
30. A9-0445/2023 - Karima Delli - Article 7, § 2, point a - Am 298................................................................................68
31. A9-0445/2023 - Karima Delli - Article 7, § 2, après le point c - Am 299 ..................................................................70
32. A9-0445/2023 - Karima Delli - Article 9, § 2, point e - Am 300................................................................................72
33. A9-0445/2023 - Karima Delli - Article 9, § 3, alinéa 1, point b - Am 301 .................................................................74
34. A9-0445/2023 - Karima Delli - Article 9, § 3, alinéa 1, point b - Am 280 .................................................................76
35. A9-0445/2023 - Karima Delli - Article 9, § 4, alinéa 1, point c - Am 241= 251= ......................................................78
36. A9-0445/2023 - Karima Delli - Article 10, § 1, point a - Am 266..............................................................................80
37. A9-0445/2023 - Karima Delli - Article 10, § 1, point b, alinéa 1 - Am 92 .................................................................82
38. A9-0445/2023 - Karima Delli - Article 10, § 1, point b, alinéa 1 - Am 252= 314= 334=...........................................84
39. A9-0445/2023 - Karima Delli - Article 10, § 2, alinéa 5 - Am 253= 315= 335= .......................................................86
40. A9-0445/2023 - Karima Delli - Article 10, § 6, alinéa 1 - Am 98..............................................................................88
41. A9-0445/2023 - Karima Delli - Article 10, § 6, alinéa 1 - Am 281............................................................................90
42. A9-0445/2023 - Karima Delli - Article 10, § 6, alinéa 1 - Am 254= 316=.................................................................92
43. A9-0445/2023 - Karima Delli - Article 10, § 6, alinéa 1 - Am 336............................................................................94
44. A9-0445/2023 - Karima Delli - Article 10, § 6, alinéa 2 - Am 99S ...........................................................................96
45. A9-0445/2023 - Karima Delli - Article 10, § 6, après l\'alinéa 2 - Am 256= 318=/1 ..................................................98
46. A9-0445/2023 - Karima Delli - Article 10, § 6, après l\'alinéa 2 - Am 256= 318=/2 ................................................100
47. A9-0445/2023 - Karima Delli - Article 10, § 6, après l\'alinéa 2 - Am 256= 318=/3 ................................................102
48. A9-0445/2023 - Karima Delli - Article 11, § 2 - Am 282 ........................................................................................104
49. A9-0445/2023 - Karima Delli - Après l\'article 13 - Am 283 ....................................................................................106
50. A9-0445/2023 - Karima Delli - Article 14 - Am 302S .............................................................................................108
51. A9-0445/2023 - Karima Delli - Article 14 - Am 114 ...............................................................................................110
52. A9-0445/2023 - Karima Delli - Article 14 - Am 115 ...............................................................................................112
53. A9-0445/2023 - Karima Delli - Article 14 - Am 116 ...............................................................................................114
54. A9-0445/2023 - Karima Delli - Article 14 - Am 117 ...............................................................................................116
55. A9-0445/2023 - Karima Delli - Article 14 - Am 118 ...............................................................................................118
56. A9-0445/2023 - Karima Delli - Article 14, § 1 - Am 284 ........................................................................................120
57. A9-0445/2023 - Karima Delli - Article 14, § 1 - Am 267 ........................................................................................122
58. A9-0445/2023 - Karima Delli - Article 14, § 1 - Am 111 ........................................................................................124
59. A9-0445/2023 - Karima Delli - Article 15, § 2 - Am 120/1 .....................................................................................126
60. A9-0445/2023 - Karima Delli - Article 15, § 2 - Am 120/2 .....................................................................................128
61. A9-0445/2023 - Karima Delli - Article 15, après le § 2 - Am 286...........................................................................130
62. A9-0445/2023 - Karima Delli - Article 15, après le § 4 - Am 270...........................................................................132
63. A9-0445/2023 - Karima Delli - Après l\'article 16 - Am 271/1 .................................................................................134
64. A9-0445/2023 - Karima Delli - Après l\'article 16 - Am 271/4 .................................................................................136
65. A9-0445/2023 - Karima Delli - Après l\'article 16 - Am 271/5 .................................................................................138
66. A9-0445/2023 - Karima Delli - Après l\'article 16 - Am 287 ....................................................................................140
67. A9-0445/2023 - Karima Delli - Article 23, § 1; Directive (UE) 2022/2561; Article 5, § 2, point c - Am 304S .........142
68. A9-0445/2023 - Karima Delli - Article 23, § 1; Directive (UE) 2022/2561; Article 5, § 2, point c - Am 135 ...........144
69. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/1....146
70. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/2....148
71. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/3....150
72. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/4....152
73. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/5....154
74. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/6....156
75. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/7....158
76. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point a - Am 322/8....160
77. A9-0445/2023 - Karima Delli - Après l\'article 23; Directive (UE) 2022/2561; Article 5, § 3, point b - Am 323.......162
78. A9-0445/2023 - Karima Delli - Article 25, après le § 1 - Am 326...........................................................................164
79. A9-0445/2023 - Karima Delli - Annexe III, sous-titre 1 - Am 258= 327= 338= ......................................................166
80. A9-0445/2023 - Karima Delli - Annexe III, sous-titre 3 - Am 259= 328= 339= ......................................................168
81. A9-0445/2023 - Karima Delli - Annexe III, point 3, alinéa 1 - Am 204S ................................................................170
82. A9-0445/2023 - Karima Delli - Annexe III, point 3, alinéa 1 - Am 260= 329=........................................................172
83. A9-0445/2023 - Karima Delli - Annexe III, point 3, alinéa 2 - Am 205...................................................................174
84. A9-0445/2023 - Karima Delli - Annexe III, point 3, alinéa 3 - Am 262= 331= 341= ..............................................176
85. A9-0445/2023 - Karima Delli - Annexe III, point 5 - Am 342S ...............................................................................178
86. A9-0445/2023 - Karima Delli - Annexe III, point 9 - Am 288S ...............................................................................180
87. A9-0445/2023 - Karima Delli - Annexe III, point 10 - Am 289S .............................................................................182
88. A9-0445/2023 - Karima Delli - Annexe III, point 10 - Am 218/2 ............................................................................184
89. A9-0445/2023 - Karima Delli - Annexe III, point 11, § 3 - Am 290S ......................................................................186
90. A9-0445/2023 - Karima Delli - Annex III, point 16 - Am 291S ...............................................................................188
91. A9-0445/2023 - Karima Delli - Après le considérant 16 - Am 272.........................................................................190
92. A9-0445/2023 - Karima Delli - Considérant 18 - Am 242= 305=...........................................................................192
93. A9-0445/2023 - Karima Delli - Après le considérant 18 - Am 243= 306=/1 ..........................................................194
94. A9-0445/2023 - Karima Delli - Après le considérant 18 - Am 243= 306=/2 ..........................................................196
95. A9-0445/2023 - Karima Delli - Considérant 19 - Am 244= 307=/1........................................................................198
96. A9-0445/2023 - Karima Delli - Considérant 19 - Am 244= 307=/2........................................................................200
97. A9-0445/2023 - Karima Delli - Considérant 19 - Am 244= 307=/3........................................................................202
98. A9-0445/2023 - Karima Delli - Considérant 28 - Am 273 ......................................................................................204
99. A9-0445/2023 - Karima Delli - Considérant 29 - Am 274S....................................................................................206
100. A9-0445/2023 - Karima Delli - Après le considérant 35 - Am 247= 311= .............................................................208
101. A9-0445/2023 - Karima Delli - Proposition de la Commission ..............................................................................210
102. A9-0020/2024 - Tiemo Wölken - Proposition de la Commission...........................................................................212
103. A9-0019/2024 - Tiemo Wölken - Proposition de la Commission...........................................................................214
104. A9-0023/2024 - Tiemo Wölken - Proposition de la Commission...........................................................................216
105. A9-0022/2024 - Tiemo Wölken - Proposition de la Commission...........................................................................218
106. A9-0016/2024 - Marion Walsmann - Proposition de la Commission.....................................................................220
107. A9-0173/2023 - Paolo De Castro - Accord provisoire - Am 286............................................................................222
108. A9-0007/2024 - Herbert Dorfmann - Après l\'article 17 - Am 63/1 .........................................................................224
109. A9-0007/2024 - Herbert Dorfmann - Après l\'article 17 - Am 64 ............................................................................226
110. A9-0007/2024 - Herbert Dorfmann - Après l\'article 17 - Am 65 ............................................................................228
111. A9-0007/2024 - Herbert Dorfmann - Après l\'article 17 - Am 66 ............................................................................230
112. A9-0007/2024 - Herbert Dorfmann - Proposition de la Commission .....................................................................232
113. A9-0009/2024 - Anna Cavazzini - Proposition de la Commission.........................................................................234
114. A9-0032/2024 - Cornelia Ernst - Après le § 1 - Am 19..........................................................................................236
115. A9-0032/2024 - Cornelia Ernst - § 9, point 9.1, sous-point b ................................................................................238
116. A9-0032/2024 - Cornelia Ernst - § 9, point 9.2, sous-point b/1 .............................................................................240
117. A9-0032/2024 - Cornelia Ernst - § 10, point 10.2, sous-point a/2 .........................................................................242
118. A9-0032/2024 - Cornelia Ernst - § 11/3.................................................................................................................244
119. A9-0032/2024 - Cornelia Ernst - Proposition de résolution (ensemble du texte) ..................................................246
120. A9-0389/2023 - David McAllister - Après le § 1 - Am 19.......................................................................................248
121. A9-0389/2023 - David McAllister - § 4, point b - Am 100 ......................................................................................250
122. A9-0389/2023 - David McAllister - § 5/2................................................................................................................252
123. A9-0389/2023 - David McAllister - § 9 - Am 101 ...................................................................................................254
124. A9-0389/2023 - David McAllister - § 13.................................................................................................................256
125. A9-0389/2023 - David McAllister - § 14.................................................................................................................258
126. A9-0389/2023 - David McAllister - § 16 - Am 28 ...................................................................................................260
127. A9-0389/2023 - David McAllister - Après le § 18 - Am 29.....................................................................................262
128. A9-0389/2023 - David McAllister - § 19 - Am 105 .................................................................................................264
129. A9-0389/2023 - David McAllister - § 27, partie introductive/2 ...............................................................................266
130. A9-0389/2023 - David McAllister - § 27, point a - Am 110S..................................................................................268
131. A9-0389/2023 - David McAllister - § 27, point b - Am 111S..................................................................................270
132. A9-0389/2023 - David McAllister - § 27, point e - Am 114S..................................................................................272
133. A9-0389/2023 - David McAllister - § 36 - Am 40 ...................................................................................................274
134. A9-0389/2023 - David McAllister - Après le § 36 - Am 20.....................................................................................276
135. A9-0389/2023 - David McAllister - § 40 - Am 30 ...................................................................................................278
136. A9-0389/2023 - David McAllister - § 56/2..............................................................................................................280
137. A9-0389/2023 - David McAllister - Après le § 58 - Am 42.....................................................................................282
138. A9-0389/2023 - David McAllister - § 59 - Am 2/1 ..................................................................................................284
139. A9-0389/2023 - David McAllister - § 59 - Am 2/2 ..................................................................................................286
140. A9-0389/2023 - David McAllister - § 63 - Am 3 .....................................................................................................288
141. A9-0389/2023 - David McAllister - Après le § 64 - Am 4.......................................................................................290
142. A9-0389/2023 - David McAllister - § 66 - Am 116 .................................................................................................292
143. A9-0389/2023 - David McAllister - § 87 - Am 120 .................................................................................................294
144. A9-0389/2023 - David McAllister - Après le § 88 - Am 32/1..................................................................................296
145. A9-0389/2023 - David McAllister - Après le § 88 - Am 32/2..................................................................................298
146. A9-0389/2023 - David McAllister - Après le § 88 - Am 33.....................................................................................300
147. A9-0389/2023 - David McAllister - Après le § 88 - Am 34.....................................................................................302
148. A9-0389/2023 - David McAllister - Après le § 88 - Am 35.....................................................................................304
149. A9-0389/2023 - David McAllister - § 89 - Am 47/1 ................................................................................................306
150. A9-0389/2023 - David McAllister - § 89 - Am 47/2 ................................................................................................308
151. A9-0389/2023 - David McAllister - § 102 - Am 13 .................................................................................................310
152. A9-0389/2023 - David McAllister - § 103 - Am 14 .................................................................................................312
153. A9-0389/2023 - David McAllister - § 108 - Am 16 .................................................................................................314
154. A9-0389/2023 - David McAllister - § 110 - Am 18 .................................................................................................316
155. A9-0389/2023 - David McAllister - § 118 - Am 36 .................................................................................................318
156. A9-0389/2023 - David McAllister - § 118 - Am 125/1 ............................................................................................320
157. A9-0389/2023 - David McAllister - Après le considérant H - Am 46/1...................................................................322
158. A9-0389/2023 - David McAllister - Après le considérant H - Am 46/2...................................................................324
159. A9-0389/2023 - David McAllister - Considérant I - Am 96.....................................................................................326
160. A9-0389/2023 - David McAllister - Proposition de résolution (ensemble du texte) ...............................................328
161. A9-0403/2023 - Sven Mikser - Après le § 1 - Am 9...............................................................................................330
162. A9-0403/2023 - Sven Mikser - § 3 - Am 11/1 ........................................................................................................332
163. A9-0403/2023 - Sven Mikser - § 3 - Am 11/2 ........................................................................................................334
164. A9-0403/2023 - Sven Mikser - § 3 - Am 27/1 ........................................................................................................336
165. A9-0403/2023 - Sven Mikser - § 3 - Am 27/2 ........................................................................................................338
166. A9-0403/2023 - Sven Mikser - § 3 - Am 27/3 ........................................................................................................340
167. A9-0403/2023 - Sven Mikser - § 3 - Am 27/4 ........................................................................................................342
168. A9-0403/2023 - Sven Mikser - § 3 - Am 27/5 ........................................................................................................344
169. A9-0403/2023 - Sven Mikser - § 3 - Am 27/6 ........................................................................................................346
170. A9-0403/2023 - Sven Mikser - Après le § 3 - Am 12.............................................................................................348
171. A9-0403/2023 - Sven Mikser - Après le § 3 - Am 14/1..........................................................................................350
172. A9-0403/2023 - Sven Mikser - Après le § 3 - Am 14/2..........................................................................................352
173. A9-0403/2023 - Sven Mikser - Après le § 3 - Am 15.............................................................................................354
174. A9-0403/2023 - Sven Mikser - Après le § 3 - Am 16.............................................................................................356
175. A9-0403/2023 - Sven Mikser - Après le § 3 - Am 17.............................................................................................358
176. A9-0403/2023 - Sven Mikser - Après le § 5 - Am 3...............................................................................................360
177. A9-0403/2023 - Sven Mikser - Après le § 5 - Am 4...............................................................................................362
178. A9-0403/2023 - Sven Mikser - Après le § 5 - Am 5...............................................................................................364
179. A9-0403/2023 - Sven Mikser - Après le § 5 - Am 6...............................................................................................366
180. A9-0403/2023 - Sven Mikser - § 10 - Am 8 ...........................................................................................................368
181. A9-0403/2023 - Sven Mikser - Après le § 14 - Am 18...........................................................................................370
182. A9-0403/2023 - Sven Mikser - § 17 - Am 63 .........................................................................................................372
183. A9-0403/2023 - Sven Mikser - § 17/1....................................................................................................................374
184. A9-0403/2023 - Sven Mikser - § 17/2....................................................................................................................376
185. A9-0403/2023 - Sven Mikser - § 18 - Am 19 .........................................................................................................378
186. A9-0403/2023 - Sven Mikser - § 18.......................................................................................................................380
187. A9-0403/2023 - Sven Mikser - Après le § 18 - Am 20...........................................................................................382
188. A9-0403/2023 - Sven Mikser - § 19.......................................................................................................................384
189. A9-0403/2023 - Sven Mikser - § 21.......................................................................................................................386
190. A9-0403/2023 - Sven Mikser - § 25 - Am 69 .........................................................................................................388
191. A9-0403/2023 - Sven Mikser - § 44 - Am 72 .........................................................................................................390
192. A9-0403/2023 - Sven Mikser - § 46 - Am 73 .........................................................................................................392
193. A9-0403/2023 - Sven Mikser - § 56 - Am 2 ...........................................................................................................394
194. A9-0403/2023 - Sven Mikser - § 64 - Am 80 .........................................................................................................396
195. A9-0403/2023 - Sven Mikser - § 67 - Am 40 .........................................................................................................398
196. A9-0403/2023 - Sven Mikser - § 72 - Am 81 .........................................................................................................400
197. A9-0403/2023 - Sven Mikser - § 83 - Am 83 .........................................................................................................402
198. A9-0403/2023 - Sven Mikser - Après le § 90 - Am 85...........................................................................................404
199. A9-0403/2023 - Sven Mikser - Avant le considérant A - Am 10 ............................................................................406
200. A9-0403/2023 - Sven Mikser - Considérant I - Am 56...........................................................................................408
201. A9-0403/2023 - Sven Mikser - Considérant L - Am 24/1.......................................................................................410
202. A9-0403/2023 - Sven Mikser - Considérant L - Am 24/2.......................................................................................412
203. A9-0403/2023 - Sven Mikser - Considérant L - Am 24/3.......................................................................................414
204. A9-0403/2023 - Sven Mikser - Considérant L - Am 24/4.......................................................................................416
205. A9-0403/2023 - Sven Mikser - Considérant L - Am 24/5.......................................................................................418
206. A9-0403/2023 - Sven Mikser - Considérant O - Am 59.........................................................................................420
207. A9-0403/2023 - Sven Mikser - Considérant V - Am 1 ...........................................................................................422
208. A9-0403/2023 - Sven Mikser - Considérant AF - Am 61 .......................................................................................424
209. A9-0403/2023 - Sven Mikser - Proposition de résolution (ensemble du texte) .....................................................426
210. A9-0424/2023 - Nacho Sánchez Amor - Après le § 11 - Am 30............................................................................428
211. A9-0424/2023 - Nacho Sánchez Amor - Après le § 12 - Am 22............................................................................430
212. A9-0424/2023 - Nacho Sánchez Amor - Après le § 19 - Am 10............................................................................432
213. A9-0424/2023 - Nacho Sánchez Amor - § 24 - Am 11..........................................................................................434
214. A9-0424/2023 - Nacho Sánchez Amor - § 24 - Am 43..........................................................................................436
215. A9-0424/2023 - Nacho Sánchez Amor - Après le § 28 - Am 12............................................................................438
216. A9-0424/2023 - Nacho Sánchez Amor - Après le § 30 - Am 13............................................................................440
217. A9-0424/2023 - Nacho Sánchez Amor - § 31 - Am 38..........................................................................................442
218. A9-0424/2023 - Nacho Sánchez Amor - § 34 - Am 39..........................................................................................444
219. A9-0424/2023 - Nacho Sánchez Amor - Après le § 40 - Am 14............................................................................446
220. A9-0424/2023 - Nacho Sánchez Amor - Après le § 40 - Am 32............................................................................448
221. A9-0424/2023 - Nacho Sánchez Amor - § 44 - Am 15..........................................................................................450
222. A9-0424/2023 - Nacho Sánchez Amor - § 44 - Am 44..........................................................................................452
223. A9-0424/2023 - Nacho Sánchez Amor - § 44/2 ....................................................................................................454
224. A9-0424/2023 - Nacho Sánchez Amor - Après le § 44 - Am 16............................................................................456
225. A9-0424/2023 - Nacho Sánchez Amor - § 76 - Am 40..........................................................................................458
226. A9-0424/2023 - Nacho Sánchez Amor - § 76 - Am 46..........................................................................................460
227. A9-0424/2023 - Nacho Sánchez Amor - § 79/2 ....................................................................................................462
228. A9-0424/2023 - Nacho Sánchez Amor - § 85 - Am 47..........................................................................................464
229. A9-0424/2023 - Nacho Sánchez Amor - § 88/2 ....................................................................................................466
230. A9-0424/2023 - Nacho Sánchez Amor - Après le visa 8 - Am 31 .........................................................................468
231. A9-0424/2023 - Nacho Sánchez Amor - Considérant G - Am 36..........................................................................470
232. A9-0424/2023 - Nacho Sánchez Amor - Considérant G - Am 37..........................................................................472
233. A9-0424/2023 - Nacho Sánchez Amor - Considérant I - Am 42 ...........................................................................474
234. A9-0424/2023 - Nacho Sánchez Amor - Après le considérant I - Am 8 ................................................................476
235. A9-0424/2023 - Nacho Sánchez Amor - Proposition de résolution (ensemble du texte) ......................................478
236. A9-0031/2024 - David Cormand - § 31 - Am 1/1...................................................................................................480
237. A9-0031/2024 - David Cormand - § 31 - Am 1/2...................................................................................................482
238. A9-0031/2024 - David Cormand - § 31 - Am 1/3...................................................................................................484
239. A9-0031/2024 - David Cormand - Après le § 37 - Am 3........................................................................................486
240. A9-0031/2024 - David Cormand - Après le § 37 - Am 4........................................................................................488
241. A9-0031/2024 - David Cormand - Après le § 42 - Am 8........................................................................................490
242. A9-0031/2024 - David Cormand - § 44 - Am 2......................................................................................................492
243. A9-0031/2024 - David Cormand - Proposition de résolution (ensemble du texte) ................................................494
244. A9-0025/2024 - Sophia in \'t Veld - Proposition de résolution de remplacement - Am 1 .......................................496
245. A9-0025/2024 - Sophia in \'t Veld - § 2 - Am 3 .......................................................................................................498
246. A9-0025/2024 - Sophia in \'t Veld - Après le § 11 - Am 4.......................................................................................500
247. A9-0025/2024 - Sophia in \'t Veld - Après le § 11 - Am 5.......................................................................................502
248. A9-0025/2024 - Sophia in \'t Veld - Après le § 11 - Am 6.......................................................................................504
249. A9-0025/2024 - Sophia in \'t Veld - § 18 - Am 8 .....................................................................................................506
250. A9-0025/2024 - Sophia in \'t Veld - § 24/1 .............................................................................................................508
251. A9-0025/2024 - Sophia in \'t Veld - § 24/2 .............................................................................................................510
252. A9-0025/2024 - Sophia in \'t Veld - § 27 - Am 9 .....................................................................................................512
253. A9-0025/2024 - Sophia in \'t Veld - § 28 - Am 10 ...................................................................................................514
254. A9-0025/2024 - Sophia in \'t Veld - § 34 - Am 11 ...................................................................................................516
255. A9-0025/2024 - Sophia in \'t Veld - § 35/1 .............................................................................................................518
256. A9-0025/2024 - Sophia in \'t Veld - § 35/2 .............................................................................................................520
257. A9-0025/2024 - Sophia in \'t Veld - § 36 - Am 12 ...................................................................................................522
258. A9-0025/2024 - Sophia in \'t Veld - § 47 - Am 13 ...................................................................................................524
259. A9-0025/2024 - Sophia in \'t Veld - § 57 - Am 14 ...................................................................................................526
260. A9-0025/2024 - Sophia in \'t Veld - § 67/1 .............................................................................................................528
261. A9-0025/2024 - Sophia in \'t Veld - § 67/2 .............................................................................................................530
262. A9-0025/2024 - Sophia in \'t Veld - § 67/3 .............................................................................................................532
263. A9-0025/2024 - Sophia in \'t Veld - § 93 ................................................................................................................534
264. A9-0025/2024 - Sophia in \'t Veld - § 71/1 .............................................................................................................536
265. A9-0025/2024 - Sophia in \'t Veld - § 71/2 .............................................................................................................538
266. A9-0025/2024 - Sophia in \'t Veld - Après le § 71 - Am 15.....................................................................................540
267. A9-0025/2024 - Sophia in \'t Veld - Après le considérant E - Am 2........................................................................542
268. A9-0025/2024 - Sophia in \'t Veld - Proposition de résolution (ensemble du texte) ...............................................544
269. A9-0041/2024 - Nathalie Loiseau - § 1, point a.....................................................................................................546
270. A9-0041/2024 - Nathalie Loiseau - § 1, point b.....................................................................................................548
271. A9-0041/2024 - Nathalie Loiseau - § 1, point g - Am 19 .......................................................................................550
272. A9-0041/2024 - Nathalie Loiseau - § 1, point i - Am 20 ........................................................................................552
273. A9-0041/2024 - Nathalie Loiseau - § 1, point k .....................................................................................................554
274. A9-0041/2024 - Nathalie Loiseau - § 1, point r - Am 4 ..........................................................................................556
275. A9-0041/2024 - Nathalie Loiseau - § 1, point r - Am 23 ........................................................................................558
276. A9-0041/2024 - Nathalie Loiseau - § 1, point r/2...................................................................................................560
277. A9-0041/2024 - Nathalie Loiseau - § 1, point r/3...................................................................................................562
278. A9-0041/2024 - Nathalie Loiseau - Après le considérant B - Am 15.....................................................................564
279. A9-0041/2024 - Nathalie Loiseau - Après le considérant N - Am 17/1..................................................................566
280. A9-0041/2024 - Nathalie Loiseau - Après le considérant N - Am 17/2..................................................................568
281. A9-0041/2024 - Nathalie Loiseau - Après le considérant N - Am 17/3..................................................................570
282. A9-0041/2024 - Nathalie Loiseau - Proposition de résolution (ensemble du texte) ..............................................572';

    $rcv_toc['PV-9-2024-02-29-RCV'] = '1. A9-0012/2024 - Alessandra Mussolini - Projet de décision du Conseil.....................................................................5
2. A9-0043/2024 - Gabriel Mato - Projet de décision du Conseil ..................................................................................7
3. A9-0270/2023 - Kim Van Sparrentak - Accord provisoire - Am 84............................................................................9
4. A9-0011/2024 - Samira Rafaela - Projet de décision du Conseil ............................................................................11
5. A9-0010/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - Projet de décision du Conseil ......................13
6. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - § 11/1 ...........................................................15
7. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - § 11/2 ...........................................................17
8. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - § 31/1 ...........................................................19
9. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - § 31/2 ...........................................................21
10. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - § 34 - Am 5 ..................................................23
11. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - Après le § 34 - Am 6 ....................................25
12. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - Après le § 36 - Am 8 ....................................27
13. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - § 49/1 ...........................................................29
14. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - § 49/2 ...........................................................31
15. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - Après le considérant G - Am 4.....................33
16. A9-0017/2024 - María Soraya Rodríguez Ramos, Samira Rafaela - Proposition de résolution (ensemble du texte) ..35
17. A9-0261/2023 - Maria da Graça Carvalho - Accord provisoire - Am 2....................................................................37
18. A9-0038/2023 - Romana Jerković - Rejet - Am 8....................................................................................................39
19. A9-0038/2023 - Romana Jerković - Demande de mettre aux voix les amendements au projet d\'acte législatif....41
20. A9-0038/2023 - Romana Jerković - Accord provisoire - Am 6 ................................................................................43
21. RC-B9-0147/2024 - § 6/1 ........................................................................................................................................45
22. RC-B9-0147/2024 - § 6/2 ........................................................................................................................................47
23. RC-B9-0147/2024 - § 12/1 ......................................................................................................................................49
24. RC-B9-0147/2024 - § 12/2 ......................................................................................................................................51
25. RC-B9-0147/2024 - § 22 - Am 3..............................................................................................................................53
26. RC-B9-0147/2024 - Proposition de résolution (ensemble du texte)........................................................................55
27. RC-B9-0143/2024 - Après le § 2 - Am 6 .................................................................................................................57
28. RC-B9-0143/2024 - Après le § 2 - Am 7 .................................................................................................................59
29. RC-B9-0143/2024 - § 7/2 ........................................................................................................................................61
30. RC-B9-0143/2024 - § 10 .........................................................................................................................................63
31. RC-B9-0143/2024 - § 11 - Am 8..............................................................................................................................65
32. RC-B9-0143/2024 - § 11 .........................................................................................................................................67
33. RC-B9-0143/2024 - § 12 .........................................................................................................................................69
34. RC-B9-0143/2024 - Après le § 19 - Am 9 ...............................................................................................................71
35. RC-B9-0143/2024 - § 21 - Am 1/1...........................................................................................................................73
36. RC-B9-0143/2024 - § 21 - Am 1/2...........................................................................................................................75
37. RC-B9-0143/2024 - § 21 - Am 1/3...........................................................................................................................77
38. RC-B9-0143/2024 - § 21 - Am 1/4...........................................................................................................................79
39. RC-B9-0143/2024 - Après le § 21 - Am 3 ...............................................................................................................81
40. RC-B9-0143/2024 - § 22 - Am 10............................................................................................................................83
41. RC-B9-0143/2024 - § 25 - Am 11............................................................................................................................85
42. RC-B9-0143/2024 - § 27 - Am 12............................................................................................................................87
43. RC-B9-0143/2024 - § 28 - Am 13............................................................................................................................89
44. RC-B9-0143/2024 - Après le § 28 - Am 14 .............................................................................................................91
45. RC-B9-0143/2024 - Après le § 28 - Am 15 .............................................................................................................93
46. RC-B9-0143/2024 - § 36 - Am 16............................................................................................................................95
47. RC-B9-0143/2024 - Après le considérant N - Am 5 ................................................................................................97
48. RC-B9-0143/2024 - Après le considérant R - Am 2 ................................................................................................99
49. RC-B9-0143/2024 - Proposition de résolution (ensemble du texte)......................................................................101
50. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - § 17/2........................................................................103
51. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - § 31/2........................................................................105
52. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - § 31/3........................................................................107
53. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - § 32/2........................................................................109
54. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - § 33/2........................................................................111
55. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - § 35/2........................................................................113
56. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - § 40/2........................................................................115
57. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - Considérant U/2........................................................117
58. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - Considérant AA/2 .....................................................119
59. A9-0015/2024 - Petras Auštrevičius, Pedro Silva Pereira - Proposition de résolution (ensemble du texte) .........121
60. A9-0024/2024 - Joachim Schuster - Proposition de résolution (ensemble du texte) ............................................123
61. RC-B9-0144/2024 - § 1 .........................................................................................................................................125
62. RC-B9-0144/2024 - § 2 .........................................................................................................................................127
63. RC-B9-0144/2024 - Après le § 3 - Am 1 ...............................................................................................................129
64. RC-B9-0144/2024 - § 4/1 ......................................................................................................................................131
65. RC-B9-0144/2024 - § 4/2 ......................................................................................................................................133
66. RC-B9-0144/2024 - § 4/3 ......................................................................................................................................135
67. RC-B9-0144/2024 - § 6/1 ......................................................................................................................................137
68. RC-B9-0144/2024 - § 6/2 ......................................................................................................................................139
69. RC-B9-0144/2024 - § 7 .........................................................................................................................................141
70. RC-B9-0144/2024 - § 8/2 ......................................................................................................................................143
71. RC-B9-0144/2024 - Après le § 8 - Am 2 ...............................................................................................................145
72. RC-B9-0144/2024 - § 9 .........................................................................................................................................147
73. RC-B9-0144/2024 - § 10 .......................................................................................................................................149
74. RC-B9-0144/2024 - Après le § 10 - Am 3 .............................................................................................................151
75. RC-B9-0144/2024 - Après le § 10 - Am 4 .............................................................................................................153
76. RC-B9-0144/2024 - § 11 .......................................................................................................................................155
77. RC-B9-0144/2024 - Proposition de résolution (ensemble du texte)......................................................................157';

    $rcv_toc['PV-9-2024-03-12-RCV'] = '1. B9-0161/2024 - Proposition de résolution .................................................................................................................4
2. A9-0216/2023 - Radan Kanev - Rejet - Am 314........................................................................................................6
3. A9-0216/2023 - Radan Kanev - Demande de mettre au voix les amendements au projet d\'acte législatif .............8
4. A9-0216/2023 - Radan Kanev - Accord provisoire - Am 313 ..................................................................................10
5. A9-0211/2023 - Radan Kanev - Accord provisoire - Am 60 ....................................................................................12
6. A9-0235/2023 - Sophia in \'t Veld - Accord provisoire - Am 92 ................................................................................14
7. A9-0047/2024 - Isabel García Muñoz - Article 1, alinéa 1, point 3, sous-point b; Directive 96/53/CE; Article 4, paragraphe 4, alinéa 2, point b - Am 62 .................................................................................................................16
8. A9-0047/2024 - Isabel García Muñoz - Article 1, alinéa 1, point 3, sous-point c; Directive 96/53/CE; Article 4, paragraphe 4 bis, alinéa 1 - Am 63S......................................................................................................................18
9. A9-0047/2024 - Isabel García Muñoz - Article 1, alinéa 1, point 3, sous-point c; Directive 96/53/CE; Article 4, paragraphe 4 bis, alinéa 1, partie introductive - Am 52..........................................................................................20
10. A9-0047/2024 - Isabel García Muñoz - Article 1, alinéa 1, point 3, sous-point c; Directive 96/53/CE; Article 4, paragraphe 4 bis, alinéa 1, après le point d - Am 54...............................................................................................22
11. A9-0047/2024 - Isabel García Muñoz - Article 1, alinéa 1, point 3, sous-point c; Directive 96/53/CE; Article 4, paragraphe 4 bis, alinéa 2 - Am 56S.......................................................................................................................24
12. A9-0047/2024 - Isabel García Muñoz - Article 1, alinéa 1, point 4; Directive 96/53/CE; Article 4 ter - Am 58S=
65S= ........................................................................................................................................................................26
13. A9-0047/2024 - Isabel García Muñoz - Considérant 9 - Am 61S............................................................................28
14. A9-0047/2024 - Isabel García Muñoz - Proposition de la Commission...................................................................30
15. A9-0069/2024 - Tilly Metz - Proposition de la Commission et amendements.........................................................32
16. A9-0026/2024 - Othmar Karas - Proposition de la Commission et amendement ...................................................34
17. A9-0033/2023 - Ciarán Cuffe - Rejet - Am 69 .........................................................................................................36
18. A9-0033/2023 - Ciarán Cuffe - Accord provisoire - Am 68......................................................................................38
19. A9-0253/2023 - Nicola Danti - Accord provisoire - Am 2.........................................................................................40
20. A9-0056/2024 - Cyrus Engerer, Andrus Ansip - Amendements de la commission compétente - vote séparé - Am
48 ............................................................................................................................................................................42
21. A9-0056/2024 - Cyrus Engerer, Andrus Ansip - Amendements de la commission compétente - vote séparé - Am
145 ..........................................................................................................................................................................44
22. A9-0056/2024 - Cyrus Engerer, Andrus Ansip - Proposition de la Commission .....................................................46
23. A9-0291/2023 - Vlad-Marius Botoş, Pascal Arimont - Accord provisoire - Am 118 ................................................48
24. A9-0330/2023 - Maria Grapini - Accord provisoire - Am 36 ....................................................................................50
25. A9-0423/2023 - Cláudia Monteiro de Aguiar - Proposition de la Commission et amendements ............................52
26. A9-0040/2024 - Cindy Franssen, Kira Marie Peter-Hansen - Projet de décision du Conseil ..................................54';

    $rcv_toc['PV-9-2024-03-13-RCV'] = '1. A9-0264/2023 - Sabine Verheyen - Demande de mettre aux voix les amendements au projet d\'acte législatif.......6
2. A9-0264/2023 - Sabine Verheyen - Accord provisoire - Am 332 ..............................................................................8
3. A9-0188/2023 - Brando Benifei, Dragoş Tudorache - Accord provisoire - Am 808.................................................10
4. A9-0060/2024 - Laura Ballarín Cereza - Proposition de la Commission et amendements .....................................12
5. A9-0058/2024 - Laura Ballarín Cereza - Proposition de Commission.....................................................................14
6. A9-0199/2023 - Loránt Vincze - Accord provisoire - Am 152 ..................................................................................16
7. A9-0006/2024 - Anne-Sophie Pelletier - Proposition de la Commission et amendements .....................................18
8. A9-0042/2024 - Adrián Vázquez Lázara - Article 11 - Am 104 ...............................................................................20
9. A9-0042/2024 - Adrián Vázquez Lázara - Article 12, § 3 - Am 105 ........................................................................22
10. A9-0042/2024 - Adrián Vázquez Lázara - Article 12, § 5 - Am 106 ........................................................................24
11. A9-0042/2024 - Adrián Vázquez Lázara - Article 12, § 6, partie introductive - Am 107 ..........................................26
12. A9-0042/2024 - Adrián Vázquez Lázara - Article 12, § 6, point a - Am 108............................................................28
13. A9-0042/2024 - Adrián Vázquez Lázara - Considérant 3 - Am 100 ........................................................................30
14. A9-0042/2024 - Adrián Vázquez Lázara - Considérant 28 - Am 101 ......................................................................32
15. A9-0042/2024 - Adrián Vázquez Lázara - Considérant 29 - Am 102 ......................................................................34
16. A9-0042/2024 - Adrián Vázquez Lázara - Considérant 30 - Am 103 ......................................................................36
17. A9-0042/2024 - Adrián Vázquez Lázara - Proposition de la Commission ..............................................................38
18. A9-0044/2024 - Marion Walsmann - Proposition de la Commission.......................................................................40
19. A9-0055/2024 - Anna Zalewska - Article 1, § 1, point 4; Directive 2008/98/CE; Article 9 bis, § 4 - Am 117...........42
20. A9-0055/2024 - Anna Zalewska - Article 1, § 1, point 4; Directive 2008/98/CE; Article 9 bis, § 4, point b - Am 118 ..44
21. A9-0055/2024 - Anna Zalewska - Article 1, § 1, point 7; Directive 2008/98/CE; Article 22 quinquies, après le § 1
et après l\'article 22 quinquies - Am 119 ..................................................................................................................46
22. A9-0055/2024 - Anna Zalewska - Proposition de la Commission ...........................................................................48
23. A9-0140/2023 - Javier Moreno Sánchez - Accord provisoire - Am 117 ..................................................................50
24. A9-0062/2024 - Sergey Lagodinsky - Proposition de la Commission .....................................................................52
25. A9-0033/2024 - Jan-Christoph Oetjen - Proposition de la Commission et amendements ......................................54
26. A9-0034/2024 - Jan-Christoph Oetjen - Proposition de la Commission et amendements ......................................56
27. A9-0370/2023 - Henna Virkkunen - Accord provisoire - Am 30 ..............................................................................58
28. A9-0065/2024 - Deirdre Clune - Avant l\'article 147 - Am 299 .................................................................................60
29. A9-0065/2024 - Deirdre Clune - Proposition de la Commission..............................................................................62
30. A9-0386/2023 - Johan Van Overtveldt - Accord provisoire - Am 2 .........................................................................64
31. A9-0298/2023 - Alexandr Vondra - Accord provisoire - Am 247 .............................................................................66
32. A9-0077/2024 - Sandra Kalniete - Rejet - Am 25....................................................................................................68
33. A9-0077/2024 - Sandra Kalniete - Article 1, après le § 2 - Am 19 ..........................................................................70
34. A9-0077/2024 - Sandra Kalniete - Article 4, § 1, alinéa 1 - Am 27..........................................................................72
35. A9-0077/2024 - Sandra Kalniete - Article 4, § 1, alinéa 1 - Am 3............................................................................74
36. A9-0077/2024 - Sandra Kalniete - mesures de sauvegarde - Am 12, 14, 15..........................................................76
37. A9-0077/2024 - Sandra Kalniete - mesures de sauvegarde - Am 23,24= 26,28= ..................................................78
38. A9-0077/2024 - Sandra Kalniete - Article 4, § 7, alinéa 1, point a - Am 5...............................................................80
39. A9-0077/2024 - Sandra Kalniete - Article 4, après le § 8 - Am 9 ............................................................................82
40. A9-0077/2024 - Sandra Kalniete - Proposition de la Commission ..........................................................................84
41. A9-0079/2024 - Markéta Gregorová - Proposition de la Commission.....................................................................86
42. A9-0063/2024 - René Repasi - Avant le § 1 - Am 12 ..............................................................................................88
43. A9-0063/2024 - René Repasi - Avant le § 1 - Am 13 ..............................................................................................90
44. A9-0063/2024 - René Repasi - Avant le § 1 - Am 14 ..............................................................................................92
45. A9-0063/2024 - René Repasi - § 3 - Am 16 ............................................................................................................94
46. A9-0063/2024 - René Repasi - Après le § 9 - Am 3................................................................................................96
47. A9-0063/2024 - René Repasi - Après le § 11 - Am 4..............................................................................................98
48. A9-0063/2024 - René Repasi - § 12 - Am 17 ........................................................................................................100
49. A9-0063/2024 - René Repasi - Après le § 12 - Am 15..........................................................................................102
50. A9-0063/2024 - René Repasi - § 15 - Am 18 ........................................................................................................104
51. A9-0063/2024 - René Repasi - Après le § 17 - Am 5............................................................................................106
52. A9-0063/2024 - René Repasi - Après le § 17 - Am 6............................................................................................108
53. A9-0063/2024 - René Repasi - Après le § 17 - Am 7............................................................................................110
54. A9-0063/2024 - René Repasi - Après le § 17 - Am 8............................................................................................112
55. A9-0063/2024 - René Repasi - Après le § 17 - Am 9............................................................................................114
56. A9-0063/2024 - René Repasi - Après le § 17 - Am 10..........................................................................................116
57. A9-0063/2024 - René Repasi - Après le § 17 - Am 11..........................................................................................118
58. A9-0063/2024 - René Repasi - Après le considérant A - Am 1.............................................................................120
59. A9-0063/2024 - René Repasi - Après le considérant H - Am 2.............................................................................122
60. A9-0063/2024 - René Repasi - Proposition de résolution (ensemble du texte) ....................................................124
61. A9-0050/2024 - Dragoş Pîslaru - Après le § 1 - Am 7...........................................................................................126
62. A9-0050/2024 - Dragoş Pîslaru - Après le § 1 - Am 8/1........................................................................................128
63. A9-0050/2024 - Dragoş Pîslaru - Après le § 1 - Am 8/2........................................................................................130
64. A9-0050/2024 - Dragoş Pîslaru - Après le § 1 - Am 8/3........................................................................................132
65. A9-0050/2024 - Dragoş Pîslaru - Après le § 3 - Am 6...........................................................................................134
66. A9-0050/2024 - Dragoş Pîslaru - § 4 - Am 2 .........................................................................................................136
67. A9-0050/2024 - Dragoş Pîslaru - § 5/2..................................................................................................................138
68. A9-0050/2024 - Dragoş Pîslaru - Après le § 7 - Am 9/1........................................................................................140
69. A9-0050/2024 - Dragoş Pîslaru - Après le § 7 - Am 9/2........................................................................................142
70. A9-0050/2024 - Dragoş Pîslaru - Après le § 7 - Am 9/3........................................................................................144
71. A9-0050/2024 - Dragoş Pîslaru - Après le § 7 - Am 9/4........................................................................................146
72. A9-0050/2024 - Dragoş Pîslaru - Après le § 8 - Am 10.........................................................................................148
73. A9-0050/2024 - Dragoş Pîslaru - § 9/1..................................................................................................................150
74. A9-0050/2024 - Dragoş Pîslaru - § 9/2..................................................................................................................152
75. A9-0050/2024 - Dragoş Pîslaru - Après le § 9 - Am 11/1......................................................................................154
76. A9-0050/2024 - Dragoş Pîslaru - Après le § 9 - Am 11/2......................................................................................156
77. A9-0050/2024 - Dragoş Pîslaru - § 10/3................................................................................................................158
78. A9-0050/2024 - Dragoş Pîslaru - § 11/1................................................................................................................160
79. A9-0050/2024 - Dragoş Pîslaru - § 11/2................................................................................................................162
80. A9-0050/2024 - Dragoş Pîslaru - § 12 - Am 3 .......................................................................................................164
81. A9-0050/2024 - Dragoş Pîslaru - § 14/2................................................................................................................166
82. A9-0050/2024 - Dragoş Pîslaru - § 15/2................................................................................................................168
83. A9-0050/2024 - Dragoş Pîslaru - § 16/2................................................................................................................170
84. A9-0050/2024 - Dragoş Pîslaru - Considérant K/1................................................................................................172
85. A9-0050/2024 - Dragoş Pîslaru - Considérant M ..................................................................................................174
86. A9-0050/2024 - Dragoş Pîslaru - Considérant R - Am 1 .......................................................................................176
87. A9-0050/2024 - Dragoş Pîslaru - Proposition de résolution (ensemble du texte) .................................................178
88. A9-0068/2024 - Victor Negrescu - Avant le § 1 - Am 4 .........................................................................................180
89. A9-0068/2024 - Victor Negrescu - § 1 - Am 21 .....................................................................................................182
90. A9-0068/2024 - Victor Negrescu - § 4 - Am 22 .....................................................................................................184
91. A9-0068/2024 - Victor Negrescu - § 9/2................................................................................................................186
92. A9-0068/2024 - Victor Negrescu - § 12 - Am 23 ...................................................................................................188
93. A9-0068/2024 - Victor Negrescu - Après le § 12 - Am 5 .......................................................................................190
94. A9-0068/2024 - Victor Negrescu - Après le § 12 - Am 6 .......................................................................................192
95. A9-0068/2024 - Victor Negrescu - Après le § 12 - Am 24 .....................................................................................194
96. A9-0068/2024 - Victor Negrescu - § 18 - Am 25 ...................................................................................................196
97. A9-0068/2024 - Victor Negrescu - § 24 - Am 26 ...................................................................................................198
98. A9-0068/2024 - Victor Negrescu - Après le § 24 - Am 7 .......................................................................................200
99. A9-0068/2024 - Victor Negrescu - Après le § 24 - Am 31 .....................................................................................202
100. A9-0068/2024 - Victor Negrescu - Après le § 26 - Am 8 .......................................................................................204
101. A9-0068/2024 - Victor Negrescu - Avant le § 27 - Am 10 .....................................................................................206
102. A9-0068/2024 - Victor Negrescu - Après le § 29 - Am 12 .....................................................................................208
103. A9-0068/2024 - Victor Negrescu - Après le § 31 - Am 13 .....................................................................................210
104. A9-0068/2024 - Victor Negrescu - Après le § 33 - Am 14 .....................................................................................212
105. A9-0068/2024 - Victor Negrescu - Après le § 33 - Am 15 .....................................................................................214
106. A9-0068/2024 - Victor Negrescu - Après le § 33 - Am 32 .....................................................................................216
107. A9-0068/2024 - Victor Negrescu - Après le § 35 - Am 33 .....................................................................................218
108. A9-0068/2024 - Victor Negrescu - Après le § 36 - Am 27 .....................................................................................220
109. A9-0068/2024 - Victor Negrescu - § 40.................................................................................................................222
110. A9-0068/2024 - Victor Negrescu - Après le § 40 - Am 34 .....................................................................................224
111. A9-0068/2024 - Victor Negrescu - Après le § 42 - Am 35 .....................................................................................226
112. A9-0068/2024 - Victor Negrescu - Après le § 42 - Am 40 .....................................................................................228
113. A9-0068/2024 - Victor Negrescu - Après le § 44 - Am 39 .....................................................................................230
114. A9-0068/2024 - Victor Negrescu - Après le § 45 - Am 16 .....................................................................................232
115. A9-0068/2024 - Victor Negrescu - § 46 - Am 41 ...................................................................................................234
116. A9-0068/2024 - Victor Negrescu - § 46/1..............................................................................................................236
117. A9-0068/2024 - Victor Negrescu - § 46/2..............................................................................................................238
118. A9-0068/2024 - Victor Negrescu - Après le § 46 - Am 36 .....................................................................................240
119. A9-0068/2024 - Victor Negrescu - Après le § 46 - Am 17 .....................................................................................242
120. A9-0068/2024 - Victor Negrescu - Après le § 47 - Am 18 .....................................................................................244
121. A9-0068/2024 - Victor Negrescu - Après le § 47 - Am 19 .....................................................................................246
122. A9-0068/2024 - Victor Negrescu - Après le § 50 - Am 38 .....................................................................................248
123. A9-0068/2024 - Victor Negrescu - Proposition de résolution (ensemble du texte) ...............................................250
124. RC-B9-0163/2024 - § 4/1 ......................................................................................................................................252
125. RC-B9-0163/2024 - § 4/2 ......................................................................................................................................254
126. RC-B9-0163/2024 - § 10/1 ....................................................................................................................................256
127. RC-B9-0163/2024 - § 10/2 ....................................................................................................................................258
128. RC-B9-0163/2024 - § 10/3 ....................................................................................................................................260
129. RC-B9-0163/2024 - § 17 - Am 2............................................................................................................................262
130. RC-B9-0163/2024 - § 18 - Am 3/1.........................................................................................................................264
131. RC-B9-0163/2024 - § 18 - Am 3/2.........................................................................................................................266
132. RC-B9-0163/2024 - § 18 .......................................................................................................................................268
133. RC-B9-0163/2024 - Proposition de résolution (ensemble du texte)......................................................................270';


    $rcv_toc['PV-9-2024-03-14-RCV'] = '1. RC-B9-0187/2024 - § 1 - Am 23................................................................................................................................6
2. RC-B9-0187/2024 - § 3 - Am 6= 27= ........................................................................................................................8
3. RC-B9-0187/2024 - § 3 - Am 19..............................................................................................................................10
4. RC-B9-0187/2024 - Après le § 3 - Am 24 ...............................................................................................................12
5. RC-B9-0187/2024 - § 4 - Am 1................................................................................................................................14
6. RC-B9-0187/2024 - § 4 - Am 17..............................................................................................................................16
7. RC-B9-0187/2024 - § 4 - Am 25/1...........................................................................................................................18
8. RC-B9-0187/2024 - § 4 - Am 25/2...........................................................................................................................20
9. RC-B9-0187/2024 - § 4 - Am 25/3...........................................................................................................................22
10. RC-B9-0187/2024 - § 4 - Am 25/4...........................................................................................................................24
11. RC-B9-0187/2024 - Après le § 4 - Am 8 .................................................................................................................26
12. RC-B9-0187/2024 - Après le § 4 - Am 16 ...............................................................................................................28
13. RC-B9-0187/2024 - Après le § 4 - Am 7/1 ..............................................................................................................30
14. RC-B9-0187/2024 - Après le § 4 - Am 7/2 ..............................................................................................................32
15. RC-B9-0187/2024 - Après le § 4 - Am 12 ...............................................................................................................34
16. RC-B9-0187/2024 - § 5/2 ........................................................................................................................................36
17. RC-B9-0187/2024 - Après le § 5 - Am 18 ...............................................................................................................38
18. RC-B9-0187/2024 - § 6 - Am 3................................................................................................................................40
19. RC-B9-0187/2024 - § 6 - Am 14..............................................................................................................................42
20. RC-B9-0187/2024 - Après le § 6 - Am 4 .................................................................................................................44
21. RC-B9-0187/2024 - Après le § 6 - Am 15 ...............................................................................................................46
22. RC-B9-0187/2024 - Après le § 6 - Am 5 .................................................................................................................48
23. RC-B9-0187/2024 - § 7 - Am 20..............................................................................................................................50
24. RC-B9-0187/2024 - Après le § 7 - Am 26 ...............................................................................................................52
25. RC-B9-0187/2024 - Après le considérant A - Am 21 ..............................................................................................54
26. RC-B9-0187/2024 - Considérant D - Am 22............................................................................................................56
27. RC-B9-0187/2024 - Après le considérant D - Am 9 ................................................................................................58
28. RC-B9-0187/2024 - Proposition de résolution (ensemble du texte)........................................................................60
29. RC-B9-0175/2024 - Après le § 2 - Am 4 .................................................................................................................62
30. RC-B9-0175/2024 - Après le § 2 - Am 5 .................................................................................................................64
31. RC-B9-0175/2024 - Après le § 3 - Am 1 .................................................................................................................66
32. RC-B9-0175/2024 - Après le § 3 - Am 2 .................................................................................................................68
33. RC-B9-0175/2024 - Après le § 8 - Am 8 .................................................................................................................70
34. RC-B9-0175/2024 - Proposition de résolution (ensemble du texte)........................................................................72
35. RC-B9-0179/2024 - Proposition de résolution (ensemble du texte)........................................................................74
36. C9-0009/2024 - Proposition de la Commission.......................................................................................................76
37. A9-0180/2023 - Monika Hohlmeier, Nils Ušakovs - Accord provisoire - Am 179 ....................................................78
38. A9-0315/2023 - Gilles Lebreton - Accord provisoire - Am 40..................................................................................80
39. A9-0317/2023 - Gilles Lebreton - Accord provisoire - Am 22..................................................................................82
40. A9-0037/2024 - Ivan Vilibor Sinčić - Proposition de la Commission et amendements............................................84
41. A9-0038/2024 - Ivan Vilibor Sinčić - Proposition de la Commission .......................................................................86
42. A9-0081/2024 - Danilo Oscar Lancini - Projet de décision du Conseil....................................................................88
43. A9-0036/2024 - Lena Düpont - Projet de décision du Conseil ................................................................................90
44. RC-B9-0169/2024 - § 1 - Am 7................................................................................................................................92
45. RC-B9-0169/2024 - § 1 ...........................................................................................................................................94
46. RC-B9-0169/2024 - § 2 - Am 8................................................................................................................................96
47. RC-B9-0169/2024 - § 2 ...........................................................................................................................................98
48. RC-B9-0169/2024 - § 4 - Am 9..............................................................................................................................100
49. RC-B9-0169/2024 - § 4 .........................................................................................................................................102
50. RC-B9-0169/2024 - Après le § 7 - Am 10 .............................................................................................................104
51. RC-B9-0169/2024 - Après le § 10 - Am 12 ...........................................................................................................106
52. RC-B9-0169/2024 - § 12 - Am 11..........................................................................................................................108
53. RC-B9-0169/2024 - § 12/1 ....................................................................................................................................110
54. RC-B9-0169/2024 - § 12/2 ....................................................................................................................................112
55. RC-B9-0169/2024 - § 13 - Am 13..........................................................................................................................114
56. RC-B9-0169/2024 - § 13 .......................................................................................................................................116
57. RC-B9-0169/2024 - § 14 - Am 14..........................................................................................................................118
58. RC-B9-0169/2024 - § 14/1 ....................................................................................................................................120
59. RC-B9-0169/2024 - § 14/2 ....................................................................................................................................122
60. RC-B9-0169/2024 - § 15 - Am 15..........................................................................................................................124
61. RC-B9-0169/2024 - § 15/1 ....................................................................................................................................126
62. RC-B9-0169/2024 - § 15/2 ....................................................................................................................................128
63. RC-B9-0169/2024 - § 16 .......................................................................................................................................130
64. RC-B9-0169/2024 - Après le visa 14 - Am 1 .........................................................................................................132
65. RC-B9-0169/2024 - Après le considérant K - Am 2 ..............................................................................................134
66. RC-B9-0169/2024 - Après le considérant K - Am 3 ..............................................................................................136
67. RC-B9-0169/2024 - Après le considérant K - Am 4 ..............................................................................................138
68. RC-B9-0169/2024 - Après le considérant K - Am 5 ..............................................................................................140
69. RC-B9-0169/2024 - Après le considérant K - Am 6 ..............................................................................................142
70. B9-0162/2024 - Après le § 2 - Am 14....................................................................................................................144
71. B9-0162/2024 - Après le § 4 - Am 5......................................................................................................................146
72. B9-0162/2024 - Après le § 4 - Am 6......................................................................................................................148
73. B9-0162/2024 - Après le § 4 - Am 8......................................................................................................................150
74. B9-0162/2024 - Après le § 4 - Am 10....................................................................................................................152
75. B9-0162/2024 - Après le § 4 - Am 11....................................................................................................................154
76. B9-0162/2024 - Après le § 5 - Am 9......................................................................................................................156
77. B9-0162/2024 - Après le § 6 - Am 12....................................................................................................................158
78. B9-0162/2024 - Après le § 7 - Am 1......................................................................................................................160
79. B9-0162/2024 - Après le § 10 - Am 2....................................................................................................................162
80. B9-0162/2024 - § 11 - Am 4 ..................................................................................................................................164
81. B9-0162/2024 - § 11 - Am 3 ..................................................................................................................................166
82. B9-0162/2024 - Après le § 11 - Am 13..................................................................................................................168
83. B9-0162/2024 - Après le visa 9 - Am 7 .................................................................................................................170
84. B9-0162/2024 - Proposition de résolution (ensemble du texte) ............................................................................172
85. B9-0174/2024 - § 1/2.............................................................................................................................................174
86. B9-0174/2024 - § 2/2.............................................................................................................................................176
87. B9-0174/2024 - § 5................................................................................................................................................178
88. B9-0174/2024 - § 6................................................................................................................................................180
89. B9-0174/2024 - § 7/2.............................................................................................................................................182
90. B9-0174/2024 - § 11..............................................................................................................................................184
91. A9-0049/2024 - Andrey Novakov - Proposition de résolution ...............................................................................186
92. B9-0173/2024 - Après le § 13 - Am 2....................................................................................................................188
93. B9-0173/2024 - Après le considérant N - Am 1.....................................................................................................190';

    $rcv_toc['PV-9-2024-04-10-RCV'] = '1. Ordre du jour de mercredi - Demande du groupe ECR (A9-0138/2024) .....................................................................9
1.1 Ordre du jour de mercredi - Demande du groupe ECR (A9-0138/2024) ................................................................9
2. Ordre du jour de mercredi - Demande du groupe ECR (A9-0329/2023) ...................................................................11
2.1 Ordre du jour de mercredi - Demande du groupe ECR (A9-0329/2023, report du vote)......................................11
3. Ordre du jour de mercredi - Demande du groupe ECR (ajout de déclarations du Conseil et de la Commission) .....13
3.1 Ordre du jour de mercredi - Demande du groupe ECR (ajout de déclarations du Conseil et de la Commission) ..13
4. Ordre du jour de mercredi - Demande du groupe Verts/ALE (Russie) ......................................................................15
4.1 Ordre du jour de mercredi - Demande du groupe Verts/ALE (Russie) .................................................................15
5. Ordre du jour de mercredi - Demande du groupe ID .................................................................................................17
5.1 Ordre du jour de mercredi - Demande du groupe ID ............................................................................................17
6. Ordre du jour de mercredi - Demande du groupe Verts/ALE (Hongrie).....................................................................19
6.1 Ordre du jour de mercredi - Demande du groupe Verts/ALE (Hongrie) ...............................................................19
7. Ordre du jour de jeudi - Demande du groupe ID........................................................................................................20
7.1 Ordre du jour de jeudi - Demande du groupe ID ..................................................................................................20
8. Modifications au règlement intérieur du Parlement mettant en œuvre la réforme parlementaire «Parlement 2024» ..22
8.1 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 16/1......22
8.2 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 16/2......24
8.3 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 73.........26
8.4 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 75/1......28
8.5 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 75/2......30
8.6 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 91/1......32
8.7 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 91/2......34
8.8 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 92.........36
8.9 A9-0158/2024 - Salvatore De Meo - Amendements de la commission compétente - vote séparé - Am 93.........38
8.10 A9-0158/2024 - Salvatore De Meo - Article 15 - Am 144 ...................................................................................40
8.11 A9-0158/2024 - Salvatore De Meo - Après l\'article 15 - Am 142........................................................................42
8.12 A9-0158/2024 - Salvatore De Meo - Article 51, § 2 - Am 25/1 ...........................................................................44
8.13 A9-0158/2024 - Salvatore De Meo - Article 51, § 2 - Am 25/2 ...........................................................................46
8.14 A9-0158/2024 - Salvatore De Meo - Article 56, après le § 8 - Am 119...............................................................48
8.15 A9-0158/2024 - Salvatore De Meo - Après le § 56 - Am 120 .............................................................................50
8.16 A9-0158/2024 - Salvatore De Meo - Article 57 - Am 41 .....................................................................................52
8.17 A9-0158/2024 - Salvatore De Meo - Article 48, § 1 - Am 15 ..............................................................................54
8.18 A9-0158/2024 - Salvatore De Meo - Article 171, § 7 - Am 123 ..........................................................................56
8.19 A9-0158/2024 - Salvatore De Meo - Après l\'article 207 - Am 124......................................................................58
8.20 A9-0158/2024 - Salvatore De Meo - Après l\'article 207 - Am 125......................................................................60
8.21 A9-0158/2024 - Salvatore De Meo - Article 208, § 1 - Am 138 ..........................................................................62
8.22 A9-0158/2024 - Salvatore De Meo - Article 208, § 4 - Am 134 ..........................................................................64
8.23 A9-0158/2024 - Salvatore De Meo - Article 208, § 12 - Am 139 ........................................................................66
8.24 A9-0158/2024 - Salvatore De Meo - Après l\'article 207 et l\'article 209, paragraphe 2 - Am 147= 148= ............68
8.25 A9-0158/2024 - Salvatore De Meo - Proposition de décision - Am 143 .............................................................70
8.26 A9-0158/2024 - Salvatore De Meo - Proposition de décision (ensemble du texte) ............................................72
9. Procédure commune de protection internationale dans l’Union ***I ..........................................................................74
9.1 A8-0171/2018 - Fabienne Keller - Demande de mettre aux voix les amendements au projet d\'acte législatif.....74
9.2 A8-0171/2018 - Fabienne Keller - Accord provisoire - Am 346 ............................................................................76
10.Faire face aux situations de crise et aux cas de force majeure ***I ...........................................................................78
10.1 A9-0127/2023 - Juan Fernando López Aguilar - Demande de mettre aux voix les amendements au projet
d\'acte législatif ............................................................................................................................................................78
10.2 A9-0127/2023 - Juan Fernando López Aguilar - Accord provisoire - Am 131 ....................................................80
11.Gestion de l’asile et de la migration ***I .....................................................................................................................82
11.1 A9-0152/2023 - Tomas Tobé - Demande de mettre aux voix les amendements au projet d\'acte législatif........82
11.2 A9-0152/2023 - Tomas Tobé - Accord provisoire - Am 473 ...............................................................................84
12.Mise en place d’une procédure de retour à la frontière et modification du règlement (UE) 2021/1148 ***I...............86
12.1 A9-0164/2024 - Fabienne Keller - Accord provisoire - Am 1 ..............................................................................86
13.Filtrage des ressortissants de pays tiers aux frontières extérieures ***I ....................................................................88
13.1 A9-0149/2023 - Birgit Sippel - Demande de mettre aux voix les amendements au projet d\'acte législatif.........88
13.2 A9-0149/2023 - Birgit Sippel - Accord provisoire - Am 210 ................................................................................90
14.Système européen d’information sur les casiers judiciaires - ressortissants de pays tiers ***I .................................92
14.1 A9-0148/2023 - Birgit Sippel - Accord provisoire - Am 21 ..................................................................................92
15.Création d’Eurodac pour la comparaison des empreintes digitales aux fins de l’application efficace du règlement
(UE) n° 604/2013, de l’identification des ressortissants de pays tiers ou apatrides en séjour irrégulier et relatif aux
demandes de comparaison avec les données d’Eurodac présentées par les autorités répressives des États
membres et Europol à des fins répressives (refonte) ***I ..........................................................................................94
15.1 A8-0212/2017 - Jorge Buxadé Villalba - Demande de mettre aux voix les amendements au projet d\'acte
législatif.......................................................................................................................................................................94
15.2 A8-0212/2017 - Jorge Buxadé Villalba - Accord provisoire - Am 158.................................................................96
16.Cadre de l’Union pour la réinstallation ***I .................................................................................................................98
16.1 A8-0316/2017 - Malin Björk - Rejet - Am 122 .....................................................................................................98
16.2 A8-0316/2017 - Malin Björk - Demande de mettre aux voix les amendements au projet d\'acte législatif ........100
16.3 A8-0316/2017 - Malin Björk - Accord provisoire - Am 121PC1.........................................................................102
17.Normes relatives aux conditions que doivent remplir les ressortissants des pays tiers ou les apatrides pour pouvoir
bénéficier d’une protection internationale ***I ..........................................................................................................104
17.1 A8-0245/2017 - Matjaž Nemec - Rejet - Am 179..............................................................................................104
17.2 A8-0245/2017 - Matjaž Nemec - Demande de mettre aux voix les amendements au projet d\'acte législatif...106
17.3 A8-0245/2017 - Matjaž Nemec - Accord provisoire - Am 178 ..........................................................................108
18.Normes pour l’accueil des personnes demandant la protection internationale (refonte) ***I ...................................110
18.1 A8-0186/2017 - Sophia in \'t Veld - Demande de mettre aux voix les amendements au projet d\'acte législatif ..110
18.2 A8-0186/2017 - Sophia in \'t Veld - Accord provisoire - Am 146 .......................................................................112
19.Fixation des règles de procédure supplémentaires relatives à l’application du règlement (UE) 2016/679 ***I .......114
19.1 A9-0045/2024 - Sergey Lagodinsky - Proposition de la Commission...............................................................114
20.Nouveau règlement sur les produits de construction ***I.........................................................................................116
20.1 A9-0207/2023 - Christian Doleschal - Accord provisoire - Am 505 ..................................................................116
21.Modification de la directive 2013/34/UE en ce qui concerne les délais d’adoption des normes d’information en
matière de durabilité pour certains secteurs et pour certaines entreprises de pays tiers ***I ..................................118
21.1 A9-0013/2024 - Axel Voss - Accord provisoire - Am 8 .....................................................................................118
22.Réduction des émissions de méthane dans le secteur de l’énergie ***I ..................................................................120
22.1 A9-0162/2023 - Pascal Canfin, Jutta Paulus - Accord provisoire - Am 281 .....................................................120
23.Mercure: amalgames dentaires et autres produits contenant du mercure ajouté faisant l’objet de restrictions de
fabrication, d’importation et d’exportation ***I ..........................................................................................................122
23.1 A9-0002/2024 - Marlene Mortler - Accord provisoire - Am 19 ..........................................................................122
24.Renforcement des normes de performance en matière d’émissions de CO2 pour les véhicules utilitaires lourds
neufs ***I ..................................................................................................................................................................124
24.1 A9-0313/2023 - Bas Eickhout - Demande de mettre aux voix les amendements au projet d\'acte législatif.....124
24.2 A9-0313/2023 - Bas Eickhout - Accord provisoire - Am 147 ............................................................................126
25.Aliments destinés à la consommation humaine: modification de certaines des directives dites "petit-déjeuner" ***I ..128
25.1 A9-0385/2023 - Alexander Bernhuber - Accord provisoire - Am 68 .................................................................128
26.Comptes économiques européens de l\'environnement: nouveaux modules ***I.....................................................130
26.1 A9-0296/2023 - Pascal Canfin - Accord provisoire - Am 44 .............................................................................130
27.Cadre de certification de l’Union relatif aux absorptions de carbone ***I.................................................................132
27.1 A9-0329/2023 - Lídia Pereira - Accord provisoire - Am 158 .............................................................................132
28.Normes applicables aux organismes chargés de l’égalité dans le domaine de l’égalité de traitement et de l’égalité
des chances entre les femmes et les hommes en matière d’emploi et de travail ***I..............................................134
28.1 A9-0354/2023 - Sirpa Pietikäinen, Marc Angel - Accord provisoire - Am 120 ..................................................134
29.Directive du Conseil relative aux normes applicables aux organismes pour l’égalité de traitement dans les
domaines de l’égalité de traitement entre les personnes sans distinction de race ou d’origine ethnique, de l’égalité
de traitement entre les personnes en matière d’emploi et de travail sans distinction de religion ou de
convictions, de handicap, d’âge ou d’orientation sexuelle et de l’égalité de traitement entre les femmes et les hommes en
matière de sécurité sociale ainsi que dans l’accès à des biens et services et la fourniture de biens et services, et
modifiant les directives 2000/43/CE et 2004/113/CE ***..........................................................................................136
29.1 A9-0128/2024 - Sirpa Pietikäinen - Projet de directive du Conseil ...................................................................136
30.Dérogation temporaire: lutte contre les abus sexuels commis contre des enfants en ligne ***I ..............................138
30.1 A9-0021/2024 - Birgit Sippel - Accord provisoire - Am 13 ................................................................................138
31.Mesures de conservation, de gestion et de contrôle applicables dans la zone de la convention CPANE ***I ........140
31.1 A9-0004/2024 - Francisco Guerreiro - Accord provisoire - Am 61....................................................................140
32.Modification de la directive 2009/18/CE établissant les principes fondamentaux régissant les enquêtes sur les
accidents dans le secteur des transports maritimes ***I ..........................................................................................142
32.1 A9-0422/2023 - Caroline Nagtegaal - Accord provisoire - Am 36.....................................................................142
33.Modification de la directive 2009/16/CE relative au contrôle par l\'État du port ***I..................................................144
33.1 A9-0419/2023 - Vera Tax - Accord provisoire - Am 41.....................................................................................144
34.Modification de la directive 2005/35/CE relative à la pollution causée par les navires et à l’introduction de sanctions
en cas d’infractions ***I ............................................................................................................................................146
34.1 A9-0365/2023 - Marian-Jean Marinescu - Accord provisoire - Am 50..............................................................146
35.Modification de la directive 2009/21/CE concernant le respect des obligations des États du pavillon ***I ..............148
35.1 A9-0418/2023 - Vera Tax - Accord provisoire - Am 34.....................................................................................148
36.Surveillance et résilience des sols (directive sur la surveillance des sols) ***I ........................................................150
36.1 A9-0138/2024 - Martin Hojsík - Rejet - Am 235= 265=.....................................................................................150
36.2 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 36 ............152
36.3 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 45 ............154
36.4 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 67 ............156
36.5 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 76/1 .........158
36.6 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 76/2 .........160
36.7 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 85/1 .........162
36.8 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 85/2 .........164
36.9 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 95/1 .........166
36.10 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 95/2 .......168
36.11 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 95/3 .......170
36.12 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 105 ........172
36.13 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 106 ........174
36.14 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 107/1 .....176
36.15 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 107/2 .....178
36.16 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 107/3 .....180
36.17 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 108 ........182
36.18 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 109 ........184
36.19 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 198/1 .....186
36.20 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 198/2 .....188
36.21 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 199/1 .....190
36.22 A9-0138/2024 - Martin Hojsík - Amendements de la commission compétente - vote séparé - Am 199/2 .....192
36.23 A9-0138/2024 - Martin Hojsík - Article 1, § 1 - Am 219 ..................................................................................194
36.24 A9-0138/2024 - Martin Hojsík - Article 1, § 2, point b - Am 220S ...................................................................196
36.25 A9-0138/2024 - Martin Hojsík - Après l\'article 1 - Am 238..............................................................................198
36.26 A9-0138/2024 - Martin Hojsík - Article 6, § 4 - Am 227S................................................................................200
36.27 A9-0138/2024 - Martin Hojsík - Article 7, § 1, alinéa 1 - Am 228....................................................................202
36.28 A9-0138/2024 - Martin Hojsík - Article 7, § 5 - Am 229S................................................................................204
36.29 A9-0138/2024 - Martin Hojsík - Article 10, § 1 - Am 248S..............................................................................206
36.30 A9-0138/2024 - Martin Hojsík - Article 11, § 1, partie introductive - Am 241..................................................208
36.31 A9-0138/2024 - Martin Hojsík - Article 11, § 1, point a, après le sous-point iii - Am 242................................210
36.32 A9-0138/2024 - Martin Hojsík - Article 11, § 1, après le point b - Am 243......................................................212
36.33 A9-0138/2024 - Martin Hojsík - Article 13, après le § 1 - Am 244 ..................................................................214
36.34 A9-0138/2024 - Martin Hojsík - Article 19, § 1 - Am 230 ................................................................................216
36.35 A9-0138/2024 - Martin Hojsík - Article 19, § 2 - Am 231 ................................................................................218
36.36 A9-0138/2024 - Martin Hojsík - Article 19, § 3 - Am 232 ................................................................................220
36.37 A9-0138/2024 - Martin Hojsík - Article 22 - Am 233S.....................................................................................222
36.38 A9-0138/2024 - Martin Hojsík - Article 23 - Am 234S.....................................................................................224
36.39 A9-0138/2024 - Martin Hojsík - Annexe III - Am 260S....................................................................................226
36.40 A9-0138/2024 - Martin Hojsík - Annexe IV, point 1 - Am 261S ......................................................................228
36.41 A9-0138/2024 - Martin Hojsík - Annexe IV, point 2 - Am 262S ......................................................................230
36.42 A9-0138/2024 - Martin Hojsík - Annexe IV, point 3 - Am 263S ......................................................................232
36.43 A9-0138/2024 - Martin Hojsík - Annexe IV, point 14 - Am 264S ....................................................................234
36.44 A9-0138/2024 - Martin Hojsík - Considérant 22 - Am 246..............................................................................236
36.45 A9-0138/2024 - Martin Hojsík - Après le considérant 22 - Am 236 ................................................................238
36.46 A9-0138/2024 - Martin Hojsík - Après le considérant 54 - Am 237 ................................................................240
36.47 A9-0138/2024 - Martin Hojsík - Proposition de la Commission ......................................................................242
37.Comptabilisation des émissions de gaz à effet de serre des services de transport ***I ..........................................244
37.1 A9-0070/2024 - Pascal Canfin, Barbara Thaler - Amendements de la commission compétente - vote séparé -
Am 104/1 ..................................................................................................................................................................244
37.2 A9-0070/2024 - Pascal Canfin, Barbara Thaler - Amendements de la commission compétente - vote séparé -
Am 104/2 ..................................................................................................................................................................246
37.3 A9-0070/2024 - Pascal Canfin, Barbara Thaler - Article 4, après le § 1 - Am 121 ...........................................248
37.4 A9-0070/2024 - Pascal Canfin, Barbara Thaler - Article 18, après le § 1 - Am 107 .........................................250
37.5 A9-0070/2024 - Pascal Canfin, Barbara Thaler - Après le considérant 5 - Am 108 .........................................252
37.6 A9-0070/2024 - Pascal Canfin, Barbara Thaler - Après le considérant 5 - Am 109 .........................................254
37.7 A9-0070/2024 - Pascal Canfin, Barbara Thaler - Proposition de la Commission .............................................256
38.Accord de partenariat entre l’Union européenne et ses États membres, d’une part, et les membres de
l’Organisation des États d’Afrique, des Caraïbes et du Pacifique, d’autre part *** ..................................................258
38.1 A9-0147/2024 - Tomas Tobé - Projet de décision du Conseil ..........................................................................258
39.Accord de partenariat entre l’Union européenne et ses États membres, d’une part, et les membres de
l’Organisation des États d’Afrique, des Caraïbes et du Pacifique, d’autre part (résolution).....................................260
39.1 A9-0159/2024 - Tomas Tobé - Proposition de résolution .................................................................................260
40.Accord de partenariat volontaire entre l’Union européenne et la République de Côte d’Ivoire sur l’application des
réglementations forestières, la gouvernance et les échanges commerciaux de bois et de produits dérivés du bois
vers l’Union européenne *** .....................................................................................................................................262
40.1 A9-0136/2024 - Karin Karlsbro - Projet de décision du Conseil .......................................................................262
41.Accord de partenariat volontaire entre l’Union européenne et la République de Côte d’Ivoire sur l’application des
réglementations forestières, la gouvernance et les échanges commerciaux de bois et de produits dérivés du bois
vers l’Union européenne (résolution) .......................................................................................................................264
41.1 A9-0137/2024 - Karin Karlsbro - Proposition de résolution ..............................................................................264
42.Accord sous forme d’échange de lettres entre l’Union européenne et la République arabe d’Égypte au titre de
l’article XXVIII de l’accord général sur les tarifs douaniers et le commerce (GATT) de 1994 relatif à la modification
des concessions pour l’ensemble des contingents tarifaires de la liste CLXXV de l’Union européenne à la suite du
retrait du Royaume-Uni de l’Union européenne ***..................................................................................................266
42.1 A9-0078/2024 - Marco Campomenosi - Projet de décision du Conseil ............................................................266
43.Accord sous forme d’échange de lettres entre l’Union européenne et la République argentine modifiant l\'accord
entre l\'Union européenne et la République argentine au titre de l’article XXVIII de l’accord général sur les tarifs
douaniers et le commerce (GATT) de 1994 relatif à la modification des concessions pour l’ensemble des
contingents tarifaires de la liste CLXXV de l’Union européenne à la suite du retrait du Royaume-Uni de l’Union
européenne ***.........................................................................................................................................................268
43.1 A9-0083/2024 - Jordi Cañas - Projet de décision du Conseil ...........................................................................268
44.TVA: modification de l\'accord entre l\'Union européenne et le Royaume de Norvège en ce qui concerne la
coopération administrative, la lutte contre la fraude et le recouvrement de créances *...........................................270
44.1 A9-0057/2024 - Irene Tinagli - Projet de décision du Conseil ..........................................................................270
45.Accord entre l’Union européenne et l’Islande définissant des règles complémentaires relatives à l’instrument de
soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du Fonds pour la gestion
intégrée des frontières *** ........................................................................................................................................272
45.1 A9-0146/2024 - Malik Azmani - Projet de décision du Conseil.........................................................................272
46.Accord entre l’Union européenne et le Royaume de Norvège définissant des règles complémentaires relatives à
l’instrument de soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du Fonds pour
la gestion intégrée des frontières ***........................................................................................................................274
46.1 A9-0143/2024 - Charlie Weimers - Projet de décision du Conseil....................................................................274
47.Accord entre l’Union européenne et la Confédération suisse définissant des règles complémentaires relatives à
l’instrument de soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du Fonds pour
la gestion intégrée des frontières ***........................................................................................................................276
47.1 A9-0145/2024 - Jadwiga Wiśniewska - Projet de décision du Conseil .............................................................276
48.Conclusion d’un accord entre l’Union européenne, d’une part, et la République d’Arménie, d’autre part, sur la
coopération entre l’Agence de l’Union européenne pour la coopération judiciaire en matière pénale (Eurojust) et les
autorités de la République d’Arménie compétentes dans le domaine de la coopération judiciaire en matière pénale
*** .............................................................................................................................................................................278
48.1 A9-0165/2024 - Thijs Reuten - Projet de décision du Conseil ..........................................................................278
49.Accord entre l’Union européenne et la Principauté de Liechtenstein définissant des règles complémentaires
relatives à l’instrument de soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du
Fonds pour la gestion intégrée des frontières ***.....................................................................................................280
49.1 A9-0144/2024 - Projet de décision du Conseil .................................................................................................280
50.Mise en place d\'un système d’imposition en fonction du siège central pour les micro, petites et moyennes
entreprises, et modification de la directive 2011/16/UE *.........................................................................................282
50.1 A9-0064/2024 - Lídia Pereira - Proposition de la Commission.........................................................................282
51.Prix de transfert * .....................................................................................................................................................284
51.1 A9-0066/2024 - Kira Marie Peter-Hansen - Proposition de la Commission......................................................284
52.Code de l’Union relatif aux médicaments à usage humain ***I................................................................................286
52.1 A9-0140/2024 - Pernille Weiss - Amendements de la commission compétente - vote séparé - Am 85...........286
52.2 A9-0140/2024 - Pernille Weiss - Amendements de la commission compétente - vote séparé - Am 159/1......288
52.3 A9-0140/2024 - Pernille Weiss - Amendements de la commission compétente - vote séparé - Am 159/2......290
52.4 A9-0140/2024 - Pernille Weiss - Article 63, § 3 - Am 343 ................................................................................292
52.5 A9-0140/2024 - Pernille Weiss - Article 81, § 1 - Am 345 ................................................................................294
52.6 A9-0140/2024 - Pernille Weiss - Article 82, § 1, alinéa 1 - Am 346..................................................................296
52.7 A9-0140/2024 - Pernille Weiss - Considérant 11 - Am 339 ..............................................................................298
52.8 A9-0140/2024 - Pernille Weiss - Proposition de la Commission ......................................................................300
53.Procédures de l’Union pour l’autorisation et la surveillance des médicaments à usage humain et règles régissant
l’Agence européenne des médicaments ***I............................................................................................................302
53.1 A9-0141/2024 - Tiemo Wölken - Article 33, § 3 - Am 373 ................................................................................302
53.2 A9-0141/2024 - Tiemo Wölken - Après l\'article 40 - Am 379............................................................................304
53.3 A9-0141/2024 - Tiemo Wölken - Article 134, avant le § 1 - Am 378.................................................................306
53.4 A9-0141/2024 - Tiemo Wölken - Après le considérant 3 - Am 377 ..................................................................308
53.5 A9-0141/2024 - Tiemo Wölken - Après le considérant 5 - Am 368 ..................................................................310
53.6 A9-0141/2024 - Tiemo Wölken - Proposition de la Commission ......................................................................312
54.Traitement des eaux urbaines résiduaires ***I.........................................................................................................314
54.1 A9-0276/2023 - Nils Torvalds - Accord provisoire - Am 268.............................................................................314';

    $rcv_toc['PV-9-2024-04-11-RCV'] = '1. Objection conformément à l’article 111, paragraphe 3, du règlement: exigences spécifiques en matière d’hygiène
applicables à certaines viandes, aux produits de la pêche, aux produits laitiers et aux œufs .....................................9
1.1 B9-0211/2024 - Proposition de résolution ..............................................................................................................9
2. Simplification de certaines règles de la PAC ***I .......................................................................................................11
2.1 C9-0120/2024 - Demande d\'application de la procédure d\'urgence.....................................................................11
3. Décharge 2022: Budget général de l\'UE - Commission.............................................................................................13
3.1 A9-0139/2024 - Isabel García Muñoz - Proposition de décision ..........................................................................13
3.2 A9-0139/2024 - Isabel García Muñoz - Après le § 5 - Am 19...............................................................................15
3.3 A9-0139/2024 - Isabel García Muñoz - § 9 - Am 24 .............................................................................................17
3.4 A9-0139/2024 - Isabel García Muñoz - Après le § 11 - Am 25.............................................................................19
3.5 A9-0139/2024 - Isabel García Muñoz - Après le § 14 - Am 9...............................................................................21
3.6 A9-0139/2024 - Isabel García Muñoz - Après le § 15 - Am 21.............................................................................23
3.7 A9-0139/2024 - Isabel García Muñoz - Après le § 15 - Am 22/1..........................................................................25
3.8 A9-0139/2024 - Isabel García Muñoz - Après le § 15 - Am 22/2..........................................................................27
3.9 A9-0139/2024 - Isabel García Muñoz - Après le § 18 - Am 3...............................................................................29
3.10 A9-0139/2024 - Isabel García Muñoz - Après le § 18 - Am 4.............................................................................31
3.11 A9-0139/2024 - Isabel García Muñoz - Après le § 18 - Am 26...........................................................................33
3.12 A9-0139/2024 - Isabel García Muñoz - Après le § 109 - Am 29/1......................................................................35
3.13 A9-0139/2024 - Isabel García Muñoz - Après le § 109 - Am 29/2......................................................................37
3.14 A9-0139/2024 - Isabel García Muñoz - Après le § 20 - Am 37/1........................................................................39
3.15 A9-0139/2024 - Isabel García Muñoz - § 191 - Am 30 .......................................................................................41
3.16 A9-0139/2024 - Isabel García Muñoz - Après le § 196 - Am 31/1......................................................................43
3.17 A9-0139/2024 - Isabel García Muñoz - Après le § 196 - Am 31/2......................................................................45
3.18 A9-0139/2024 - Isabel García Muñoz - Après le § 213 - Am 35.........................................................................47
3.19 A9-0139/2024 - Isabel García Muñoz - Après le § 275 - Am 32.........................................................................49
3.20 A9-0139/2024 - Isabel García Muñoz - § 280, après le point i - Am 33..............................................................51
3.21 A9-0139/2024 - Isabel García Muñoz - Proposition de résolution (ensemble du texte) .....................................53
4. Décharge 2022: Budget général de l\'UE - Parlement européen................................................................................55
4.1 A9-0067/2024 - Andrey Novakov - Proposition de décision .................................................................................55
4.2 A9-0067/2024 - Andrey Novakov - § 30 - Am 35..................................................................................................57
4.3 A9-0067/2024 - Andrey Novakov - Après le § 34 - Am 48....................................................................................59
4.4 A9-0067/2024 - Andrey Novakov - Après le § 35 - Am 37....................................................................................61
4.5 A9-0067/2024 - Andrey Novakov - Après le § 37 - Am 13....................................................................................63
4.6 A9-0067/2024 - Andrey Novakov - § 42/2 ............................................................................................................65
4.7 A9-0067/2024 - Andrey Novakov - § 43 ...............................................................................................................67
4.8 A9-0067/2024 - Andrey Novakov - Après le § 45 - Am 3......................................................................................69
4.9 A9-0067/2024 - Andrey Novakov - Après le § 53 - Am 4......................................................................................71
4.10 A9-0067/2024 - Andrey Novakov - Après le § 54 - Am 41..................................................................................73
4.11 A9-0067/2024 - Andrey Novakov - Après le § 55 - Am 5....................................................................................75
4.12 A9-0067/2024 - Andrey Novakov - § 61 - Am 21................................................................................................77
4.13 A9-0067/2024 - Andrey Novakov - Après le § 61 - Am 22..................................................................................79
4.14 A9-0067/2024 - Andrey Novakov - Après le § 76 - Am 6....................................................................................81
4.15 A9-0067/2024 - Andrey Novakov - Après le § 124 - Am 67................................................................................83
4.16 A9-0067/2024 - Andrey Novakov - Après le § 126 - Am 59................................................................................85
4.17 A9-0067/2024 - Andrey Novakov - § 127 - Am 60..............................................................................................87
4.18 A9-0067/2024 - Andrey Novakov - § 129 - Am 15/1...........................................................................................89
4.19 A9-0067/2024 - Andrey Novakov - § 129 - Am 15/2...........................................................................................91
4.20 A9-0067/2024 - Andrey Novakov - Après le § 131 - Am 26................................................................................93
4.21 A9-0067/2024 - Andrey Novakov - Après le § 131 - Am 27................................................................................95
4.22 A9-0067/2024 - Andrey Novakov - Après le § 140 - Am 16................................................................................97
4.23 A9-0067/2024 - Andrey Novakov - Après le § 152 - Am 17/1.............................................................................99
4.24 A9-0067/2024 - Andrey Novakov - Après le § 152 - Am 17/2...........................................................................101
4.25 A9-0067/2024 - Andrey Novakov - Après le § 152 - Am 17/3...........................................................................103
4.26 A9-0067/2024 - Andrey Novakov - Après le § 152 - Am 17/4...........................................................................105
4.27 A9-0067/2024 - Andrey Novakov - § 159 - Am 28............................................................................................107
4.28 A9-0067/2024 - Andrey Novakov - § 159 - Am 18............................................................................................109
4.29 A9-0067/2024 - Andrey Novakov - Après le § 159 - Am 66..............................................................................111
4.30 A9-0067/2024 - Andrey Novakov - Après le § 170 - Am 49..............................................................................113
4.31 A9-0067/2024 - Andrey Novakov - Après le considérant C - Am 1 ..................................................................115
4.32 A9-0067/2024 - Andrey Novakov - Après le considérant C - Am 47 ................................................................117
4.33 A9-0067/2024 - Andrey Novakov - Proposition de résolution (ensemble du texte) ..........................................119
5. Décharge 2022: Budget général de l\'UE - Cour de justice de l\'Union européenne .................................................121
5.1 A9-0075/2024 - Luke Ming Flanagan - Proposition de décision .........................................................................121
5.2 A9-0075/2024 - Luke Ming Flanagan - Après le § 36 - Am 1 .............................................................................123
5.3 A9-0075/2024 - Luke Ming Flanagan - Proposition de résolution (ensemble du texte)......................................125
6. Décharge 2022: Budget général de l\'UE - Cour des comptes .................................................................................127
6.1 A9-0074/2024 - Luke Ming Flanagan - Proposition de décision .........................................................................127
6.2 A9-0074/2024 - Luke Ming Flanagan - § 15/2 ....................................................................................................129
6.3 A9-0074/2024 - Luke Ming Flanagan - Proposition de résolution (ensemble du texte)......................................131
7. Décharge 2022: Budget général de l\'UE - Comité économique et social ................................................................133
7.1 A9-0072/2024 - Luke Ming Flanagan - Proposition de décision .........................................................................133
7.2 A9-0072/2024 - Luke Ming Flanagan - Proposition de résolution (ensemble du texte)......................................135
8. Décharge 2022: Budget général de l\'UE - Comité des régions ...............................................................................137
8.1 A9-0073/2024 - Luke Ming Flanagan - Proposition de décision .........................................................................137
8.2 A9-0073/2024 - Luke Ming Flanagan - Proposition de résolution (ensemble du texte)......................................139
9. Décharge 2022: Budget général de l\'UE - Médiateur européen ..............................................................................141
9.1 A9-0084/2024 - Luke Ming Flanagan - Proposition de décision .........................................................................141
9.2 A9-0084/2024 - Luke Ming Flanagan - Proposition de résolution (ensemble du texte)......................................143
10.Décharge 2022: Budget général de l\'UE - Contrôleur européen de la protection des données ..............................145
10.1 A9-0086/2024 - Luke Ming Flanagan - Proposition de décision .......................................................................145
10.2 A9-0086/2024 - Luke Ming Flanagan - Proposition de résolution.....................................................................147
11.Décharge 2022: Budget général de l\'UE - Service européen pour l\'action extérieure .............................................149
11.1 A9-0102/2024 - Monika Hohlmeier - Proposition de décision...........................................................................149
11.2 A9-0102/2024 - Monika Hohlmeier - Après le § 89 - Am 11 .............................................................................151
11.3 A9-0102/2024 - Monika Hohlmeier - Proposition de résolution (ensemble du texte) .......................................153
12.Décharge 2022: Parquet européen..........................................................................................................................155
12.1 A9-0117/2024 - Luke Ming Flanagan - Proposition de décision .......................................................................155
12.2 A9-0117/2024 - Luke Ming Flanagan - Après le § 29 - Am 2 ...........................................................................157
12.3 A9-0117/2024 - Luke Ming Flanagan - Après le considérant K - Am 1 ............................................................159
12.4 A9-0117/2024 - Luke Ming Flanagan - Proposition de résolution (ensemble du texte)....................................161
13.Décharge 2022: Centre européen pour le développement de la formation professionnelle (Cedefop) ...................163
13.1 A9-0080/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................163
14.Décharge 2022: Fondation européenne pour l’amélioration des conditions de vie et de travail (Eurofound)..........165
14.1 A9-0122/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................165
15.Décharge 2022: Agence des droits fondamentaux de l’Union européenne.............................................................167
15.1 A9-0112/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................167
16.Décharge 2022: Observatoire européen des drogues et des toxicomanies ............................................................169
16.1 A9-0104/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................169
17.Décharge 2022: Agence européenne de l\'environnement.......................................................................................171
17.1 A9-0130/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................171
18.Décharge 2022: Agence européenne pour la sécurité et la santé au travail (EU-OSHA)........................................173
18.1 A9-0115/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................173
19.Décharge 2022: Centre de traduction des organes de l’Union européenne ............................................................175
19.1 A9-0082/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................175
20.Décharge 2022: Agence européenne des médicaments .........................................................................................177
20.1 A9-0133/2024 - Petri Sarvamaa - Après le § 2 - Am 1 .....................................................................................177
20.2 A9-0133/2024 - Petri Sarvamaa - Après le § 5 - Am 2 .....................................................................................179
20.3 A9-0133/2024 - Petri Sarvamaa - Après le § 7 - Am 3 .....................................................................................181
20.4 A9-0133/2024 - Petri Sarvamaa - Après le § 7 - Am 4 .....................................................................................183
20.5 A9-0133/2024 - Petri Sarvamaa - Proposition de résolution (ensemble du texte)............................................185
21.Décharge 2022: Agence de l’Union européenne pour la coopération judiciaire en matière pénale (Eurojust)........187
21.1 A9-0099/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................187
22.Décharge 2022: Fondation européenne pour la formation ......................................................................................189
22.1 A9-0114/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................189
23.Décharge 2022: Agence européenne pour la sécurité maritime..............................................................................191
23.1 A9-0107/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................191
24.Décharge 2022: Agence de l’Union européenne pour la sécurité aérienne ............................................................193
24.1 A9-0116/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................193
25.Décharge 2022: Autorité européenne de sécurité des aliments ..............................................................................195
25.1 A9-0129/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................195
26.Décharge 2022: Centre européen de prévention et de contrôle des maladies........................................................197
26.1 A9-0109/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................197
27.Décharge 2022: ENISA (Agence de l\'Union européenne pour la cybersécurité).....................................................199
27.1 A9-0134/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................199
28.Décharge 2022: Agence de l\'Union européenne pour les chemins de fer...............................................................201
28.1 A9-0092/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................201
29.Décharge 2022: Agence de l\'Union européenne pour la formation des services répressifs (CEPOL) ....................203
29.1 A9-0098/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................203
30.Décharge 2022: Agence européenne de garde-frontières et de garde-côtes..........................................................205
30.1 A9-0113/2024 - Petri Sarvamaa - Proposition de décision...............................................................................205
30.2 A9-0113/2024 - Petri Sarvamaa - Après le § 4 - Am 3 .....................................................................................207
30.3 A9-0113/2024 - Petri Sarvamaa - § 14 - Am 7 .................................................................................................209
30.4 A9-0113/2024 - Petri Sarvamaa - Après le § 14 - Am 8 ...................................................................................211
30.5 A9-0113/2024 - Petri Sarvamaa - Après le § 14 - Am 9 ...................................................................................213
30.6 A9-0113/2024 - Petri Sarvamaa - Après le § 14 - Am 10 .................................................................................215
30.7 A9-0113/2024 - Petri Sarvamaa - § 15/1 ..........................................................................................................217
30.8 A9-0113/2024 - Petri Sarvamaa - § 15/2 ..........................................................................................................219
30.9 A9-0113/2024 - Petri Sarvamaa - § 21/1 ..........................................................................................................221
30.10 A9-0113/2024 - Petri Sarvamaa - § 23 ...........................................................................................................223
30.11 A9-0113/2024 - Petri Sarvamaa - Proposition de résolution (ensemble du texte)..........................................225
31.Décharge 2022: Agence de l’Union européenne pour le programme spatial ..........................................................227
31.1 A9-0121/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................227
32.Décharge 2022: Agence européenne de contrôle des pêches ................................................................................229
32.1 A9-0100/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................229
33.Décharge 2022: Agence européenne des produits chimiques ................................................................................231
33.1 A9-0135/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................231
34.Décharge 2022: Agence d\'approvisionnement d\'Euratom .......................................................................................233
34.1 A9-0108/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................233
35.Décharge 2022: Agence de l’Union européenne pour la coopération des services répressifs (Europol) ................235
35.1 A9-0119/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................235
36.Décharge 2022: Institut européen pour l\'égalité entre les hommes et les femmes .................................................237
36.1 A9-0096/2024 - Petri Sarvamaa - Après le § 7 - Am 1 .....................................................................................237
36.2 A9-0096/2024 - Petri Sarvamaa - Proposition de résolution (ensemble du texte)............................................239
37.Décharge 2022: Autorité bancaire européenne .......................................................................................................241
37.1 A9-0111/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................241
38.Décharge 2022: Autorité européenne des assurances et des pensions professionnelles ......................................243
38.1 A9-0097/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................243
39.Décharge 2022: Autorité européenne des marchés financiers ................................................................................245
39.1 A9-0103/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................245
40.Décharge 2022: Agence de l\'Union européenne pour la coopération des régulateurs de l\'énergie (ACER)...........247
40.1 A9-0120/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................247
41.Décharge 2022: Agence de soutien à l’ORECE (Office de l\'ORECE) .....................................................................249
41.1 A9-0123/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................249
42.Décharge 2022: Institut européen d\'innovation et de technologie (EIT) ..................................................................251
42.1 A9-0132/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................251
43.Décharge 2022: Agence de l\'Union européenne pour l\'asile (avant le 19 janvier 2022: Bureau européen d’appui en
matière d’asile).........................................................................................................................................................253
43.1 A9-0106/2024 - Petri Sarvamaa - Après le § 10 - Am 2 ...................................................................................253
43.2 A9-0106/2024 - Petri Sarvamaa - Proposition de résolution (ensemble du texte)............................................255
44.Décharge 2022: Agence de l’Union européenne pour la gestion opérationnelle des systèmes d’information à
grande échelle au sein de l’espace de liberté, de sécurité et de justice (eu-LISA)..................................................257
44.1 A9-0105/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................257
45.Décharge 2022: entreprise commune européenne pour ITER et le développement de l’énergie de fusion (Fusion
for Energy) ...............................................................................................................................................................259
45.1 A9-0090/2024 - Michal Wiezik - Proposition de résolution ...............................................................................259
46.Décharge 2022: entreprise commune «Recherche sur la gestion du trafic aérien dans le ciel unique européen 3» ..261
46.1 A9-0094/2024 - Michal Wiezik - Proposition de résolution ...............................................................................261
47.Décharge 2022: entreprise commune «Aviation propre».........................................................................................263
47.1 A9-0087/2024 - Michal Wiezik - Proposition de résolution ...............................................................................263
48.Décharge 2022: entreprise commune «Une Europe fondée sur la bioéconomie circulaire» ...................................265
48.1 A9-0088/2024 - Michal Wiezik - Proposition de résolution ...............................................................................265
49.Décharge 2022: entreprise commune «Initiative en matière de santé innovante» ..................................................267
49.1 A9-0093/2024 - Michal Wiezik - Proposition de résolution ...............................................................................267
50.Décharge 2022: entreprise commune «Hydrogène propre»....................................................................................269
50.1 A9-0089/2024 - Michal Wiezik - Proposition de résolution ...............................................................................269
51.Décharge 2022: entreprise commune «Système ferroviaire européen»..................................................................271
51.1 A9-0091/2024 - Josef Mihál - Proposition de résolution ...................................................................................271
52.Décharge 2022: entreprise commune «Technologies numériques clés» ................................................................273
52.1 A9-0101/2024 - Michal Wiezik - Proposition de résolution ...............................................................................273
53.Décharge 2022: entreprise commune pour le calcul à haute performance européen .............................................275
53.1 A9-0095/2024 - Michal Wiezik - Proposition de résolution ...............................................................................275
54.Décharge 2022: Budget général de l\'UE - FED (9e, 10e et 11e).............................................................................277
54.1 A9-0110/2024 - Joachim Kuhs - Après le § 47 - Am 4 .....................................................................................277
54.2 A9-0110/2024 - Joachim Kuhs - Proposition de résolution (ensemble du texte)..............................................279
55.Décharge 2022: performance, gestion financière et contrôle des agences de l’Union européenne........................281
55.1 A9-0118/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................281
56.Décharge 2022: Autorité européenne du travail ......................................................................................................283
56.1 A9-0131/2024 - Petri Sarvamaa - Proposition de résolution ............................................................................283
57.Marchés intérieurs des gaz naturel et renouvelable et de l\'hydrogène (refonte) ***I ...............................................285
57.1 A9-0032/2023 - Jerzy Buzek - Accord provisoire - Am 2..................................................................................285
58.Règles communes pour les marchés intérieurs des gaz naturel et renouvelable et de l\'hydrogène (refonte) ***I ..287
58.1 A9-0035/2023 - Jens Geier - Accord provisoire - Am 2 ....................................................................................287
59.Organisation du marché de l\'électricité de l\'Union: règlement ***I ...........................................................................289
59.1 A9-0255/2023 - Nicolás González Casares - Rejet - Am 9 ..............................................................................289
59.2 A9-0255/2023 - Nicolás González Casares - Accord provisoire - Am 2 ...........................................................291
60.Organisation du marché de l\'électricité de l\'Union: directive ***I..............................................................................293
60.1 A9-0151/2024 - Nicolás González Casares - Rejet - Am 3 ..............................................................................293
60.2 A9-0151/2024 - Nicolás González Casares - Accord provisoire - Am 2 ...........................................................295
61.Inscription du droit à l\'avortement dans la charte des droits fondamentaux de l\'UE................................................297
61.1 B9-0205/2024 - § 1 - Am 14 .............................................................................................................................297
61.2 B9-0205/2024 - § 3 - Am 4 ...............................................................................................................................299
61.3 B9-0205/2024 - § 3/1 ........................................................................................................................................301
61.4 B9-0205/2024 - § 3/2 ........................................................................................................................................303
61.5 B9-0205/2024 - Après le § 3 - Am 7 .................................................................................................................305
61.6 B9-0205/2024 - Après le § 3 - Am 15 ...............................................................................................................307
61.7 B9-0205/2024 - Après le § 4 - Am 16 ...............................................................................................................309
61.8 B9-0205/2024 - § 5 ...........................................................................................................................................311
61.9 B9-0205/2024 - Après le § 5 - Am 17 ...............................................................................................................313
61.10 B9-0205/2024 - § 6 - Am 6S ...........................................................................................................................315
61.11 B9-0205/2024 - § 6/1 ......................................................................................................................................317
61.12 B9-0205/2024 - § 6/2 ......................................................................................................................................319
61.13 B9-0205/2024 - § 8 - Am 5 .............................................................................................................................321
61.14 B9-0205/2024 - § 8 .........................................................................................................................................323
61.15 B9-0205/2024 - § 11 .......................................................................................................................................325
61.16 B9-0205/2024 - Après le § 11 - Am 9 .............................................................................................................327
61.17 B9-0205/2024 - Après le § 12 - Am 8 .............................................................................................................329
61.18 B9-0205/2024 - Visas - Am 3..........................................................................................................................331
61.19 B9-0205/2024 - Considérant A/1 ....................................................................................................................333
61.20 B9-0205/2024 - Après le considérant A - Am 10 ............................................................................................335
61.21 B9-0205/2024 - Après le considérant A - Am 11 ............................................................................................337
61.22 B9-0205/2024 - Après le considérant A - Am 12 ............................................................................................339
61.23 B9-0205/2024 - Après le considérant A - Am 13 ............................................................................................341
61.24 B9-0205/2024 - Considérant B/2 ....................................................................................................................343
61.25 B9-0205/2024 - Considérant P .......................................................................................................................345
61.26 B9-0205/2024 - Considérant Q/1....................................................................................................................347
61.27 B9-0205/2024 - Considérant Q/2....................................................................................................................349
61.28 B9-0205/2024 - Considérant N .......................................................................................................................351
61.29 B9-0205/2024 - Considérant T .......................................................................................................................353
61.30 B9-0205/2024 - Considérant U .......................................................................................................................355
61.31 B9-0205/2024 - Proposition de résolution (ensemble du texte)......................................................................357';

    $rcv_toc['PV-9-2024-04-22-RCV'] = '1. Ordre du jour de lundi - Demande du groupe ECR......................................................................................................4
1.1 Ordre du jour de lundi - Demande du groupe ECR ................................................................................................4
2. Ordre du jour de mardi - Demande du groupe The Left (armée israélienne dans la bande de Gaza) ........................6
2.1 Ordre du jour de mardi - Demande du groupe The Left (armée israélienne dans la bande de Gaza) ...................6
3. Ordre du jour de mardi - Demande des groupes The Left et S&D...............................................................................8
3.1 Ordre du jour de mardi - Demande des groupes The Left et S&D .........................................................................8
4. Ordre du jour de mardi - Demande du groupe Verts/ALE (Simplification de certaines règles de la PAC).................10
4.1 Ordre du jour de mardi - Demande du groupe Verts/ALE (Simplification de certaines règles de la PAC) ...........10
5. Ordre du jour de mardi - Demande du groupe The Left (Equateur et Mexique) ........................................................12
5.1 Ordre du jour de mardi - Demande du groupe The Left (Equateur et Mexique)...................................................12
6. Ordre du jour de mardi - Demande du groupe Verts/ALE (Les responsabilités des entreprises fossiles dans la crise
du coût de la vie)........................................................................................................................................................13
6.1 Ordre du jour de mardi - Demande du groupe Verts/ALE (Les responsabilités des entreprises fossiles dans la
crise du coût de la vie)................................................................................................................................................13
7. Ordre du jour de mercredi - Demande du groupe The Left (Attaque sans précédent lancée par l\'Iran contre Israël) ..14
7.1 Ordre du jour de mercredi - Demande du groupe The Left (Attaque sans précédent lancée par l\'Iran contre
Israël)..........................................................................................................................................................................14
8. Ordre du jour de mercredi - Demande du groupe The Left (Palestine)......................................................................16
8.1 Ordre du jour de mercredi - Demande du groupe The Left (Palestine) ................................................................16
9. Ordre du jour de mercredi - Demande du groupe S&D..............................................................................................17
9.1 Ordre du jour de mercredi - Demande du groupe S&D ........................................................................................17
10.Ordre du jour de mercredi - Demande du groupe Renew..........................................................................................18
10.1 Ordre du jour de mercredi - Demande du groupe Renew ..................................................................................18
11.Ordre du jour de jeudi - Demande du groupe PPE ....................................................................................................19
11.1 Ordre du jour de jeudi - Demande du groupe PPE.............................................................................................19';

    $rcv_toc['PV-9-2024-04-23-RCV'] = '1. Décharge 2022: Budget général de l\'UE - Conseil européen et Conseil .....................................................................5
1.1 A9-0071/2024 - Luke Ming Flanagan - Proposition de décision .............................................................................5
1.2 A9-0071/2024 - Luke Ming Flanagan - Après le § 16 - Am 1 .................................................................................7
1.3 A9-0071/2024 - Luke Ming Flanagan - § 24/1 ........................................................................................................9
1.4 A9-0071/2024 - Luke Ming Flanagan - § 24/2 ......................................................................................................11
1.5 A9-0071/2024 - Luke Ming Flanagan - § 24/3 ......................................................................................................13
1.6 A9-0071/2024 - Luke Ming Flanagan - Proposition de résolution.........................................................................15
2. Recommandation de décision ne faisant pas objection à un acte délégué: règles relatives au ratio concernant la
norme 1 relative aux BCAE........................................................................................................................................17
2.1 B9-0199/2024 - Projet de décision .......................................................................................................................17
3. Objection conformément à l\'article 111, paragraphe 3, du règlement intérieur: retrait de Gibraltar du tableau figurant
au point I de l\'annexe du règlement délégué (UE) 2016/1675...................................................................................19
3.1 B9-0210/2024 - Proposition de résolution ............................................................................................................19
4. Objection conformément à l\'article 111, paragraphe 3, du règlement intérieur: matières premières pour la
production de biocarburants et de biogaz ..................................................................................................................21
4.1 B9-0218/2024 - Proposition de résolution ............................................................................................................21
5. Mesures visant à réduire le coût du déploiement de réseaux gigabit de communications électroniques (règlement
sur les infrastructures gigabit) ***I..............................................................................................................................23
5.1 A9-0275/2023 - Alin Mituța - Accord provisoire - Am 2.........................................................................................23
6. Transmission des procédures pénales ***I ................................................................................................................25
6.1 A9-0008/2024 - Assita Kanko - Accord provisoire - Am 73 ..................................................................................25
7. Établissement d\'un cadre pour le redressement et la résolution des entreprises d’assurance et de réassurance ***I ..27
7.1 A9-0251/2023 - Markus Ferber - Accord provisoire - Am 2 ..................................................................................27
8. Modification de la directive Solvabilité II ***I ..............................................................................................................29
8.1 A9-0256/2023 - Markus Ferber - Accord provisoire - Am 2 ..................................................................................29
9. Classification, étiquetage et emballage des substances et des mélanges ***I ..........................................................31
9.1 A9-0271/2023 - Maria Spyraki - Accord provisoire - Am 177 ...............................................................................31
10.Services de paiement et services de monnaie électronique dans le marché intérieur ***I ........................................33
10.1 A9-0046/2024 - Ondřej Kovařík - Proposition de la Commission et amendement .............................................33
11.Services de paiement dans le marché intérieur et modification du règlement (UE) nº 1093/2010 ***I......................35
11.1 A9-0052/2024 - Marek Belka - Proposition de la Commission et amendement .................................................35
12.Lutte contre le retard de paiement dans les transactions commerciales ***I .............................................................37
12.1 A9-0156/2024 - Róża Thun und Hohenstein - Article 1, § 3, après le point c - Am 109 .....................................37
12.2 A9-0156/2024 - Róża Thun und Hohenstein - Article 2, § 1, après le point 9 - Am 94.......................................39
12.3 A9-0156/2024 - Róża Thun und Hohenstein - Article 3, § 1 et après le § 1 - Am 104PC1= 110PC1= ..............41
12.4 A9-0156/2024 - Róża Thun und Hohenstein - Article 3, § 1 et après le § 1 - Am 104PC2= 110PC2= ..............43
12.5 A9-0156/2024 - Róża Thun und Hohenstein - Article 4 - Am 106S= 112S= ......................................................45
12.6 A9-0156/2024 - Róża Thun und Hohenstein - Après le considérant 9 - Am 92..................................................47
12.7 A9-0156/2024 - Róża Thun und Hohenstein - Proposition de la Commission ...................................................49
13.Modification de la directive 2014/62/UE en ce qui concerne certaines obligations déclaratives ***I .........................51
13.1 A9-0152/2024 - Juan Fernando López Aguilar - Proposition de la Commission ..................................................................................................................................51
14.Accès des autorités compétentes aux registres centralisés des comptes bancaires par l’intermédiaire du point
d\'accès unique ***I .....................................................................................................................................................53
14.1 A9-0004/2023 - Emil Radev - Accord provisoire - Am 8 .....................................................................................53
15.Règlement relatif à l\'importation, à l\'exportation et au transit des armes à feu, de leurs parties essentielles et
munitions (refonte) ***I ...............................................................................................................................................55
15.1 A9-0312/2023 - Bernd Lange - Accord provisoire - Am 93.................................................................................55
16.Règlement sur l’écoconception ***I............................................................................................................................57
16.1 A9-0218/2023 - Alessandra Moretti - Accord provisoire - Am 258 .....................................................................57
17.Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables
aux produits ukrainiens au titre de l\'accord d’association UE/Euratom/Ukraine ***I..................................................59
17.1 A9-0077/2024 - Sandra Kalniete - Accord provisoire - Am 29............................................................................59
18.Exemption de visa pour les titulaires d’un passeport serbe délivré par la direction de coordination serbe ***I .........61
18.1 A9-0172/2024 - Matjaž Nemec - Proposition de la Commission ........................................................................61
19.Protocole à l’accord euro-méditerranéen: participation de l’Égypte aux programmes de l’Union ***.........................63
19.1 A9-0175/2024 - Michael Gahler - Projet de décision du Conseil ........................................................................63
20.Prévention des pertes de granulés plastiques en vue de réduire la pollution par les microplastiques ***I ................65
20.1 A9-0148/2024 - João Albuquerque - Amendements de la commission compétente - vote séparé - Am 70 ......65
20.2 A9-0148/2024 - João Albuquerque - Article 9, § 1, point b - Am 96/1 ................................................................67
20.3 A9-0148/2024 - João Albuquerque - Article 9, § 1, point b - Am 96/2 ................................................................69
20.4 A9-0148/2024 - João Albuquerque - Article 16, § 4 - Am 95S............................................................................71
20.5 A9-0148/2024 - João Albuquerque - Après le considérant 15 - Am 97 ..............................................................73
20.6 A9-0148/2024 - João Albuquerque - Proposition de la Commission ..................................................................75
21.Règles communes visant à promouvoir la réparation des biens ***I .........................................................................77
21.1 A9-0316/2023 - René Repasi - Accord provisoire - Am 86.................................................................................77
22.Interdire sur le marché de l\'Union les produits issus du travail forcé ***I...................................................................79
22.1 A9-0306/2023 - Samira Rafaela, Maria-Manuel Leitão-Marques - Accord provisoire - Am 177.........................79
23.Modification de la directive 2011/36/UE concernant la prévention de la traite des êtres humains et la lutte contre ce
phénomène ainsi que la protection des victimes ***I .................................................................................................81
23.1 A9-0285/2023 - Malin Björk, Eugenia Rodríguez Palop - Accord provisoire - Am 2...........................................81
24.Coordination efficace des politiques économiques et surveillance budgétaire multilatérale ***I ...............................83
24.1 A9-0439/2023 - Markus Ferber, Margarida Marques - Rejet - Am 2 ..................................................................83
24.2 A9-0439/2023 - Markus Ferber, Margarida Marques - Accord provisoire - Am 3...............................................85
25.Accélération et clarification de la mise en œuvre de la procédure concernant les déficits excessifs – règlement
modificatif *.................................................................................................................................................................87
25.1 A9-0444/2023 - Markus Ferber, Margarida Marques - Proposition de la Commission.......................................87
26.Exigences applicables aux cadres budgétaires des États membres – directive modificative * .................................89
26.1 A9-0440/2023 - Markus Ferber, Margarida Marques - Proposition de la Commission.......................................89';

    $rcv_toc['PV-9-2024-04-25-RCV'] = '1. Déploiement progressif d’Eudamed, obligation d’information en cas d’interruption d’approvisionnement et
dispositions transitoires applicables à certains dispositifs médicaux de diagnostic in vitro ***I ...................................6
1.1 C9-0010/2024 - Proposition de la Commission .....................................................................................................6
2. Azerbaïdjan, notamment la répression à l\'égard de la société civile et le cas de Gubad Ibadoghlu et d\'Ilhamiz
Guliyev .........................................................................................................................................................................8
2.1 RC-B9-0227/2024 - § 5 - Am 3 ...............................................................................................................................8
2.2 RC-B9-0227/2024 - Après le § 5 - Am 4...............................................................................................................10
2.3 RC-B9-0227/2024 - Considérant A - Am 2 ...........................................................................................................12
2.4 RC-B9-0227/2024 - Proposition de résolution (ensemble du texte) .....................................................................14
3. Proposition d\'abrogation de la loi interdisant les mutilations génitales féminines en Gambie ...................................16
3.1 RC-B9-0228/2024 - Proposition de résolution (ensemble du texte) .....................................................................16
4. Organisme interinstitutionnel chargé des normes éthiques .......................................................................................18
4.1 A9-0181/2024 - Daniel Freund - Après le § 1 - Am 1 ...........................................................................................18
4.2 A9-0181/2024 - Daniel Freund - Après le § 2 - Am 2 ...........................................................................................20
4.3 A9-0181/2024 - Daniel Freund - Après le § 2 - Am 3 ...........................................................................................22
4.4 A9-0181/2024 - Daniel Freund - § 3/2 ..................................................................................................................24
4.5 A9-0181/2024 - Daniel Freund - § 3/3 ..................................................................................................................26
4.6 A9-0181/2024 - Daniel Freund - § 7 - Am 4..........................................................................................................28
4.7 A9-0181/2024 - Daniel Freund - § 9 - Am 6..........................................................................................................30
4.8 A9-0181/2024 - Daniel Freund - Après le § 9 - Am 5 ...........................................................................................32
4.9 A9-0181/2024 - Daniel Freund - § 15 - Am 7........................................................................................................34
4.10 A9-0181/2024 - Daniel Freund - § 16 - Am 84....................................................................................................36
4.11 A9-0181/2024 - Daniel Freund - Article 1, § 1 - Am 86= 117= ...........................................................................38
4.12 A9-0181/2024 - Daniel Freund - Article 2, § 1, après le point b - Am 81= 85= 119=..........................................40
4.13 A9-0181/2024 - Daniel Freund - Proposition de décision (ensemble du texte) ..................................................42
5. État prévisionnel des dépenses et des recettes pour l\'exercice 2025 - Section I - Parlement européen ..................44
5.1 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 3 - Am 8..................................................................44
5.2 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 16 - Am 1................................................................46
5.3 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 16 - Am 2................................................................48
5.4 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 27 - Am 3................................................................50
5.5 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 27 - Am 4................................................................52
5.6 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 27 - Am 5................................................................54
5.7 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 27 - Am 6/1.............................................................56
5.8 A9-0180/2024 - Anna-Michelle Asimakopoulou - Après le § 27 - Am 6/2.............................................................58
5.9 A9-0180/2024 - Anna-Michelle Asimakopoulou - Proposition de résolution (ensemble du texte) ........................60
6. Projet de budget rectificatif nº 1/2024: modification du budget 2024 requise à la suite de la révision du CFP .........62
6.1 A9-0174/2024 - Siegfried Mureşan - Proposition de résolution ............................................................................62
7. Projet de budget rectificatif nº 3/2024: renforcement du Parquet européen à la suite de l\'adhésion de la Pologne et
de la participation attendue de la Suède....................................................................................................................64
7.1 A9-0179/2024 - Siegfried Mureşan - Proposition de résolution ............................................................................64
8. Informations préalables sur les passagers: renforcer et faciliter les contrôles aux frontières extérieures ***I...........66
8.1 A9-0409/2023 - Jan-Christoph Oetjen - Accord provisoire - Am 139 ...................................................................66
9. Informations préalables sur les passagers: prévention et détection des infractions terroristes et des formes graves
de criminalité, et enquêtes et poursuites en la matière ***I........................................................................................68
9.1 A9-0411/2023 - Assita Kanko - Accord provisoire - Am 138 ................................................................................68
10.Cadre de mesures en vue de renforcer l’écosystème européen de la fabrication de produits de technologie «zéro
net» (règlement pour une industrie «zéro net») ***I...................................................................................................70
10.1 A9-0343/2023 - Christian Ehler - Accord provisoire - Am 41..............................................................................70
11.Les élections présidentielles non démocratiques en Russie et leur extension illégitime aux territoires occupés ......72
11.1 RC-B9-0253/2024 - Après le § 14 - Am 7...........................................................................................................72
11.2 RC-B9-0253/2024 - Après le considérant C - Am 2............................................................................................74
11.3 RC-B9-0253/2024 - Proposition de résolution (ensemble du texte) ...................................................................76
12.Nouvelles allégations d’ingérence russe au Parlement européen, dans les prochaines élections européennes, et
incidence sur l’Union ..................................................................................................................................................78
12.1 RC-B9-0262/2024 - § 2 - Am 10 .........................................................................................................................78
12.2 RC-B9-0262/2024 - § 2/1....................................................................................................................................80
12.3 RC-B9-0262/2024 - § 4.......................................................................................................................................82
12.4 RC-B9-0262/2024 - § 5.......................................................................................................................................84
12.5 RC-B9-0262/2024 - Après le § 6 - Am 11...........................................................................................................86
12.6 RC-B9-0262/2024 - § 8.......................................................................................................................................88
12.7 RC-B9-0262/2024 - § 13.....................................................................................................................................90
12.8 RC-B9-0262/2024 - Après le § 13 - Am 4...........................................................................................................92
12.9 RC-B9-0262/2024 - § 14.....................................................................................................................................94
12.10 RC-B9-0262/2024 - § 18/2................................................................................................................................96
12.11 RC-B9-0262/2024 - § 20/1................................................................................................................................98
12.12 RC-B9-0262/2024 - § 20/3..............................................................................................................................100
12.13 RC-B9-0262/2024 - § 24 - Am 12PC1 ............................................................................................................102
12.14 RC-B9-0262/2024 - § 25 - Am 12PC2 ............................................................................................................104
12.15 RC-B9-0262/2024 - § 25/2..............................................................................................................................106
12.16 RC-B9-0262/2024 - § 27/4..............................................................................................................................108
12.17 RC-B9-0262/2024 - Considérant C - Am 5 .....................................................................................................110
12.18 RC-B9-0262/2024 - Après le considérant C - Am 13......................................................................................112
12.19 RC-B9-0262/2024 - Considérant N - Am 6 .....................................................................................................114
12.20 RC-B9-0262/2024 - Considérant Q ................................................................................................................116
12.21 RC-B9-0262/2024 - Considérant R.................................................................................................................118
12.22 RC-B9-0262/2024 - Après le considérant R - Am 3........................................................................................120
12.23 RC-B9-0262/2024 - Considérant Z - Am 7 .....................................................................................................122
12.24 RC-B9-0262/2024 - Après le considérant Z - Am 8 ........................................................................................124
12.25 RC-B9-0262/2024 - Après le considérant Z - Am 9 ........................................................................................126
12.26 RC-B9-0262/2024 - Proposition de résolution (ensemble du texte) ...............................................................128
13.Tentatives de réintroduction d\'une loi sur les agents de l\'étranger en Géorgie et ses restrictions à l\'égard de la
société civile.............................................................................................................................................................130
13.1 RC-B9-0244/2024 - Après le § 12 - Am 3.........................................................................................................130
13.2 RC-B9-0244/2024 - Après le § 14 - Am 1.........................................................................................................132
13.3 RC-B9-0244/2024 - Après le § 15 - Am 4.........................................................................................................134
13.4 RC-B9-0244/2024 - Après le § 15 - Am 2= 5=..................................................................................................136
13.5 RC-B9-0244/2024 - Proposition de résolution (ensemble du texte) .................................................................138
14.Attaque sans précédent lancée par l\'Iran contre Israël, nécessité d\'une désescalade et d\'une réaction de l\'Union
européenne ..............................................................................................................................................................140
14.1 RC-B9-0235/2024 - § 1.....................................................................................................................................140
14.2 RC-B9-0235/2024 - Après le § 1 - Am 1...........................................................................................................142
14.3 RC-B9-0235/2024 - Après le § 1 - Am 2...........................................................................................................144
14.4 RC-B9-0235/2024 - § 2 - Am 3 .........................................................................................................................146
14.5 RC-B9-0235/2024 - Après le § 2 - Am 4...........................................................................................................148
14.6 RC-B9-0235/2024 - § 9 - Am 19 .......................................................................................................................150
14.7 RC-B9-0235/2024 - Après le § 9 - Am 6...........................................................................................................152
14.8 RC-B9-0235/2024 - Après le § 9 - Am 7...........................................................................................................154
14.9 RC-B9-0235/2024 - Après le § 9 - Am 8...........................................................................................................156
14.10 RC-B9-0235/2024 - Après le § 9 - Am 9.........................................................................................................158
14.11 RC-B9-0235/2024 - Après le § 11 - Am 20.....................................................................................................160
14.12 RC-B9-0235/2024 - § 12 - Am 21 ...................................................................................................................162
14.13 RC-B9-0235/2024 - Après le § 18 - Am 10.....................................................................................................164
14.14 RC-B9-0235/2024 - § 19/2..............................................................................................................................166
14.15 RC-B9-0235/2024 - Après le § 19 - Am 11.....................................................................................................168
14.16 RC-B9-0235/2024 - Considérant H.................................................................................................................170
14.17 RC-B9-0235/2024 - Considérant M - Am 18...................................................................................................172
14.18 RC-B9-0235/2024 - Proposition de résolution (ensemble du texte) ...............................................................174';

    return $rcv_toc[$doc_id] ?? [];

    $rcv_toc['PV-9-2024-04-24-RCV'] = '1. Modifications du règlement intérieur du Parlement concernant la formation à la prévention des conflits et du
harcèlement sur le lieu de travail ainsi qu\'à la bonne gestion d’un bureau..................................................................8
1.1 A9-0163/2024 - Gabriele Bischoff - Article 10, § 6, alinéa 2 - Am 1 .......................................................................8
1.2 A9-0163/2024 - Gabriele Bischoff - Article 21, § 1 - Am 2....................................................................................10
1.3 A9-0163/2024 - Gabriele Bischoff - Article 21, § 1 - Am 7....................................................................................12
1.4 A9-0163/2024 - Gabriele Bischoff - Article 21, § 2 - Am 3....................................................................................14
1.5 A9-0163/2024 - Gabriele Bischoff - Article 21, § 2 - Am 9....................................................................................16
1.6 A9-0163/2024 - Gabriele Bischoff - Article 176, § 1, alinéa 3 - Am 4= 10= ..........................................................18
1.7 A9-0163/2024 - Gabriele Bischoff - Annexe II, point 5 - Am 6/1...........................................................................20
1.8 A9-0163/2024 - Gabriele Bischoff - Annexe II, point 5 - Am 6/2...........................................................................22
1.9 A9-0163/2024 - Gabriele Bischoff - Après le § 1 - Am 12.....................................................................................24
1.10 A9-0163/2024 - Gabriele Bischoff - Proposition de décision ..............................................................................26
2. Objections formulées en vertu de l’article 111, paragraphe 3, du règlement: Nouveaux aliments - définition d’un «nanomatériau manufacturé» ....................................................................................................................................28
2.1 B9-0225/2024 - Proposition de résolution ............................................................................................................28
3. Réseau transeuropéen de transport ***I ....................................................................................................................30
3.1 A9-0147/2023 - Barbara Thaler, Dominique Riquet - Accord provisoire - Am 549...............................................30
4. Emballages et déchets d\'emballages ***I ..................................................................................................................32
4.1 A9-0319/2023 - Frédérique Ries - Accord provisoire - Am 532............................................................................32
5. La qualité de l’air ambiant et un air pur pour l’Europe ***I .........................................................................................34
5.1 A9-0233/2023 - Javi López - Accord provisoire - Am 355 ....................................................................................34
6. Instrument du marché unique pour les situations d’urgence ***I ...............................................................................36
6.1 A9-0246/2023 - Andreas Schwab - Accord provisoire - Am 296 ..........................................................................36
7. Modification de certains règlements en ce qui concerne l\'établissement d\'un instrument du marché unique pour les situations d\'urgence ***I .............................................................................................................................................38
7.1 A9-0244/2023 - Andreas Schwab - Accord provisoire - Am 165 ..........................................................................38
8. Modification de certaines directives en ce qui concerne l\'établissement d\'un instrument du marché unique pour les situations d\'urgence ***I .............................................................................................................................................40
8.1 A9-0245/2023 - Andreas Schwab - Accord provisoire - Am 329 ..........................................................................40
9. Code frontières Schengen ***I ...................................................................................................................................42
9.1 A9-0280/2023 - Sylvie Guillaume - Accord provisoire - Am 163...........................................................................42
10.Échange transfrontalier d’informations concernant les infractions en matière de sécurité routière ***I.....................44
10.1 A9-0396/2023 - Kosma Złotowski - Accord provisoire - Am 75 ..........................................................................44
11.Végétaux obtenus au moyen de certaines nouvelles techniques génomiques et denrées alimentaires et aliments pour animaux qui en sont dérivés ***I ........................................................................................................................46
11.1 A9-0014/2024 - Jessica Polfjärd - Rejet - Am 317..............................................................................................46
11.2 A9-0014/2024 - Jessica Polfjärd - Proposition de la Commission (amendée le 7 février 2024).........................48
12.Mesures d’intervention précoce, conditions de résolution et financement des mesures de résolution (règlement MRU 3) ***I.................................................................................................................................................................50
12.1 A9-0155/2024 - Pedro Marques - Proposition de la Commission.......................................................................50
13.Mesures d’intervention précoce, conditions de déclenchement d’une procédure de résolution et financement des mesures de résolution (BRRD3) ***I ..........................................................................................................................52
13.1 A9-0153/2024 - Luděk Niedermayer - Proposition de la Commission................................................................52
14.Champ de protection des dépôts, utilisation des fonds des systèmes de garantie des dépôts, coopération transfrontière et transparence ***I..............................................................................................................................54
14.1 A9-0154/2024 - Kira Marie Peter-Hansen - Proposition de la Commission........................................................54
15.Devoir de vigilance des entreprises en matière de durabilité ***I ..............................................................................56
15.1 A9-0184/2023 - Lara Wolters - Accord provisoire - Am 430 ...............................................................................56
16.Amélioration des conditions de travail dans le cadre du travail via une plateforme ***I.............................................58
16.1 A9-0301/2022 - Elisabetta Gualmini - Accord provisoire - Am 187 ....................................................................58
17.Espace européen des données de santé ***I ............................................................................................................60
17.1 A9-0395/2023 - Tomislav Sokol, Annalisa Tardino - Accord provisoire - Am 558 ..............................................60
18.Mobilisation du Fonds européen d’ajustement à la mondialisation: demande EGF/2023/004 DK/Danish Crown - Danemark...................................................................................................................................................................62
18.1 A9-0171/2024 - Janusz Lewandowski - Proposition de décision........................................................................62
19.Mobilisation du Fonds européen d’ajustement à la mondialisation: demande EGF/2023/003 DE/Vallourec - Allemagne ..................................................................................................................................................................64
19.1 A9-0166/2024 - Jens Geier - Proposition de décision ........................................................................................64
20.Mobilisation du Fonds européen d’ajustement à la mondialisation: demande EGF/2024/000 TA 2024 – Assistance technique à l’initiative de la Commission ***I .............................................................................................................66
20.1 A9-0173/2024 - Margarida Marques - Proposition de décision ..........................................................................66
21.Retrait de l\'Union du traité sur la Charte de l\'énergie *** ...........................................................................................68
21.1 A9-0176/2024 - Anna Cavazzini, Marc Botenga - Projet de décision du Conseil...............................................68
22.Mesures pour faciliter la protection consulaire des citoyens de l’Union non représentés dans des pays tiers * .......70
22.1 A9-0178/2024 - Loránt Vincze - Proposition de la Commission .........................................................................70
23.Accord se rapportant à la convention des Nations unies sur le droit de la mer et portant sur la conservation et l\'utilisation durable de la diversité biologique marine des zones ne relevant pas de la juridiction nationale *** ........72
23.1 A9-0177/2024 - Silvia Modig - Projet de décision du Conseil.............................................................................72
24.Lutte contre la violence à l’égard des femmes et la violence domestique ***I ...........................................................74
24.1 A9-0234/2023 - Evin Incir, Frances Fitzgerald - Demande de mettre aux voix les amendements au projet d\'acte législatif ............................................................................................................................................................74
24.2 A9-0234/2023 - Evin Incir, Frances Fitzgerald - Accord provisoire - Am 298.....................................................76
25.Carte européenne du handicap et carte européenne de stationnement pour les personnes handicapées ***I ........78
25.1 A9-0003/2024 - Lucia Ďuriš Nicholsonová - Accord provisoire - Am 99.............................................................78
26.Carte européenne du handicap et carte européenne de stationnement pour personnes handicapées pour les ressortissants de pays tiers résidant légalement dans un État membre ***I..............................................................80
26.1 A9-0059/2024 - Antonius Manders, Alice Kuhnke - Accord provisoire - Am 25 .................................................80
27.Production et commercialisation des matériels de reproduction des végétaux ***I ...................................................82
27.1 A9-0149/2024 - Herbert Dorfmann - Amendements de la commission compétente - votes séparés - Am 54 ...82
27.2 A9-0149/2024 - Herbert Dorfmann - Amendements de la commission compétente - votes séparés - Am 60 ...84
27.3 A9-0149/2024 - Herbert Dorfmann - Amendements de la commission compétente - votes séparés - Am 202/1 ....................................................................................................................................................................................86
27.4 A9-0149/2024 - Herbert Dorfmann - Amendements de la commission compétente - votes séparés - Am 202/2 ....................................................................................................................................................................................88
27.5 A9-0149/2024 - Herbert Dorfmann - Amendements de la commission compétente - votes séparés - Am 203/1 ....................................................................................................................................................................................90
27.6 A9-0149/2024 - Herbert Dorfmann - Amendements de la commission compétente - votes séparés - Am 203/2 ....................................................................................................................................................................................92
27.7 A9-0149/2024 - Herbert Dorfmann - Amendements de la commission compétente - votes séparés - Am 245 .94
27.8 A9-0149/2024 - Herbert Dorfmann - Article 3, § 1, après le point 35 - Am 62....................................................96
27.9 A9-0149/2024 - Herbert Dorfmann - Article 3, § 1, après le point 35 - Am 65....................................................98
27.10 A9-0149/2024 - Herbert Dorfmann - Article 29, § 1, alinéa 1 - Am 154..........................................................100
27.11 A9-0149/2024 - Herbert Dorfmann - Article 29, § 1, alinéa 3, point c - Am 158 .............................................102
27.12 A9-0149/2024 - Herbert Dorfmann - Article 29, après le § 2 - Am 342...........................................................104
27.13 A9-0149/2024 - Herbert Dorfmann - Article 39, § 1, alinéa 2 - Am 196/2.......................................................106
27.14 A9-0149/2024 - Herbert Dorfmann - Après l\'article 68 - Am 322= 330=.........................................................108
27.15 A9-0149/2024 - Herbert Dorfmann - Article 80, § 1, point 1; Règlement (UE) 2017/625; Article 1, § 2, après le point k - Am 333........................................................................................................................................................110
27.16 A9-0149/2024 - Herbert Dorfmann - Après l\'annexe III - Am 315...................................................................112
27.17 A9-0149/2024 - Herbert Dorfmann - Proposition de la Commission ..............................................................114
28.Production et commercialisation de matériel forestier de reproduction ***I .............................................................116
28.1 A9-0142/2024 - Herbert Dorfmann - Article 7 - Am 105S= 125S= ...................................................................116
28.2 A9-0142/2024 - Herbert Dorfmann - Après l\'article 22 - Am 112= 137=...........................................................118
28.3 A9-0142/2024 - Herbert Dorfmann - Proposition de la Commission ................................................................120
29.Établissement d\'une facilité pour les réformes et la croissance en faveur des Balkans occidentaux ***I................122
29.1 A9-0085/2024 - Tonino Picula, Karlo Ressler - Accord provisoire - Am 2 ........................................................122
30.Simplification de certaines règles de la PAC ...........................................................................................................124
30.1 C9-0120/2024 - Rejet - Am 13= 23=.................................................................................................................124
30.2 C9-0120/2024 - Article 1, § 1, point 2, sous-point a; Règlement (UE) 2021/2115; Article 13, § 1, alinéa 1 - Am 3................................................................................................................................................................................126
30.3 C9-0120/2024 - Article 1, § 1, point 2, sous-point a; Règlement (UE) 2021/2115; Article 13, § 1, alinéa 2 - Am 4................................................................................................................................................................................128
30.4 C9-0120/2024 - Article 1, § 1, après le point 2; Règlement (UE) 2021/2115; Article 17, § 1 - Am 6................130
30.5 C9-0120/2024 - Article 1, § 1, point 3; Règlement (UE) 2021/2115; Article 31, § 1 bis - Am 5 ........................132
30.6 C9-0120/2024 - Article 1, § 1, après le point 3; Règlement (UE) 2021/2115; Article 108, § 1, après le point c - Am 35 .......................................................................................................................................................................134
30.7 C9-0120/2024 - Article 1, § 1, après le point 3; Règlement (UE) 2021/2115; Article 108, § 1, après le point c - Am 37 .......................................................................................................................................................................136
30.8 C9-0120/2024 - Article 1, § 1, point 6, sous-point b; Règlement (UE) 2021/2115; Annexe III, ligne 9 - Am 7 ...138
30.9 C9-0120/2024 - Article 1, § 1, point 6, sous-point c; Règlement (UE) 2021/2115; Annexe III, ligne 12 - Am 8 ..................................................................................................................................................................................140
30.10 C9-0120/2024 - Après l\'article 2, § 1, partie introductive et point 1; Règlement (UE) 1308/2013; Article 15, après le § 2 - Am 9 ...................................................................................................................................................142
30.11 C9-0120/2024 - Après l\'article 2, § 1, point 2; Règlement (UE) 1308/2013; Article 148, § 2 - Am 10 ............144
30.12 C9-0120/2024 - Après l\'article 2, § 1, point 3; Règlement (UE) 1308/2013; Article 168, § 4, point c, sous-point i, tiret 2 - Am 11 ........................................................................................................................................................146
30.13 C9-0120/2024 - Après l\'article 2, § 1, point 4; Règlement (UE) 1308/2013; Après l\'article 206 - Am 12........148
30.14 C9-0120/2024 - Article 4, § 2 - Am 14PC3= 19= ............................................................................................150
30.15 C9-0120/2024 - Après le considérant 1 - Am 34 ............................................................................................152
30.16 C9-0120/2024 - Après le considérant 2 - Am 28 ............................................................................................154
30.17 C9-0120/2024 - Après le considérant 2 - Am 29 ............................................................................................156
30.18 C9-0120/2024 - Après le considérant 3 - Am 30 ............................................................................................158
30.19 C9-0120/2024 - Après le considérant 5 - Am 31 ............................................................................................160
30.20 C9-0120/2024 - Considérant 8 - Am 1............................................................................................................162
30.21 C9-0120/2024 - Après le considérant 8 - Am 2 ..............................................................................................164
30.22 C9-0120/2024 - Après le considérant 20 - Am 32 ..........................................................................................166
30.23 C9-0120/2024 - Après le considérant 21 - Am 33 ..........................................................................................168
30.24 C9-0120/2024 - Proposition de la Commission ..............................................................................................170
31.Réception et surveillance du marché des engins mobiles non routiers circulant sur la voie publique ***I ..............172
31.1 A9-0382/2023 - Tom Vandenkendelaere - Accord provisoire - Am 76 .............................................................172
32.Modification du règlement (UE) 2016/2031 relatif aux mesures de protection contre les organismes nuisibles aux végétaux ***I ............................................................................................................................................................174
32.1 A9-0035/2024 - Clara Aguilera - Accord provisoire - Am 34.............................................................................174
33.Transparence et intégrité des activités de notation environnementale, sociale et de gouvernance (ESG) ***I.......176
33.1 A9-0417/2023 - Aurore Lalucq - Accord provisoire - Am 2 ...............................................................................176
34.Mesures visant à atténuer les expositions excessives aux contreparties centrales de pays tiers et à améliorer l’efficience des marchés de la compensation de l’Union ***I ...................................................................................178
34.1 A9-0398/2023 - Danuta Maria Hübner - Accord provisoire - Am 2 ...................................................................178
35.Traitement du risque de concentration vis-à-vis des contreparties centrales et du risque de contrepartie des transactions sur instruments dérivés faisant l\'objet d\'une compensation centrale ***I.............................................180
35.1 A9-0399/2023 - Danuta Maria Hübner - Accord provisoire - Am 2 ...................................................................180
36.Rendre les marchés des capitaux plus attractifs et faciliter l’accès des PME aux capitaux – modification de certains règlements ***I .........................................................................................................................................................182
36.1 A9-0302/2023 - Alfred Sant - Accord provisoire - Am 2....................................................................................182
37.Rendre les marchés des capitaux plus attractifs et faciliter l’accès des PME aux capitaux - modification de la directive ***I..............................................................................................................................................................184
37.1 A9-0303/2023 - Alfred Sant - Accord provisoire - Am 2....................................................................................184
38.Structures avec actions à votes multiples dans les entreprises qui demandent l’admission à la négociation de leurs actions sur un marché de croissance des PME ***I.................................................................................................186
38.1 A9-0300/2023 - Alfred Sant - Accord provisoire - Am 2....................................................................................186
39.Normes de qualité et de sécurité des substances d’origine humaine destinées à une application humaine ***I ....188
39.1 A9-0250/2023 - Nathalie Colin-Oesterlé - Accord provisoire - Am 244 ............................................................188
40.Services de sécurité gérés ***I.................................................................................................................................190
40.1 A9-0307/2023 - Josianne Cutajar - Accord provisoire - Am 2 ..........................................................................190
41.Règlement sur la cybersolidarité ***I........................................................................................................................192
41.1 A9-0426/2023 - Lina Gálvez Muñoz - Accord provisoire - Am 2.......................................................................192
42.Statistiques européennes du marché du travail concernant les entreprises ***I......................................................194
42.1 A9-0054/2024 - Irene Tinagli - Proposition de la Commission .........................................................................194
43.Modification du règlement (UE) 2016/1011 en ce qui concerne le champ d\'application des règles applicables aux indices de référence, l\'utilisation dans l\'Union d\'indices de référence fournis par un administrateur situé dans un pays tiers et certaines obligations d’information ***I ................................................................................................196
43.1 A9-0076/2024 - Jonás Fernández - Proposition de la Commission et amendement .......................................196
44.Polluants des eaux de surface et des eaux souterraines ***I ..................................................................................198
44.1 A9-0238/2023 - Milan Brglez - Proposition de la Commission (amendée le 12 septembre 2023) ...................198
45.Initiative EuroHPC en faveur des start-up visant à renforcer le rôle moteur de l’Europe dans le domaine de l’intelligence artificielle digne de confiance * ............................................................................................................200
45.1 A9-0161/2024 - Proposition de la Commission ................................................................................................200
46.Droit des sociétés - Extension et amélioration de l’utilisation des outils et processus numériques ***I...................202
46.1 A9-0394/2023 - Emil Radev - Accord provisoire - Am 58 .................................................................................202
47.Statistiques européennes sur la population et le logement ***I ...............................................................................204
47.1 A9-0284/2023 - Irena Joveva - Ensemble du texte - Am 56PC1......................................................................204
47.2 A9-0284/2023 - Irena Joveva - Article 2, § 1, point 3 - Am 57..........................................................................206
47.3 A9-0284/2023 - Irena Joveva - Annexe - Am 58 ..............................................................................................208
47.4 A9-0284/2023 - Irena Joveva - Annexe - Am 56PC2 .......................................................................................210
47.5 A9-0284/2023 - Irena Joveva - Proposition de la Commission.........................................................................212
48.Modification de la directive 2013/36/UE en ce qui concerne les pouvoirs de surveillance, les sanctions, les succursales de pays tiers et les risques environnementaux, sociaux et de gouvernance ***I.................................214
48.1 A9-0029/2023 - Jonás Fernández - Accord provisoire - Am 2..........................................................................214
49.Modification du règlement (UE) nº 575/2013 en ce qui concerne les exigences pour risque de crédit, risque d’ajustement de l’évaluation de crédit, risque opérationnel et risque de marché et le plancher de fonds propres ***I ..216
49.1 A9-0030/2023 - Jonás Fernández - Accord provisoire - Am 2..........................................................................216
50.Sixième directive anti-blanchiment ***I ....................................................................................................................218
50.1 A9-0150/2023 - Luděk Niedermayer, Paul Tang - Accord provisoire - Am 387................................................218
51.Règlement anti-blanchiment ***I ..............................................................................................................................220
51.1 A9-0151/2023 - Eero Heinäluoma, Damien Carême - Accord provisoire - Am 329 .........................................220
52.Institution de l\'Autorité de lutte contre le blanchiment de capitaux et le financement du terrorisme ***I..................222
52.1 A9-0128/2023 - Eva Maria Poptcheva, Emil Radev - Accord provisoire - Am 2...............................................222
53.Auditions actuellement menées au titre de l’article 7, paragraphe 1, du traité UE en ce qui concerne la Hongrie pour renforcer l\'état de droit, et leurs incidences budgétaires .................................................................................224
53.1 B9-0223/2024 - § 1 - Am 5 ...............................................................................................................................224
53.2 B9-0223/2024 - § 3 - Am 2 ...............................................................................................................................226
53.3 B9-0223/2024 - § 5 - Am 1 ...............................................................................................................................228
53.4 B9-0223/2024 - § 8 - Am 3 ...............................................................................................................................230
53.5 B9-0223/2024 - § 8 - Am 4 ...............................................................................................................................232
53.6 B9-0223/2024 - § 9 - Am 6 ...............................................................................................................................234
53.7 B9-0223/2024 - § 16 - Am 7 .............................................................................................................................236
53.8 B9-0223/2024 - Proposition de résolution (ensemble du texte)........................................................................238';





    return $rcv_toc[$doc_id];
  }
  function resultMap($doc_id) {
    $map = [];

    $map['PV-9-2023-09-12-RCV'] = array (
      '2. Plan pluriannuel de gestion du thon rouge dans l’Atlantique Est et la mer Méditerranée ***II (vote)' =>
      array (
      ),
      '3. Étiquetage des aliments biologiques pour animaux familiers ***I (vote)' =>
      array (
        'A9-0159/2023' =>
        array (
        ),
      ),
      '4. Normes de qualité et de sécurité des substances d’origine humaine destinées à une application humaine ***I (vote)' =>
      array (
        'A9-0250/2023' =>
        array (
        ),
      ),
      '5. Adhésion à l’acte de Genève de l’arrangement de Lisbonne sur les appellations d’origine et les indications géographiques *** (vote)' =>
      array (
        'A9-0237/2023' =>
        array (
        ),
      ),
      '6. Mise en place de l’instrument visant à renforcer l’industrie européenne de la défense au moyen d’acquisitions conjointes ***I (vote)' =>
      array (
        'A9-0161/2023' =>
        array (
        ),
      ),
      '7. Polluants des eaux de surface et des eaux souterraines ***I (vote)' =>
      array (
        'A9-0238/2023' =>
        array (
        ),
      ),
      '8. Directive sur les énergies renouvelables ***I (vote)' =>
      array (
        'A9-0208/2022' =>
        array (
        ),
      ),
      '9. Crédits aux consommateurs ***I (vote)' =>
      array (
        'A9-0212/2022' =>
        array (
        ),
      ),
      '10. Protection des indications géographiques pour les produits artisanaux et industriels ***I (vote)' =>
      array (
        'A9-0049/2023' =>
        array (
        ),
      ),
      '11. Système des écoles européennes – état des lieux, enjeux et perspectives (vote)' =>
      array (
        'A9-0205/2023' =>
        array (
        ),
      ),
      '12. Objection formulée conformément à l’article 112, paragraphes 2 et 3, du règlement: maïs génétiquement modifié MON 87419 (vote)' =>
      array (
        'B9-0362/2023' =>
        array (
        ),
      ),
      '13. Objection formulée conformément à l’article 112, paragraphes 2 et 3, du règlement: maïs génétiquement modifié GA21 x T25 (vote)' =>
      array (
        'B9-0363/2023' =>
        array (
        ),
      ),
    );

    $map['PV-9-2023-09-13-RCV'] = array (
      '1. Composition du Parlement européen *** (vote)' =>
      array (
        'A9-0265/2023' =>
        array (
        ),
      ),
      '2. Nomination d’un membre de la Cour des comptes - Katarína Kaszasová (vote)' =>
      array (
      ),
      '3. Reconduction de l’accord de coopération scientifique et technologique UE/USA *** (vote)' =>
      array (
        'A9-0242/2023' =>
        array (
        ),
      ),
      '4. Lignes directrices pour les politiques de l’emploi des États membres * (vote)' =>
      array (
        'A9-0241/2023' =>
        array (
        ),
      ),
      '5. Coopération administrative dans le domaine fiscal * (vote)' =>
      array (
        'A9-0236/2023' =>
        array (
        ),
      ),
      '6. Modifications du règlement intérieur du Parlement en vue de renforcer l’intégrité, l’indépendance et la responsabilité (vote)' =>
      array (
        'A9-0262/2023' =>
        array (
        ),
      ),
      '7. Instrument du marché unique pour les situations d’urgence ***I (vote)' =>
      array (
        'A9-0246/2023' =>
        array (
        ),
      ),
      '8. La qualité de l’air ambiant et un air pur pour l’Europe ***I (vote)' =>
      array (
        'A9-0233/2023' =>
        array (
        ),
      ),
      '9. Carburants durables pour l’aviation (Initiative «ReFuel EU Aviation») ***I (vote)' =>
      array (
        'A9-0199/2022' =>
        array (
        ),
      ),
      '10. Rapport 2022 concernant la Turquie (vote)' =>
      array (
        'A9-0247/2023' =>
        array (
        ),
      ),
      '11. Relations avec la Biélorussie (vote)' =>
      array (
        'A9-0258/2023' =>
        array (
        ),
      ),
    );

    $map['PV-9-2023-09-14-RCV'] = array (
      '1. Modification des règlements (UE) 2019/943 et (UE) 2019/942 ainsi que des directives (UE) 2018/2001 et (UE) 2019/944 pour améliorer l\'organisation du marché de l\'électricité de l\'Union ***I (vote)' =>
      array (
        'A9-0255/2023' =>
        array (
        ),
      ),
      '2. Guatemala: situation après les élections, état de droit et indépendance de la justice (vote)' =>
      array (
      ),
      '3. Le cas de Gubad Ibadoghlu, emprisonné en Azerbaïdjan (vote)' =>
      array (
        'RC-B9-0369/2023' =>
        array (
        ),
      ),
      '4. Situation des droits de l’homme au Bangladesh, en particulier le cas d’Odhikar (vote)' =>
      array (
        'RC-B9-0378/2023' =>
        array (
        ),
      ),
      '5. Cadre permettant d’assurer un approvisionnement durable et sûr en matières premières critiques ***I (vote)' =>
      array (
        'A9-0260/2023' =>
        array (
        ),
      ),
      '6. Ouverture de négociations en vue d’un accord avec les États-Unis d’Amérique sur le renforcement des chaînes internationales d’approvisionnement en minerais critiques (vote)' =>
      array (
      ),
      '7. Modification du mécanisme proposé visant à lever les obstacles juridiques et administratifs dans un contexte transfrontalier (vote)' =>
      array (
        'A9-0252/2023' =>
        array (
        ),
      ),
      '8. Réglementation de la prostitution dans l’Union européenne: implications transfrontières et incidence sur l’égalité entre les hommes et les femmes et les droits des femmes (vote)' =>
      array (
        'A9-0240/2023' =>
        array (
        ),
      ),
      '9. L’avenir du secteur européen du livre (vote)' =>
      array (
        'A9-0257/2023' =>
        array (
        ),
      ),
      '10. Parlementarisme, citoyenneté européenne et démocratie (vote)' =>
      array (
        'A9-0249/2023' =>
        array (
        ),
      ),
    );

    $map['PV-9-2023-10-03-RCV'] = array (
      '1. Nomination du président du conseil de surveillance de la Banque centrale européenne (vote)' =>
      array (
      ),
      '2. Protection des travailleurs contre l\'amiante ***I (vote)' =>
      array (
        'A9-0160/2023' =>
        array (
        ),
      ),
      '3. Coercition économique exercée par des pays tiers ***I (vote)' =>
      array (
        'A9-0246/2022' =>
        array (
        ),
      ),
      '4. Systèmes de transport routier intelligents ***I (vote)' =>
      array (
        'A9-0265/2022' =>
        array (
        ),
      ),
      '5. Rapport intérimaire sur la proposition de révision à mi-parcours du cadre financier pluriannuel 2021-2027 (vote)' =>
      array (
        'A9-0273/2023' =>
        array (
        ),
      ),
      '6. Législation européenne sur la liberté des médias ***I (vote)' =>
      array (
        'A9-0264/2023' =>
        array (
        ),
      ),
      '7. Objection conformément à l\'article 112, paragraphes 2 et 3, du règlement: maïs génétiquement modifié MON 89034 × 1507 × MIR162 × NK603 × DAS-40278-9 et neuf sous-combinaisons (vote)' =>
      array (
        'B9-0387/2023' =>
        array (
        ),
      ),
      '8. Objection conformément à l\'article 112, paragraphes 2 et 3, du règlement: maïs génétiquement modifié MIR162 (vote)' =>
      array (
        'B9-0388/2023' =>
        array (
        ),
      ),
      '9. Des transports européens qui fonctionnent pour les femmes (vote)' =>
      array (
        'A9-0239/2023' =>
        array (
        ),
      ),
    );

    $map['PV-9-2023-10-04-RCV'] = array (
      '1. Classification, étiquetage et emballage des substances et des mélanges ***I (vote)' =>
      array (
        'A9-0271/2023' =>
        array (
        ),
      ),
      '2. Mobilisation du Fonds de solidarité de l’Union européenne pour venir en aide à la Roumanie, à l\'Italie et à la Turquie (vote)' =>
      array (
        'A9-0269/2023' =>
        array (
        ),
      ),
      '3. Ségrégation et discrimination des enfants roms dans l’éducation (vote)' =>
      array (
        'B9-0394/2023' =>
        array (
        ),
      ),
      '4. Harmonisation des droits des personnes autistes (vote)' =>
      array (
      ),
      '5. Dimensions standard des bagages à main (vote)' =>
      array (
      ),
      '6. Relations UE-Suisse (vote)' =>
      array (
        'A9-0248/2023' =>
        array (
        ),
      ),
      '7. Ouzbékistan (vote)' =>
      array (
        'A9-0227/2023' =>
        array (
        ),
      ),
    );

    $map['PV-9-2023-10-05-RCV'] = array (
      '1. Approbation de la nomination de Wopke Hoekstra en tant que membre de la Commission européenne (vote)' =>
      array (
      ),
      '2. Approbation de l\'attribution de nouvelles responsabilités au vice-président exécutif de la Commission Maroš Šefčovič (vote)' =>
      array (
      ),
      '3. Modification du règlement (UE) 2016/399 concernant un code de l’Union relatif au régime de franchissement des frontières par les personnes (vote)' =>
      array (
        'A9-0280/2023' =>
        array (
        ),
      ),
      '4. Situation des droits de l\'homme en Afghanistan, en particulier la persécution d\'anciens responsables du gouvernement (vote)' =>
      array (
        'RC-B9-0395/2023' =>
        array (
        ),
      ),
      '5. Le cas de Zarema Moussaïeva en Tchétchénie (vote)' =>
      array (
        'RC-B9-0415/2023' =>
        array (
        ),
      ),
      '6. Égypte, en particulier la condamnation d\'Hicham Kassem (vote)' =>
      array (
        'RC-B9-0396/2023' =>
        array (
        ),
      ),
      '7. Obligations vertes européennes ***I (vote)' =>
      array (
        'A9-0156/2022' =>
        array (
        ),
      ),
      '8. Schéma de préférences tarifaires généralisées ***I (vote)' =>
      array (
        'A9-0267/2023' =>
        array (
        ),
      ),
      '9. Contrats de services financiers conclus à distance ***I (vote)' =>
      array (
        'A9-0097/2023' =>
        array (
        ),
      ),
      '10. Traitement des eaux urbaines résiduaires ***I (vote)' =>
      array (
        'A9-0276/2023' =>
        array (
        ),
      ),
      '11. La situation au Haut-Karabakh après l\'attaque menée par l\'Azerbaïdjan et les menaces continues contre l\'Arménie (vote)' =>
      array (
        'RC-B9-0393/2023' =>
        array (
        ),
      ),
      '12. Le point sur la progression de la Moldavie sur la voie de l’adhésion à l’Union européenne (vote)' =>
      array (
        'RC-B9-0408/2023' =>
        array (
        ),
      ),
      '13. La nouvelle stratégie européenne pour un internet mieux adapté aux enfants (BIK+) (vote)' =>
      array (
      //       '' =>
      //       array (
        //       ),
      ),
      );

      $map['PV-9-2023-10-17-RCV'] = array (
        '1. Réseau d’information sur la durabilité des exploitations agricoles ***I (vote)' =>
        array (
          'A9-0075/2023' =>
          array (
          ),
        ),
        '2. Mécanisme de protection civile de l’Union ***I (vote)' =>
        array (
          'A9-0266/2023' =>
          array (
          ),
        ),
        '3. Décharge 2021: budget général de l’UE - Conseil européen et Conseil (vote)' =>
        array (
          'A9-0274/2023' =>
          array (
          ),
        ),
        '4. Nomination d\'un membre du directoire de la Banque centrale européenne (vote)' =>
        array (
        ),
        '5. Création de la facilité pour l\'Ukraine ***I (vote)' =>
        array (
          'A9-0286/2023' =>
          array (
          ),
        ),
        '6. Établissement de la plateforme Technologies stratégiques pour l\'Europe («STEP») ***I (vote)' =>
        array (
          'A9-0290/2023' =>
          array (
          ),
        ),
        '7. Contrôle des pêches ***I (vote)' =>
        array (
          'A9-0016/2021' =>
          array (
          ),
        ),
        '8. Implications des activités de pêche chinoises sur les pêcheries de l’Union et la voie à suivre (vote)' =>
        array (
          'A9-0282/2023' =>
          array (
          ),
        ),
      );

      $map['PV-9-2023-10-18-RCV'] = array (
        '1. Projet de budget général de l\'Union européenne pour l\'exercice 2024' => [
          'PV-9-2023-10-18-RCV__1' => []
        ],
        '2. Budget général de l’Union européenne pour l’exercice 2024 – toutes sections (vote)' =>
        array (
          'A9-0288/2023' =>
          array (
          ),
        ),
        '3. Projet de budget rectificatif nº 3 au budget général 2023 – Mise à jour des recettes (ressources propres) et autres ajustements techniques (vote)' =>
        array (
          'A9-0287/2023' =>
          array (
          ),
        ),
        '4. Objection formulée conformément à l\'article 111, paragraphe 3, du règlement: Critères d’examen technique supplémentaires (vote)' =>
        array (
          'B9-0431/2023' =>
          array (
          ),
        ),
        '5. Objection formulée conformément à l\'article 111, paragraphe 3, du règlement: Normes d’information en matière de durabilité (vote)' =>
        array (
          'B9-0426/2023' =>
          array (
          ),
        ),
        '6. Rapport 2022 de la Commission sur le Monténégro (vote)' =>
        array (
          'A9-0277/2023' =>
          array (
          ),
        ),
        '7. Espace Schengen: numérisation de la procédure de visa ***I (vote)' =>
        array (
          'A9-0025/2023' =>
          array (
          ),
        ),
        '8. Espace Schengen: modification du règlement sur la vignette-visa ***I (vote)' =>
        array (
          'A9-0268/2023' =>
          array (
          ),
        ),
      );

      $map['PV-9-2023-10-19-RCV'] = array (
        '1. Évolution récente du dialogue Serbie-Kosovo, notamment la situation dans les municipalités du nord du Kosovo (vote)' => [],

        //     array (
          //       'RC-B9-0436/2023' =>
          //       array (
            //       ),
          //     ),
      '4. Stratégie européenne en matière de protéines (vote)' => [
        'A9-0281/2023' => []
      ],
          //     array (
            //       'B9-0449/2023' =>
            //       array (
              //       ),
            //     ),
      '5. Renouvellement des générations dans les exploitations agricoles de l’UE de l\'avenir (vote)' => [
        'A9-0283/2023' => []
      ],
            //     array (
              //       'A9-0281/2023' =>
              //       array (
                //       ),
              //     ),
      );

      $map['PV-9-2023-11-09-RCV'] = array (
        '1. Élection d’un questeur du Parlement européen (en remplacement de Monika Beňová) (vote)' =>
        array (
        ),
        '2. Demande de défense de l\'immunité de Stefano Maullu (vote)' =>
        array (
        ),
        '3. Demande de levée de l\'immunité de Patryk Jaki (vote)' =>
        array (
        ),
        '4. Demande de levée de l\'immunité de Beata Kempa (vote)' =>
        array (
        ),
        '5. Demande de levée de l\'immunité de Beata Mazurek (vote)' =>
        array (
        ),
        '6. Demande de levée de l\'immunité de Tomasz Piotr Poręba (vote)' =>
        array (
        ),
        '7. Nomination d\'un membre de la Cour des comptes – Tony Murphy (vote)' =>
        array (
        ),
        '8. Nomination d\'un membre de la Cour des comptes - Bettina Michelle Jakobsen (vote)' =>
        array (
        ),
        '9. Nomination d\'un membre de la Cour des comptes – Alejandro Blanco Fernández (vote)' =>
        array (
        ),
        '10. Règlement sur les données ***I (vote)' =>
        array (
          'A9-0031/2023' =>
          array (
          ),
        ),
        '11. Modification de certains règlements en ce qui concerne l’établissement et le fonctionnement du point d’accès unique européen ***I (vote)' =>
        array (
          'A9-0024/2023' =>
          array (
          ),
        ),
        '12. Point d’accès unique européen: accès aux informations concernant les services financiers, les marchés des capitaux et la durabilité ***I (vote)' =>
        array (
          'A9-0026/2023' =>
          array (
          ),
        ),
        '13. Modification de certaines directives eu égard à l’établissement et le fonctionnement du point d’accès unique européen ***I (vote)' =>
        array (
          'A9-0023/2023' =>
          array (
          ),
        ),
        '14. Discipline en matière de règlement, la prestation transfrontalière de services, la coopération en matière de surveillance, la fourniture de services accessoires de type bancaire et les exigences relatives aux dépositaires centraux de titres de pays tiers ***I (vote)' =>
        array (
          'A9-0047/2023' =>
          array (
          ),
        ),
        '15. Comptes économiques européens de l\'environnement: nouveaux modules ***I (vote)' =>
        array (
          'A9-0296/2023' =>
          array (
          ),
        ),
        '16. Mesures de conservation et d’exécution applicables dans la zone de réglementation de l’Organisation des pêches de l’Atlantique du Nord-Ouest (OPANO) ***I (vote)' =>
        array (
          'A9-0279/2023' =>
          array (
          ),
        ),
        '17. Déchets d’équipements électriques et électroniques (DEEE) ***I (vote)' =>
        array (
          'A9-0311/2023' =>
          array (
          ),
        ),
        '18. Accord de partenariat dans le secteur de la pêche durable UE/Madagascar et protocole de mise en oeuvre (2023-2027) *** (vote)' =>
        array (
          'A9-0299/2023' =>
          array (
          ),
        ),
        '19. Réception par type des véhicules à moteur et des moteurs en ce qui concerne leurs émissions et leur durabilité (Euro 7) ***I (vote)' =>
        array (
          'A9-0298/2023' =>
          array (
          ),
        ),
        '20. Systèmes des ressources propres de l\'Union * (vote)' =>
        array (
          'A9-0295/2023' =>
          array (
          ),
        ),
        '21. Renforcer le droit à la participation: légitimité et résilience des processus électoraux dans les systèmes politiques illibéraux et les régimes autoritaires (vote)' =>
        array (
          'A9-0323/2023' =>
          array (
          ),
        ),
        '22. Efficacité des sanctions de l\'UE à l’encontre de la Russie (vote)' =>
        array (
          'RC-B9-0453/2023' =>
          array (
          ),
        ),
      );

      $map['PV-9-2023-11-21-RCV'] = array (
        '1. Mesures destinées à assurer un niveau élevé commun de cybersécurité dans les institutions, organes et organismes de l’Union ***I (vote)' =>
        array (
          'A9-0064/2023' =>
          array (
          ),
        ),
        '2. Programme de documentation des captures de thon rouge ***I (vote)' =>
        array (
          'A9-0172/2021' =>
          array (
          ),
        ),
        '3. Règles communes visant à promouvoir la réparation des biens ***I (vote)' =>
        array (
          'A9-0316/2023' =>
          array (
          ),
        ),
        '4. Cadre de mesures en vue de renforcer l’écosystème européen de la fabrication de produits de technologie «zéro net» (règlement pour une industrie «zéro net») ***I (vote)' =>
        array (
          'A9-0343/2023' =>
          array (
          ),
        ),
        '5. Cadre de certification de l’Union relatif aux absorptions de carbone ***I (vote)' =>
        array (
          'A9-0329/2023' =>
          array (
          ),
        ),
        '6. Renforcement des normes de performance en matière d’émissions de CO2 pour les véhicules utilitaires lourds neufs ***I (vote)' =>
        array (
          'A9-0313/2023' =>
          array (
          ),
        ),
        '7. Possibilités d’amélioration de la fiabilité des audits et des contrôles réalisés par les autorités nationales dans le cadre de la gestion partagée (vote)' =>
        array (
          'A9-0297/2023' =>
          array (
          ),
        ),
        '8. Cadre de l’Union pour la situation sociale et professionnelle des artistes et des travailleurs des secteurs de la culture et de la création (vote)' =>
        array (
          'A9-0304/2023' =>
          array (
          ),
        ),
        '9. Mise en œuvre du principe de la primauté du droit de l’Union européenne (vote)' =>
        array (
          'A9-0341/2023' =>
          array (
          ),
        ),
        '10. Conférence des Nations unies sur le changement climatique 2023, Dubaï, Émirats arabes unis (COP28) (vote)' =>
        array (
          'B9-0458/2023' =>
          array (
          ),
        ),
        '11. Réduire les inégalités et promouvoir l’inclusion sociale en temps de crise pour les enfants et leurs familles (vote)' =>
        array (
          'A9-0360/2023' =>
          array (
          ),
        ),
        '12. Les enfants d’abord – Renforcer la garantie pour l’enfance deux ans après son adoption (vote)' =>
        array (
          'B9-0462/2023' =>
          array (
          ),
        ),
        '13. Mise en œuvre du programme "Corps européen de solidarité" 2021-2027 (vote)' =>
        array (
          'A9-0308/2023' =>
          array (
          ),
        ),
        '14. Mise en oeuvre du règlement instituant des mesures de reconstitution du stock d’anguilles européennes (vote)' =>
        array (
          'A9-0353/2023' =>
          array (
          ),
        ),
      );

      $map['PV-9-2023-11-22-RCV'] = array (
        '1. Projet de budget rectificatif nº 4/2023 – Réduction des crédits de paiement, autres ajustements et actualisations techniques (vote)' =>
        array (
          'A9-0363/2023' =>
          array (
          //         'Projet de budget rectificatif nº 4/2023 – Réduction des crédits de paiement, autres ajustements et actualisations techniques  - Draft amending budget No 4/2023: Reduction in payment appropriations, other adjustments and technical updates - Entwurf des Berichtigungshaushaltsplans Nr. 4/2023: Kürzung der Mittel für Zahlungen, sonstige Anpassungen und technische Aktualisierungen' => 'Projet de budget rectificatif nº 4/2023 – Réduction des crédits de paiement, autres ajustements et actualisations techniques  - Draft amending budget No 4/2023: Reduction in payment appropriations, other adjustments and technical updates - Entwurf des Berichtigungshaushaltsplans Nr. 4/2023: Kürzung der Mittel für Zahlungen, sonstige Anpassungen und technische Aktualisierungen',
          ),
        ),
        '2. Projet commun de budget général de l’Union européenne pour l’exercice 2024 (vote)' =>
        array (
          'A9-0362/2023' =>
          array (
          //         'Projet commun de budget général de l’Union européenne pour l’exercice 2024 - 2024 budgetary procedure: Joint text - Haushaltsverfahren 2024 – gemeinsamer Entwurf' => 'Projet commun de budget général de l’Union européenne pour l’exercice 2024 - 2024 budgetary procedure: Joint text - Haushaltsverfahren 2024 – gemeinsamer Entwurf',
          ),
        ),
        '3. Mobilisation du Fonds européen d’ajustement à la mondialisation en faveur des travailleurs licenciés - demande EGF/2023/002 BE/Makro - Belgique (vote)' =>
        array (
          'A9-0351/2023' =>
          array (
          //         'Mobilisation du Fonds européen d’ajustement à la mondialisation en faveur des travailleurs licenciés - demande EGF/2023/002 BE/Makro - Belgique - Mobilisation of the European Globalisation Adjustment Fund – application EGF/2023/002 BE/Makro - Belgium - Inanspruchnahme des Europäischen Fonds für die Anpassung an die Globalisierung – EGF/2023/002 BE/Makro – Belgien' => 'Mobilisation du Fonds européen d’ajustement à la mondialisation en faveur des travailleurs licenciés - demande EGF/2023/002 BE/Makro - Belgique - Mobilisation of the European Globalisation Adjustment Fund – application EGF/2023/002 BE/Makro - Belgium - Inanspruchnahme des Europäischen Fonds für die Anpassung an die Globalisierung – EGF/2023/002 BE/Makro – Belgien',
          ),
        ),
        '4. Publication électronique du Journal officiel de l’Union européenne *** (vote)' =>
        array (
          'A9-0352/2023' =>
          array (
          //         'Publication électronique du Journal officiel de l’Union européenne - Official Journal of the EU: electronic publication - Amtsblatt der Europäischen Union – elektronische Veröffentlichung' => 'Publication électronique du Journal officiel de l’Union européenne - Official Journal of the EU: electronic publication - Amtsblatt der Europäischen Union – elektronische Veröffentlichung',
          ),
        ),
        '5. Conclusion d\'un accord entre l’Union européenne et le Monténégro sur les activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes sur le territoire du Monténégro *** (vote)' =>
        array (
          'A9-0369/2023' =>
          array (
          //         'Conclusion d\'un accord entre l’Union européenne et le Monténégro sur les activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes sur le territoire du Monténégro - EU/Montenegro Agreement: operational activities carried out by the European Border and Coast Guard Agency in Montenegro - Vereinbarung zwischen der EU und Montenegro – operative Tätigkeiten, die von der Europäischen Agentur für die Grenz- und Küstenwache in Montenegro durchgeführt werden' => 'Conclusion d\'un accord entre l’Union européenne et le Monténégro sur les activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes sur le territoire du Monténégro - EU/Montenegro Agreement: operational activities carried out by the European Border and Coast Guard Agency in Montenegro - Vereinbarung zwischen der EU und Montenegro – operative Tätigkeiten, die von der Europäischen Agentur für die Grenz- und Küstenwache in Montenegro durchgeführt werden',
          ),
        ),
        '6. Accord de libre-échange entre l\'Union européenne et la Nouvelle-Zélande *** (vote)' =>
        array (
          'A9-0305/2023' =>
          array (
          //         'Accord de libre-échange entre l\'Union européenne et la Nouvelle-Zélande - EU/New Zealand Free Trade Agreement - Freihandelsabkommen zwischen der EU und Neuseeland' => 'Accord de libre-échange entre l\'Union européenne et la Nouvelle-Zélande - EU/New Zealand Free Trade Agreement - Freihandelsabkommen zwischen der EU und Neuseeland',
          ),
        ),
        '7. Accord de libre-échange entre l’Union européenne et la Nouvelle-Zélande (résolution) (vote)' =>
        array (
          'A9-0314/2023' =>
          array (
          //         'Accord de libre-échange entre l’Union européenne et la Nouvelle-Zélande (résolution) - EU/New Zealand Free Trade Agreement (Resolution) - Freihandelsabkommen zwischen der EU und Neuseeland (Entschließung)' => 'Accord de libre-échange entre l’Union européenne et la Nouvelle-Zélande (résolution) - EU/New Zealand Free Trade Agreement (Resolution) - Freihandelsabkommen zwischen der EU und Neuseeland (Entschließung)',
          ),
        ),
        '8. Nomination d’un membre de la Cour des comptes - Petri Sarvamaa (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '9. Nomination d’un membre de la Cour des comptes - Annemie Turtelboom (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '10. TVA: les règles à l’ère du numérique * (vote)' =>
        array (
          'A9-0327/2023' =>
          array (
          //         'TVA: les règles à l’ère du numérique - VAT: rules for the digital age - MwSt – Vorschriften für das digitale Zeitalter' => 'TVA: les règles à l’ère du numérique - VAT: rules for the digital age - MwSt – Vorschriften für das digitale Zeitalter',
          ),
        ),
        '11. TVA: accords de coopération administrative à l’ère du numérique * (vote)' =>
        array (
          'A9-0324/2023' =>
          array (
          //         'TVA: accords de coopération administrative à l’ère du numérique - VAT: administrative cooperation arrangements for the digital age - MwSt – für das digitale Zeitalter erforderliche Regelungen für die Zusammenarbeit der Verwaltungsbehörden' => 'TVA: accords de coopération administrative à l’ère du numérique - VAT: administrative cooperation arrangements for the digital age - MwSt – für das digitale Zeitalter erforderliche Regelungen für die Zusammenarbeit der Verwaltungsbehörden',
          ),
        ),
        '12. TVA: assujettis, régimes particuliers concernant les ventes à distance de biens importés et la déclaration et le paiement de la TVA à l\'importation * (vote)' =>
        array (
          'A9-0320/2023' =>
          array (
          //         'TVA: assujettis, régimes particuliers concernant les ventes à distance de biens importés et la déclaration et le paiement de la TVA à l\'importation - VAT: taxable persons, special scheme and special arrangements for declaration and payment relating to distance sales of imported goods - MwSt – Steuerpflichtige und Sonderregelungen für die Erklärung und Entrichtung bei Fernverkäufen von eingeführten Gegenständen' => 'TVA: assujettis, régimes particuliers concernant les ventes à distance de biens importés et la déclaration et le paiement de la TVA à l\'importation - VAT: taxable persons, special scheme and special arrangements for declaration and payment relating to distance sales of imported goods - MwSt – Steuerpflichtige und Sonderregelungen für die Erklärung und Entrichtung bei Fernverkäufen von eingeführten Gegenständen',
          ),
        ),
        '13. Utilisation des produits phytopharmaceutiques compatible avec le développement durable ***I (vote)' =>
        array (
          'A9-0339/2023' =>
          array (
          //         'Utilisation des produits phytopharmaceutiques compatible avec le développement durable - Sustainable use of plant protection products - Nachhaltige Verwendung von Pflanzenschutzmitteln' => 'Utilisation des produits phytopharmaceutiques compatible avec le développement durable - Sustainable use of plant protection products - Nachhaltige Verwendung von Pflanzenschutzmitteln',
          ),
        ),
        '14. Emballages et déchets d\'emballages ***I (vote)' =>
        array (
          'A9-0319/2023' =>
          array (
          //         'Emballages et déchets d\'emballages - Packaging and packaging waste   - Verpackungen und Verpackungsabfälle' => 'Emballages et déchets d\'emballages - Packaging and packaging waste   - Verpackungen und Verpackungsabfälle',
          ),
        ),
        '15. Transition numérique et droit administratif (vote)' =>
        array (
          'A9-0309/2023' =>
          array (
          //         'Transition numérique et droit administratif - Digitalisation and Administrative Law - Digitalisierung und Verwaltungsrecht' => 'Transition numérique et droit administratif - Digitalisation and Administrative Law - Digitalisierung und Verwaltungsrecht',
          ),
        ),
        '16. Propositions du Parlement européen pour la révision des traités (vote)' =>
        array (
          'A9-0337/2023' =>
          array (
          //         'Propositions du Parlement européen pour la révision des traités - Proposals of the European Parliament for the amendment of the Treaties - Vorschläge des Europäischen Parlaments zur Änderung der Verträge' => 'Propositions du Parlement européen pour la révision des traités - Proposals of the European Parliament for the amendment of the Treaties - Vorschläge des Europäischen Parlaments zur Änderung der Verträge',
          ),
        ),
        '17. Négociations concernant un accord sur le statut des activités opérationnelles menées par Frontex en Mauritanie (vote)' =>
        array (
          'A9-0358/2023' =>
          array (
          //         'Négociations concernant un accord sur le statut des activités opérationnelles menées par Frontex en Mauritanie - Negotiations on a status agreement on operational activities carried out by Frontex in Mauritania - Verhandlungen über eine Statusvereinbarung über operative Tätigkeiten, die von Frontex in Mauretanien durchgeführt werden' => 'Négociations concernant un accord sur le statut des activités opérationnelles menées par Frontex en Mauritanie - Negotiations on a status agreement on operational activities carried out by Frontex in Mauritania - Verhandlungen über eine Statusvereinbarung über operative Tätigkeiten, die von Frontex in Mauretanien durchgeführt werden',
          ),
        ),
      );

      $map['PV-9-2023-12-12-RCV'] = array (
        '1. Constitution des délégations à l\'Assemblée parlementaire paritaire OEACP-UE, à l\'Assemblée parlementaire Afrique-UE, à l\'Assemblée parlementaire Caraïbes-UE et à l\'Assemblée parlementaire Pacifique-UE, et définition de leur composition numérique (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '2. Aliments destinés à la consommation humaine: modification de certaines des directives dites "petit-déjeuner" ***I (vote)' =>
        array (
          'A9-0385/2023' => []
        ),
        '3. Redevances et droits dus à l’Agence européenne des médicaments ***I (vote)' =>
        array (
          'A9-0224/2023' => []
        ),
        '4. Reconnaissance des qualifications professionnelles des infirmiers responsables de soins généraux formés en Roumanie ***I (vote)' =>
        array (
          'A9-0381/2023' => []
        ),
        '5. Autorisation octroyée à la France de négocier un accord bilatéral avec l’Algérie sur des questions liées à la coopération judiciaire en matière civile et commerciale ***I (vote)' =>
        array (
          'A9-0356/2023' => []
        ),
        '6. Autorisation octroyée à la France de négocier un accord bilatéral avec l’Algérie sur des questions liées à la coopération judiciaire en matière de droit de la famille * (vote)' =>
        array (
          'A9-0355/2023' => []
        ),
        '7. Nomination d\'un membre de la Cour des comptes - Hans Lindblad (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '8. Nomination d\'un membre de la Cour des comptes - João Leão (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '9. Accord de partenariat dans le secteur de la pêche CE/Kiribati (2023-2028). Protocole de mise en œuvre *** (vote)' =>
        array (
          'A9-0380/2023' => []
        ),
        '10. Exigences minimales relatives aux durées minimales des pauses et des temps de repos journaliers et hebdomadaires dans le secteur du transport occasionnel de voyageurs ***I (vote)' =>
        array (
          'A9-0370/2023' => []
        ),
        '11. Cadre permettant d’assurer un approvisionnement durable et sûr en matières premières critiques ***I (vote)' =>
        array (
          'A9-0260/2023' => []
        ),
        '12. Les élections européennes 2024 (vote)' =>
        array (
          'A9-0332/2023' => []
        ),
        '13. Petits réacteurs modulaires (vote)' =>
        array (
          'A9-0408/2023' => []
        ),
        '14. Santé mentale (vote)' =>
        array (
          'A9-0367/2023' => []
        ),
        '15. Mise en œuvre de l\'instrument de voisinage, de coopération au développement et de coopération internationale - Europe dans le monde (vote)' =>
        array (
          'A9-0374/2023' => []
        ),
        '16. Conception addictive des services en ligne et protection des consommateurs sur le marché unique de l’UE (vote)' =>
        array (
          'A9-0340/2023' => []
        ),
        '17. Le rôle de la politique fiscale en temps de crise (vote)' =>
        array (
          'A9-0336/2023' => []
        ),
        '18. Poursuite de la réforme des règles d’imposition des sociétés (vote)' =>
        array (
          'A9-0359/2023' => []
        ),
        '19. Redéfinition du futur cadre des fonds structurels de l’Union visant à soutenir les régions particulièrement touchées par les défis liés aux transitions écologique, numérique et du secteur automobile (vote)' =>
        array (
          'A9-0326/2023' => []
        ),
      );

      $map['PV-9-2023-12-13-RCV'] = array (
        '1. Espace européen des données de santé ***I (vote)' =>
        array (
          'A9-0395/2023' => []
        ),
        '2. Objection à un acte délégué conformément à l’article 111, paragraphe 3, du règlement: Ajustement des critères de taille pour les micro-, petites, moyennes et grandes entreprises ou pour les groupes (vote)' =>
        array (
          'B9-0493/2023' => []
        ),
        '3. Relations UE-Japon (vote)' =>
        array (
          'A9-0373/2023' => []
        ),
        '4. La situation des enfants privés de liberté dans le monde (vote)' =>
        array (
          'A9-0371/2023' => []
        ),
        '5. Rôle de la politique de développement de l\'UE dans la transformation des industries extractives pour le développement durable dans les pays en développement (vote)' =>
        array (
          'A9-0322/2023' => []
        ),
        '6. La coopération au développement de l’Union européenne pour améliorer l’accès à l’éducation et à la formation dans les pays en développement (vote)' =>
        array (
          'A9-0338/2023' => []
        ),
        '7. Maladies non transmissibles (vote)' =>
        array (
          'A9-0366/2023' => []
        ),
        '8. Relations UE-États-Unis (vote)' =>
        array (
          'A9-0372/2023' => []
        ),
        '9. Relations UE-Chine (vote)' =>
        array (
          'A9-0375/2023' => []
        ),
        '10. Mise en œuvre des dispositions du traité relatives aux procédures législatives spéciales (vote)' =>
        array (
          'A9-0384/2023' => []
        ),
        '11. 30 ans des critères de Copenhague - donner un nouvel élan à la politique d\'élargissement de l\'Union européenne (vote)' =>
        array (
          'RC-B9-0500/2023' => []
        ),
        '12. Relations UE-Taïwan en matière de commerce et d’investissement (vote)' => [],
        '13. Mise en œuvre du règlement de 2018 relatif au blocage géographique dans le marché unique numérique (vote)' =>
        array (
          'A9-0335/2023' => []
        ),
      );

      $map['PV-9-2023-12-14-RCV'] = array (
        '1. Objection conformément à l’article 112, paragraphes 2 et 3 ainsi que 4, point c), du règlement: limites maximales applicables aux résidus de fipronil (vote)' =>
        array (
          'B9-0488/2023' => []
        ),
        '2. Objection conformément à l’article 112, paragraphes 2 et 3 ainsi que 4, point c), du règlement: limites maximales applicables aux résidus de tricyclazole (vote)' =>
        array (
          'B9-0494/2023' => []
        ),
        '3. Objection conformément à l’article 112, paragraphes 2 et 3, du règlement: maïs génétiquement modifié Bt11 × MIR162 × MIR604 × MON 89034 × 5307 × GA21 et trente sous-combinaisons (vote)' =>
        array (
          'B9-0492/2023' => []
        ),
        '4. Objection conformément à l’article 112, paragraphes 2 et 3, du règlement: colza génétiquement modifié Ms8, Rf3 et Ms8 × Rf3 (vote)' =>
        array (
          'B9-0490/2023' => []
        ),
        '5. Absence d\'informations sur la situation de Mikalaï Statkevitch et récentes attaques contre des membres de la famille de personnalités politiques et de militants biélorusses (vote)' =>
        array (
          'RC-B9-0509/2023' => []
        ),
        '6. Les communautés massaï en Tanzanie (vote)' =>
        array (
          'RC-B9-0511/2023' => []
        ),
        '7. Enlèvement d\'enfants tibétains et pratiques d\'assimilation forcée dans des internats chinois au Tibet (vote)' =>
        array (
          'RC-B9-0510/2023' => []
        ),
        '8. Accroître l’innovation et la compétitivité industrielle et technologique grâce à un environnement favorable aux jeunes pousses et aux entreprises en expansion (vote)' => [
          'A9-0383/2023' => []
        ],
        '9. Compétence, loi applicable, reconnaissance des décisions et acceptation d\'actes authentiques en matière de parentalité et concernant la création d\'un certificat européen de parentalité * (vote)' =>
        array (
          'A9-0368/2023' => []
        ),
        '10. Jeunes chercheurs (vote)' => [],
        '11. Frontex: construire sur la base de l’enquête du groupe de travail LIBE sur le contrôle de Frontex (vote)' =>
        array (
          'B9-0499/2023' => []
        ),
        '12. Banque européenne de l\'hydrogène (vote)' =>
        array (
          'A9-0379/2023' => []
        ),
        '13. Tentative de coup d\'État au Guatemala (vote)' =>
        array (
          'RC-B9-0526/2023' => []
        ),
      );

      $map['PV-9-2024-01-16-RCV'] = array (
        '1. Élection d’un vice-président du Parlement européen (en remplacement de Nicola Beer) (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '2. Objectifs ciblés pour la fixation des possibilités de pêche ***I (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '3. Substances appauvrissant la couche d’ozone ***I (vote)' =>
        array (
          'A9-0050/2023' =>
          array (
            'Substances appauvrissant la couche d’ozone' => 'Substances appauvrissant la couche d’ozone',
          ),
        ),
        '4. Règlement sur les gaz fluorés ***I (vote)' =>
        array (
          'A9-0048/2023' =>
          array (
            'Règlement sur les gaz fluorés' => 'Règlement sur les gaz fluorés',
          ),
        ),
        '5. Modification de la directive concernant les marchés d’instruments financiers (MiFID II) ***I (vote)' =>
        array (
          'A9-0039/2023' =>
          array (
            'Modification de la directive concernant les marchés d’instruments financiers (MiFID II)' => 'Modification de la directive concernant les marchés d’instruments financiers (MiFID II)',
          ),
        ),
        '6. Modification du règlement sur les marchés d’instruments financiers (MiFIR) ***I (vote)' =>
        array (
          'A9-0040/2023' =>
          array (
            'Modification du règlement sur les marchés d’instruments financiers (MiFIR)' => 'Modification du règlement sur les marchés d’instruments financiers (MiFIR)',
          ),
        ),
        '7. Mesures commerciales exceptionnelles en faveur des pays et territoires qui participent ou sont liés au processus de stabilisation et d’association (codification) ***I (vote)' =>
        array (
          'A9-0001/2024' =>
          array (
            'Mesures commerciales exceptionnelles en faveur des pays et territoires qui participent ou sont liés au processus de stabilisation et d’association (codification)' => 'Mesures commerciales exceptionnelles en faveur des pays et territoires qui participent ou sont liés au processus de stabilisation et d’association (codification)',
          ),
        ),
        '8. Abattement pour la réduction de la distorsion fiscale en faveur de l’endettement et limitation de la déductibilité des intérêts aux fins de l’impôt sur les sociétés * (vote)' =>
        array (
          'A9-0387/2023' =>
          array (
            'Abattement pour la réduction de la distorsion fiscale en faveur de l’endettement et limitation de la déductibilité des intérêts aux fins de l’impôt sur les sociétés' => 'Abattement pour la réduction de la distorsion fiscale en faveur de l’endettement et limitation de la déductibilité des intérêts aux fins de l’impôt sur les sociétés',
          ),
        ),
        '9. Exécution du programme Erasmus+ 2021-2027 (vote)' =>
        array (
          'A9-0413/2023' =>
          array (
            'Exécution du programme Erasmus+ 2021-2027' => 'Exécution du programme Erasmus+ 2021-2027',
          ),
        ),
        '10. Mise en œuvre du programme «Europe créative» 2021-2027 (vote)' =>
        array (
          'A9-0425/2023' =>
          array (
            'Mise en œuvre du programme «Europe créative» 2021-2027' => 'Mise en œuvre du programme «Europe créative» 2021-2027',
          ),
        ),
        '11. Mise en œuvre du programme «Citoyens, égalité, droits et valeurs» 2021-2027 - Engagement et participation des citoyens (vote)' =>
        array (
          'A9-0392/2023' =>
          array (
            'Mise en œuvre du programme «Citoyens, égalité, droits et valeurs» 2021-2027 - Engagement et participation des citoyens' => 'Mise en œuvre du programme «Citoyens, égalité, droits et valeurs» 2021-2027 - Engagement et participation des citoyens',
          ),
        ),
        '12. Mise en œuvre du développement territorial (RDC, titre III, chapitre II) et son application dans l’agenda territorial européen 2030 (vote)' =>
        array (
          'A9-0420/2023' =>
          array (
            'Mise en œuvre du développement territorial (RDC, titre III, chapitre II) et son application dans l’agenda territorial européen 2030' => 'Mise en œuvre du développement territorial (RDC, titre III, chapitre II) et son application dans l’agenda territorial européen 2030',
          ),
        ),
        '13. Politique de concurrence – rapport annuel 2023 (vote)' =>
        array (
          'A9-0427/2023' =>
          array (
            'Politique de concurrence – rapport annuel 2023' => 'Politique de concurrence – rapport annuel 2023',
          ),
        ),
        '14. L’union bancaire – rapport annuel 2023 (vote)' =>
        array (
          'A9-0431/2023' =>
          array (
            'L’union bancaire – rapport annuel 2023' => 'L’union bancaire – rapport annuel 2023',
          ),
        ),
        '15. Rôle du Parlement européen et sa diplomatie parlementaire dans la politique étrangère et de sécurité de l’Union (vote)' =>
        array (
          'A9-0405/2023' =>
          array (
            'Rôle du Parlement européen et sa diplomatie parlementaire dans la politique étrangère et de sécurité de l’Union' => 'Rôle du Parlement européen et sa diplomatie parlementaire dans la politique étrangère et de sécurité de l’Union',
          ),
        ),
        '16. L’aviation électrique - une solution pour les vols court et moyen-courriers (vote)' =>
        array (
          'A9-0438/2023' =>
          array (
            'L’aviation électrique - une solution pour les vols court et moyen-courriers' => 'L’aviation électrique - une solution pour les vols court et moyen-courriers',
          ),
        ),
        '17. Les enjeux actuels et futurs en matière de coopération transfrontalière avec les pays voisins (vote)' =>
        array (
          'A9-0415/2023' =>
          array (
            'Les enjeux actuels et futurs en matière de coopération transfrontalière avec les pays voisins' => 'Les enjeux actuels et futurs en matière de coopération transfrontalière avec les pays voisins',
          ),
        ),
      );

      $map['PV-9-2024-01-17-RCV'] = array (
        '1. Décision d\'engager des négociations interinstitutionnelles: Coordination efficace des politiques économiques et surveillance budgétaire multilatérale***I (vote)' => [
          'A9-0439/2023' => [
            'Coordination efficace des politiques économiques et surveillance budgétaire multilatérale' => 'Coordination efficace des politiques économiques et surveillance budgétaire multilatérale'
          ],
        ],
        '2. Objection en vertu de l\'article 112, paragraphes 2 et 3, et paragraphe 4, point c): limites maximales applicables aux résidus de thiaclopride (vote)' =>
        array (
          'B9-0057/2024' =>
          array (
            'Objection en vertu de l\'article 112, paragraphes 2 et 3, et paragraphe 4, point c): limites maximales applicables aux résidus de thiaclopride' => 'Objection en vertu de l\'article 112, paragraphes 2 et 3, et paragraphe 4, point c): limites maximales applicables aux résidus de thiaclopride',
          ),
        ),
        '3. Accord-cadre global de partenariat et de coopération CE/Indonésie: adhésion de la Croatie à l\'Union européenne *** (vote)' =>
        array (
          'A9-0428/2023' =>
          array (
            'Accord-cadre global de partenariat et de coopération CE/Indonésie: adhésion de la Croatie à l\'Union européenne' => 'Accord-cadre global de partenariat et de coopération CE/Indonésie: adhésion de la Croatie à l\'Union européenne',
          ),
        ),
        '4. Donner aux consommateurs les moyens d’agir en faveur de la transition écologique ***I (vote)' =>
        array (
          'A9-0099/2023' =>
          array (
            'Donner aux consommateurs les moyens d’agir en faveur de la transition écologique' => 'Donner aux consommateurs les moyens d’agir en faveur de la transition écologique',
          ),
        ),
        '5. Mercure: amalgames dentaires et autres produits contenant du mercure ajouté faisant l’objet de restrictions de fabrication, d’importation et d’exportation ***I (vote)' =>
        array (
          'A9-0002/2024' =>
          array (
            'Mercure: amalgames dentaires et autres produits contenant du mercure ajouté faisant l’objet de restrictions de fabrication, d’importation et d’exportation' => 'Mercure: amalgames dentaires et autres produits contenant du mercure ajouté faisant l’objet de restrictions de fabrication, d’importation et d’exportation',
          ),
        ),
        '6. La diversité culturelle et les conditions pour les auteurs sur le marché européen de la diffusion de musique en continu (vote)' =>
        array (
          'A9-0388/2023' =>
          array (
            'La diversité culturelle et les conditions pour les auteurs sur le marché européen de la diffusion de musique en continu' => 'La diversité culturelle et les conditions pour les auteurs sur le marché européen de la diffusion de musique en continu',
          ),
        ),
        '7. Dissolution prévue des principales structures anti-corruption en Slovaquie, et répercussions sur l\'état de droit (vote)' =>
        array (
          'B9-0062/2024' =>
          array (
            'Dissolution prévue des principales structures anti-corruption en Slovaquie, et répercussions sur l\'état de droit' => 'Dissolution prévue des principales structures anti-corruption en Slovaquie, et répercussions sur l\'état de droit',
          ),
        ),
        '8. Promotion de la liberté de la recherche scientifique dans l\'UE (vote)' =>
        array (
          'A9-0393/2023' =>
          array (
            'Promotion de la liberté de la recherche scientifique dans l\'UE' => 'Promotion de la liberté de la recherche scientifique dans l\'UE',
          ),
        ),
        '9. Mise en œuvre des dispositions du traité concernant les parlements nationaux (vote)' =>
        array (
          'A9-0429/2023' =>
          array (
            'Mise en œuvre des dispositions du traité concernant les parlements nationaux' => 'Mise en œuvre des dispositions du traité concernant les parlements nationaux',
          ),
        ),
        '10. Mise en œuvre des dispositions du traité relatives à la citoyenneté de l’Union (vote)' =>
        array (
          'A9-0436/2023' =>
          array (
            'Mise en œuvre des dispositions du traité relatives à la citoyenneté de l’Union' => 'Mise en œuvre des dispositions du traité relatives à la citoyenneté de l’Union',
          ),
        ),
        '11. Construction d’une stratégie portuaire européenne globale (vote)' =>
        array (
          'A9-0443/2023' =>
          array (
            'Construction d’une stratégie portuaire européenne globale' => 'Construction d’une stratégie portuaire européenne globale',
          ),
        ),
        '12. Mise en œuvre de l’accord économique et commercial global (AECG) entre l\'Union européenne et le Canada (vote)' =>
        array (
          'A9-0400/2023' =>
          array (
            'Mise en œuvre de l’accord économique et commercial global (AECG) entre l\'Union européenne et le Canada' => 'Mise en œuvre de l’accord économique et commercial global (AECG) entre l\'Union européenne et le Canada',
          ),
        ),
        '13. Stratégie de l\'UE pour l\'Asie centrale (vote)' =>
        array (
          'A9-0407/2023' =>
          array (
            'Stratégie de l\'UE pour l\'Asie centrale' => 'Stratégie de l\'UE pour l\'Asie centrale',
          ),
        ),
        '14. Implications en matière de sécurité et de défense de l’influence de la Chine sur les infrastructures critiques dans l’Union européenne (vote)' =>
        array (
          'A9-0401/2023' =>
          array (
            'Implications en matière de sécurité et de défense de l’influence de la Chine sur les infrastructures critiques dans l’Union européenne' => 'Implications en matière de sécurité et de défense de l’influence de la Chine sur les infrastructures critiques dans l’Union européenne',
          ),
        ),
        '15. Implications stratégiques du développement des mondes virtuels - aspects de droit civil, de droit des sociétés, de droit commercial et de droit de la propriété intellectuelle (vote)' =>
        array (
          'A9-0442/2023' =>
          array (
            'Implications stratégiques du développement des mondes virtuels - aspects de droit civil, de droit des sociétés, de droit commercial et de droit de la propriété intellectuelle' => 'Implications stratégiques du développement des mondes virtuels - aspects de droit civil, de droit des sociétés, de droit commercial et de droit de la propriété intellectuelle',
          ),
        ),
        '16. Conscience historique européenne (vote)' =>
        array (
          'A9-0402/2023' =>
          array (
            'Conscience historique européenne' => 'Conscience historique européenne',
          ),
        ),
        '17. La coopération au développement de l’Union en faveur de l’accès à l’énergie dans les pays en développement (vote)' =>
        array (
          'A9-0441/2023' =>
          array (
            'La coopération au développement de l’Union en faveur de l’accès à l’énergie dans les pays en développement' => 'La coopération au développement de l’Union en faveur de l’accès à l’énergie dans les pays en développement',
          ),
        ),
        '18. Mondes virtuels - perspectives, risques et implications politiques pour le marché unique (vote)' =>
        array (
          'A9-0397/2023' =>
          array (
            'Mondes virtuels - perspectives, risques et implications politiques pour le marché unique' => 'Mondes virtuels - perspectives, risques et implications politiques pour le marché unique',
          ),
        ),
        '19. Relations UE-Inde (vote)' =>
        array (
          'A9-0435/2023' =>
          array (
            'Relations UE-Inde' => 'Relations UE-Inde',
          ),
        ),
        '20. Le rôle de la diplomatie préventive dans la gestion des conflits gelés dans le monde: une occasion manquée ou un changement pour l’avenir? (vote)' =>
        array (
          'A9-0404/2023' =>
          array (
            'Le rôle de la diplomatie préventive dans la gestion des conflits gelés dans le monde: une occasion manquée ou un changement pour l’avenir?' => 'Le rôle de la diplomatie préventive dans la gestion des conflits gelés dans le monde: une occasion manquée ou un changement pour l’avenir?',
          ),
        ),
        '21. Activités du Médiateur européen – rapport annuel 2022 (vote)' =>
        array (
          'A9-0414/2023' =>
          array (
            'Activités du Médiateur européen – rapport annuel 2022' => 'Activités du Médiateur européen – rapport annuel 2022',
          ),
        ),
        '22. Transparence et responsabilité des organisations non gouvernementales financées par le budget de l’Union (vote)' =>
        array (
          'A9-0446/2023' =>
          array (
            'Transparence et responsabilité des organisations non gouvernementales financées par le budget de l’Union' => 'Transparence et responsabilité des organisations non gouvernementales financées par le budget de l’Union',
          ),
        ),
      );

      $map['PV-9-2024-01-18-RCV'] = array (
        '1. La persécution persistante du Falun Gong en Chine, notamment le cas de M. Ding Yuande (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '2. Menace de famine à la suite de l’extension du conflit au Soudan (vote)' =>
        array (
        //       'B9-0064/2024' =>
        //       array (
          //         'Tadjikistan: répression de l\'État contre les médias indépendants' => 'Tadjikistan: répression de l\'État contre les médias indépendants',
        //       ),
        ),
        '4. Rapport de mise en oeuvre sur le règlement (CE) n° 1924/2006 concernant les allégations nutritionnelles et de santé portant sur les denrées alimentaires (vote)' =>
        array (
          'A9-0416/2023' =>
          array (
            'Rapport de mise en oeuvre sur le règlement (CE) n° 1924/2006 concernant les allégations nutritionnelles et de santé portant sur les denrées alimentaires' => 'Rapport de mise en oeuvre sur le règlement (CE) n° 1924/2006 concernant les allégations nutritionnelles et de santé portant sur les denrées alimentaires',
          ),
        ),
        '5. Protection des intérêts financiers de l’Union européenne – lutte contre la fraude – rapport annuel 2022 (vote)' =>
        array (
          'A9-0434/2023' =>
          array (
            'Protection des intérêts financiers de l’Union européenne – lutte contre la fraude – rapport annuel 2022' => 'Protection des intérêts financiers de l’Union européenne – lutte contre la fraude – rapport annuel 2022',
          ),
        ),
        '6. Façonner la position de l’Union sur l’instrument contraignant des Nations unies relatif aux entreprises et aux droits de l’homme, en particulier en ce qui concerne l’accès aux voies de recours et la protection des victimes (vote)' =>
        array (
          'A9-0421/2023' =>
          array (
            'Façonner la position de l’Union sur l’instrument contraignant des Nations unies relatif aux entreprises et aux droits de l’homme, en particulier en ce qui concerne l’accès aux voies de recours et la protection des victimes' => 'Façonner la position de l’Union sur l’instrument contraignant des Nations unies relatif aux entreprises et aux droits de l’homme, en particulier en ce qui concerne l’accès aux voies de recours et la protection des victimes',
          ),
        ),
        '7. Impact de la pêche illicite sur la sécurité alimentaire: le rôle de l\'Union européenne (vote)' =>
        array (
          'A9-0433/2023' =>
          array (
            'Impact de la pêche illicite sur la sécurité alimentaire: le rôle de l\'Union européenne' => 'Impact de la pêche illicite sur la sécurité alimentaire: le rôle de l\'Union européenne',
          ),
        ),
        '8. Extension de la liste des infractions de l\'UE aux discours de haine et aux crimes de haine (vote)' =>
        array (
          'A9-0377/2023' =>
          array (
            'Extension de la liste des infractions de l\'UE aux discours de haine et aux crimes de haine - Extending the list of EU crimes to hate speech and hate crime - Erweiterung der Liste der EU-Straftatbestände um Hetze und Hasskriminalität' => 'Extension de la liste des infractions de l\'UE aux discours de haine et aux crimes de haine - Extending the list of EU crimes to hate speech and hate crime - Erweiterung der Liste der EU-Straftatbestände um Hetze und Hasskriminalität',
          ),
        ),
        '9. État des lieux de la mise en œuvre de la politique commune de la pêche et perspectives d\'avenir (vote)' =>
        array (
          'A9-0357/2023' =>
          array (
            'État des lieux de la mise en œuvre de la politique commune de la pêche et perspectives d\'avenir' => 'État des lieux de la mise en œuvre de la politique commune de la pêche et perspectives d\'avenir',
          ),
        ),
        '10. Plan d’action de l’UE: protéger et restaurer les écosystèmes marins pour une pêche durable et résiliente (vote)' =>
        array (
          'A9-0437/2023' =>
          array (
            'Plan d’action de l’UE: protéger et restaurer les écosystèmes marins pour une pêche durable et résiliente' => 'Plan d’action de l’UE: protéger et restaurer les écosystèmes marins pour une pêche durable et résiliente',
          ),
        ),
        '11. Mise en œuvre du règlement portant organisation commune des marchés dans le secteur des produits de la pêche et de l’aquaculture – règlement (UE) nº 1379/2013 (vote)' =>
        array (
          'A9-0406/2023' =>
          array (
            'Mise en œuvre du règlement portant organisation commune des marchés dans le secteur des produits de la pêche et de l’aquaculture – règlement (UE) nº 1379/2013' => 'Mise en œuvre du règlement portant organisation commune des marchés dans le secteur des produits de la pêche et de l’aquaculture – règlement (UE) nº 1379/2013',
          ),
        ),
        '12. Dimension de genre de l’augmentation du coût de la vie et des répercussions de la crise énergétique (vote)' =>
        array (
          'A9-0430/2023' =>
          array (
            'Dimension de genre de l’augmentation du coût de la vie et des répercussions de la crise énergétique' => 'Dimension de genre de l’augmentation du coût de la vie et des répercussions de la crise énergétique',
          ),
        ),
        '13. Énergie géothermique (vote)' =>
        array (
          'A9-0432/2023' =>
          array (
            'Énergie géothermique' => 'Énergie géothermique',
          ),
        ),
        '14. Situation des droits fondamentaux dans l\'UE en 2022 et 2023 (vote)' =>
        array (
          'A9-0376/2023' =>
          array (
            'Situation des droits fondamentaux dans l\'UE en 2022 et 2023' => 'Situation des droits fondamentaux dans l\'UE en 2022 et 2023',
          ),
        ),
        '15. Situation humanitaire à Gaza, nécessité de parvenir à un cessez-le-feu et risques d\'escalade régionale (vote)' =>
        array (
          'RC-B9-0068/2024' =>
          array (
            'Situation humanitaire à Gaza, nécessité de parvenir à un cessez-le-feu et risques d\'escalade régionale' => 'Situation humanitaire à Gaza, nécessité de parvenir à un cessez-le-feu et risques d\'escalade régionale',
          ),
        ),
        '16. Révision du mandat de l\'Autorité européenne du travail (vote)' =>
        array (
          'B9-0059/2024' =>
          array (
            'Révision du mandat de l\'Autorité européenne du travail' => 'Révision du mandat de l\'Autorité européenne du travail',
          ),
        ),
      );

      $map['PV-9-2024-02-06-RCV'] = array (
        '1. Demande de levée de l\'immunité de Ioannis Lagos (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '2. Demande de levée de l\'immunité de Georgios Kyrtsos (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '3. Demande de levée de l\'immunité d\'Eva Kaili (vote)' =>
        array (
        //       'pushed' => NULL,
        ),
        '4. Effet à l’échelle de l’Union de certaines décisions de déchéance du droit de conduire ***I (vote)' =>
        array (
          'A9-0410/2023' =>
          array (
            'Effet à l’échelle de l’Union de certaines décisions de déchéance du droit de conduire' => 'Effet à l’échelle de l’Union de certaines décisions de déchéance du droit de conduire',
          ),
        ),
        '5. Modification de la décision 2009/917/JAI du Conseil en ce qui concerne sa mise en conformité avec les règles de l’Union relatives à la protection des données à caractère personnel ***I (vote)' =>
        array (
          'A9-0361/2023' =>
          array (
            'Modification de la décision 2009/917/JAI du Conseil en ce qui concerne sa mise en conformité avec les règles de l’Union relatives à la protection des données à caractère personnel' => 'Modification de la décision 2009/917/JAI du Conseil en ce qui concerne sa mise en conformité avec les règles de l’Union relatives à la protection des données à caractère personnel',
          ),
        ),
        '6. Déchets d’équipements électriques et électroniques (DEEE) ***I (vote)' =>
        array (
          'A9-0311/2023' =>
          array (
            'Déchets d’équipements électriques et électroniques (DEEE)' => 'Déchets d’équipements électriques et électroniques (DEEE)',
          ),
        ),
        '7. Mesures destinées à assurer un niveau élevé d’interopérabilité du secteur public dans l’ensemble de l’Union (règlement pour une Europe interopérable) ***I (vote)' =>
        array (
          'A9-0254/2023' =>
          array (
            'Mesures destinées à assurer un niveau élevé d’interopérabilité du secteur public dans l’ensemble de l’Union (règlement pour une Europe interopérable)' => 'Mesures destinées à assurer un niveau élevé d’interopérabilité du secteur public dans l’ensemble de l’Union (règlement pour une Europe interopérable)',
          ),
        ),
        '8. Modification du règlement établissant des mesures de gestion, de conservation et de contrôle applicables dans la zone de la convention de la CICTA et du règlement établissant un plan pluriannuel de gestion du thon rouge dans l’Atlantique Est et la mer Méditerranée ***I (vote)' =>
        array (
          'A9-0301/2023' =>
          array (
            'Modification du règlement établissant des mesures de gestion, de conservation et de contrôle applicables dans la zone de la convention de la CICTA et du règlement établissant un plan pluriannuel de gestion du thon rouge dans l’Atlantique Est et la mer Méditerranée' => 'Modification du règlement établissant des mesures de gestion, de conservation et de contrôle applicables dans la zone de la convention de la CICTA et du règlement établissant un plan pluriannuel de gestion du thon rouge dans l’Atlantique Est et la mer Méditerranée',
          ),
        ),
      );

      $map['PV-9-2024-02-07-RCV'] = array (
        '1. Dérogation temporaire: lutte contre les abus sexuels commis contre des enfants en ligne ***I (vote)' =>
        array (
          'A9-0021/2024' =>
          array (
            'Dérogation temporaire: lutte contre les abus sexuels commis contre des enfants en ligne' => 'Dérogation temporaire: lutte contre les abus sexuels commis contre des enfants en ligne',
          ),
        ),
        '2. Décision habilitant la République française à négocier, à signer et à conclure un accord international sur les exigences en matière de sécurité et d’interopérabilité sur la liaison fixe transmanche ***I (vote)' =>
        array (
          'A9-0018/2024' =>
          array (
            'Décision habilitant la République française à négocier, à signer et à conclure un accord international sur les exigences en matière de sécurité et d’interopérabilité sur la liaison fixe transmanche' => 'Décision habilitant la République française à négocier, à signer et à conclure un accord international sur les exigences en matière de sécurité et d’interopérabilité sur la liaison fixe transmanche',
          ),
        ),
        '3. Conclusion de l’accord de facilitation des investissements durables entre l’Union européenne et la République d’Angola *** (vote)' =>
        array (
          'A9-0005/2024' =>
          array (
            'Conclusion de l’accord de facilitation des investissements durables entre l’Union européenne et la République d’Angola' => 'Conclusion de l’accord de facilitation des investissements durables entre l’Union européenne et la République d’Angola',
          ),
        ),
        '4. Amendements à la directive sur les gestionnaires de fonds d\'investissement alternatifs (GFIA) et à la directive sur les organismes de placement collectif en valeurs mobilières (OPCVM) ***I (vote)' =>
        array (
          'A9-0020/2023' =>
          array (
            'Modification de la Directive sur les gestionnaires de fonds d’investissement alternatifs (GFIA) et de la directive sur les organismes de placement collectif en valeurs mobilières (OPCVM)' => 'Modification de la Directive sur les gestionnaires de fonds d’investissement alternatifs (GFIA) et de la directive sur les organismes de placement collectif en valeurs mobilières (OPCVM)',
          ),
        ),
        '5. Virements instantanés en euros ***I (vote)' =>
        array (
          'A9-0230/2023' =>
          array (
            'Virements instantanés en euros' => 'Virements instantanés en euros',
          ),
        ),
        '6. Valeurs limites pour le plomb, ses composés inorganiques et les diisocyanates ***I (vote)' =>
        array (
          'A9-0263/2023' =>
          array (
            'Valeurs limites pour le plomb, ses composés inorganiques et les diisocyanates' => 'Valeurs limites pour le plomb, ses composés inorganiques et les diisocyanates',
          ),
        ),
        '7. Végétaux obtenus au moyen de certaines nouvelles techniques génomiques et les denrées alimentaires et aliments pour animaux qui en sont dérivés ***I (vote)' =>
        array (
          'A9-0014/2024' =>
          array (
            'Végétaux obtenus au moyen de certaines nouvelles techniques génomiques et les denrées alimentaires et aliments pour animaux qui en sont dérivés' => 'Végétaux obtenus au moyen de certaines nouvelles techniques génomiques et les denrées alimentaires et aliments pour animaux qui en sont dérivés',
          ),
        ),
      );

      $map['PV-9-2024-02-08-RCV'] = array (
        '4. Échange automatisé de données dans le cadre de la coopération policière («Prüm II») ***I (vote)' =>
        array (
          'A9-0200/2023' =>
          array (
            'Échange automatisé de données aux fins de la coopération policière («Prüm II»)' => 'Échange automatisé de données aux fins de la coopération policière («Prüm II»)',
          ),
        ),
        '7. Rapport de mise en œuvre de la stratégie de l’Union en faveur de l’égalité de traitement à l’égard des personnes LGBTIQ pour la période 2020-2025 (vote)' =>
        array (
          'A9-0030/2024' =>
          array (
            'Rapport de mise en œuvre de la stratégie de l’Union en faveur de l’égalité de traitement à l’égard des personnes LGBTIQ pour la période 2020-2025' => 'Rapport de mise en œuvre de la stratégie de l’Union en faveur de l’égalité de traitement à l’égard des personnes LGBTIQ pour la période 2020-2025',
          ),
        ),
        '9. Négociations multilatérales en vue de la treizième conférence ministérielle de l’OMC à Abou Dhabi, qui aura lieu du 26 au 29 février 2024 (vote)' =>
        array (
        //       'B9-0096/2024' => '8. Accords d\'association pour la participation de pays tiers aux programmes de l\'Union (vote)',
        ),
      );

      $map['PV-9-2024-02-27-RCV'] = array (
        '1. Cadre financier pluriannuel 2021-2027 *** (vote)' =>
        array (
          'A9-0051/2024' =>
          array (
            'Cadre financier pluriannuel 2021-2027' => 'Cadre financier pluriannuel 2021-2027',
          ),
        ),
        '2. Cadre financier pluriannuel 2021-2027 (résolution) (vote)' =>
        array (
          'A9-0053/2024' =>
          array (
            'Cadre financier pluriannuel 2021-2027 (résolution)' => 'Cadre financier pluriannuel 2021-2027 (résolution)',
          ),
        ),
        '3. Création de la facilité pour l’Ukraine ***I (vote)' =>
        array (
          'A9-0286/2023' =>
          array (
            'Création de la facilité pour l’Ukraine' => 'Création de la facilité pour l’Ukraine',
          ),
        ),
        '4. Établissement de la plateforme Technologies stratégiques pour l\'Europe («STEP») ***I (vote)' =>
        array (
          'A9-0290/2023' =>
          array (
            'Établissement de la plateforme Technologies stratégiques pour l\'Europe («STEP»)' => 'Établissement de la plateforme Technologies stratégiques pour l\'Europe («STEP»)',
          ),
        ),
        '5. Protection des journalistes et des défenseurs des droits de l’homme contre les procédures judiciaires manifestement infondées ou abusives ***I (vote)' =>
        array (
          'A9-0223/2023' =>
          array (
            'Protection des journalistes et des défenseurs des droits de l’homme contre les procédures judiciaires manifestement infondées ou abusives' => 'Protection des journalistes et des défenseurs des droits de l’homme contre les procédures judiciaires manifestement infondées ou abusives',
          ),
        ),
        '6. Modifications du protocole nº 3 sur le statut de la Cour de justice de l’Union européenne ***I (vote)' =>
        array (
          'A9-0278/2023' =>
          array (
            'Modifications du protocole nº 3 sur le statut de la Cour de justice de l’Union européenne' => 'Modifications du protocole nº 3 sur le statut de la Cour de justice de l’Union européenne',
          ),
        ),
        '7. Transferts de déchets ***I (vote)' =>
        array (
          'A9-0290/2022' =>
          array (
            'Transferts de déchets' => 'Transferts de déchets',
          ),
        ),
        '8. Certains aspects de l’exigence minimale de fonds propres et d’engagements éligibles ***I (vote)' =>
        array (
          'A9-0344/2023' =>
          array (
            'Certains aspects de l’exigence minimale de fonds propres et d’engagements éligibles' => 'Certains aspects de l’exigence minimale de fonds propres et d’engagements éligibles',
          ),
        ),
        '9. Restauration de la nature ***I (vote)' =>
        array (
          'A9-0220/2023' =>
          array (
            'Restauration de la nature' => 'Restauration de la nature',
          ),
        ),
        '10. Transparence et ciblage de la publicité à caractère politique ***I (vote)' =>
        array (
          'A9-0009/2023' =>
          array (
            'Transparence et ciblage de la publicité à caractère politique' => 'Transparence et ciblage de la publicité à caractère politique',
          ),
        ),
        '11. Détergents et agents de surface ***I (vote)' =>
        array (
          'A9-0039/2024' =>
          array (
            'Détergents et agents de surface' => 'Détergents et agents de surface',
          ),
        ),
        '12. Modification de la décision (UE) 2017/1324 en ce qui concerne la poursuite de la participation de l’Union au PRIMA au titre d’Horizon Europe ***I (vote)' =>
        array (
          'A9-0378/2023' =>
          array (
            'Modification de la décision (UE) 2017/1324 en ce qui concerne la poursuite de la participation de l’Union au PRIMA au titre d’Horizon Europe' => 'Modification de la décision (UE) 2017/1324 en ce qui concerne la poursuite de la participation de l’Union au PRIMA au titre d’Horizon Europe',
          ),
        ),
        '13. Protection de l’environnement par le droit pénal et remplacement de la directive 2008/99/CE ***I (vote)' =>
        array (
          'A9-0087/2023' =>
          array (
            'Protection de l’environnement par le droit pénal et remplacement de la directive 2008/99/CE' => 'Protection de l’environnement par le droit pénal et remplacement de la directive 2008/99/CE',
          ),
        ),
        '14. Banque centrale européenne - rapport annuel 2023 (vote)' =>
        array (
          'A9-0412/2023' =>
          array (
            'Banque centrale européenne - rapport annuel 2023' => 'Banque centrale européenne - rapport annuel 2023',
          ),
        ),
      );

      return $map[$doc_id] ?? resultMapArchive($doc_id);
  }

  function resultMapArchive($doc_id) {

    $map['PV-9-2024-02-28-RCV'] = array (
      '1. Permis de conduire ***I (vote)' =>
      array (
        'A9-0445/2023' =>
        array (
          'Permis de conduire' => 'Permis de conduire',
        ),
      ),
      '2. Certificat complémentaire de protection unitaire pour les produits phytopharmaceutiques ***I (vote)' =>
      array (
        'A9-0020/2024' =>
        array (
          'Certificat complémentaire de protection unitaire pour les produits phytopharmaceutiques' => 'Certificat complémentaire de protection unitaire pour les produits phytopharmaceutiques',
        ),
      ),
      '3. Certificat complémentaire de protection unitaire pour les médicaments ***I (vote)' =>
      array (
        'A9-0019/2024' =>
        array (
          'Certificat complémentaire de protection unitaire pour les médicaments' => 'Certificat complémentaire de protection unitaire pour les médicaments',
        ),
      ),
      '4. Certificat complémentaire de protection pour les produits phytopharmaceutiques (refonte) ***I (vote)' =>
      array (
        'A9-0023/2024' =>
        array (
          'Certificat complémentaire de protection pour les produits phytopharmaceutiques (refonte)' => 'Certificat complémentaire de protection pour les produits phytopharmaceutiques (refonte)',
        ),
      ),
      '5. Certificat complémentaire de protection pour les médicaments (refonte) ***I (vote)' =>
      array (
        'A9-0022/2024' =>
        array (
          'Certificat complémentaire de protection pour les médicaments (refonte)' => 'Certificat complémentaire de protection pour les médicaments (refonte)',
        ),
      ),
      '6. Brevets essentiels liés à une norme ***I (vote)' =>
      array (
        'A9-0016/2024' =>
        array (
          'Brevets essentiels liés à une norme' => 'Brevets essentiels liés à une norme',
        ),
      ),
      '7. Indications géographiques pour les vins, les boissons spiritueuses et les produits agricoles ***I (vote)' =>
      array (
        'A9-0173/2023' =>
        array (
          'Indications géographiques pour les vins, les boissons spiritueuses et les produits agricoles' => 'Indications géographiques pour les vins, les boissons spiritueuses et les produits agricoles',
        ),
      ),
      '8. Dégrèvement plus rapide et plus sûr des excédents de retenues à la source * (vote)' =>
      array (
        'A9-0007/2024' =>
        array (
          'Dégrèvement plus rapide et plus sûr des excédents de retenues à la source' => 'Dégrèvement plus rapide et plus sûr des excédents de retenues à la source',
        ),
      ),
      '9. Exigences en matière d’obligations d’information ***I (vote)' =>
      array (
        'A9-0009/2024' =>
        array (
          'Exigences en matière d’obligations d’information' => 'Exigences en matière d’obligations d’information',
        ),
      ),
      '10. Négociations en cours en vue d’un accord sur le statut des activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes (Frontex) au Sénégal (vote)' =>
      array (
        'A9-0032/2024' =>
        array (
          'Négociations en cours en vue d’un accord sur le statut des activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes (Frontex) au Sénégal' => 'Négociations en cours en vue d’un accord sur le statut des activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes (Frontex) au Sénégal',
        ),
      ),
      '1. Mise en œuvre de la politique étrangère et de sécurité commune - rapport annuel 2023 (vote)' =>
      array (
        'A9-0389/2023' =>
        array (
          'Mise en œuvre de la politique étrangère et de sécurité commune - rapport annuel 2023' => 'Mise en œuvre de la politique étrangère et de sécurité commune - rapport annuel 2023',
        ),
      ),
      '2. Mise en œuvre de la politique de sécurité et de défense commune - rapport annuel 2023 (vote)' =>
      array (
        'A9-0403/2023' =>
        array (
          'Mise en œuvre de la politique de sécurité et de défense commune - rapport annuel 2023' => 'Mise en œuvre de la politique de sécurité et de défense commune - rapport annuel 2023',
        ),
      ),
      '3. Les droits de l’homme et la démocratie dans le monde et la politique de l’Union européenne en la matière – rapport annuel 2023 (vote)' =>
      array (
        'A9-0424/2023' =>
        array (
          'Les droits de l’homme et la démocratie dans le monde et la politique de l’Union européenne en la matière – rapport annuel 2023' => 'Les droits de l’homme et la démocratie dans le monde et la politique de l’Union européenne en la matière – rapport annuel 2023',
        ),
      ),
      '4. Activités financières de la Banque européenne d’investissement - rapport annuel 2023 (vote)' =>
      array (
        'A9-0031/2024' =>
        array (
          'Activités financières de la Banque européenne d’investissement - rapport annuel 2023' => 'Activités financières de la Banque européenne d’investissement - rapport annuel 2023',
        ),
      ),
      '5. Rapport sur le rapport 2023 de la Commission sur l’état de droit (vote)' =>
      array (
        'A9-0025/2024' =>
        array (
          'Rapport sur le rapport 2023 de la Commission sur l’état de droit' => 'Rapport sur le rapport 2023 de la Commission sur l’état de droit',
        ),
      ),
      '6. Recommandation au Conseil, à la Commission et au SEAE sur la situation en Syrie (vote)' =>
      array (
        'A9-0041/2024' =>
        array (
          'Recommandation au Conseil, à la Commission et au SEAE sur la situation en Syrie' => 'Recommandation au Conseil, à la Commission et au SEAE sur la situation en Syrie',
        ),
      ),
    );

    $map['PV-9-2024-02-29-RCV'] = array (
      '1. Accord de partenariat économique entre l’Union européenne, d’une part, et la République du Kenya, membre de la Communauté de l’Afrique de l’Est, d’autre part *** (vote)' =>
      array (
        'A9-0012/2024' =>
        array (
          'Accord de partenariat économique entre l’Union européenne, d’une part, et la République du Kenya, membre de la Communauté de l’Afrique de l’Est, d’autre part' => 'Accord de partenariat économique entre l’Union européenne, d’une part, et la République du Kenya, membre de la Communauté de l’Afrique de l’Est, d’autre part',
        ),
      ),
      '2. Conclusion d’un accord entre l’Union européenne et la République des Seychelles concernant l’accès des navires de pêche des Seychelles aux eaux de Mayotte *** (vote)' =>
      array (
        'A9-0043/2024' =>
        array (
          'Conclusion d’un accord entre l’Union européenne et la République des Seychelles concernant l’accès des navires de pêche des Seychelles aux eaux de Mayotte' => 'Conclusion d’un accord entre l’Union européenne et la République des Seychelles concernant l’accès des navires de pêche des Seychelles aux eaux de Mayotte',
        ),
      ),
      '3. Collecte et partage des données relatives aux services de location de logements à court terme ***I (vote)' =>
      array (
        'A9-0270/2023' =>
        array (
          'Collecte et partage des données relatives aux services de location de logements à court terme' => 'Collecte et partage des données relatives aux services de location de logements à court terme',
        ),
      ),
      '4. Accord intérimaire sur le commerce entre l’Union européenne et la République du Chili *** (vote)' =>
      array (
        'A9-0011/2024' =>
        array (
          'Accord intérimaire sur le commerce entre l’Union européenne et la République du Chili' => 'Accord intérimaire sur le commerce entre l’Union européenne et la République du Chili',
        ),
      ),
      '5. Accord-cadre avancé UE/Chili *** (vote)' =>
      array (
        'A9-0010/2024' =>
        array (
          'Accord-cadre avancé UE/Chili' => 'Accord-cadre avancé UE/Chili',
        ),
      ),
      '6. Accord-cadre avancé UE/Chili (résolution) (vote)' =>
      array (
        'A9-0017/2024' =>
        array (
          'Accord-cadre avancé UE/Chili (résolution)' => 'Accord-cadre avancé UE/Chili (résolution)',
        ),
      ),
      '7. Améliorer la protection de l’Union contre la manipulation du marché de gros de l’énergie ***I (vote)' =>
      array (
        'A9-0261/2023' =>
        array (
          'Améliorer la protection de l’Union contre la manipulation du marché de gros de l’énergie' => 'Améliorer la protection de l’Union contre la manipulation du marché de gros de l’énergie',
        ),
      ),
      '8. Cadre européen relatif à une identité numérique ***I (vote)' =>
      array (
        'A9-0038/2023' =>
        array (
          'Cadre européen relatif à une identité numérique' => 'Cadre européen relatif à une identité numérique',
        ),
      ),
      '11. Approfondir l’intégration européenne dans la perspective des futurs élargissements (vote)' =>
      array (
        'A9-0015/2024' => [
          'Approfondir l’intégration européenne dans la perspective des futurs élargissements' => 'Approfondir l’intégration européenne dans la perspective des futurs élargissements'
        ]
        //       'pushed' => NULL,
      ),
      '12. Mise en œuvre de l’accord de partenariat économique (APE) UE-Communauté de développement de l’Afrique australe (CDAA) (vote)' =>
      array (
        'A9-0024/2024' =>
        array (
          'Mise en œuvre de l’accord de partenariat économique (APE) UE-Communauté de développement de l’Afrique australe (CDAA)' => 'Mise en œuvre de l’accord de partenariat économique (APE) UE-Communauté de développement de l’Afrique australe (CDAA)',
        ),
      ),
    );

    $map['PV-9-2024-03-12-RCV'] = array (
      '1. Calendrier des périodes de session du Parlement - 2025 (vote)' =>
      array (
      //       'pushed' => NULL,
      ),
      '2. Souscription, par l’Union, de parts supplémentaires dans le capital de la BERD et modification de l\'accord portant création de la BERD ***I (vote)' =>
      array (
      //       'pushed' => NULL,
      ),
      '3. Objection au titre de l’article 111, paragraphe 3, du règlement: liste des projets d’intérêt commun et des projets d’intérêt mutuel de l’Union (vote)' =>
      array (
        'B9-0161/2024' =>
        array (
          'Objection au titre de l’article 111, paragraphe 3, du règlement: liste des projets d’intérêt commun et des projets d’intérêt mutuel de l’Union' => 'Objection au titre de l’article 111, paragraphe 3, du règlement: liste des projets d’intérêt commun et des projets d’intérêt mutuel de l’Union',
        ),
      ),
      '4. Directive relative aux émissions industrielles ***I (vote)' =>
      array (
        'A9-0216/2023' =>
        array (
          'Directive relative aux émissions industrielles' => 'Directive relative aux émissions industrielles',
        ),
      ),
      '5. Portail des émissions industrielles ***I (vote)' =>
      array (
        'A9-0211/2023' =>
        array (
          'Portail des émissions industrielles' => 'Portail des émissions industrielles',
        ),
      ),
      '6. Définition des infractions pénales et des sanctions applicables en cas de violation des mesures restrictives de l’Union ***I (vote)' =>
      array (
        'A9-0235/2023' =>
        array (
          'Définition des infractions pénales et des sanctions applicables en cas de violation des mesures restrictives de l’Union' => 'Définition des infractions pénales et des sanctions applicables en cas de violation des mesures restrictives de l’Union',
        ),
      ),
      '7. Poids et dimensions de certains véhicules routiers ***I (vote)' =>
      array (
        'A9-0047/2024' =>
        array (
          'Poids et dimensions de certains véhicules routiers' => 'Poids et dimensions de certains véhicules routiers',
        ),
      ),
      '8. Utilisation des capacités de l\'infrastructure ferroviaire dans l\'espace ferroviaire unique européen, modification de la directive 2012/34/UE et abrogation du règlement (UE) n° 913/2010 ***I (vote)' =>
      array (
        'A9-0069/2024' =>
        array (
          'Utilisation des capacités de l\'infrastructure ferroviaire dans l\'espace ferroviaire unique européen, modification de la directive 2012/34/UE et abrogation du règlement (UE) n° 913/2010' => 'Utilisation des capacités de l\'infrastructure ferroviaire dans l\'espace ferroviaire unique européen, modification de la directive 2012/34/UE et abrogation du règlement (UE) n° 913/2010',
        ),
      ),
      '9. Modification de certains règlements dans les domaines des services financiers et du soutien à l\'investissement en ce qui concerne certaines obligations d’information ***I (vote)' =>
      array (
        'A9-0026/2024' =>
        array (
          'Modification de certains règlements dans les domaines des services financiers et du soutien à l\'investissement en ce qui concerne certaines obligations d’information' => 'Modification de certains règlements dans les domaines des services financiers et du soutien à l\'investissement en ce qui concerne certaines obligations d’information',
        ),
      ),
      '10. Performance énergétique des bâtiments (refonte) ***I (vote)' =>
      array (
        'A9-0033/2023' =>
        array (
          'Performance énergétique des bâtiments (refonte)' => 'Performance énergétique des bâtiments (refonte)',
        ),
      ),
      '11. Exigences horizontales de cybersécurité pour les produits comportant des éléments numériques et modification du règlement (UE) 2019/1020 ***I (vote)' =>
      array (
        'A9-0253/2023' =>
        array (
          'Exigences horizontales de cybersécurité pour les produits comportant des éléments numériques et modification du règlement (UE) 2019/1020' => 'Exigences horizontales de cybersécurité pour les produits comportant des éléments numériques et modification du règlement (UE) 2019/1020',
        ),
      ),
      '12. Justification et communication des allégations environnementales explicites (directive sur les allégations écologiques) ***I (vote)' =>
      array (
        'A9-0056/2024' =>
        array (
          'Justification et communication des allégations environnementales explicites (directive sur les allégations écologiques)' => 'Justification et communication des allégations environnementales explicites (directive sur les allégations écologiques)',
        ),
      ),
      '13. Responsabilité du fait des produits défectueux ***I (vote)' =>
      array (
        'A9-0291/2023' =>
        array (
          'Responsabilité du fait des produits défectueux' => 'Responsabilité du fait des produits défectueux',
        ),
      ),
      '14. Modification du règlement (UE) 2019/1009 en ce qui concerne l’étiquetage numérique des fertilisants UE ***I (vote)' =>
      array (
        'A9-0330/2023' =>
        array (
          'Modification du règlement (UE) 2019/1009 en ce qui concerne l’étiquetage numérique des fertilisants UE' => 'Modification du règlement (UE) 2019/1009 en ce qui concerne l’étiquetage numérique des fertilisants UE',
        ),
      ),
      '15. Agence européenne pour la sécurité maritime et abrogation du règlement (CE) nº 1406/2002 ***I (vote)' =>
      array (
        'A9-0423/2023' =>
        array (
          'Agence européenne pour la sécurité maritime et abrogation du règlement (CE) nº 1406/2002' => 'Agence européenne pour la sécurité maritime et abrogation du règlement (CE) nº 1406/2002',
        ),
      ),
      '16. Décision du Conseil invitant les États membres à ratifier la convention (n° 190) sur la violence et le harcèlement, 2019, de l\'Organisation internationale du travail *** (vote)' =>
      array (
        'A9-0040/2024' =>
        array (
          'Décision du Conseil invitant les États membres à ratifier la convention (n° 190) sur la violence et le harcèlement, 2019, de l\'Organisation internationale du travail' => 'Décision du Conseil invitant les États membres à ratifier la convention (n° 190) sur la violence et le harcèlement, 2019, de l\'Organisation internationale du travail',
        ),
      ),
      '17. Prorogation de l’article 168 du règlement intérieur du Parlement jusqu’à la fin de la dixième législature (vote)' =>
      array (
      //       '' => NULL,
      ),
    );

    $map['PV-9-2024-03-13-RCV'] = array (
      '1. Législation européenne sur la liberté des médias ***I (vote)' =>
      array (
        'A9-0264/2023' =>
        array (
          'Législation européenne sur la liberté des médias' => 'Législation européenne sur la liberté des médias',
        ),
      ),
      '2. Législation sur l’intelligence artificielle ***I' =>
      array (
        'A9-0188/2023' =>
        array (
          'Législation sur l’intelligence artificielle' => 'Législation sur l’intelligence artificielle',
        ),
      ),
      '3. Modification de la directive relative au règlement extrajudiciaire des litiges de consommation ***I (vote)' =>
      array (
        'A9-0060/2024' =>
        array (
          'Modification de la directive relative au règlement extrajudiciaire des litiges de consommation' => 'Modification de la directive relative au règlement extrajudiciaire des litiges de consommation',
        ),
      ),
      '4. Arrêt de la plateforme européenne de RLL ***I (vote)' =>
      array (
        'A9-0058/2024' =>
        array (
          'Arrêt de la plateforme européenne de RLL' => 'Arrêt de la plateforme européenne de RLL',
        ),
      ),
      '5. Recouvrement et confiscation d’avoirs ***I' =>
      array (
        'A9-0199/2023' =>
        array (
          'Recouvrement et confiscation d’avoirs' => 'Recouvrement et confiscation d’avoirs',
        ),
      ),
      '6. Utilisation du système d’information du marché intérieur et du portail numérique unique aux fins de certaines exigences prévues par la directive concernant les associations transfrontalières européennes ***I (vote)' =>
      array (
        'A9-0006/2024' =>
        array (
          'Utilisation du système d’information du marché intérieur et du portail numérique unique aux fins de certaines exigences prévues par la directive concernant les associations transfrontalières européennes' => 'Utilisation du système d’information du marché intérieur et du portail numérique unique aux fins de certaines exigences prévues par la directive concernant les associations transfrontalières européennes',
        ),
      ),
      '7. Octroi de licences obligatoires pour la gestion de crise et modification du règlement (CE) nº 816/2016 ***I (vote)' =>
      array (
        'A9-0042/2024' =>
        array (
          'Octroi de licences obligatoires pour la gestion de crise et modification du règlement (CE) nº 816/2016' => 'Octroi de licences obligatoires pour la gestion de crise et modification du règlement (CE) nº 816/2016',
        ),
      ),
      '8. Sécurité des jouets et abrogation de la directive 2009/48/CE ***I (vote)' =>
      array (
        'A9-0044/2024' =>
        array (
          'Sécurité des jouets et abrogation de la directive 2009/48/CE' => 'Sécurité des jouets et abrogation de la directive 2009/48/CE',
        ),
      ),
      '9. Modification de la directive 2008/98/CE relative aux déchets ***I (vote)' =>
      array (
        'A9-0055/2024' =>
        array (
          'Modification de la directive 2008/98/CE relative aux déchets' => 'Modification de la directive 2008/98/CE relative aux déchets',
        ),
      ),
      '10. Procédure de demande unique en vue de la délivrance d\'un permis unique autorisant les ressortissants de pays tiers à résider et à travailler sur le territoire d\'un État membre et établissant un socle commun de droits pour les travailleurs issus de pays tiers qui résident légalement dans un État membre (refonte) ***I' =>
      array (
        'A9-0140/2023' =>
        array (
          'Procédure de demande unique en vue de la délivrance d\'un permis unique autorisant les ressortissants de pays tiers à résider et à travailler sur le territoire d\'un État membre et socle commun de droits pour les travailleurs issus de pays tiers qui résident légalement dans un État membre (refonte)' => 'Procédure de demande unique en vue de la délivrance d\'un permis unique autorisant les ressortissants de pays tiers à résider et à travailler sur le territoire d\'un État membre et socle commun de droits pour les travailleurs issus de pays tiers qui résident légalement dans un État membre (refonte)',
        ),
      ),
      '11. Associations transfrontalières européennes ***I (vote)' =>
      array (
        'A9-0062/2024' =>
        array (
          'Associations transfrontalières européennes' => 'Associations transfrontalières européennes',
        ),
      ),
      '12. Exigences en matière de déclaration dans les domaines du transport routier et de l’aviation: règlement ***I (vote)' =>
      array (
        'A9-0033/2024' =>
        array (
          'Exigences en matière de déclaration dans les domaines du transport routier et de l’aviation: règlement' => 'Exigences en matière de déclaration dans les domaines du transport routier et de l’aviation: règlement',
        ),
      ),
      '13. Obligations de déclaration dans les domaines du transport routier et de l’aviation: décision ***I (vote)' =>
      array (
        'A9-0034/2024' =>
        array (
          'Obligations de déclaration dans les domaines du transport routier et de l’aviation: décision' => 'Obligations de déclaration dans les domaines du transport routier et de l’aviation: décision',
        ),
      ),
      '14. Exigences minimales relatives aux durées minimales des pauses et des temps de repos journaliers et hebdomadaires dans le secteur du transport occasionnel de voyageurs ***I' =>
      array (
        'A9-0370/2023' =>
        array (
          'Exigences minimales relatives aux durées minimales des pauses et des temps de repos journaliers et hebdomadaires dans le secteur du transport occasionnel de voyageurs' => 'Exigences minimales relatives aux durées minimales des pauses et des temps de repos journaliers et hebdomadaires dans le secteur du transport occasionnel de voyageurs',
        ),
      ),
      '15. Établissement du code des douanes de l\'Union et de l\'Autorité douanière de l\'Union européenne, et abrogation du règlement (UE) n° 952/2013 ***I (vote)' =>
      array (
        'A9-0065/2024' =>
        array (
          'Établissement du code des douanes de l\'Union et de l\'Autorité douanière de l\'Union européenne, et abrogation du règlement (UE) n° 952/2013' => 'Établissement du code des douanes de l\'Union et de l\'Autorité douanière de l\'Union européenne, et abrogation du règlement (UE) n° 952/2013',
        ),
      ),
      '16. Modification du règlement (CE) nº 223/2009 relatif aux statistiques européennes ***I' =>
      array (
        'A9-0386/2023' =>
        array (
          'Modification du  règlement (CE) nº 223/2009 relatif aux statistiques européennes' => 'Modification du  règlement (CE) nº 223/2009 relatif aux statistiques européennes',
        ),
      ),
      '1. Réception par type des véhicules à moteur et des moteurs en ce qui concerne leurs émissions et leur durabilité (Euro 7) ***I (vote)' =>
      array (
        'A9-0298/2023' =>
        array (
          'Réception par type des véhicules à moteur et des moteurs en ce qui concerne leurs émissions et leur durabilité (Euro 7)' => 'Réception par type des véhicules à moteur et des moteurs en ce qui concerne leurs émissions et leur durabilité (Euro 7)',
        ),
      ),
      '2. Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits ukrainiens au titre de l\'accord d’association UE/Euratom/Ukraine ***I (vote)' =>
      array (
        'A9-0077/2024' =>
        array (
          'Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits ukrainiens au titre de l\'accord d’association UE/Euratom/Ukraine' => 'Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits ukrainiens au titre de l\'accord d’association UE/Euratom/Ukraine',
        ),
      ),
      '3. Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits moldaves au titre de l\'accord d\'association UE/Euratom/Moldavie ***I (vote)' =>
      array (
        'A9-0079/2024' =>
        array (
          'Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits moldaves au titre de l\'accord d\'association UE/Euratom/Moldavie' => 'Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits moldaves au titre de l\'accord d\'association UE/Euratom/Moldavie',
        ),
      ),
      '4. Semestre européen pour la coordination des politiques économiques 2024 (vote)' =>
      array (
        'A9-0063/2024' =>
        array (
          'Semestre européen pour la coordination des politiques économiques 2024' => 'Semestre européen pour la coordination des politiques économiques 2024',
        ),
      ),
      '5. Semestre européen pour la coordination des politiques économiques: priorités sociales et en matière d’emploi pour 2024 (vote)' =>
      array (
        'A9-0050/2024' =>
        array (
          'Semestre européen pour la coordination des politiques économiques: priorités sociales et en matière d’emploi pour 2024' => 'Semestre européen pour la coordination des politiques économiques: priorités sociales et en matière d’emploi pour 2024',
        ),
      ),
      '6. Orientations pour le budget 2025 – Section III (vote)' =>
      array (
        'A9-0068/2024' =>
        array (
          'Orientations pour le budget 2025 – Section III' => 'Orientations pour le budget 2025 – Section III',
        ),
      ),
    );

    $map['PV-9-2024-03-14-RCV'] = array (
    //     '2. L\'environnement répressif en Afghanistan, notamment les exécutions publiques et les violences à l\'égard des femmes (vote)' =>
    //     array (
      //       'RC-B9-0175/2024' =>
    //       array (
      //         'L\'environnement répressif en Afghanistan, notamment les exécutions publiques et les violences à l\'égard des femmes' => 'L\'environnement répressif en Afghanistan, notamment les exécutions publiques et les violences à l\'égard des femmes',
    //       ),
    //     ),
      '5. Règles financières applicables au budget général de l\'Union (refonte) ***I (vote)' =>
      array (
        'A9-0180/2023' =>
        array (
          'Règles financières applicables au budget général de l\'Union (refonte)' => 'Règles financières applicables au budget général de l\'Union (refonte)',
        ),
      ),
      '6. Propriété industrielle: protection des dessins ou modèles communautaires ***I (vote)' =>
      array (
        'A9-0315/2023' =>
        array (
          'Propriété industrielle: protection des dessins ou modèles communautaires' => 'Propriété industrielle: protection des dessins ou modèles communautaires',
        ),
      ),
      '7. Propriété industrielle: protection juridique des dessins ou modèles (refonte) ***I (vote)' =>
      array (
        'A9-0317/2023' =>
        array (
          'Propriété industrielle: protection juridique des dessins ou modèles (refonte)' => 'Propriété industrielle: protection juridique des dessins ou modèles (refonte)',
        ),
      ),
      '8. Exigences en matière de communication d’informations relatives aux infrastructures d’information géographique ***I (vote)' =>
      array (
        'A9-0037/2024' =>
        array (
          'Exigences en matière de communication d’informations relatives aux infrastructures d’information géographique' => 'Exigences en matière de communication d’informations relatives aux infrastructures d’information géographique',
        ),
      ),
      '9. Obligations d\'information dans les domaines des denrées et ingrédients alimentaires, des émissions sonores à l’extérieur, des droits des patients et des équipements radioélectriques ***I (vote)' =>
      array (
        'A9-0038/2024' =>
        array (
          'Obligations d\'information dans les domaines des denrées et ingrédients alimentaires, des émissions sonores à l’extérieur, des droits des patients et des équipements radioélectriques' => 'Obligations d\'information dans les domaines des denrées et ingrédients alimentaires, des émissions sonores à l’extérieur, des droits des patients et des équipements radioélectriques',
        ),
      ),
      '10. Protocole modifiant l\'accord entre l\'Union européenne et le Japon pour un partenariat économique *** (vote)' =>
      array (
        'A9-0081/2024' =>
        array (
          'Protocole modifiant l\'accord entre l\'Union européenne et le Japon pour un partenariat économique' => 'Protocole modifiant l\'accord entre l\'Union européenne et le Japon pour un partenariat économique',
        ),
      ),
      '11. Accord entre l’Union européenne et la République d’Albanie concernant les activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes en République d’Albanie *** (vote)' =>
      array (
        'A9-0036/2024' =>
        array (
          'Accord entre l’Union européenne et la République d’Albanie concernant les activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes en République d’Albanie' => 'Accord entre l’Union européenne et la République d’Albanie concernant les activités opérationnelles menées par l’Agence européenne de garde-frontières et de garde-côtes en République d’Albanie',
        ),
      ),
      '12. Nomination proposée d\'un membre de la Cour des comptes européenne - Carlo Alberto Manfredi Selvaggi (vote)'
      => [],
      //     '12. Nomination proposée d\'un membre de la Cour des comptes européenne - Carlo Alberto Manfredi Selvaggi (vote)' =>
    //     array (
      //       'pushed' => NULL,
    //     ),
      '16. Politique de cohésion 2014-2020 – mise en œuvre et résultats dans les États membres (vote)' =>
      array (
        'A9-0049/2024' =>
        array (
          'Politique de cohésion 2014-2020 – mise en œuvre et résultats dans les États membres' => 'Politique de cohésion 2014-2020 – mise en œuvre et résultats dans les États membres',
        ),
      ),
      //     '13. Restitution du trésor de la Roumanie ayant fait l\'objet d\'une appropriation illégale par la Russie (vote)' =>
        //     array (
          //       'RC-B9-0169/2024' =>
        //       array (
          //         'Restitution du trésor de la Roumanie ayant fait l\'objet d\'une appropriation illégale par la Russie' => 'Restitution du trésor de la Roumanie ayant fait l\'objet d\'une appropriation illégale par la Russie',
        //       ),
        //     ),
        //     '14. Le délai nécessaire à la Commission pour traiter les demandes d\'accès du public aux documents (vote)' =>
        //     array (
          //       'B9-0162/2024' => '14. Le délai nécessaire à la Commission pour traiter les demandes d\'accès du public aux documents (vote)',
          //     ),
        //     '16. Politique de cohésion 2014-2020 – mise en œuvre et résultats dans les États membres (vote)'
        //     => [],
        );

        $map['PV-9-2024-04-10-RCV'] = array (
          '1. Modifications au règlement intérieur du Parlement mettant en œuvre la réforme parlementaire «Parlement 2024» (vote)' =>
          array (
            'A9-0158/2024' =>
            array (
              'Amendments to Parliament’s Rules of Procedure implementing the parliamentary reform “Parliament 2024”' => 'Amendments to Parliament’s Rules of Procedure implementing the parliamentary reform “Parliament 2024”',
            ),
          ),
          '2. Procédure commune de protection internationale dans l’Union ***I (vote)' =>
          array (
            'A8-0171/2018' =>
            array (
              'Common procedure for international protection in the Union' => 'Common procedure for international protection in the Union',
            ),
          ),
          '3. Faire face aux situations de crise et aux cas de force majeure ***I (vote)' =>
          array (
            'A9-0127/2023' =>
            array (
              'Addressing situations of crisis and force majeure' => 'Addressing situations of crisis and force majeure',
            ),
          ),
          '4. Gestion de l’asile et de la migration ***I (vote)' =>
          array (
            'A9-0152/2023' =>
            array (
              'Asylum and migration management' => 'Asylum and migration management',
            ),
          ),
          '5. Mise en place d’une procédure de retour à la frontière et modification du règlement (UE) 2021/1148 ***I (vote)' =>
          array (
            'A9-0164/2024' =>
            array (
              'Establishing a return border procedure, and amending Regulation (EU) 2021/1148' => 'Establishing a return border procedure, and amending Regulation (EU) 2021/1148',
            ),
          ),
          '6. Filtrage des ressortissants de pays tiers aux frontières extérieures ***I (vote)' =>
          array (
            'A9-0149/2023' =>
            array (
              'Screening of third country nationals at the external borders' => 'Screening of third country nationals at the external borders',
            ),
          ),
          '7. Système européen d’information sur les casiers judiciaires - ressortissants de pays tiers ***I (vote)' =>
          array (
            'A9-0148/2023' =>
            array (
              'European Criminal Records Information System - Third Country Nationals' => 'European Criminal Records Information System - Third Country Nationals',
            ),
          ),
          '8. Création d’Eurodac pour la comparaison des empreintes digitales aux fins de l’application efficace du règlement (UE) n° 604/2013, de l’identification des ressortissants de pays tiers ou apatrides en séjour irrégulier et relatif aux demandes de comparaison avec les données d’Eurodac présentées par les autorités répressives des États membres et Europol à des fins répressives (refonte) ***I (vote)' =>
          array (
            'A8-0212/2017' =>
            array (
              'Establishment of \'Eurodac\'' => 'Establishment of \'Eurodac\'',
            ),
          ),
          '9. Cadre de l’Union pour la réinstallation ***I (vote)' =>
          array (
            'A8-0316/2017' =>
            array (
              'Union Resettlement Framework' => 'Union Resettlement Framework',
            ),
          ),
          '10. Normes relatives aux conditions que doivent remplir les ressortissants des pays tiers ou les apatrides pour pouvoir bénéficier d’une protection internationale ***I (vote)' =>
          array (
            'A8-0245/2017' =>
            array (
              'Standards for the qualification of third-country nationals or stateless persons as beneficiaries of international protection' => 'Standards for the qualification of third-country nationals or stateless persons as beneficiaries of international protection',
            ),
          ),
          '11. Normes pour l’accueil des personnes demandant la protection internationale (refonte) ***I (vote)' =>
          array (
            'A8-0186/2017' =>
            array (
              'Standards for the reception of applicants for international protection (recast' => 'Standards for the reception of applicants for international protection (recast',
            ),
          ),
          '12. Fixation des règles de procédure supplémentaires relatives à l’application du règlement (UE) 2016/679 ***I (vote)' =>
          array (
            'A9-0045/2024' =>
            array (
              'Laying down additional procedural rules relating to the enforcement of Regulation (EU) 2016/679' => 'Laying down additional procedural rules relating to the enforcement of Regulation (EU) 2016/679',
            ),
          ),
          '13. Nouveau règlement sur les produits de construction ***I (vote)' =>
          array (
            'A9-0207/2023' =>
            array (
              'New Regulation on Construction Products' => 'New Regulation on Construction Products',
            ),
          ),
          '14. Modification de la directive 2013/34/UE en ce qui concerne les délais d’adoption des normes d’information en matière de durabilité pour certains secteurs et pour certaines entreprises de pays tiers ***I (vote)' =>
          array (
            'A9-0013/2024' =>
            array (
              'Amending Directive 2013/34/EU as regards the time limits for the adoption of sustainability reporting standards for certain sectors and for certain third-country undertakings' => 'Amending Directive 2013/34/EU as regards the time limits for the adoption of sustainability reporting standards for certain sectors and for certain third-country undertakings',
            ),
          ),
          '15. Réduction des émissions de méthane dans le secteur de l’énergie ***I (vote)' =>
          array (
            'A9-0162/2023' =>
            array (
              'Methane emissions reduction in the energy sector' => 'Methane emissions reduction in the energy sector',
            ),
          ),
          '16. Mercure: amalgames dentaires et autres produits contenant du mercure ajouté faisant l’objet de restrictions de fabrication, d’importation et d’exportation ***I (vote)' =>
          array (
            'A9-0002/2024' =>
            array (
              'Mercury: dental amalgam and other mercury-added products subject to  manufacturing, import and export restrictions' => 'Mercury: dental amalgam and other mercury-added products subject to  manufacturing, import and export restrictions',
            ),
          ),
          '17. Renforcement des normes de performance en matière d’émissions de CO2 pour les véhicules utilitaires lourds neufs ***I (vote)' =>
          array (
            'A9-0313/2023' =>
            array (
              'Strengthening the CO2 emission performance targets for new heavy-duty vehicles' => 'Strengthening the CO2 emission performance targets for new heavy-duty vehicles',
            ),
          ),
          '18. Aliments destinés à la consommation humaine: modification de certaines des directives dites "petit-déjeuner" ***I (vote)' =>
          array (
            'A9-0385/2023' =>
            array (
              'Foodstuffs for human consumption: amending certain \'Breakfast\' Directives' => 'Foodstuffs for human consumption: amending certain \'Breakfast\' Directives',
            ),
          ),
          '19. Comptes économiques européens de l\'environnement: nouveaux modules ***I (vote)' =>
          array (
            'A9-0296/2023' =>
            array (
              'European environmental economic accounts: new modules' => 'European environmental economic accounts: new modules',
            ),
          ),
          '20. Cadre de certification de l’Union relatif aux absorptions de carbone ***I (vote)' =>
          array (
            'A9-0329/2023' =>
            array (
              'Union certification framework for carbon removals' => 'Union certification framework for carbon removals',
            ),
          ),
          '21. Normes applicables aux organismes chargés de l’égalité dans le domaine de l’égalité de traitement et de l’égalité des chances entre les femmes et les hommes en matière d’emploi et de travail ***I (vote)' =>
          array (
            'A9-0354/2023' =>
            array (
              'Standards for equality bodies in the field of equal treatment and equal opportunities between women and men in matters of employment and occupation' => 'Standards for equality bodies in the field of equal treatment and equal opportunities between women and men in matters of employment and occupation',
            ),
          ),
          '22. Directive du Conseil relative aux normes applicables aux organismes pour l’égalité de traitement dans les domaines de l’égalité de traitement entre les personnes sans distinction de race ou d’origine ethnique, de l’égalité de traitement entre les personnes en matière d’emploi et de travail sans distinction de religion ou de convictions, de handicap, d’âge ou d’orientation sexuelle et de l’égalité de traitement entre les femmes et les hommes en matière de sécurité sociale ainsi que dans l’accès à des biens et services et la fourniture de biens et services, et modifiant les directives 2000/43/CE et 2004/113/CE *** (vote)' =>
          array (
            'A9-0128/2024' =>
            array (
              'Council Directive on standards for equality bodies in the field of equal treatment between persons irrespective of their racial or ethnic origin' => 'Council Directive on standards for equality bodies in the field of equal treatment between persons irrespective of their racial or ethnic origin',
            ),
          ),
          '23. Dérogation temporaire: lutte contre les abus sexuels commis contre des enfants en ligne ***I (vote)' =>
          array (
            'A9-0021/2024' =>
            array (
              'Temporary derogation: combating online child sexual abuse' => 'Temporary derogation: combating online child sexual abuse',
            ),
          ),
          '24. Mesures de conservation, de gestion et de contrôle applicables dans la zone de la convention CPANE ***I (vote)' =>
          array (
            'A9-0004/2024' =>
            array (
              'Conservation, management and control measures applicable in the area covered by the NEAFC Convention' => 'Conservation, management and control measures applicable in the area covered by the NEAFC Convention',
            ),
          ),
          '25. Modification de la directive 2009/18/CE établissant les principes fondamentaux régissant les enquêtes sur les accidents dans le secteur des transports maritimes ***I (vote)' =>
          array (
            'A9-0422/2023' =>
            array (
              'Amending Directive 2009/18/EC establishing the fundamental principles governing the investigation of accidents in the maritime transport sector' => 'Amending Directive 2009/18/EC establishing the fundamental principles governing the investigation of accidents in the maritime transport sector',
            ),
          ),
          '26. Modification de la directive 2009/16/CE relative au contrôle par l\'État du port ***I (vote)' =>
          array (
            'A9-0419/2023' =>
            array (
              'Amending Directive 2009/16/EC on port State control' => 'Amending Directive 2009/16/EC on port State control',
            ),
          ),
          '27. Modification de la directive 2005/35/CE relative à la pollution causée par les navires et à l’introduction de sanctions en cas d’infractions ***I (vote)' =>
          array (
            'A9-0365/2023' =>
            array (
              'Amending Directive 2005/35/EC on ship-source pollution and on the introduction of penalties for infringements' => 'Amending Directive 2005/35/EC on ship-source pollution and on the introduction of penalties for infringements',
            ),
          ),
          '28. Modification de la directive 2009/21/CE concernant le respect des obligations des États du pavillon ***I (vote)' =>
          array (
            'A9-0418/2023' =>
            array (
              'Amending Directive 2009/21/EC on compliance with flag State requirements' => 'Amending Directive 2009/21/EC on compliance with flag State requirements',
            ),
          ),
          '29. Surveillance et résilience des sols (directive sur la surveillance des sols) ***I (vote)' =>
          array (
            'A9-0138/2024' =>
            array (
              'Soil Monitoring and Resilience (Soil Monitoring Directive)' => 'Soil Monitoring and Resilience (Soil Monitoring Directive)',
            ),
          ),
          '30. Comptabilisation des émissions de gaz à effet de serre des services de transport ***I (vote)' =>
          array (
            'A9-0070/2024' =>
            array (
              'Accounting of greenhouse gas emissions of transport services' => 'Accounting of greenhouse gas emissions of transport services',
            ),
          ),
          '31. Accord de partenariat entre l’Union européenne et ses États membres, d’une part, et les membres de l’Organisation des États d’Afrique, des Caraïbes et du Pacifique, d’autre part *** (vote)' =>
          array (
            'A9-0147/2024' =>
            array (
              'Partnership Agreement between the European Union and the Members of the Organisation of African, Caribbean and Pacific States' => 'Partnership Agreement between the European Union and the Members of the Organisation of African, Caribbean and Pacific States',
            ),
          ),
          '32. Accord de partenariat entre l’Union européenne et ses États membres, d’une part, et les membres de l’Organisation des États d’Afrique, des Caraïbes et du Pacifique, d’autre part (résolution) (vote)' =>
          array (
            'A9-0159/2024' =>
            array (
              'Partnership Agreement between the European Union and the Members of the Organisation of African, Caribbean and Pacific States' => 'Partnership Agreement between the European Union and the Members of the Organisation of African, Caribbean and Pacific States',
            ),
          ),
          '33. Accord de partenariat volontaire entre l’Union européenne et la République de Côte d’Ivoire sur l’application des réglementations forestières, la gouvernance et les échanges commerciaux de bois et de produits dérivés du bois vers l’Union européenne *** (vote)' =>
          array (
            'A9-0136/2024' =>
            array (
              'EU/Côte d’Ivoire Voluntary Partnership Agreement: forest law enforcement, governance and trade in timber and timber products to the EU' => 'EU/Côte d’Ivoire Voluntary Partnership Agreement: forest law enforcement, governance and trade in timber and timber products to the EU',
            ),
          ),
          '34. Accord de partenariat volontaire entre l’Union européenne et la République de Côte d’Ivoire sur l’application des réglementations forestières, la gouvernance et les échanges commerciaux de bois et de produits dérivés du bois vers l’Union européenne (résolution) (vote)' =>
          array (
            'A9-0137/2024' =>
            array (
              'EU/Côte d’Ivoire Voluntary Partnership Agreement: forest law enforcement, governance and trade in timber and timber products to the EU' => 'EU/Côte d’Ivoire Voluntary Partnership Agreement: forest law enforcement, governance and trade in timber and timber products to the EU',
            ),
          ),
          '35. Accord sous forme d’échange de lettres entre l’Union européenne et la République arabe d’Égypte au titre de l’article XXVIII de l’accord général sur les tarifs douaniers et le commerce (GATT) de 1994 relatif à la modification des concessions pour l’ensemble des contingents tarifaires de la liste CLXXV de l’Union européenne à la suite du retrait du Royaume-Uni de l’Union européenne *** (vote)' =>
          array (
            'A9-0078/2024' =>
            array (
              'Agreement in the form of an Exchange of Letters between the European Union and the Arab Republic of Egypt pursuant to Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994' => 'Agreement in the form of an Exchange of Letters between the European Union and the Arab Republic of Egypt pursuant to Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994',
            ),
          ),
          '36. Accord sous forme d’échange de lettres entre l’Union européenne et la République argentine modifiant l\'accord entre l\'Union européenne et la République argentine au titre de l’article XXVIII de l’accord général sur les tarifs douaniers et le commerce (GATT) de 1994 relatif à la modification des concessions pour l’ensemble des contingents tarifaires de la liste CLXXV de l’Union européenne à la suite du retrait du Royaume-Uni de l’Union européenne *** (vote)' =>
          array (
            'A9-0083/2024' =>
            array (
              'Agreement in the form of an Exchange of Letters between the European Union and the Argentine Republic amending the Agreement of the European Union and the Argentine Republic pursuant to Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994' => 'Agreement in the form of an Exchange of Letters between the European Union and the Argentine Republic amending the Agreement of the European Union and the Argentine Republic pursuant to Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994',
            ),
          ),
          '37. TVA: modification de l\'accord entre l\'Union européenne et le Royaume de Norvège en ce qui concerne la coopération administrative, la lutte contre la fraude et le recouvrement de créances * (vote)' =>
          array (
            'A9-0057/2024' =>
            array (
              'VAT: amendment of the EU–Norway Agreement on administrative cooperation,  combating fraud and recovery of claims' => 'VAT: amendment of the EU–Norway Agreement on administrative cooperation,  combating fraud and recovery of claims',
            ),
          ),
          '38. Accord entre l’Union européenne et l’Islande définissant des règles complémentaires relatives à l’instrument de soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du Fonds pour la gestion intégrée des frontières *** (vote)' =>
          array (
            'A9-0146/2024' =>
            array (
              'Agreement between the European Union and Iceland on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund' => 'Agreement between the European Union and Iceland on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund',
            ),
          ),
          '39. Accord entre l’Union européenne et le Royaume de Norvège définissant des règles complémentaires relatives à l’instrument de soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du Fonds pour la gestion intégrée des frontières *** (vote)' =>
          array (
            'A9-0143/2024' =>
            array (
              'Agreement between the European Union and the Kingdom of Norway on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund' => 'Agreement between the European Union and the Kingdom of Norway on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund',
            ),
          ),
          '40. Accord entre l’Union européenne et la Confédération suisse définissant des règles complémentaires relatives à l’instrument de soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du Fonds pour la gestion intégrée des frontières *** (vote)' =>
          array (
            'A9-0145/2024' =>
            array (
              'Agreement between the European Union and the Swiss Confederation on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund' => 'Agreement between the European Union and the Swiss Confederation on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund',
            ),
          ),
          '41. Conclusion d’un accord entre l’Union européenne, d’une part, et la République d’Arménie, d’autre part, sur la coopération entre l’Agence de l’Union européenne pour la coopération judiciaire en matière pénale (Eurojust) et les autorités de la République d’Arménie compétentes dans le domaine de la coopération judiciaire en matière pénale *** (vote)' =>
          array (
            'A9-0165/2024' =>
            array (
              'Conclusion of an Agreement between the European Union and the Republic of Armenia' => 'Conclusion of an Agreement between the European Union and the Republic of Armenia',
            ),
          ),
          '42. Accord entre l’Union européenne et la Principauté de Liechtenstein définissant des règles complémentaires relatives à l’instrument de soutien financier à la gestion des frontières et à la politique des visas, dans le cadre du Fonds pour la gestion intégrée des frontières *** (vote)' =>
          array (
            'A9-0144/2024' =>
            array (
              'Agreement between the European Union and the Principality of Liechtenstein on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund' => 'Agreement between the European Union and the Principality of Liechtenstein on supplementary rules in relation to the instrument for financial support for border management and visa policy, as part of the Integrated Border Management Fund',
            ),
          ),
          '43. Mise en place d\'un système d’imposition en fonction du siège central pour les micro, petites et moyennes entreprises, et modification de la directive 2011/16/UE * (vote)' =>
          array (
            'A9-0064/2024' =>
            array (
              'Establishing a Head Office Tax system for micro, small and medium sized enterprises, and amending Directive 2011/16/EU' => 'Establishing a Head Office Tax system for micro, small and medium sized enterprises, and amending Directive 2011/16/EU',
            ),
          ),
          '44. Prix de transfert * (vote)' =>
          array (
            'A9-0066/2024' =>
            array (
              'Transfer pricing' => 'Transfer pricing',
            ),
          ),
          '45. Code de l’Union relatif aux médicaments à usage humain ***I (vote)' =>
          array (
            'A9-0140/2024' =>
            array (
              'Union code relating to medicinal products for human use' => 'Union code relating to medicinal products for human use',
            ),
          ),
          '46. Procédures de l’Union pour l’autorisation et la surveillance des médicaments à usage humain et règles régissant l’Agence européenne des médicaments ***I (vote)' =>
          array (
            'A9-0141/2024' =>
            array (
              'Union procedures for the authorisation and supervision of medicinal products for human use and rules governing the European Medicines Agency' => 'Union procedures for the authorisation and supervision of medicinal products for human use and rules governing the European Medicines Agency',
            ),
          ),
          '47. Traitement des eaux urbaines résiduaires ***I (vote)' =>
          array (
            'A9-0276/2023' =>
            array (
              'Urban wastewater treatment' => 'Urban wastewater treatment',
            ),
          ),
          '48. Traitement des poids lourds aux points de passage des frontières (vote)' =>
          array (
            '' => NULL,
          ),
        );

        $map['PV-9-2024-04-11-RCV'] = array (
          '2. Demande de levée de l’immunité d’Andris Ameriks (vote)' =>
          array (
          //       'Skiped 2' => NULL,
          ),
          '3. Demande de levée de l’immunité d’Anna Júlia Donáth (vote)' =>
          array (
          //       'Skiped 1' => NULL,
          ),
          '4. Demande de levée de l’immunité de Nils Ušakovs (vote)' =>
          [],
          '5. Objection conformément à l’article 111, paragraphe 3, du règlement: exigences spécifiques en matière d’hygiène applicables à certaines viandes, aux produits de la pêche, aux produits laitiers et aux œufs (vote)' =>
          array (
            'B9-0211/2024' =>
            array (
            //         'Proposition de résolution' => 'Proposition de résolution',
            ),
          ),

          //     array (
            //       'C9-0120/2024' =>
            //       array (
              //         'Good agricultural and environmental condition standards, schemes for climate, environment and animal welfare' => 'Good agricultural and environmental condition standards, schemes for climate, environment and animal welfare',
              //       ),
            //     ),
          '7. Décharge 2022: Budget général de l\'UE - Commission (vote)' =>
          array (
            'A9-0139/2024' =>
            array (
              'Discharge 2022: EU general budget - Commission' => 'Discharge 2022: EU general budget - Commission',
            ),
          ),
          '8. Décharge 2022: Budget général de l\'UE - Parlement européen (vote)' =>
          array (
            'A9-0067/2024' =>
            array (
              'Discharge 2022: EU general budget - European Parliament' => 'Discharge 2022: EU general budget - European Parliament',
            ),
          ),
          '9. Décharge 2022: Budget général de l\'UE - Conseil européen et Conseil (vote)' =>
          [],
          '10. Décharge 2022: Budget général de l\'UE - Cour de justice de l\'Union européenne (vote)' =>
          array (
            'A9-0075/2024' =>
            array (
              'Discharge 2022: EU general budget - Court of Justice of the European Union' => 'Discharge 2022: EU general budget - Court of Justice of the European Union',
            ),
          ),
          '11. Décharge 2022: Budget général de l\'UE - Cour des comptes (vote)' =>
          array (
            'A9-0074/2024' =>
            array (
              'Discharge 2022: EU general budget - Court of Auditors' => 'Discharge 2022: EU general budget - Court of Auditors',
            ),
          ),

          '12. Décharge 2022: Budget général de l\'UE - Comité économique et social (vote)' =>
          array (
            'A9-0072/2024' =>
            array (
              'Discharge 2022: EU general budget - European Economic and Social Committee' => 'Discharge 2022: EU general budget - European Economic and Social Committee',
            ),
          ),

          '13. Décharge 2022: Budget général de l\'UE - Comité des régions (vote)' =>
          array (
            'A9-0073/2024' =>
            array (
              'Discharge 2022: EU general budget - Committee of the Regions' => 'Discharge 2022: EU general budget - Committee of the Regions',
            ),
          ),
          '14. Décharge 2022: Budget général de l\'UE - Médiateur européen (vote)' =>
          array (
            'A9-0084/2024' =>
            array (
              'Discharge 2022: EU general budget - European Ombudsman' => 'Discharge 2022: EU general budget - European Ombudsman',
            ),
          ),
          '15. Décharge 2022: Budget général de l\'UE - Contrôleur européen de la protection des données (vote)' =>
          array (
            'A9-0086/2024' =>
            array (
              'Discharge 2022: EU general budget - European Data Protection Supervisor' => 'Discharge 2022: EU general budget - European Data Protection Supervisor',
            ),
          ),
          '16. Décharge 2022: Budget général de l\'UE - Service européen pour l\'action extérieure (vote)' =>
          array (
            'A9-0102/2024' =>
            array (
              'Discharge 2022: EU general budget - European External Action Service' => 'Discharge 2022: EU general budget - European External Action Service',
            ),
          ),
          '17. Décharge 2022: Parquet européen (vote)' =>
          array (
            'A9-0117/2024' =>
            array (
              'Discharge 2022: EU general budget - European Public Prosecutors Office (the ‘EPPO’)' => 'Discharge 2022: EU general budget - European Public Prosecutors Office (the ‘EPPO’)',
            ),
          ),
          '18. Décharge 2022: Centre européen pour le développement de la formation professionnelle (Cedefop) (vote)' =>
          array (
            'A9-0080/2024' =>
            array (
              'Discharge 2022: European Centre for the Development of Vocational Training (Cedefop)' => 'Discharge 2022: European Centre for the Development of Vocational Training (Cedefop)',
            ),
          ),
          '19. Décharge 2022: Fondation européenne pour l’amélioration des conditions de vie et de travail (Eurofound) (vote)' =>
          array (
            'A9-0122/2024' =>
            array (
              'Discharge 2022: European Foundation for the improvement of living and working conditions (Eurofound)' => 'Discharge 2022: European Foundation for the improvement of living and working conditions (Eurofound)',
            ),
          ),
          '20. Décharge 2022: Agence des droits fondamentaux de l’Union européenne (vote)' =>
          array (
            'A9-0112/2024' =>
            array (
              'Discharge 2022: European Union Agency for Fundamental Rights' => 'Discharge 2022: European Union Agency for Fundamental Rights',
            ),
          ),
          '21. Décharge 2022: Observatoire européen des drogues et des toxicomanies (vote)' =>
          array (
            'A9-0104/2024' =>
            array (
              'Discharge 2022: European Monitoring Centre for Drugs and Drug Addiction' => 'Discharge 2022: European Monitoring Centre for Drugs and Drug Addiction',
            ),
          ),
          '22. Décharge 2022: Agence européenne de l\'environnement (vote)' =>
          array (
            'A9-0130/2024' =>
            array (
              'Discharge 2022: European Environment Agency' => 'Discharge 2022: European Environment Agency',
            ),
          ),
          '23. Décharge 2022: Agence européenne pour la sécurité et la santé au travail (EU-OSHA) (vote)' =>
          array (
            'A9-0115/2024' =>
            array (
              'Discharge 2022: European Agency for Safety and Health at Work (EU-OSHA)' => 'Discharge 2022: European Agency for Safety and Health at Work (EU-OSHA)',
            ),
          ),
          '24. Décharge 2022: Centre de traduction des organes de l’Union européenne (vote)' =>
          array (
            'A9-0082/2024' =>
            array (
              'Discharge 2022: Translation Centre for the Bodies of the European Union' => 'Discharge 2022: Translation Centre for the Bodies of the European Union',
            ),
          ),
          '25. Décharge 2022: Agence européenne des médicaments (vote)' =>
          array (
            'A9-0133/2024' =>
            array (
              'Discharge 2022: European Medicines Agency' => 'Discharge 2022: European Medicines Agency',
            ),
          ),
          '26. Décharge 2022: Agence de l’Union européenne pour la coopération judiciaire en matière pénale (Eurojust) (vote)' =>
          array (
            'A9-0099/2024' =>
            array (
              'Discharge 2022: European Union Agency for Criminal Justice Cooperation (Eurojust)' => 'Discharge 2022: European Union Agency for Criminal Justice Cooperation (Eurojust)',
            ),
          ),
          '27. Décharge 2022: Fondation européenne pour la formation (vote)' =>
          array (
            'A9-0114/2024' =>
            array (
              'Discharge 2022: European Training Foundation' => 'Discharge 2022: European Training Foundation',
            ),
          ),
          '28. Décharge 2022: Agence européenne pour la sécurité maritime (vote)' =>
          array (
            'A9-0107/2024' =>
            array (
              'Discharge 2022: European Maritime Safety Agency' => 'Discharge 2022: European Maritime Safety Agency',
            ),
          ),
          '29. Décharge 2022: Agence de l’Union européenne pour la sécurité aérienne (vote)' =>
          array (
            'A9-0116/2024' =>
            array (
              'Discharge 2022: European Union Aviation Safety Agency' => 'Discharge 2022: European Union Aviation Safety Agency',
            ),
          ),
          '30. Décharge 2022: Autorité européenne de sécurité des aliments (vote)' =>
          array (
            'A9-0129/2024' =>
            array (
              'Discharge 2022: European Food Safety Authority' => 'Discharge 2022: European Food Safety Authority',
            ),
          ),
          '31. Décharge 2022: Centre européen de prévention et de contrôle des maladies (vote)' =>
          array (
            'A9-0109/2024' =>
            array (
              'Discharge 2022: European Centre for Disease Prevention and Control' => 'Discharge 2022: European Centre for Disease Prevention and Control',
            ),
          ),
          '32. Décharge 2022: ENISA (Agence de l\'Union européenne pour la cybersécurité) (vote)' =>
          array (
            'A9-0134/2024' =>
            array (
              'Discharge 2022: ENISA (European Union Agency for Cybersecurity)' => 'Discharge 2022: ENISA (European Union Agency for Cybersecurity)',
            ),
          ),
          '33. Décharge 2022: Agence de l\'Union européenne pour les chemins de fer (vote)' =>
          array (
            'A9-0092/2024' =>
            array (
              'Discharge 2022: European Union Agency for Railways' => 'Discharge 2022: European Union Agency for Railways',
            ),
          ),
          '34. Décharge 2022: Agence de l\'Union européenne pour la formation des services répressifs (CEPOL) (vote)' =>
          array (
            'A9-0098/2024' =>
            array (
              'Discharge 2022: European Union Agency for Law Enforcement Training (CEPOL)' => 'Discharge 2022: European Union Agency for Law Enforcement Training (CEPOL)',
            ),
          ),
          '35. Décharge 2022: Agence européenne de garde-frontières et de garde-côtes (vote)' =>
          array (
            'A9-0113/2024' =>
            array (
              'Discharge 2022: European Border and Coast Guard Agency' => 'Discharge 2022: European Border and Coast Guard Agency',
            ),
          ),
          '36. Décharge 2022: Agence de l’Union européenne pour le programme spatial (vote)' =>
          array (
            'A9-0121/2024' =>
            array (
              'Discharge 2022: European Union Agency for the Space Programme' => 'Discharge 2022: European Union Agency for the Space Programme',
            ),
          ),
          '37. Décharge 2022: Agence européenne de contrôle des pêches (vote)' =>
          array (
            'A9-0100/2024' =>
            array (
              'Discharge 2022: European Fisheries Control Agency' => 'Discharge 2022: European Fisheries Control Agency',
            ),
          ),
          '38. Décharge 2022: Agence européenne des produits chimiques (vote)' =>
          array (
            'A9-0135/2024' =>
            array (
              'Discharge 2022: European Chemicals Agency' => 'Discharge 2022: European Chemicals Agency',
            ),
          ),
          '39. Décharge 2022: Agence d\'approvisionnement d\'Euratom (vote)' =>
          array (
            'A9-0108/2024' =>
            array (
              'Discharge 2022: Euratom Supply Agency' => 'Discharge 2022: Euratom Supply Agency',
            ),
          ),
          '40. Décharge 2022: Agence de l’Union européenne pour la coopération des services répressifs (Europol) (vote)' =>
          array (
            'A9-0119/2024' =>
            array (
              'Discharge 2022: European Union Agency for Law Enforcement Cooperation (Europol)' => 'Discharge 2022: European Union Agency for Law Enforcement Cooperation (Europol)',
            ),
          ),
          '41. Décharge 2022: Institut européen pour l\'égalité entre les hommes et les femmes (vote)' =>
          array (
            'A9-0096/2024' =>
            array (
              'Discharge 2022: European Institute for Gender Equality' => 'Discharge 2022: European Institute for Gender Equality',
            ),
          ),
          '42. Décharge 2022: Autorité bancaire européenne (vote)' =>
          array (
            'A9-0111/2024' =>
            array (
              'Discharge 2022: European Banking Authority' => 'Discharge 2022: European Banking Authority',
            ),
          ),
          '43. Décharge 2022: Autorité européenne des assurances et des pensions professionnelles (vote)' =>
          array (
            'A9-0097/2024' =>
            array (
              'Discharge 2022: European Insurance and Occupational Pensions Authority' => 'Discharge 2022: European Insurance and Occupational Pensions Authority',
            ),
          ),
          '44. Décharge 2022: Autorité européenne des marchés financiers (vote)' =>
          array (
            'A9-0103/2024' =>
            array (
              'Discharge 2022: European Securities and Markets Authority' => 'Discharge 2022: European Securities and Markets Authority',
            ),
          ),
          '45. Décharge 2022: Agence de l\'Union européenne pour la coopération des régulateurs de l\'énergie (ACER) (vote)' =>
          array (
            'A9-0120/2024' =>
            array (
              'Discharge 2022: European Union Agency for the Cooperation of Energy Regulators (ACER)' => 'Discharge 2022: European Union Agency for the Cooperation of Energy Regulators (ACER)',
            ),
          ),
          '46. Décharge 2022: Agence de soutien à l’ORECE (Office de l\'ORECE) (vote)' =>
          array (
            'A9-0123/2024' =>
            array (
              'Discharge 2022: Agency for Support for BEREC (BEREC Office)' => 'Discharge 2022: Agency for Support for BEREC (BEREC Office)',
            ),
          ),
          '47. Décharge 2022: Institut européen d\'innovation et de technologie (EIT) (vote)' =>
          array (
            'A9-0132/2024' =>
            array (
              'Discharge 2022: European Institute of Innovation and Technology (EIT)' => 'Discharge 2022: European Institute of Innovation and Technology (EIT)',
            ),
          ),
          '48. Décharge 2022: Agence de l\'Union européenne pour l\'asile (avant le 19 janvier 2022: Bureau européen d’appui en matière d’asile) (vote)' =>
          array (
            'A9-0106/2024' =>
            array (
              'Discharge 2022: European Union Agency for Asylum (before 19 January 2022: European Asylum Support Office)' => 'Discharge 2022: European Union Agency for Asylum (before 19 January 2022: European Asylum Support Office)',
            ),
          ),
          '49. Décharge 2022: Agence de l’Union européenne pour la gestion opérationnelle des systèmes d’information à grande échelle au sein de l’espace de liberté, de sécurité et de justice (eu-LISA) (vote)' =>
          array (
            'A9-0105/2024' =>
            array (
              'Discharge 2022: European Union Agency for the Operational Management of Large-Scale IT Systems in the Area of Freedom, Security and Justice (eu-LISA)' => 'Discharge 2022: European Union Agency for the Operational Management of Large-Scale IT Systems in the Area of Freedom, Security and Justice (eu-LISA)',
            ),
          ),
          '50. Décharge 2022: entreprise commune européenne pour ITER et le développement de l’énergie de fusion (Fusion for Energy) (vote)' =>
          array (
            'A9-0090/2024' =>
            array (
              'Discharge 2022: European Joint Undertaking for ITER and the Development of Fusion Energy (Fusion for Energy)' => 'Discharge 2022: European Joint Undertaking for ITER and the Development of Fusion Energy (Fusion for Energy)',
            ),
          ),
          '51. Décharge 2022: entreprise commune «Recherche sur la gestion du trafic aérien dans le ciel unique européen 3» (vote)' =>
          array (
            'A9-0094/2024' =>
            array (
              'Discharge 2022: Single European Sky ATM Research 3 Joint Undertaking' => 'Discharge 2022: Single European Sky ATM Research 3 Joint Undertaking',
            ),
          ),
          '52. Décharge 2022: entreprise commune «Aviation propre» (vote)' =>
          array (
            'A9-0087/2024' =>
            array (
              'Discharge 2022: Clean Aviation Joint Undertaking' => 'Discharge 2022: Clean Aviation Joint Undertaking',
            ),
          ),
          '53. Décharge 2022: entreprise commune «Une Europe fondée sur la bioéconomie circulaire» (vote)' =>
          array (
            'A9-0088/2024' =>
            array (
              'Discharge 2022: Circular Bio-based Europe Joint Undertaking' => 'Discharge 2022: Circular Bio-based Europe Joint Undertaking',
            ),
          ),
          '54. Décharge 2022: entreprise commune «Initiative en matière de santé innovante» (vote)' =>
          array (
            'A9-0093/2024' =>
            array (
              'Discharge 2022: Innovative Health Initiative Joint Undertaking' => 'Discharge 2022: Innovative Health Initiative Joint Undertaking',
            ),
          ),
          '55. Décharge 2022: entreprise commune «Hydrogène propre» (vote)' =>
          array (
            'A9-0089/2024' =>
            array (
              'Discharge 2022: Clean Hydrogen Joint Undertaking' => 'Discharge 2022: Clean Hydrogen Joint Undertaking',
            ),
          ),
          '56. Décharge 2022: entreprise commune «Système ferroviaire européen» (vote)' =>
          array (
            'A9-0091/2024' =>
            array (
              'Discharge 2022: Europe’s Rail Joint Undertaking' => 'Discharge 2022: Europe’s Rail Joint Undertaking',
            ),
          ),
          '57. Décharge 2022: entreprise commune «Technologies numériques clés» (vote)' =>
          array (
            'A9-0101/2024' =>
            array (
              'Discharge 2022: Key Digital Technologies Joint Undertaking' => 'Discharge 2022: Key Digital Technologies Joint Undertaking',
            ),
          ),
          '58. Décharge 2022: entreprise commune pour le calcul à haute performance européen (vote)' =>
          array (
            'A9-0095/2024' =>
            array (
              'Discharge 2022: European High Performance Computing Joint Undertaking' => 'Discharge 2022: European High Performance Computing Joint Undertaking',
            ),
          ),
          '59. Décharge 2022: Budget général de l\'UE - FED (9e, 10e et 11e) (vote)' =>
          array (
            'A9-0110/2024' =>
            array (
              'Discharge 2022: EU general budget - EDF (9th, 10th and 11th)' => 'Discharge 2022: EU general budget - EDF (9th, 10th and 11th)',
            ),
          ),
          '60. Décharge 2022: performance, gestion financière et contrôle des agences de l’Union européenne (vote)' =>
          array (
            'A9-0118/2024' =>
            array (
              'Discharge 2022: Performance, financial management and control of EU agencies' => 'Discharge 2022: Performance, financial management and control of EU agencies',
            ),
          ),
          '61. Décharge 2022: Autorité européenne du travail (vote)' =>
          array (
            'A9-0131/2024' =>
            array (
              'Discharge 2022: European Labour Authority' => 'Discharge 2022: European Labour Authority',
            ),
          ),
          '62. Marchés intérieurs des gaz naturel et renouvelable et de l\'hydrogène (refonte) ***I (vote)' =>
          array (
            'A9-0032/2023' =>
            array (
              'Internal markets for renewable gas, natural gas and hydrogen (recast)' => 'Internal markets for renewable gas, natural gas and hydrogen (recast)',
            ),
          ),
          '63. Règles communes pour les marchés intérieurs des gaz naturel et renouvelable et de l\'hydrogène (refonte) ***I (vote)' =>
          array (
            'A9-0035/2023' =>
            array (
              'Common rules for the internal markets for renewable gas, natural gas and hydrogen  (recast)' => 'Common rules for the internal markets for renewable gas, natural gas and hydrogen  (recast)',
            ),
          ),
          '64. Organisation du marché de l\'électricité de l\'Union: règlement ***I (vote)' =>
          array (
            'A9-0255/2023' =>
            array (
              'Union’s electricity market design: Regulation' => 'Union’s electricity market design: Regulation',
            ),
          ),
          '65. Organisation du marché de l\'électricité de l\'Union: directive ***I (vote)' =>
          array (
            'A9-0151/2024' =>
            array (
              'Union’s electricity market design: Directive' => 'Union’s electricity market design: Directive',
            ),
          ),

          // From toc.pdf => "61.Inscription du droit à l'avortement dans la charte des droits fondamentaux de l'UE"
            //     array (
              //       'B9-0205/2024' =>
            //       array (
              //         '§ 1 - Am 14' => '§ 1 - Am 14',
            //         '§ 3 - Am 4' => '§ 3 - Am 4',
            //         '§ 3/1' => '§ 3/1',
            //         '§ 3/2' => '§ 3/2',
            //         'Après le § 3 - Am 7' => 'Après le § 3 - Am 7',
            //         'Après le § 3 - Am 15' => 'Après le § 3 - Am 15',
            //         'Après le § 4 - Am 16' => 'Après le § 4 - Am 16',
            //         '§ 5' => '§ 5',
            //         'Après le § 5 - Am 17' => 'Après le § 5 - Am 17',
            //         '§ 6 - Am 6S' => '§ 6 - Am 6S',
            //         '§ 6/1' => '§ 6/1',
            //         '§ 6/2' => '§ 6/2',
            //         '§ 8 - Am 5' => '§ 8 - Am 5',
            //         '§ 8' => '§ 8',
            //         '§ 11' => '§ 11',
            //         'Après le § 11 - Am 9' => 'Après le § 11 - Am 9',
            //         'Après le § 12 - Am 8' => 'Après le § 12 - Am 8',
            //         'Visas - Am 3' => 'Visas - Am 3',
            //         'Considérant A/1' => 'Considérant A/1',
            //         'Après le considérant A - Am 10' => 'Après le considérant A - Am 10',
            //         'Après le considérant A - Am 11' => 'Après le considérant A - Am 11',
            //         'Après le considérant A - Am 12' => 'Après le considérant A - Am 12',
            //         'Après le considérant A - Am 13' => 'Après le considérant A - Am 13',
            //         'Considérant B/2' => 'Considérant B/2',
            //         'Considérant P' => 'Considérant P',
            //         'Considérant Q/1' => 'Considérant Q/1',
            //         'Considérant Q/2' => 'Considérant Q/2',
            //         'Considérant N' => 'Considérant N',
            //         'Considérant T' => 'Considérant T',
            //         'Considérant U' => 'Considérant U',
            //         'Proposition de résolution (ensemble du texte)' => 'Proposition de résolution (ensemble du texte)',
            //       ),
            //     ),
            );

            // @todo : first compare to -RCV*_fr.pdf
            $map['PV-9-2024-04-23-RCV'] = array (
              '1. Déploiement progressif d’Eudamed, obligation d’information en cas d’interruption d’approvisionnement et dispositions transitoires applicables à certains dispositifs médicaux de diagnostic in vitro ***I (vote)'
              => [],
              '2. Règles de l\'Union en matière de protection des investisseurs de détail ***I (vote)'
              => [],
              '7. Objection conformément à l\'article 111, paragraphe 3, du règlement: règlement délégué de la Commission modifiant le règlement (CE) nº 810/2009 en ce qui concerne le montant des droits de visa (vote)'
              => [],
              //     '1. Déploiement progressif d’Eudamed, obligation d’information en cas d’interruption d’approvisionnement et dispositions transitoires applicables à certains dispositifs médicaux de diagnostic in vitro ***I (vote)' =>
            //     array (
              //       'Skiped 2' => NULL,
            //     ),
            //     '2. Règles de l\'Union en matière de protection des investisseurs de détail ***I (vote)' =>
            //     array (
              //       'Skiped 1' => NULL,
            //     ),
              '3. Décharge 2022: Budget général de l\'UE - Conseil européen et Conseil (vote)' =>
              array (
                'A9-0071/2024' =>
                array (
                  'Discharge 2022: EU general budget - European Council and Council' => 'Discharge 2022: EU general budget - European Council and Council',
                ),
              ),
              '5. Recommandation de décision ne faisant pas objection à un acte délégué: règles relatives au ratio concernant la norme 1 relative aux BCAE (vote)' =>
              array (
                'B9-0199/2024' =>
                array (
                //         'Projet de décision' => 'Projet de décision',
                ),
              ),
              '6. Objection conformément à l\'article 111, paragraphe 3, du règlement: retrait de Gibraltar du tableau figurant au point I de l\'annexe du règlement délégué (UE) 2016/1675 (vote)' =>
              array (
                'B9-0210/2024' =>
                array (
                //         'Proposition de résolution' => 'Proposition de résolution',
                ),
              ),
              '8. Objection conformément à l\'article 111, paragraphe 3, du règlement intérieur: matières premières pour la production de biocarburants et de biogaz (vote)' =>
              array (
                'B9-0218/2024' =>
                array (
                //         'Proposition de résolution' => 'Proposition de résolution',
                ),
              ),

              '9. Mesures visant à réduire le coût du déploiement de réseaux gigabit de communications électroniques (règlement sur les infrastructures gigabit) ***I (vote)' =>
              array (
                'A9-0275/2023' =>
                array (
                  'Measures to reduce the cost of deploying gigabit electronic communications networks' => 'Measures to reduce the cost of deploying gigabit electronic communications networks',
                ),
              ),

              '10. Transmission des procédures pénales ***I (vote)' =>
              array (
                'A9-0008/2024' =>
                array (
                  'The transfer of proceedings in criminal matters' => 'The transfer of proceedings in criminal matters',
                ),
              ),

              '11. Établissement d\'un cadre pour le redressement et la résolution des entreprises d’assurance et de réassurance ***I (vote)' =>
              array (
                'A9-0251/2023' =>
                array (
                  'Establishing a framework for the recovery and resolution of insurance and reinsurance undertakings' => 'Establishing a framework for the recovery and resolution of insurance and reinsurance undertakings',
                ),
              ),

              '12. Modification de la directive Solvabilité II ***I (vote)' =>
              array (
                'A9-0256/2023' =>
                array (
                  'Amendments to the Solvency II Directive' => 'Amendments to the Solvency II Directive',
                ),
              ),

              '13. Classification, étiquetage et emballage des substances et des mélanges ***I (vote)' =>
              array (
                'A9-0271/2023' =>
                array (
                  'Classification, labelling and packaging of substances and mixtures' => 'Classification, labelling and packaging of substances and mixtures',
                ),
              ),

              '14. Services de paiement et services de monnaie électronique dans le marché intérieur ***I (vote)' =>
              array (
                'A9-0046/2024' =>
                array (
                  'Payment services and electronic money services in the Internal Market amending Directive 98/26/EC and repealing Directives 2015/2366/EU and 2009/110/EC' => 'Payment services and electronic money services in the Internal Market amending Directive 98/26/EC and repealing Directives 2015/2366/EU and 2009/110/EC',
                ),
              ),

              '15. Services de paiement dans le marché intérieur et modification du règlement (UE) nº 1093/2010 ***I (vote)' =>
              array (
                'A9-0052/2024' =>
                array (
                  'Payment services in the internal market and amending Regulation (EU) No 1093/2010' => 'Payment services in the internal market and amending Regulation (EU) No 1093/2010',
                ),
              ),
              '16. Lutte contre le retard de paiement dans les transactions commerciales ***I (vote)' =>
              array (
                'A9-0156/2024' =>
                array (
                  'Combating late payment in commercial transactions' => 'Combating late payment in commercial transactions',
                ),
              ),
              '17. Modification de la directive 2014/62/UE en ce qui concerne certaines obligations déclaratives ***I (vote)' =>
              array (
                'A9-0152/2024' =>
                array (
                  'Amending Directive 2014/62/EU as regards certain reporting requirements' => 'Amending Directive 2014/62/EU as regards certain reporting requirements',
                ),
              ),
              '18. Accès des autorités compétentes aux registres centralisés des comptes bancaires par l’intermédiaire du point d\'accès unique ***I (vote)' =>
              array (
                'A9-0004/2023' =>
                array (
                  'Access of competent authorities to centralised bank account registries through the single  access point' => 'Access of competent authorities to centralised bank account registries through the single  access point',
                ),
              ),
              '19. Règlement relatif à l\'importation, à l\'exportation et au transit des armes à feu, de leurs parties essentielles et munitions (refonte) ***I (vote)' =>
              array (
                'A9-0312/2023' =>
                array (
                  'Regulation on import, export and transit measures for firearms, their essential components and ammunition(recast)' => 'Regulation on import, export and transit measures for firearms, their essential components and ammunition(recast)',
                ),
              ),
              '20. Règlement sur l’écoconception ***I (vote)' =>
              array (
                'A9-0218/2023' =>
                array (
                  'Ecodesign Regulation' => 'Ecodesign Regulation',
                ),
              ),
              '21. Mesures de libéralisation temporaire des échanges en complément des concessions commerciales applicables aux produits ukrainiens au titre de l\'accord d’association UE/Euratom/Ukraine ***I (vote)' =>
              array (
                'A9-0077/2024' =>
                array (
                  'Temporary trade-liberalisation measures supplementing trade concessions applicable to Ukrainian products under the EU/Euratom/Ukraine Association Agreement' => 'Temporary trade-liberalisation measures supplementing trade concessions applicable to Ukrainian products under the EU/Euratom/Ukraine Association Agreement',
                ),
              ),
              '22. Exemption de visa pour les titulaires d’un passeport serbe délivré par la direction de coordination serbe ***I (vote)' =>
              array (
                'A9-0172/2024' =>
                array (
                  'Visa exception for holders of Serbian passports issued by the Serbian Coordination  Directorate' => 'Visa exception for holders of Serbian passports issued by the Serbian Coordination  Directorate',
                ),
              ),
              '23. Protocole à l’accord euro-méditerranéen: participation de l’Égypte aux programmes de l’Union *** (vote)' =>
              array (
                'A9-0175/2024' =>
                array (
                  'Protocol to the Euro-Mediterranean Agreement: participation of Egypt in Union programmes' => 'Protocol to the Euro-Mediterranean Agreement: participation of Egypt in Union programmes',
                ),
              ),
              '24. Prévention des pertes de granulés plastiques en vue de réduire la pollution par les microplastiques ***I (vote)' =>
              array (
                'A9-0148/2024' =>
                array (
                  'Preventing plastic pellet losses to reduce microplastic pollution' => 'Preventing plastic pellet losses to reduce microplastic pollution',
                ),
              ),
              '25. Règles communes visant à promouvoir la réparation des biens ***I (vote)' =>
              array (
                'A9-0316/2023' =>
                array (
                  'Common rules promoting the repair of goods' => 'Common rules promoting the repair of goods',
                ),
              ),
              '26. Interdire sur le marché de l\'Union les produits issus du travail forcé ***I (vote)' =>
              array (
                'A9-0306/2023' =>
                array (
                  'Prohibiting products made with forced labour on the Union market' => 'Prohibiting products made with forced labour on the Union market',
                ),
              ),
              '27. Modification de la directive 2011/36/UE concernant la prévention de la traite des êtres humains et la lutte contre ce phénomène ainsi que la protection des victimes ***I (vote)' =>
              array (
                'A9-0285/2023' =>
                array (
                  'Amending Directive 2011/36/EU on preventing and combating trafficking in human beings and protecting its victims' => 'Amending Directive 2011/36/EU on preventing and combating trafficking in human beings and protecting its victims',
                ),
              ),
              '28. Coordination efficace des politiques économiques et surveillance budgétaire multilatérale ***I (vote)' =>
              array (
                'A9-0439/2023' =>
                array (
                  'Effective coordination of economic policies and multilateral  budgetary surveillance' => 'Effective coordination of economic policies and multilateral  budgetary surveillance',
                ),
              ),
              '29. Accélération et clarification de la mise en œuvre de la procédure concernant les déficits excessifs – règlement modificatif * (vote)' =>
              array (
                'A9-0444/2023' =>
                array (
                  'Speeding up and clarifying the implementation of the excessive deficit procedure' => 'Speeding up and clarifying the implementation of the excessive deficit procedure',
                ),
              ),
              '30. Exigences applicables aux cadres budgétaires des États membres – directive modificative * (vote)' =>
              array (
                'A9-0440/2023' =>
                array (
                  'Amending Directive 2011/85/EU on requirements for budgetary frameworks of the Member States' => 'Amending Directive 2011/85/EU on requirements for budgetary frameworks of the Member States',
                ),
              ),
            );

            $map['PV-9-2024-04-25-RCV'] = array (
              '4. Nouvelle loi sur la sécurité à Hong Kong et le cas d\'Andy Li et de Joseph John (vote)' =>
              array (
              //       'RC-B9-0228/2024' => '3. Proposition d\'abrogation de la loi interdisant les mutilations génitales féminines en Gambie (vote)',
              ),
              '5. Organisme interinstitutionnel chargé des normes éthiques (vote)' =>
              array (
                'A9-0181/2024' =>
                array (
                  'Interinstitutional Body for Ethical Standards' => 'Interinstitutional Body for Ethical Standards',
                ),
              ),
              '6. État prévisionnel des dépenses et des recettes pour l\'exercice 2025 - Section I - Parlement européen (vote)' =>
              array (
                'A9-0180/2024' =>
                array (
                  'Estimates of revenue and expenditure for the financial year 2025' => 'Estimates of revenue and expenditure for the financial year 2025',
                ),
              ),
              '7. Projet de budget rectificatif nº 1/2024: modification du budget 2024 requise à la suite de la révision du CFP (vote)' =>
              array (
                'A9-0174/2024' =>
                array (
                  'Draft amending budget No 1/2024; Amendments of the 2024 budget required due to the MFF revision' => 'Draft amending budget No 1/2024; Amendments of the 2024 budget required due to the MFF revision',
                ),
              ),
              '8. Projet de budget rectificatif nº 3/2024: renforcement du Parquet européen à la suite de l\'adhésion de la Pologne et de la participation attendue de la Suède (vote)' =>
              array (
                'A9-0179/2024' =>
                array (
                  'Draft amending budget No 3/2024: Reinforcing the European Public Prosecutor\'s Office following the accession of Poland and the expected participation of Sweden' => 'Draft amending budget No 3/2024: Reinforcing the European Public Prosecutor\'s Office following the accession of Poland and the expected participation of Sweden',
                ),
              ),
              '9. Informations préalables sur les passagers: renforcer et faciliter les contrôles aux frontières extérieures ***I (vote)' =>
              array (
                'A9-0409/2023' =>
                array (
                  'Collection and transfer of advance passenger information (API) for enhancing and facilitating external border controls, amending Regulation (EU) 2019/817 and Regulation (EU) 2018/1726, and repealing Council Directive 2004/82/EC' => 'Collection and transfer of advance passenger information (API) for enhancing and facilitating external border controls, amending Regulation (EU) 2019/817 and Regulation (EU) 2018/1726, and repealing Council Directive 2004/82/EC',
                ),
              ),
              '10. Informations préalables sur les passagers: prévention et détection des infractions terroristes et des formes graves de criminalité, et enquêtes et poursuites en la matière ***I (vote)' =>
              array (
                'A9-0411/2023' =>
                array (
                  'Collection and transfer of advance passenger information for the prevention, detection, investigation and prosecution of terrorist offences and serious crime' => 'Collection and transfer of advance passenger information for the prevention, detection, investigation and prosecution of terrorist offences and serious crime',
                ),
              ),
              '11. Cadre de mesures en vue de renforcer l’écosystème européen de la fabrication de produits de technologie «zéro net» (règlement pour une industrie «zéro net») ***I (vote)' =>
              array (
                'A9-0343/2023' =>
                array (
                  'Framework of measures for strengthening Europe’s net-zero technology products manufacturing ecosystem (Net Zero Industry Act)' => 'Framework of measures for strengthening Europe’s net-zero technology products manufacturing ecosystem (Net Zero Industry Act)',
                ),
              ),
            );

            //   $map['PV-9-2024-04-25-RCV'] = [
            //     '5. Organisme interinstitutionnel chargé des normes éthiques (vote)'
            //     => ['A9-0181/2024' => 'Interinstitutional Body for Ethical Standards'],
            //     '6. État prévisionnel des dépenses et des recettes pour l\'exercice 2025 - Section I - Parlement européen (vote)'
            //     => ['A9-0180/2024' => 'Estimates of revenue and expenditure for the financial year 2025'],
            //     '7. Projet de budget rectificatif nº 1/2024: modification du budget 2024 requise à la suite de la révision du CFP (vote)'
            //     => ['A9-0174/2024' => 'Draft amending budget No 1/2024; Amendments of the 2024 budget required due to the MFF revision'],
            //     '8. Projet de budget rectificatif nº 3/2024: renforcement du Parquet européen à la suite de l\'adhésion de la Pologne et de la participation attendue de la Suède (vote)'
            //     => ['A9-0179/2024' => 'Draft amending budget No 3/2024: Reinforcing the European Public Prosecutor\'s Office following the accession of Poland and the expected participation of Sweden'],
            //     '9. Informations préalables sur les passagers: renforcer et faciliter les contrôles aux frontières extérieures ***I (vote)'
            //     => ['A9-0409/2023' => 'Collection and transfer of advance passenger information (API) for enhancing and facilitating external border controls, amending Regulation (EU) 2019/817 and Regulation (EU) 2018/1726, and repealing Council Directive 2004/82/EC'],
            //     '10. Informations préalables sur les passagers: prévention et détection des infractions terroristes et des formes graves de criminalité, et enquêtes et poursuites en la matière ***I (vote)'
            //     => ['A9-0411/2023' => 'Collection and transfer of advance passenger information for the prevention, detection, investigation and prosecution of terrorist offences and serious crime'],
            //     '11. Cadre de mesures en vue de renforcer l’écosystème européen de la fabrication de produits de technologie «zéro net» (règlement pour une industrie «zéro net») ***I (vote)'
            //     => ['A9-0343/2023' => 'Framework of measures for strengthening Europe’s net-zero technology products manufacturing ecosystem (Net Zero Industry Act)'],
            //   ];


            $map['PV-9-2024-04-24-RCV'] = [
              '1. Modifications du règlement intérieur du Parlement concernant la formation à la prévention des conflits et du harcèlement sur le lieu de travail ainsi qu\'à la bonne gestion d’un bureau (vote)'
              => ['A9-0163/2024' => 'Amendments to Parliament’s Rules of Procedure concerning the training on preventing conflict and harassment in the workplace and on good office management'],

              '2. Objections formulées en vertu de l’article 111, paragraphe 3, du règlement: Nouveaux aliments - définition d’un «nanomatériau manufacturé» (vote)'
              // rcf pdf : 2. Objections formulées en vertu de l’article 111, paragraphe 3, du règlement: Nouveaux aliments - définition d’un «nanomatériau manufacturé» : B9-0225/2024
              // 2. Objections formulées en vertu de l’article 111, paragraphe 3, du règlement: Nouveaux aliments - définition d’un «nanomatériau manufacturé»
              => ['B9-0225/2024' => 'Proposition de résolution'],

              '3. Objection conformément à l’article 112, paragraphes 2 et 3, du règlement: Calcul, vérification et communication des données relatives à la teneur en plastique recyclé des bouteilles pour boissons en plastique à usage unique (vote)'
              => [],
              // rcf pdf : 3. Réseau transeuropéen de transport ***I : 3.1 A9-0147/2023
              '4. Réseau transeuropéen de transport ***I (vote)' =>
              array (
                'A9-0147/2023' => 'Trans-European transport network',
              ),
              '5. Emballages et déchets d\'emballages ***I (vote)' =>
              array (
                'A9-0319/2023' => 'Packaging and packaging waste',
              ),
              '6. La qualité de l’air ambiant et un air pur pour l’Europe ***I (vote)' =>
              array (
                'A9-0233/2023' => 'Ambient air quality and cleaner air for Europe',
              ),
              '7. Instrument du marché unique pour les situations d’urgence ***I (vote)' =>
              array (
                'A9-0246/2023' => 'Single Market Emergency Instrument',
              ),
              '8. Modification de certains règlements en ce qui concerne l\'établissement d\'un instrument du marché unique pour les situations d\'urgence ***I (vote)' =>
              array (
                'A9-0244/2023' => 'Amending certain Regulations as regards the establishment of the Single Market Emergency Instrument',
              ),
              '9. Modification de certaines directives en ce qui concerne l\'établissement d\'un instrument du marché unique pour les situations d\'urgence ***I (vote)' =>
              array (
                'A9-0245/2023' => 'Amending certain Directives as regards the establishment of the Single Market Emergency Instrument',
              ),
              '10. Code frontières Schengen ***I (vote)' =>
              array (
                'A9-0280/2023' => 'Schengen Borders Code',
              ),
              '11. Échange transfrontalier d’informations concernant les infractions en matière de sécurité routière ***I (vote)' =>
              array (
                'A9-0396/2023' => 'Cross-border exchange of information on road-safety related traffic offences',
              ),
              '12. Végétaux obtenus au moyen de certaines nouvelles techniques génomiques et denrées alimentaires et aliments pour animaux qui en sont dérivés ***I (vote)' =>
              array (
                'A9-0014/2024' => 'Plants obtained by certain new genomic techniques and their food and feed',
              ),
              '13. Mesures d’intervention précoce, conditions de résolution et financement des mesures de résolution (règlement MRU 3) ***I (vote)' =>
              array (
                'A9-0155/2024' => 'Early intervention measures, conditions for resolution and funding of resolution action (SRMR3)',
              ),
              '14. Mesures d’intervention précoce, conditions de déclenchement d’une procédure de résolution et financement des mesures de résolution (BRRD3) ***I (vote)' =>
              array (
                'A9-0153/2024' => 'Amending Directive 2014/59/EU as regards early intervention measures, conditions for resolution and financing of resolution action',
              ),
              '15. Champ de protection des dépôts, utilisation des fonds des systèmes de garantie des dépôts, coopération transfrontière et transparence ***I (vote)' =>
              array (
                'A9-0154/2024' => 'Scope of deposit protection, use of deposit guarantee schemes funds, cross-border cooperation, and transparency (DGSD2)',
              ),
              '16. Devoir de vigilance des entreprises en matière de durabilité ***I (vote)' =>
              array (
                'A9-0184/2023' => 'Corporate Sustainability Due Diligence',
              ),
              '17. Amélioration des conditions de travail dans le cadre du travail via une plateforme ***I (vote)' =>
              array (
                'A9-0301/2022' => 'Improving working conditions in platform work',
              ),
              '18. Espace européen des données de santé ***I (vote)' =>
              array (
                'A9-0395/2023' => 'European Health Data Space',
              ),
              '19. Mobilisation du Fonds européen d’ajustement à la mondialisation: demande EGF/2023/004 DK/Danish Crown - Danemark (vote)' =>
              array (
                'A9-0171/2024' => 'mobilisation of the European Globalisation Adjustment Fund - Application  EGF/2023/004 DK/Danish Crown - Denmark',
              ),
              '20. Mobilisation du Fonds européen d’ajustement à la mondialisation: demande EGF/2023/003 DE/Vallourec - Allemagne (vote)' =>
              array (
                'A9-0166/2024' => 'Mobilisation of the European Globalisation Adjustment Fund - Application  EGF/2023/003 DE/Vallourec - Germany',
              ),
              '21. Mobilisation du Fonds européen d’ajustement à la mondialisation: demande EGF/2024/000 TA 2024 – Assistance technique à l’initiative de la Commission (vote)' =>
              array (
                'A9-0173/2024' => 'Mobilisation of the European Globalisation Adjustment Fund: Application  EGF/2024/000 TA 2024 - Technical assistance at the initiative of the Commission',
              ),
              '22. Retrait de l\'Union du traité sur la Charte de l\'énergie *** (vote)' =>
              array (
                'A9-0176/2024' => 'Withdrawal of the Union from the Energy Charter Treaty',
              ),
              '23. Mesures pour faciliter la protection consulaire des citoyens de l’Union non représentés dans des pays tiers * (vote)' =>
              array (
                'A9-0178/2024' => 'Measures to facilitate consular protection for unrepresented citizens of the Union in third countries',
              ),
              '24. Accord se rapportant à la convention des Nations unies sur le droit de la mer et portant sur la conservation et l\'utilisation durable de la diversité biologique marine des zones ne relevant pas de la juridiction nationale *** (vote)' =>
              array (
                'A9-0177/2024' => 'Agreement under the United Nations Convention on the Law of the Sea on the conservation and sustainable use of marine biological diversity of areas beyond national jurisdiction',
              ),
              '25. Lutte contre la violence à l’égard des femmes et la violence domestique ***I (vote)' =>
              array (
                'A9-0234/2023' => 'Combating violence against women and domestic violence',
              ),
              '26. Carte européenne du handicap et carte européenne de stationnement pour les personnes handicapées ***I (vote)' =>
              array (
                'A9-0003/2024' => 'European Disability Card and European Parking Card for persons with disabilities',
              ),
              '27. Carte européenne du handicap et carte européenne de stationnement pour personnes handicapées pour les ressortissants de pays tiers résidant légalement dans un État membre ***I (vote)' =>
              array (
                'A9-0059/2024' => 'European Disability Card and European Parking Card for persons with disabilities for third country nationals legally residing in a Member State',
              ),
              '28. Production et commercialisation des matériels de reproduction des végétaux ***I (vote)' =>
              array (
                'A9-0149/2024' => 'Production and marketing of plant reproductive material',
              ),
              '29. Production et commercialisation de matériel forestier de reproduction ***I (vote)' =>
              array (
                'A9-0142/2024' => 'Production and marketing of forest reproductive material',
              ),
              '30. Établissement d\'une facilité pour les réformes et la croissance en faveur des Balkans occidentaux ***I (vote)' =>
              array (
                'A9-0085/2024' => 'Establishing the Reform and Growth Facility for the Western Balkans',
              ),


              // '2. Réception et surveillance du marché des engins mobiles non routiers circulant sur la voie publique ***I (vote)' =>
              // array (
              //   'C9-0120/2024' => '1. Simplification de certaines règles de la PAC ***I (vote)',
                // ),
                //**** Finally from xml ==> C9-0120/202 : 1. Simplification de certaines règles de la PAC ***I (vote)
              //****   => english version : 'Good agricultural and environmental condition standards, schemes for climate, environment and animal welfare'


              // '30. Établissement d\'une facilité pour les réformes et la croissance en faveur des Balkans occidentaux ***I (vote)' =>
              // array (
              //   'C9-0120/2024' => 'Good agricultural and environmental condition standards, schemes for climate, environment and animal welfare',
                // ),

              '1. Simplification de certaines règles de la PAC ***I (vote)' => [
                'C9-0120/2024' => 'Good agricultural and environmental condition standards, schemes for climate, environment and animal welfare',
              ],
              '2. Réception et surveillance du marché des engins mobiles non routiers circulant sur la voie publique ***I (vote)' =>
              array (
                'A9-0382/2023' => 'Approval and market surveillance of non-road mobile machinery circulating on public roads and amending Regulation (EU) 2019/1020',
              ),
              '3. Modification du règlement (UE) 2016/2031 relatif aux mesures de protection contre les organismes nuisibles aux végétaux ***I (vote)' =>
              array (
                'A9-0035/2024' => 'Amendment of Regulation (EU) 2016/2031 on protective measures against pests of plants',
              ),
              '4. Transparence et intégrité des activités de notation environnementale, sociale et de gouvernance (ESG) ***I (vote)' =>
              array (
                'A9-0417/2023' => 'Transparency and integrity of Environmental, Social and Governance (ESG) rating activities',
              ),
              '5. Mesures visant à atténuer les expositions excessives aux contreparties centrales de pays tiers et à améliorer l’efficience des marchés de la compensation de l’Union ***I (vote)' =>
              array (
                'A9-0398/2023' => 'Amending Regulations (EU) No 648/2012, (EU) No 575/2013 and (EU) 2017/1131 as regards measures to mitigate excessive exposures to third-country central counterparties and improve the efficiency of Union clearing markets',
              ),
              '6. Traitement du risque de concentration vis-à-vis des contreparties centrales et du risque de contrepartie des transactions sur instruments dérivés faisant l\'objet d\'une compensation centrale ***I (vote)' =>
              array (
                'A9-0399/2023' => 'Treatment of concentration risk towards central counterparties and the counterparty risk on centrally cleared derivative transactions',
              ),
              '7. Rendre les marchés des capitaux plus attractifs et faciliter l’accès des PME aux capitaux – modification de certains règlements ***I (vote)' =>
              array (
                'A9-0302/2023' => 'Increasing the attractiveness of public capital markets and facilitating access to capital for SMEs – amending certain Regulations',
              ),
              '8. Rendre les marchés des capitaux plus attractifs et faciliter l’accès des PME aux capitaux - modification de la directive ***I (vote)' =>
              array (
                'A9-0303/2023' => 'Increasing the attractiveness of public capital markets and facilitating access to capital for  SMEs – amending Directive',
              ),
              '9. Structures avec actions à votes multiples dans les entreprises qui demandent l’admission à la négociation de leurs actions sur un marché de croissance des PME ***I (vote)' =>
              array (
                'A9-0300/2023' => 'Multiple-vote share structures in companies that seek the admission to trading of their shares on an SME growth market',
              ),
              '10. Normes de qualité et de sécurité des substances d’origine humaine destinées à une application humaine ***I (vote)' =>
              array (
                'A9-0250/2023' => 'Standards of quality and safety for substances of human origin intended for human application',
              ),
              '11. Services de sécurité gérés ***I (vote)' =>
              array (
                'A9-0307/2023' => 'Managed security services',
              ),
              '12. Règlement sur la cybersolidarité ***I (vote)' =>
              array (
                'A9-0426/2023' => 'Laying down measures to strengthen solidarity and capacities in the Union to detect, prepare for and respond to cybersecurity threats and incidents',
              ),
              '13. Statistiques européennes du marché du travail concernant les entreprises ***I (vote)' =>
              array (
                'A9-0054/2024' => 'European labour market statistics on businesses',
              ),
              '14. Modification du règlement (UE) 2016/1011 en ce qui concerne le champ d\'application des règles applicables aux indices de référence, l\'utilisation dans l\'Union d\'indices de référence fournis par un administrateur situé dans un pays tiers et certaines obligations d’information ***I (vote)' =>
              array (
                'A9-0076/2024' => 'Amending Regulation (EU) 2016/1011 as regards the scope of the rules for benchmarks, the use in the Union of benchmarks provided by an administrator located in a third country, and certain reporting requirements',
              ),
              '15. Polluants des eaux de surface et des eaux souterraines ***I (vote)' =>
              array (
                'A9-0238/2023' => 'Surface water and groundwater pollutants',
              ),
              '16. Initiative EuroHPC en faveur des start-up visant à renforcer le rôle moteur de l’Europe dans le domaine de l’intelligence artificielle digne de confiance * (vote)' =>
              array (
                'A9-0161/2024' => 'EuroHPC initiative for start-ups to boost European leadership in trustworthy Artificial Intelligence',
              ),
              '17. Droit des sociétés - Extension et amélioration de l’utilisation des outils et processus numériques ***I (vote)' =>
              array (
                'A9-0394/2023' => 'Further expanding and upgrading the use of digital tools and processes in company law',
              ),
              '18. Statistiques européennes sur la population et le logement ***I (vote)' =>
              array (
                'A9-0284/2023' => 'Statistics on population and housing, amending Regulation (EC) No 862/2007 and repealing Regulations (EC) No 763/2008 and (EU) No 1260/2013',
              ),
              '19. Modification de la directive 2013/36/UE en ce qui concerne les pouvoirs de surveillance, les sanctions, les succursales de pays tiers et les risques environnementaux, sociaux et de gouvernance ***I (vote)' =>
              array (
                'A9-0029/2023' => 'Amending Directive 2013/36/EU as regards supervisory powers, sanctions, third-country branches, and environmental, social and governance risks',
              ),
              '20. Modification du règlement (UE) nº 575/2013 en ce qui concerne les exigences pour risque de crédit, risque d’ajustement de l’évaluation de crédit, risque opérationnel et risque de marché et le plancher de fonds propres ***I (vote)' =>
              array (
                'A9-0030/2023' => 'Amending Regulation (EU) No 575/2013 as regards requirements for credit risk,  credit valuation adjustment risk, operational risk, market risk and the output floo',
              ),
              '21. Sixième directive anti-blanchiment ***I (vote)' =>
              array (
                'A9-0150/2023' => 'Mechanisms to be put in place by the Member States for the prevention of the use of the financial system for the purposes of money laundering or terrorist financing and repealing Directive (EU) 2015/849',
              ),
              '22. Règlement anti-blanchiment ***I (vote)' =>
              array (
                'A9-0151/2023' => 'Prevention of the use of the financial system for the purposes of money laundering or terrorist financing',
              ),
              '23. Institution de l\'Autorité de lutte contre le blanchiment de capitaux et le financement du terrorisme ***I (vote)' =>
              array (
                'A9-0128/2023' => 'Establishing the Authority for Anti-Money Laundering and  Countering the Financing of Terrorism',
              ),
            ];

            return $map[$doc_id] ?? [];
  }

}
