<?php
namespace Drupal\votewatch\Helper;

use Drupal\Component\Serialization\Json;

class Migrate {


  /**
   * @example drush ev 'Drupal\votewatch\Helper\Migrate::populateVoteCount("2024-04-25");'
   * @param unknown $plenary_date
   */
  public static function populateVoteCount($plenary_date = NULL) {
    $curent_user = \Drupal::currentUser();

    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');

    $timer = \Drupal::time();
    $start = $timer->getCurrentMicroTime();

    if (!$plenary_date) {

      $filename = \Drupal::config('migrate_plus.migration.mep_watch_vote')->get('source.urls.0');
      $source_filename = $file_system->realpath($filename);
      $source_basename = $file_system->basename($source_filename);

      $matches = [];
      if (preg_match('/^members_votes_(\d{4,4}-\d{2,2}-\d{2,2})\.json/', $source_basename, $matches)) {
        $plenary_date = $matches[1];
      }
    }

    if (!$plenary_date) {
      return;
    }

    /**@var $date_formatter \Drupal\Core\Datetime\DateFormatter */
    $date_formatter = \Drupal::service('date.formatter');
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $site_timezone = 'Europe/Paris';

    $plenary = ucwords($date_formatter->format(strtotime($plenary_date), 'custom', 'd F Y', $site_timezone));

    $keys = ['name' => $plenary, 'vid' => 'plenary_session'];
    if($plenary_terms = $term_storage->loadByProperties($keys)) {
      $plenary_term_id = key($plenary_terms);
    }
    else {
      return;
    }

    $msg = 'Populate from plenary : @plenary_date';
    $ctxt = ['@plenary_date' => $plenary_date];
    if (function_exists('drush_main')) {
      \Drupal::logger('votewatch')->notice($msg, $ctxt);
    }
    else if (!$curent_user->isAnonymous()) {
      \Drupal::messenger()->addStatus(t($msg, $ctxt));
    }

    $plenary_date_static = & drupal_static('votewatch.meps_map.plenary_date');
    $plenary_date_static = $plenary_date;

    $query_all = \Drupal::request()->query->all(); //set('field_citizenship_value', $country);
    $field_citizenship_value = $query_all['field_citizenship_value'] ?? NULL;
    $plenary_tid_depth = $query_all['term_node_tid_depth_1'] ?? NULL;

    \Drupal::request()->query->set('term_node_tid_depth_1', [$plenary_term_id]);

    $vote_nids = [];
    $allowed_values = \Drupal::config('field.storage.taxonomy_term.field_citizenship')->get('settings.allowed_values');
    $last_vc_time = $start;
    foreach($allowed_values as $allowed_value) {
      $country = $allowed_value['value'];
      //     return;
      foreach (['block_2' => 'for', 'block_3' => 'against', 'block_4' => 'abstention',] as $display => $field_name) {
        \Drupal::request()->query->set('field_citizenship_value', $country);

        $result = views_get_view_result('content', $display);
        // return;
        // 0.1034
        $msg = '@count votes processed from view display "@position", @country';
        $ctxt = ['@position' => $field_name, '@count' => count($result), '@country' => $country];
        if (function_exists('drush_main')) {
          \Drupal::logger('votewatch')->notice($msg, $ctxt);
        }
        else if (!$curent_user->isAnonymous()) {
          \Drupal::messenger()->addStatus(t($msg, $ctxt), TRUE);
        }

        /**@var $uuid_service \Drupal\Component\Uuid\Php */
        $uuid_service = \Drupal::service('uuid');


        $party_weight = [
          'NI (non-inscrits)' => 1,
          'ID (Identité et Démocratie)' => 2,
          'ECR (Conservateurs et Réformistes)' => 3,
          'PPE (Démocrates-Chrétiens)' => 4,
          'Renew' => 5,
          'S&D (Socialistes et Démocrates)' => 6,
          'Verts/ALE' => 7,
          'The Left' => 8,
        ];
        foreach ($result as $row) {

          $keys = [
            'vote' => $row->nid,
            // Term name
            'party' => $row->taxonomy_term_field_data_taxonomy_term__parent_name,
            'country' => $country,
          ];

          if ($field_name == 'for') {
            $delete_q = \Drupal::database()->delete('vote_count');
            foreach ($keys as $key => $value) {
              $delete_q->condition($key, $value);
            }
            $deleted = $delete_q->execute();

            //dpm($deleted, 'vote_count');
            if ($row->nid == 1) {
//               var_dump($keys, $deleted);
            }
          }
          // Node counts, BY country :
          // ->for_count : "for" 65
          // ->for_count_percent : "for %" : for_count/count_all 82
          // ->count_all : all parties, all members counts for a country 79
          //

          $party_tid_prop = substr('taxonomy_term_field_data_node__field_' . $field_name . '__taxonomy_term__parent', 0, 60);
          $fields = [
            'party_weight' => $row->taxonomy_term__parent_order, //$party_weight[$row->taxonomy_term_field_data_taxonomy_term__parent_name],
            'party_tid' => $row->{$party_tid_prop},
            // Party vote : count of members:
          $field_name . '_count' => $row->{'taxonomy_term_field_data_node__field_' . $field_name . '_tid'},
          // Party vote : group concat of members names:
          $field_name . '_meps__value' => $row->{'taxonomy_term_field_data_node__field_' . $field_name . '_name'},
          // ->taxonomy_term__parent_count : Party all members count
          'count_all' => $row->taxonomy_term__parent_count,
          'uuid' => $uuid_service->generate()
          // ->taxonomy_term__parent_percent : Party vote percent
          ];
          $merge = \Drupal::database()->merge('vote_count');
          $vote_nids[$keys['vote']][$keys['country']][$keys['party']] = $merge->keys($keys)
          ->fields($keys + $fields)
          ->execute();
        }
        // $merge->execute();
        // 0.2115 for 'FRA'

        $current_vc_time = $timer->getCurrentMicroTime();
        $msg = 'Votes process time : @time';
        $ctxt = ['@time' => round($current_vc_time - $last_vc_time, 4)];
        $last_vc_time = $current_vc_time;
        if (function_exists('drush_main')) {
          \Drupal::logger('votewatch')->notice($msg, $ctxt);
        }
        else if (!$curent_user->isAnonymous()) {
          \Drupal::messenger()->addStatus(t($msg, $ctxt), TRUE);
        }
      }
    }
    $msg = 'All votes process time : @time';
    $ctxt = ['@time' => round($last_vc_time - $start, 4)];
    if (function_exists('drush_main')) {
      \Drupal::logger('votewatch')->notice($msg, $ctxt);
    }
    else if (!$curent_user->isAnonymous()) {
      \Drupal::messenger()->addStatus(t($msg, $ctxt), TRUE);
    }

    if (!is_null($field_citizenship_value)) {
      \Drupal::request()->query->set('field_citizenship_value', $field_citizenship_value);
    }
    else {
      \Drupal::request()->query->remove('field_citizenship_value');
    }
    if (!is_null($plenary_tid_depth)) {
      \Drupal::request()->query->set('term_node_tid_depth_1', $plenary_tid_depth);
    }
    else {
      \Drupal::request()->query->remove('term_node_tid_depth_1');
    }

    // By two streps : first vote_count, second node.field_vote
    // @todo : Replace truncate with delete->keys([vc.vote]) for the current import plenary
    $nids = array_keys($vote_nids);
    // drush entity:save node 22,24
    sort($nids);
//     dpm($nids);
    // dpm('drush cr', '', 'warning');
    // dpm('drush entity:save node ' . implode(',', $nids), '', 'warning');
    //$msg = 'drush cr && drush entity:save node @nids';
    $msg = 'drush cr && drush ev \'foreach(\Drupal::entityTypeManager()->getStorage("node")->loadMultiple([@nids]) as $node) {$node->save(); }\'';
    $ctxt = ['@nids' => implode(',', $nids)];
    if (function_exists('drush_main')) {
      \Drupal::logger('votewatch')->warning($msg, $ctxt);
    }
    else if (!$curent_user->isAnonymous()) {
      \Drupal::messenger()->addWarning(t($msg, $ctxt), TRUE);
    }

    $deleted = \Drupal::database()->delete('node__field_vote')
    ->condition('entity_id', $nids, 'IN')
    ->execute();
    //     dpm($deleted, 'node__field_vote');

    $deleted = \Drupal::database()->delete('node_revision__field_vote')
    ->condition('entity_id', $nids, 'IN')
    ->execute();
    //     dpm($deleted, 'node_revision__field_vote');
    // All field_vote for now
    // UPDATE `vote_count` SET `absence_meps__value` = NULL
    //     \Drupal::database()->query('truncate node__field_vote');
    //     \Drupal::database()->query('truncate node_revision__field_vote');
    $votes_q = \Drupal::database()->select('vote_count', 'vc');
    $votes_q->join('node_field_data', 'nfd', 'vc.vote = nfd.nid');
    $votes_q->condition('vc.vote', $nids, 'IN');
    $votes_q->orderBy('vc.vote', 'ASC');
    $votes = $votes_q->fields('vc', ['id', 'vote'])
    ->fields('nfd', ['vid'])
    ->execute()->fetchAll();

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
//     dpm($votes);
    //     $i = 0;
    $vote_prev = 0;
    $node_prev = NULL;
    foreach ($votes as $vote) {

      if ($vote_prev != $vote->vote) {
        $delta = 0;

      }
      else {
        $delta++;
      }

      $keys = [
        'entity_id' => $vote->vote,
        'deleted' => 0,
        'delta' => $delta,
        'langcode' => 'fr',
      ];
      $fields = $keys + [
        'bundle' => 'vote',
        'field_vote_target_id' => $vote->id,
      ]
      + [
        'revision_id' => $vote->vid
      ];

      $merge = \Drupal::database()->merge('node__field_vote');
      $merge->keys($keys)
      ->fields($fields);
      $merge->execute();

      $merge = \Drupal::database()->merge('node_revision__field_vote');
      $merge->keys($keys + ['revision_id' => $vote->vid])
      ->fields($fields);
      $merge->execute();


      $vote_prev = $vote->vote;
    }

    $msg = 'Field vote process time : @time';
    $ctxt = ['@time' => round($timer->getCurrentMicroTime() - $last_vc_time, 4)];
    if (function_exists('drush_main')) {
      \Drupal::logger('votewatch')->notice($msg, $ctxt);
    }
    else if (!$curent_user->isAnonymous()) {
      \Drupal::messenger()->addStatus(t($msg, $ctxt), TRUE);
    }
  }


  public static function titleTooLongCallback($value) {
    static $call;

    return mb_substr($value, 0, 255);
  }

  public static function plenarySessionLookupCallback($value) {
    static $call;

    /**@var $file_system \Drupal\Core\File\FileSystem */
    $file_system = \Drupal::service('file_system');
    /**@var $date_formatter \Drupal\Core\Datetime\DateFormatter */
    $date_formatter = \Drupal::service('date.formatter');
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $site_timezone = 'Europe/Paris';

    static $plenary_session_term_name_ids = [];

    $source_filename = \Drupal::config('migrate_plus.migration.mep_watch_vote')->get('source.urls.0');
    $source_basename = $file_system->basename($source_filename);
    //     dpm($source_basename);
    // vid plenary_session
    $return = 0 && $value ?? [];

    $matches = [];
    if (preg_match('/^members_votes_(\d{4,4}-\d{2,2}-\d{2,2})\.json/', $source_basename, $matches)) {

      // no hours for $matches[1], so use the UTC to have 2024-04-24 00:00:00
      // dpm($date_formatter->format(strtotime($matches[1]), 'custom', 'Y-m-d H:i:s', $SERVER_IMPORT_TIMEZONE));
      $year = $date_formatter->format(strtotime($matches[1]), 'custom', 'Y', $site_timezone);
      $month = ucwords($date_formatter->format(strtotime($matches[1]), 'custom', 'F Y', $site_timezone));
      $month_nb = $date_formatter->format(strtotime($matches[1]), 'custom', 'm', $site_timezone);
      $day = ucwords($date_formatter->format(strtotime($matches[1]), 'custom', 'd F Y', $site_timezone));
      $day_nb = $date_formatter->format(strtotime($matches[1]), 'custom', 'd', $site_timezone);

      // Year term
      $keys = ['name' => $year, 'vid' => 'plenary_session'];
      $fields = ['status' => 1]; //, $weight => ];
      if (isset($plenary_session_term_name_ids[$year])) {
        $year_term_id = $plenary_session_term_name_ids[$year];
      }
      else if($year_terms = $term_storage->loadByProperties($keys)) {
        $year_term_id = key($year_terms);
      }
      else {
        $year_term = $term_storage->create($keys + $fields);
        $year_term->save();
        $year_term_id = $year_term->id();
        var_dump('Created year ' . $year . ' : ' . $year_term_id);
      }

      // Month term
      $keys = ['name' => $month, 'vid' => 'plenary_session'];
      $fields = ['status' => 1, 'weight' => (0-$month_nb)];
      if (isset($plenary_session_term_name_ids[$month])) {
        $month_term_id = $plenary_session_term_name_ids[$month];
      }
      else if($month_terms = $term_storage->loadByProperties($keys)) {
        $month_term_id = key($month_terms);
      }
      else {
        $month_term = $term_storage->create($keys + $fields);
        $month_term->parent->target_id = $year_term_id;
        $month_term->save();
        $month_term_id = $month_term->id();
        var_dump('Created month ' . $month . ' : ' . $month_term_id);
      }

      // Day term
      $keys = ['name' => $day, 'vid' => 'plenary_session'];
      $fields = ['status' => 1, 'weight' => (0-$day_nb)];
      if (isset($plenary_session_term_name_ids[$day])) {
        $day_term = $plenary_session_term_name_ids[$day];
        $day_term_id = $day_term->id();
      }
      else if($day_terms = $term_storage->loadByProperties($keys)) {
        $plenary_session_term_name_ids[$day] = $day_term = reset($day_terms);
        $day_term_id = key($day_terms);
      }
      else {
        $plenary_session_term_name_ids[$day] = $day_term = $term_storage->create($keys + $fields);
        $day_term->parent->target_id = $month_term_id;
        $day_term->save();
        $day_term_id = $day_term->id();
        var_dump('Created day ' . $day . ' : ' . $day_term_id);
      }

      if (empty($day_term->field_machine_name->value)) {
        $day_term->field_machine_name->value = $year . '-' . $month_nb  . '-' . $day_nb;
        $day_term->save();
      }

      $return[] = $day_term_id;
    }
    else if (!$call) {
      var_dump('Source file doesnt have date part : ' . $source_basename);
    }


    $call++;
    //     var_dump($return);
    return $return;
  }

  public static function citizenshipCallback($value) {

    [$pers_id, $citizenship] = $value;
    //
    // Caroline ROOSE 197506:1828 'BEL' => 'FRA'
    if ($pers_id == '197506' && $citizenship = 'BEL') {
      $citizenship = 'FRA';
    }
    // Eva Maria POPTCHEVA 237320:1604 'BGR' => 'ESP'
    else if ($pers_id == '237320' && $citizenship = 'BGR') {
      $citizenship = 'ESP';
    }
    // Sandro GOZI 204419:1567 'ITA' => 'FRA'
    else if ($pers_id == '204419' && $citizenship = 'ITA') {
      $citizenship = 'FRA';
    }


    return $citizenship;
  }

  public static function callback($value) {
    var_dump($value);

    return $value;
  }

  /**
   * @deprecated to be removed
   * @param unknown $value
   * @return unknown
   */
  public static function mepGroupCallback($value) {
    $machine_name = static::groupMap($value);
    //     var_dump($machine_name, $value);
    //     $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    //     $terms = $term_storage->loadByProperties(['field_machine_name' => $machine_name]);
    //     $term = reset($terms);

    return $machine_name; //$term->id();
  }

  /**
   * @deprecated to be removed
   */
  public static function groupMap($group_id = NULL) {
    $map = [
      30 => 'NI',
      32 => 'ID',
      31 => 'ECR',
      25 => 'PPE',
      27 => 'Renew',
      26 => 'S&D',
      28 => 'Verts/ALE',
      29 => 'The Left',
    ];
    return $group_id? $map[$group_id] : $map;
  }
}