<?php
namespace Drupal\votewatch\Helper;

use Drupal\Core\Database\Query\AlterableInterface;

class Query {

  /**
   * See ::build => $result = views_get_view_result('content', 'page_1');
   * @param AlterableInterface $query
   */
  public static function voteCountAlter(AlterableInterface $query) {
  /**@var $query \Drupal\Core\Database\Query\Select */

  $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

  if (!$query->hasTag('votewatch-terms-current-semaphore')) {
    $query->addTag('votewatch-terms-current-semaphore');

    $tables = $query->getTables();
    $table_term_current = '';
    foreach ($tables as $alias => $table) {
      [$taxonomy_term_field_data_node, $match] = array_pad(explode('__', $alias), 2, NULL);
      if ($taxonomy_term_field_data_node == 'taxonomy_term_field_data_node') {
        $table_term_current = $match;
      }
    }

      $route_match = \Drupal::routeMatch();
      $warmup_vote_count = $route_match->getRouteName() == 'system.admin_structure';
      $plenary_date_static = & drupal_static('votewatch.meps_map.plenary_date');

      $orderby = &$query->getOrderBy();
      if (isset($orderby['node_field_data.changed'])) {
        unset($orderby['node_field_data.changed']);
//         if (!$query->hasTag('block_grp_title')) {
//           $orderby['taxonomy_term_field_data_node__field_for_name'] = 'DESC';
//         }

        //         $orderby['taxonomy_term__parent_percent'] = 'DESC';
        $orderby['node_field_data.title'] = 'ASC';

        $query->addExpression('CASE
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'NI (non-inscrits)\' THEN 1
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'ID (Identité et Démocratie)\' THEN 2
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'ECR (Conservateurs et Réformistes)\' THEN 3
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'PPE (Démocrates-Chrétiens)\' THEN 4
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'Renew\' THEN 5
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'S&D (Socialistes et Démocrates)\' THEN 6
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'Verts/ALE\' THEN 7
WHEN taxonomy_term_field_data_taxonomy_term__parent.name = \'The Left\' THEN 8

END', 'taxonomy_term__parent_order');
        $orderby['taxonomy_term__parent_order'] = 'ASC';

        $field_citizenship_value = _votewatch_query_request_get('field_citizenship_value');
        $title_value = _votewatch_query_request_get('title_value');
        if (is_null($title_value) && !$field_citizenship_value) {

          \Drupal::request()->request->set('field_citizenship_value', 'FRA');
          \Drupal::request()->query->set('field_citizenship_value', 'FRA');
        }
//         dpm($field_citizenship_value);


        //dpm($plenary_date_static);
        $map = Map::meps(FALSE, $plenary_date_static);
        $case = '';
        if (isset($map['PPE'][$field_citizenship_value]['count'])) {

          $subquery = $term_storage->getQuery()
          ->accessCheck()
          ->condition('vid', 'member')
          //->condition('parent', ['23', '24'], 'IN')
          ->addTag('custom_callback')
          ->addMetaData('callback', [function($query, $entity_type_id, $field_name) use ($map) {
            //$field_name_property = $field_name . '_target_id';
            $field_name_property = $field_name;
            $field_data_table = $entity_type_id . '_field_data';
            /**@var $query \Drupal\Core\Database\Query\Select */
            // $query->addField($entity_type_id . '__' . $field_name, $field_name_property);
            $query->addField($field_data_table, $field_name_property);
            // All keyed => will be indexed by the first field, i.e. grouped by.
            // Move it at the begining.
            $fields = &$query->getFields();
            // Drop first field
            $revision_id = array_shift($fields);


          }, ['taxonomy_term', 'name']])
          //             ->addTag('debug')
            ;

          $entity_type_id = 'taxonomy_term';
          $subquery = \drupal::database()->select($entity_type_id . '_field_data'); //, 'fd');
          $subquery->addField('taxonomy_term_field_data', 'tid');
          $subquery->addField('taxonomy_term_field_data', 'name');
          $subquery->join('taxonomy_term__parent', 'fp', 'taxonomy_term_field_data.tid = fp.entity_id');
          $subquery->condition('taxonomy_term_field_data.vid', 'member')
    //       ->addTag('debug')
          ;

          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` = \'NI (non-inscrits)\' THEN ' . $map['NI'][$field_citizenship_value]['count'];
          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` LIKE \'ID (Identit_ et D_mocratie)\' THEN ' . $map['ID'][$field_citizenship_value]['count'];
          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` LIKE \'ECR (Conservateurs et R_formistes)\' THEN ' . $map['ECR'][$field_citizenship_value]['count'];
          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` LIKE \'PPE (D_mocrates-Chr_tiens)\' THEN ' . $map['PPE'][$field_citizenship_value]['count'];
          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` = \'Renew\' THEN ' . $map['Renew'][$field_citizenship_value]['count'];
          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` LIKE \'S&D (Socialistes et D_mocrates)\' THEN ' . $map['S&D'][$field_citizenship_value]['count'];
          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` = \'Verts/ALE\' THEN ' . $map['Verts/ALE'][$field_citizenship_value]['count'];
          $case .= "\n" . 'WHEN `taxonomy_term_field_data`.`name` = \'The Left\' THEN ' . $map['The Left'][$field_citizenship_value]['count'];

          $subquery->addExpression('CASE ' . $case . ' END', 'taxonomy_term__parent_count');

          $subquery->groupBy('taxonomy_term_field_data.tid');
          $subquery->groupBy('taxonomy_term_field_data.name');



          //           dpm($subquery->execute()->fetchAll());
          /**@var $subquery \Drupal\Core\Entity\Query\Sql\Query*/

          $query->join($subquery, 'taxonomy_term__parent_all', '`taxonomy_term_field_data_node__' . $table_term_current . '__taxonomy_term__parent`.`parent_target_id` = `taxonomy_term__parent_all`.`tid`');
          $query->addField('taxonomy_term__parent_all', 'taxonomy_term__parent_count');
          $query->addExpression('count(taxonomy_term__parent_all.taxonomy_term__parent_count)', 'taxonomy_term__parent_all_count');
          $query->groupBy('taxonomy_term__parent_count');


          if (!$plenary_date_static) {
            $subquery2 = \Drupal::database()->select('vote_count', 'vc');
            $subquery2->condition('country', $field_citizenship_value);
            $subquery2->groupBy('vc.vote');
            $subquery2->addField('vc', 'vote');
            $subquery2->addExpression('sum(vc.for_count)', 'for_count');
            $subquery2->addExpression('sum(vc.against_count)', 'against_count');
            $subquery2->addExpression('sum(vc.abstention_count)', 'abstention_count');
            $subquery2->addExpression('sum(vc.count_all)', 'count_all');

            $query->leftJoin($subquery2, 'vc', 'vc.vote = node_field_data.nid');
            $query->addExpression('MIN(vc.for_count)', 'for_count');
            $query->addExpression('MIN(vc.against_count)', 'against_count');
            $query->addExpression('MIN(vc.abstention_count)', 'abstention_count');
            $query->addExpression('MIN(vc.count_all)', 'count_all');
            $query->addExpression('MIN(ROUND((vc.for_count/vc.count_all)*100, 0))', 'for_count_percent');
            $query->addExpression('MIN(ROUND((vc.against_count/vc.count_all)*100, 0))', 'against_count_percent');
            $query->addExpression('MIN(ROUND((vc.abstention_count/vc.count_all)*100, 0))', 'abstention_count_percent');

            $query->addExpression('ROUND((COUNT(DISTINCT taxonomy_term_field_data_node__' . $table_term_current . '.tid)/taxonomy_term__parent_count)*100, 0)', 'taxonomy_term__parent_percent');
          }

          if ($plenary_date_static) {
            $query->range(0, 500);
          }
        }
      }
    }
  }
}