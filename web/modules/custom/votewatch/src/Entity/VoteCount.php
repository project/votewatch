<?php

declare(strict_types=1);

namespace Drupal\votewatch\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\votewatch\VoteCountInterface;

/**
 * Defines the vote count entity class.
 *
 * @ContentEntityType(
 *   id = "vote_count",
 *   label = @Translation("Vote Count"),
 *   label_collection = @Translation("Vote Counts"),
 *   label_singular = @Translation("vote count"),
 *   label_plural = @Translation("vote counts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count vote counts",
 *     plural = "@count vote counts",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\votewatch\VoteCountListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\votewatch\Form\VoteCountForm",
 *       "edit" = "Drupal\votewatch\Form\VoteCountForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\votewatch\Routing\VoteCountHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "vote_count",
 *   admin_permission = "administer vote_count",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/vote-count",
 *     "add-form" = "/vote-count/add",
 *     "canonical" = "/vote-count/{vote_count}",
 *     "edit-form" = "/vote-count/{vote_count}",
 *     "delete-form" = "/vote-count/{vote_count}/delete",
 *     "delete-multiple-form" = "/admin/content/vote-count/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.vote_count.settings",
 * )
 */
final class VoteCount extends ContentEntityBase implements VoteCountInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

//       The "number_integer" plugin does not exist. Valid plugin IDs for Drupal\Core\Field\FieldTypePluginManager are: comment, datetime, file_uri, file, image, layout_section, link, list_string, list_integer, list_float,
//       path, text_with_summary, text_long, text, decimal, map, password, uri, entity_reference, float, email, integer, changed, uuid, string, language, string_long, timestamp, created, boolean

      // ALTER TABLE `vote_count` CHANGE `vote` `vote` INT(11) NOT NULL;
      $fields['vote'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Vote Id'))
//       ->setDefaultValue($value)
//       ->setDisplayOptions('form', [
//         'type' => 'text_textarea',
//         'weight' => 10,
//       ])
      ->setDisplayConfigurable('form', FALSE)
//       ->setDisplayOptions('view', [
//         'type' => 'text_default',
//         'label' => 'above',
//         'weight' => 10,
//       ])
      ->setDisplayConfigurable('view', FALSE);


      $fields['party'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Party'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

      $fields['party_tid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Party Tid'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setTranslatable(FALSE)
//       ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);


      // ALTER TABLE `vote_count` ADD INDEX(`party_weight`);
      $fields['party_weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Party weight'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

      $fields['country'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Country'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

      $allowed_values = \Drupal::config('field.storage.taxonomy_term.field_citizenship')->get('settings.allowed_values');
      $allowed_values_settings = [];
      foreach($allowed_values as $allowed_value) {
        $allowed_values_settings[$allowed_value['value']] = $allowed_value['label'];
      }

      $fields['country'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Country'))
      ->setSettings([
        'allowed_values' => $allowed_values_settings,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      // ALTER TABLE `vote_count` CHANGE `for_count` `for_count` INT(11) NOT NULL DEFAULT '0';
      $fields['for_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('For Count'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
      $fields['for_meps'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('For Meps'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

      // ALTER TABLE `vote_count` CHANGE `against_count` `against_count` INT(11) NOT NULL DEFAULT '0';
      $fields['against_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Against Count'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
      $fields['against_meps'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Against Meps'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

      // ALTER TABLE `vote_count` CHANGE `abstention_count` `abstention_count` INT(11) NOT NULL DEFAULT '0';
      $fields['abstention_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Abstention Count'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
      $fields['abstention_meps'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Abstention Meps'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

      $fields['absence_meps'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Absence Meps'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

      // ALTER TABLE `vote_count` CHANGE `count_all` `count_all` INT(11) NOT NULL;
      $fields['count_all'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Count All'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

}
