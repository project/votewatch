<?php

declare(strict_types=1);

namespace Drupal\votewatch;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a vote count entity type.
 */
interface VoteCountInterface extends ContentEntityInterface {

}
