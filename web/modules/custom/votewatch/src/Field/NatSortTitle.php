<?php

namespace Drupal\votewatch\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

class NatSortTitle extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $this->ensurePopulated();
  }

  public static function subtitleForSort($title) {

    $title = trim(preg_replace('/\s+/', ' ', $title));
    // Explode any 4 digits ex. an year to eclude from processing
    $title_parts = array_pad(explode('____', preg_replace('/(\d{4})/', '____$1', $title, 1), 2), 2, '');
    // 3 levels subtitle parts of 3 digits each. Ex: 1.97.145
    $subtitle_parts = explode('____', preg_replace('/(\d+)\.*/', '$1____', mb_substr($title_parts[0], 0, 13)));
    // Pop out the last part as a text, which can be an empty text
    $text = array_pop($subtitle_parts);
    $subtitle_parts = array_pad($subtitle_parts, 3, 0);

    foreach ($subtitle_parts as & $subtitle_part) {
      $subtitle_part = preg_replace('/(\d+)/', '00$1', $subtitle_part);
      $subtitle_part = preg_replace('/0*([0-9]{3,})/', '$1', $subtitle_part);
      $subtitle_part = preg_replace('/([^a-z0-9])/', '', $subtitle_part);
    }
    // $subtitle_parts[] = trim($text);
    $subtitle = implode('.', $subtitle_parts) . ' ' . ltrim($text) . mb_substr($title_parts[0], 13) . $title_parts[1];

    return $subtitle;
  }

  /**
   * Computes the calculated values for this item list.
   */
  protected function ensurePopulated() {
    $entity = $this->getEntity();

    $offset = 0;
    // First 128 is ok just for sorting
    // $value = mb_substr(static::subtitleForSort($entity->field_long_title->value), 0, 128);
    // $value = mb_substr(static::subtitleForSort($entity->field_long_title->value), 0, 60);
    $value = static::subtitleForSort($entity->field_long_title->value);
    $this->list[$offset] = $this->createItem($offset, $value);

  }
}