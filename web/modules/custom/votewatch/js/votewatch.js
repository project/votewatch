(function ($, Drupal, drupalSettings) {

dialogs = {};
imgs = {};


$(document).ready(function () {
  $('.block-facet-block-nested-dropdown .facet-item.facet-item--expanded,' +
   '.block-facet--checkbox.menu--plenary .facet-item.facet-item--expanded'
   ).hover(function () {
    // At the current depth, we should delete all "hover-intent" classes.
    // Other wise we get unwanted behaviour where menu items are expanded while already in hovering other ones.
    $(this).parent().find('li').removeClass('hover-intent');
    $(this).addClass('hover-intent');
  },
  function () {
    $(this).removeClass('hover-intent');
  });
});

Drupal.behaviors.votewatch = {
    attach: function (context, settings) {
      
      once('search-list-hide-facets', '.block-facet-block-nested-dropdown,.block-facet--checkbox.menu--plenary', context).forEach(function (element) {
        console.log($(element).children('h2').length);
        return;
        
        $(element).children('.block__content').hide();
        
        $(element).hover(function(e){
          $(this).children('.block__content').show();
          // console.log($(this).nextAll('.content').length);
          //let $ul = $(this).find('.block__content > .facets-widget-checkbox > ul');
          //$ul.children('li').addClass('hover-intent');
          //$ul.show();
        },
        function(e){
          $(this).children('.block__content').hide();
          // console.log($(this).nextAll('.content').length);
          //let $ul = $(this).find('.block__content > .facets-widget-checkbox > ul');
          //$ul.children('li').removeClass('hover-intent');
          //$ul.hide();
        });
      });      
    }
  };
})(jQuery, Drupal, drupalSettings);
